@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('checkout');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--4" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Checkout</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Checkout</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start Checkout Area -->
    <section class="wn__checkout__area section-padding--lg bg__white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wn_checkout_wrap">
                        <div class="checkout_info">
                            <span>Have a coupon? </span>
                            <a class="showcoupon" href="#">Click here to enter your code</a>
                        </div>
                        <div class="checkout_coupon">
                            <form action="#">
                                <div class="form__coupon">
                                    <input type="text" placeholder="Coupon code" name="coupon_code" id="coupon_code">
                                    <button type="button" id="apply-coupon">Apply coupon</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-12">
                    <form action="{{ route('checkout.post') }}" method="post" id="checkout-form">
                        {{ csrf_field() }}
                        <div class="customer_details">
                            <h3>Billing details</h3>
                            <div class="customar__field">
                                <div class="margin_between">
                                    <div class="input_box space_between">
                                        <label>First name <span>*</span></label>
                                        <input type="text" name="billing_first_name" value="{{ ($addresses) ? $addresses->first_name : '' }}">
                                    </div>
                                    <div class="input_box space_between">
                                        <label>Last name <span>*</span></label>
                                        <input type="text" name="billing_last_name" value="{{ ($addresses) ? $addresses->last_name : '' }}">
                                    </div>
                                </div>
                                <div class="input_box">
                                    <label>Address <span>*</span></label>
                                    <input type="text" name="billing_address_line_1" placeholder="Address Line 1" value="{{ ($addresses) ? $addresses->address_line_1 : "" }}">
                                </div>
                                <div class="input_box">
                                    <input type="text" name="billing_address_line_2" placeholder="Address Line 2" value="{{ ($addresses) ? $addresses->address_line_2 : "" }}">
                                </div>
                                <div class="input_box">
                                    <label>City <span>*</span></label>
                                    <input type="text" name="billing_city" placeholder="City" value="{{ ($addresses) ? $addresses->city : "" }}">
                                </div>
                                <div class="input_box">
                                    <label>Postcode / ZIP <span>*</span></label>
                                    <input type="text" name="billing_zip_code" placeholder="Postcode / Zip" value="{{ ($addresses) ? $addresses->zip_code : "" }}">
                                </div>
                                <div class="margin_between">
                                    <div class="input_box space_between">
                                        <label>Telephone</label>
                                        <input type="text" name="billing_phone_number" class="billing_phone_number" placeholder="Telephone number" value="{{ ($addresses) ? $addresses->phone_number : "" }}">
                                    </div>
                                    <div class="input_box space_between">
                                        <label>Mobile</label>
                                        <input type="text" name="billing_mobile_number" class="billing_mobile_number" placeholder="Mobile number" value="{{ ($addresses) ? $addresses->mobile_number : "" }}">
                                    </div>
                                </div>
                                <div class="input_box">
                                    <label><span>*</span> Telephone or Mobile is required</label>
                                </div>
                            </div>
                        </div>
                        <div class="customer_details mt--20">
                            <div class="differt__address">
                                <input name="ship_to_different_address" id="ship_to_different_address" value="1" type="checkbox" @if($shipping_addresses) checked @endif>
                                <span>Ship to a different address ?</span>
                            </div>
                            <div class="customar__field differt__form mt--40 shipping-details" @if($shipping_addresses) style="display: block" @endif>
                                <h3>Shipping details</h3>
                                <div class="margin_between">
                                    <div class="input_box space_between">
                                        <label>First name <span>*</span></label>
                                        <input type="text" name="shipping_first_name" value="{{ ($shipping_addresses) ? $shipping_addresses->first_name : "" }}">
                                    </div>
                                    <div class="input_box space_between">
                                        <label>Last name <span>*</span></label>
                                        <input type="text" name="shipping_last_name" value="{{ ($shipping_addresses) ? $shipping_addresses->last_name : "" }}">
                                    </div>
                                </div>
                                <div class="input_box">
                                    <label>Address <span>*</span></label>
                                    <input type="text" name="shipping_address_line_1" placeholder="Address Line 1" value="{{ ($shipping_addresses) ? $shipping_addresses->address_line_1 : "" }}">
                                </div>
                                <div class="input_box">
                                    <input type="text" name="shipping_address_line_2" placeholder="Address Line 2" value="{{ ($shipping_addresses) ? $shipping_addresses->address_line_2 : "" }}">
                                </div>
                                <div class="input_box">
                                    <label>City <span>*</span></label>
                                    <input type="text" name="shipping_city" placeholder="City" value="{{ ($shipping_addresses) ? $shipping_addresses->city : "" }}">
                                </div>
                                <div class="input_box">
                                    <label>Postcode / ZIP <span>*</span></label>
                                    <input type="text" name="shipping_zip_code" placeholder="Postcode / Zip" value="{{ ($shipping_addresses) ? $shipping_addresses->zip_code : "" }}">
                                </div>
                                <div class="margin_between">
                                    <div class="input_box space_between">
                                        <label>Telephone</label>
                                        <input type="text" name="shipping_phone_number" class="shipping_phone_number" placeholder="Telephone number" value="{{ ($shipping_addresses) ? $shipping_addresses->phone_number : "" }}">
                                    </div>
                                    <div class="input_box space_between">
                                        <label>Mobile</label>
                                        <input type="text" name="shipping_mobile_number" class="shipping_mobile_number" placeholder="Mobile number" value="{{ ($shipping_addresses) ? $shipping_addresses->mobile_number : "" }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <button class="btn_checkout">Checkout</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 col-12 md-mt-40 sm-mt-40">
                    <div class="wn__order__box">
                        <h3 class="onder__title">Your order</h3>
                        <ul class="order__total">
                            <li>Product</li>
                            <li>Total</li>
                        </ul>
                        <ul class="order_product">
                            <?php $cart_total = 0;
                                $coupon_price = ($coupon_code) ? $coupon_value * (-1): 0;
                            ?>
                            @foreach($carts as $cart)
                                <?php
                                    $product_price = 0;
                                    $sub_total = 0;
                                    if($cart->customer_type == 2){
                                        $product_price = $cart->school_price;
                                    }else{
                                        $product_price = $cart->normal_price - $cart->normal_discount;
                                    }
                                    $sub_total = $product_price * $cart->qty;
                                    $cart_total += $sub_total;
                                ?>
                                    <li>{{ $cart->name }} × {{ $cart->qty }}<span>{{ final_price_format($sub_total) }}</span></li>
                            @endforeach
                        </ul>
                        <ul class="shipping__method">
                            <li>Cart Subtotal <span>{{ final_price_format($cart_total) }}</span></li>
                            <li>Coupon <span id="coupon_value_text">{{ final_price_format($coupon_price) }}</span></li>
                            @if($coupon_code)(<label id="coupon_code_text">{{ $coupon_code }}</label>) @endif
                            @if(false)
                            <li>Shipping
                                <ul>
                                    <li>
                                        <input name="shipping_method[0]" data-index="0" value="legacy_flat_rate" checked="checked" type="radio">
                                        <label>Flat Rate: $48.00</label>
                                    </li>
                                    <li>
                                        <input name="shipping_method[0]" data-index="0" value="legacy_flat_rate" checked="checked" type="radio">
                                        <label>Flat Rate: $48.00</label>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                        <ul class="total__amount">
                            <li>Order Total <span id="grand_total_text">{{ final_price_format($cart_total + $coupon_price) }}</span></li>
                        </ul>
                        <input type="hidden" id="cart_sub_total" value="{{ $cart_total }}" />
                        <input type="hidden" id="coupon_total" value="{{ $cart_total }}" />
                        <input type="hidden" id="grand_total" value="{{ ($cart_total + $coupon_price) }}">
                    </div>
                    <div id="accordion" class="checkout_accordion mt--30" role="tablist">
                        @if(false)
                            <div class="payment">
                            <div class="che__header" role="tab" id="headingOne">
                                <a class="checkout__title" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <span>Direct Bank Transfer</span>
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="payment-body">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</div>
                            </div>
                        </div>
                        @endif
                        @if(false)
                            <div class="payment">
                            <div class="che__header" role="tab" id="headingTwo">
                                <a class="collapsed checkout__title" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <span>Cheque Payment</span>
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="payment-body">Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</div>
                            </div>
                        </div>
                        @endif
                        <div class="payment">
                            <div class="che__header" role="tab" id="headingThree">
                                <a class="checkout__title" data-toggle="collapse" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    <span>Cash on Delivery</span>
                                </a>
                            </div>
                            <div id="collapseThree" class="collapse show" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="payment-body">Pay with cash upon delivery.</div>
                            </div>
                        </div>
                        @if(false)
                        <div class="payment">
                            <div class="che__header" role="tab" id="headingFour">
                                <a class="collapsed checkout__title" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <span>PayPal <img src="{{ asset('theme/images/icons/payment.png') }}" alt="payment images"> </span>
                                </a>
                            </div>
                            <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="payment-body">Pay with cash upon delivery.</div>
                            </div>
                        </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End Checkout Area -->

    <script>
        $(document).ready(function(){
            $('#apply-coupon').on('click', function(){
               var coupon_code = $('#coupon_code').val();
                let _token = $('input[name="_token"]').val();
               $.ajax({
                   url: "{{ route('coupon') }}",
                   type: 'post',
                   data: {_token: _token, coupon_code:coupon_code},
                   success: function(data){
                       let return_data = jQuery.parseJSON(data);
                       if(return_data.status == 200)
                       {
                           $('#coupon_value_text').html(return_data.coupon_total_text);
                           $('#grand_total_text').html(return_data.grand_total_text);
                           $('#coupon_code_text').html(coupon_code);
                           toastr.success( return_data.message, 'Success!');
                       }
                       else if(return_data.status == 202){
                           window.refresh();
                       }
                       else{
                           $('#coupon_value_text').html(return_data.coupon_total_text);
                           $('#grand_total_text').html(return_data.grand_total_text);
                           toastr.error( return_data.message, 'Failed!');
                       }
                   }
               });
            });

            $('#ship_to_different_address').on('click', function(){
                $('.shipping-details .input_box').removeClass('has-error');
                $('.shipping-details .help-block').remove();
                $('.shipping-details .input_box input').removeAttr('aria-describedby');
            });
        });
    </script>
@endsection
