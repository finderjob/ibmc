@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('blog_details');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--6" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Blog Details</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Blog-Details</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <div class="page-blog-details section-padding--lg bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-12">
                    <div class="blog-details content">
                        <article class="blog-post-details">
                            <div class="post-thumbnail">
                                <img src="{{ asset($blog->image) }}" alt="blog images">
                            </div>
                            <div class="post_wrapper">
                                <div class="post_header">
                                    <h2>{{ $blog->name }}</h2>
                                    <div class="blog-date-categori">
                                        <ul>
                                            <li>{{ date('M d Y', strtotime($blog->created_at)) }}</li>
                                            <li><a href="#" title="Posts by boighor" rel="author">
                                                    {{ \App\User::find($blog->created_by)->name }}
                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="post_content"><?php echo json_decode($blog->content)?></div>
                                <ul class="blog_meta">
                                    <li id="comment-count"></li>
                                    <li> / </li>
                                    <li>Tags:<span>fashion</span> <span>t-shirt</span> <span>white</span></li>
                                </ul>
                            </div>
                        </article>
                        <div class="comments_area" id="comments_area">
                            <h3 class="comment__title">comments</h3>
                        </div>
                        <div class="comment_respond">
                            <h3 class="reply_title">Leave a Comment</h3>
                            <form class="comment__form" action="{{ route('blog.comment') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" id="post-id" name="post_id" value="{{ $blog->id }}" />
                                <input type="hidden" id="comment-id" name="comment_id" value="" />
                                <p>Your email address will not be published.Required fields are marked </p>
                                <div class="input__box">
                                    <textarea name="comment" placeholder="Your comment here"></textarea>
                                </div>
                                <div class="input__wrapper clearfix">
                                    <div class="input__box name one--third">
                                        <input type="text" name="name" placeholder="name">
                                    </div>
                                    <div class="input__box email one--third">
                                        <input type="email" name="email" placeholder="email">
                                    </div>
                                </div>
                                <div class="submite__btn">
                                    <button>Post Comment</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-12 md-mt-40 sm-mt-40">
                    <div class="wn__sidebar">
                        <!-- Start Single Widget -->
                        <aside class="widget search_widget">
                            <h3 class="widget-title">Search</h3>
                            <form action="#">
                                <div class="form-input">
                                    <input type="text" placeholder="Search...">
                                    <button><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </aside>
                        <!-- End Single Widget -->
                        <!-- Start Single Widget -->
                        <aside class="widget recent_widget">
                            <h3 class="widget-title">Recent</h3>
                            <div class="recent-posts">
                                <ul>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/1.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Blog image post</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/2.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Post with Gallery</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/3.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Post with Video</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/4.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Maecenas ultricies</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/5.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Blog image post</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                        <!-- End Single Widget -->
                        <!-- Start Single Widget -->
                        <aside class="widget comment_widget">
                            <h3 class="widget-title">Comments</h3>
                            <ul>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>demo says:</p>
                                            <a href="#">Quisque semper nunc vitae...</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>Admin says:</p>
                                            <a href="#">Curabitur aliquet pulvinar...</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>Irin says:</p>
                                            <a href="#">Quisque semper nunc vitae...</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>Boighor says:</p>
                                            <a href="#">Quisque semper nunc vitae...</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>demo says:</p>
                                            <a href="#">Quisque semper nunc vitae...</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </aside>
                        <!-- End Single Widget -->
                        <!-- Start Single Widget -->
                        <aside class="widget category_widget">
                            <h3 class="widget-title">Categories</h3>
                            <ul>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Creative</a></li>
                                <li><a href="#">Electronics</a></li>
                                <li><a href="#">Kids</a></li>
                                <li><a href="#">Flower</a></li>
                                <li><a href="#">Books</a></li>
                                <li><a href="#">Jewelle</a></li>
                            </ul>
                        </aside>
                        <!-- End Single Widget -->
                        <!-- Start Single Widget -->
                        <aside class="widget archives_widget">
                            <h3 class="widget-title">Archives</h3>
                            <ul>
                                <?php $months = post_archives();?>
                                @if($months)
                                    @for($m = 0; $m < count($months) and $m < 12; $m++)
                                        <li><a href="#">{{ $months[$m] }}</a></li>
                                    @endfor
                                @endif
                            </ul>
                        </aside>
                        <!-- End Single Widget -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            //comments_area
            var post_id  = $('#post-id').val();
            $.ajax({
                url: '{{ route('blog.comment.get') }}',
                type: 'get',
                data: {'post_id': post_id},
                success: function(data){
                   $('#comments_area').append(data['resource']);
                   var count = data['count'];
                   if(count == 0){
                       count = "No Comment";
                   }else if(count == 1){
                       count += " Comment";
                   }else{
                       count += " Comments";
                   }
                   $('#comment-count').html(count);
                }
            });
        });

        function replyThatComment(comment_id, row_id) {
            event.preventDefault();
            var text = 'Leave a Reply <small><a href="#" onclick="cancelReply('+row_id+')">Cancel reply</a></small>';
            $('.reply_title').html(text);
            $('#comment-id').val(comment_id);
            $('.content').removeClass('set-boarder-color');
            $('#content-' + row_id ).addClass("set-boarder-color");
            return false;
        }
        function cancelReply(row_id) {
            event.preventDefault();
            $('#comment-id').val("");
            var text = 'Leave a Comment';
            $('.reply_title').html(text);
            $('#content-' + row_id ).removeClass("set-boarder-color");
        }
    </script>
@endsection
