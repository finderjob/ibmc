@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('shop');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--6" @if($path) style="background-image: url('{{ $path }}');" @endif>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bradcaump__inner text-center">
                            <h2 class="bradcaump-title">
                                @if($slug)
                                    @if($sub_slug)
                                        {{ $sub_category->name }}
                                    @else
                                        {{ $category->name }}
                                    @endif
                                @else All Books
                                @endif</h2>
                            <nav class="bradcaump-content">
                                @if($slug)
                                    <a class="breadcrumb_item" href="#">Home</a>
                                    <span class="brd-separetor">/</span>
                                    <a class="breadcrumb_item" href="#">Category</a>
                                    @if($sub_slug)
                                        <span class="brd-separetor">/</span>
                                        <span class="breadcrumb_item">{{ $category->name }}</span>
                                        <span class="brd-separetor">/</span>
                                        <a class="breadcrumb_item active" href="#">Sub category</a>
                                    @endif
                                @else
                                    <a class="breadcrumb_item" href="#">Home</a>
                                    <span class="brd-separetor">/</span>
                                    <span class="breadcrumb_item active">All Books</span>
                                @endif

                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- Start Shop Page -->
        <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
                        @include('theme.partials.widget')
                    </div>
                    <div class="col-lg-9 col-12 order-1 order-lg-2">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="shop__list__wrapper d-flex flex-wrap flex-md-nowrap justify-content-between">
                                    <div class="shop__list nav justify-content-center" role="tablist">
                                        <a class="nav-item nav-link active" data-toggle="tab" href="#nav-grid" role="tab"><i class="fa fa-th"></i></a>
                                    </div>
                                    <p>Showing {{($products->currentpage()-1)*$products->perpage()+1}} to {{$products->currentpage()*$products->perpage()}} of {{ $products->total() }} results</p>
                                    <div class="orderby__wrapper">
                                        <span>Sort By</span>
                                        <?php $order_by = \Request::get('order_by');
                                                $order = \Request::get('order');
                                                if($order == 'price')
                                                {
                                                    if($order_by == 'desc')
                                                    {
                                                        $option = 4;
                                                    }else{
                                                        $option = 3;
                                                    }
                                                }else{
                                                    if($order_by == 'desc')
                                                    {
                                                        $option = 2;
                                                    }else{
                                                        $option = 1;
                                                    }
                                                }
                                        ?>
                                        <select class="shot__byselect" id="sort_option">
                                            <option value="1" @if($option == 1) selected="selected" @endif>Name ASC</option>
                                            <option value="2" @if($option == 2) selected="selected" @endif>Name DESC</option>
                                            <option value="3" @if($option == 3) selected="selected" @endif>Price ASC</option>
                                            <option value="4" @if($option == 4) selected="selected" @endif>Price DESC</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab__container">
                            <div class="shop-grid tab-pane fade show active" id="nav-grid" role="tabpanel">
                                <div class="row">
                                    <!-- Start Single Product -->
                                    @foreach($products as $product)
                                        <?php $cover_images = products_cover_images($product->id);
                                                if($slug){
                                                    $url = route('product.with.category',[ $slug, $product->slug]);
                                                    if($sub_slug){
                                                        $url = route('product.with.sub-category',[ $slug, $sub_slug, $product->slug]);
                                                    }
                                                }else{
                                                    $url = route('product',[$product->slug]);
                                                }
                                        ?>
                                    <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{ $url }}"><img src="{{ asset($cover_images->image_path) }}" alt="{{ $product->name }}"></a>
                                            <a class="second__img animation1" href="{{ $url }}"><img src="{{ asset($cover_images->image_path) }}" alt="{{ $product->name }}"></a>
                                            <?php $checkBestSeller = checkBestSellerProduct($product->id); ?>
                                            @if($checkBestSeller)
                                            <div class="hot__box">
                                                <span class="hot-label">BEST SALLER</span>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="product__content content--center">
                                            <h4><a href="{{ $url }}">{{ $product->name }}</a></h4>
                                            <ul class="prize d-flex">
                                                @if($customer_type == 2)
                                                    <li>{{ final_price_format($product->school_price) }}</li>
                                                    <li class="old_prize">{{ final_price_format($product->normal_price) }}</li>
                                                @else
                                                    <li>{{ final_price_format($product->normal_price - $product->normal_discount) }}</li>
                                                    @if(($product->normal_price - $product->normal_discount) != $product->normal_price)
                                                        <li class="old_prize">{{ final_price_format($product->normal_price) }}</li>
                                                    @endif
                                                @endif

                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        <li><a class="cart" data-id="{{ $product->id }}" href="#"><i class="bi bi-shopping-cart-full" title="Cart"></i></a></li>
                                                        <li><a class="wishlists" href="#" data-id="{{ $product->id }}"><i class="bi bi-heart-beat" title="Wishlist"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product__hover--content">
                                                <ul class="rating d-flex">
                                                    <?php $avg_rating = get_product_rating_value($product->id);?>
                                                        @for($j = 1; $j <= 5; $j++)
                                                            @if($j <= $avg_rating)
                                                                <li class="on"><i class="fa fa-star"></i></li>
                                                            @else
                                                                <li><i class="fa fa-star-o"></i></li>
                                                            @endif
                                                        @endfor
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <!-- End Single Product -->
                                </div>
                                @if($products->total() != 0)
                                <ul class="wn__pagination">
                                    <?php
                                    $params = [ 'order' => $order, 'order_by' => $order_by];
                                    $back_url = $products->url($products->currentPage() - 1);
                                    $amount = \Request::get('amount');
                                    if($order){
                                        $params['order'] = $order;
                                        $params[ 'order_by'] = $order_by;
                                    }
                                    if($amount){
                                        $params[ 'amount'] = $amount;
                                    }
                                    if(count($params) != 0){
                                        $back_url = $products->appends($params)->url($products->currentPage() - 1);
                                    }
                                    ?>
                                    @if(1 != $products->currentPage())
                                        <li><a href="{{ $products->url($products->currentPage() - 1) }}"><i class="zmdi zmdi-chevron-left"></i></a></li>
                                    @endif
                                    <li><a href="#"></a></li>
                                    @for($i = 1; $i <= $products->lastPage(); $i++)
                                            <?php
                                            $pagination_url = $products->url($i);
                                            if(count($params) != 0){
                                                $pagination_url = $products->appends($params)->url($i);
                                            }
                                            ?>
                                        <li @if($i == $products->currentPage()) class="active" @endif><a @if($i != $products->currentPage()) href="{{ $pagination_url }}"  @endif>{{ $i }}</a></li>
                                    @endfor
                                    @if($products->lastPage() != $products->currentPage())
                                            <li><a href="{{ $products->nextPageUrl() }}" ><i class="zmdi zmdi-chevron-right"></i></a></li>
                                    @endif
                                </ul>
                                @else
                                    <div class="no-search-jobs-list">Opps!!!. There is no any records</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('.cart').on('click', function (event) {
                    event.preventDefault();
                    var product_id = $(this).data('id');
                    minshoppingcart.add(product_id, 'cart');
                });

                $('.wishlists').on('click', function(){
                    event.preventDefault();
                    var product_id = $(this).data('id');
                    minshoppingcart.add(product_id, null);
                });
                $('#sort_option').on('change', function () {
                    var sort = $(this).val();

                    switch (sort) {
                        case '1':
                            order= 'name';
                            orderBy = 'asc';
                            break;
                        case '2':
                            order= 'name';
                            orderBy = 'desc';
                            break;
                        case '3':
                            order= 'price';
                            orderBy = 'asc';
                            break;
                        case '4':
                            order= 'price';
                            orderBy = 'desc';
                            break;
                        default:
                            order= 'name';
                            orderBy = 'asc';
                    }
                    url = '?order=' + order + '&order_by=' + orderBy;
                    document.location.search = url;
                });
                $('#filter_btn').click(function(event) {
                    event.preventDefault();
                    var amount = $('#amount').val();
                    amount = encodeURI(amount);
                    var kvp = document.location.search.substr(1).split('&');
                    if(kvp[0])
                    {
                        var status = 1;
                       for(var i = 0; i < kvp.length; i++)
                       {
                           x = kvp[i].split('=');
                           if(x[0] == 'amount')
                           {
                               x[1] = amount;
                               kvp[i] = x.join('=');
                               status = 0;
                               break;
                           }
                       }
                        if(status)
                        {
                            kvp[kvp.length] = ['amount',amount].join('=');
                        }
                        url = kvp.join('&');
                    }else{
                        url = '?amount='+amount;
                    }
                    document.location.search = url;
                });

            <?php $amount = \Request::get('amount');
                if($amount){
            ?>
                var min = $('#min_amount').val();
                var max = $('#max_amount').val();
                $("#slider-range").slider('values',[min, max]);
                $('#amount').val('LKR' + $('#slider-range').slider('values', 0) +
                    " - LKR" + $('#slider-range').slider('values', 1));
            <?php }
            ?>


            });
        </script>
@endsection


