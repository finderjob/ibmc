@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('auth');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--6" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Password Reset</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="{{ route('home') }}">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Password Reset</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start My Account Area -->
    <section class="my_account_area pt--80 pb--55 bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12"></div>
                <div class="col-lg-6 col-12">
                    <div class="my__account__wrapper">
                        <h3 class="account__title">Password Reset</h3>
                        <form method="POST" action="{{ route('password.request.customer') }}">
                            @csrf
                            <div class="account__form">
                                <div class="input__box">
                                    <label>Email address <span>*</span></label>
                                    <input type="email" name="email" class="input-icon {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" value="{{ old('email') }}" autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form__btn">
                                    <button>Send ResetLink</button>
                                </div>
                                <a class="forget_pass" id="back-to-login" href="{{ route('login') }}">Back To Login</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            <?php
            if(session()->has('success_message')){
                $message = session()->get('success_message');
            ?>
                toastr.success( "<?php echo $message; ?>", 'Success!');
            <?php }
            if(session()->has('error_message')){
                $message = session()->get('error_message');
            ?>
                toastr.error("<?php echo $message; ?>", 'Failed!');
            <?php } ?>
        });
    </script>
@endsection
