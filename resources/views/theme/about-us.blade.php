@extends('theme.default')

@section('content')
    <!-- Start Bradcaump area -->
    <?php $bg_image_path = bg_images('about_us');
        $path = false;
        if($bg_image_path){
            if(file_exists($bg_image_path)){
                $path = $bg_image_path;
            }
        }
    ?>
    <div class="ht__bradcaump__area bg-image--3" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">About us</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">About us</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start About Area -->
    <div class="page-about about_area bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section__title--3 text-center pb--30">
                        <h2><?php echo $data['content']['content_title']; ?></h2>
                        <p><?php echo $data['content']['summary']; ?></p>
                    </div>
                </div>
            </div>
            @if(false)
                <div class="row align-items-center">
                    <div class="col-lg-6 col-sm-12 col-12">
                        <div class="content">
                            <h2 style="text-align: center">Our vision</h2>
                            <?php echo $data['content']['our_vision']; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-12">
                        <div class="content">
                            <h3 style="text-align: center">Our mission</h3>
                            <p><?php echo $data['content']['our_mission']; ?></p>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <!-- End About Area -->
    <?php
        $team_members = team_members();
    ?>
    @if(!empty($team_members))
    <!-- Start Team Area -->
    <section class="wn__team__area pb--75 bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section__title--3 text-center">
                        <h2>Meet our team of experts</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @for($i = 0; $i < count($team_members['team_member']); $i++)
                    <?php $image = 'theme/images/default.png';
                        if(file_exists($team_members['team_member'][$i]['image'])){
                            $image = $team_members['team_member'][$i]['image'];
                        }
                    ?>
                    <!-- Start Single Team -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="wn__team">
                                <div class="thumb">
                                    <img src="{{ $image }}" alt="{{ $team_members['team_member'][$i]['name'] }}">
                                </div>
                                <div class="content text-center">
                                    <h4>{{ $team_members['team_member'][$i]['name'] }}</h4>
                                    <p>{{ $team_members['team_member'][$i]['position'] }}</p>
                                    <ul class="team__social__net">
                                        @if($team_members['team_member'][$i]['twitter'])
                                            <li><a target="_blank" href="{{ $team_members['team_member'][$i]['twitter'] }}"><i class="icon-social-twitter icons"></i></a></li>
                                        @endif
                                        @if($team_members['team_member'][$i]['tumblr'])
                                            <li><a target="_blank" href="{{ $team_members['team_member'][$i]['tumblr'] }}"><i class="icon-social-tumblr icons"></i></a></li>
                                        @endif
                                        @if($team_members['team_member'][$i]['facebook'])
                                            <li><a target="_blank" href="{{ $team_members['team_member'][$i]['facebook'] }}"><i class="icon-social-facebook icons"></i></a></li>
                                        @endif
                                        @if($team_members['team_member'][$i]['linkedin'])
                                            <li><a target="_blank" href="{{ $team_members['team_member'][$i]['linkedin'] }}"><i class="icon-social-linkedin icons"></i></a></li>
                                        @endif
                                        @if($team_members['team_member'][$i]['pinterest'])
                                            <li><a target="_blank" href="{{ $team_members['team_member'][$i]['pinterest'] }}"><i class="icon-social-pinterest icons"></i></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Team -->
                @endfor
            </div>
        </div>
    </section>
    <!-- End Team Area -->
    @endif
@endsection

