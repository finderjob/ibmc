@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('contact_us');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--6" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Contact Us</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Contact Us</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start Contact Area -->
    <section class="wn_contact_area bg--white pt--80 pb--80">
        @if(false)
        <div class="google__map pb--80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="googleMap"></div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-12">
                    <div class="contact-form-wrap">
                        <h2 class="contact__title">Get in touch</h2>
                        <p><?php echo $data['content']['get_in_touch_content']; ?></p>
                        @if(session()->has('success_message'))
                            <div style="margin: 5px">
                                <div class="alert alert-success" style="border-left: 5px solid #fb0303">
                                    <ul>
                                        <li>{{ session()->get('success_message') }}</li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                        @if(session()->has('error_message'))
                            <div style="margin: 5px">
                                <div class="alert alert-danger" style="border-left: 5px solid #fb0303">
                                    <ul>
                                        <li>{{ session()->get('error_message') }}</li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <form action="{{ route('contact.us.post') }}" method="post">
                            {{ csrf_field() }}
                            <div class="single-contact-form">
                                <input type="text" name="name" placeholder="Name*" class="{{ $errors->has('name') ? 'error-input' : '' }}">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="single-contact-form">
                                <input type="email" name="email" placeholder="Email*" class="{{ $errors->has('email') ? 'error-input' : '' }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="single-contact-form">
                                <input type="text" name="subject" placeholder="Subject*" class="{{ $errors->has('subject') ? 'error-input' : '' }}">
                                @if ($errors->has('subject'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="single-contact-form message">
                                <textarea name="message" placeholder="Type your message here.." class="{{ $errors->has('message') ? 'error-input' : '' }}"></textarea>
                                @if ($errors->has('message'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="contact-btn">
                                <button type="submit">Send Email</button>
                            </div>
                        </form>
                    </div>
                    <div class="form-output">
                        <p class="form-messege">
                    </div>
                </div>
                <div class="col-lg-4 col-12 md-mt-40 sm-mt-40">
                    <div class="wn__address">
                        <h2 class="contact__title">Get office info.</h2>
                        <p><?php echo $data['content']['office_info_content']; ?></p>
                        <div class="wn__addres__wreapper">
                            @if($data['content']['office_address'])
                            <div class="single__address">
                                <i class="icon-location-pin icons"></i>
                                <div class="content">
                                    <span>Address:</span>
                                    <p>{{ $data['content']['office_address'] }}</p>
                                </div>
                            </div>
                            @endif
                            @if($data['content']['office_phone'])
                            <div class="single__address">
                                <i class="icon-phone icons"></i>
                                <div class="content">
                                    <span>Phone Number:</span>
                                    <p><a href="tel:<?php echo str_replace(" ", '', $data['content']['office_phone']);?>">{{ $data['content']['office_phone'] }}</a> </p>
                                </div>
                            </div>
                            @endif
                            @if($data['content']['office_email'])
                            <div class="single__address">
                                <i class="icon-envelope icons"></i>
                                <div class="content">
                                    <span>Email address:</span>
                                    <p><a href="mailto:{{ $data['content']['office_email'] }}"> {{ $data['content']['office_email'] }}</a></p>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Area -->
@endsection
