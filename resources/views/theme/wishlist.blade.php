@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('wishlist');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--5" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Wishlist</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Wishlist</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- cart-main-area start -->
    <div class="wishlist-area section-padding--lg bg__white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="wishlist-content">
                        <form action="#">
                            <div class="wishlist-table wnro__table table-responsive">
                                <table class="wishlist-table">
                                    <thead>
                                    <tr>
                                        <th class="product-remove"></th>
                                        <th class="product-thumbnail">Image</th>
                                        <th class="product-name"><span class="nobr">Product Name</span></th>
                                        <th class="product-price"><span class="nobr"> Unit Price </span></th>
                                        <th class="product-stock-stauts"><span class="nobr"> Stock Status </span></th>
                                        <th class="product-add-to-cart"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($wishlists) == 0)
                                        <tr>
                                            <td colspan="6" style="text-align: center"> Wishlist is empty</td>
                                        </tr>
                                    @endif
                                    @foreach($wishlists as $wishlist)
                                        <?php $cover_images = products_cover_images($wishlist->product_id);
                                                $product_price = 0;
                                                if($wishlist->customer_type == 2){
                                                    $product_price = $wishlist->school_price;
                                                }else{
                                                    $product_price = $wishlist->normal_price - $wishlist->normal_discount;
                                                }
                                        ?>
                                        <tr id="wishlist_id_{{ $wishlist->id }}">
                                            <td class="product-remove"><a href="#" class="remove" data-id="{{ $wishlist->id }}">×</a></td>
                                            <td class="product-thumbnail" style="width: 150px;"><a href="#"><img src="{{ asset($cover_images->image_path) }}" alt="" style="max-width: 66%"></a></td>
                                            <td class="product-name"><a href="#">{{ $wishlist->name }}</a></td>
                                            <td class="product-price"><span class="amount">{{ final_price_format($product_price) }}</span></td>
                                            <td class="product-stock-status">
                                                @if($wishlist->products_qty == 0)
                                                    <span class="wishlist-in-stock">Out of Stock</span>
                                                @else
                                                    <span class="wishlist-in-stock">In Stock</span>
                                                @endif

                                            </td>
                                            <td class="product-add-to-cart"><a href="#" class="add-cart" data-id="{{ $wishlist->id }}" data-product="{{ $wishlist->product_id }}"> Add to Cart</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- cart-main-area end -->
    <script>
        $(document).ready(function() {
            $('.remove').on('click', function (event) {
                event.preventDefault();
                let _token = $('input[name="_token"]').val();
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ route('wishlist.remove') }}",
                    type: 'post',
                    data: {_token: _token, row_id:id},
                    success: function(data){
                        let return_data = jQuery.parseJSON(data);
                        if(return_data.status == 200)
                        {
                            $('#wishlist_id_' + id).remove();
                            toastr.success( return_data.message, 'Success!');
                        }
                        else{
                            toastr.error( return_data.message, 'Failed!');
                        }
                        var rowCount = $('.wishlist-table tbody tr').length;
                        if(rowCount == 0){
                            emptyTr();
                        }
                    }
                });

            });

            $('.add-cart').on('click', function (event) {
                event.preventDefault();
                let _token = $('input[name="_token"]').val();
                let id = $(this).data('id');
                let product_id = $(this).data('product');
                $.ajax({
                    url: "{{ route('cart.add') }}",
                    type: 'post',
                    data: {_token: _token, product_id:product_id, row_id: id},
                    success: function(data){
                        var return_data = jQuery.parseJSON(data);
                        if(return_data.status == 200)
                        {
                            minshoppingcart.init();
                            $('#wishlist_id_' + id).remove();
                            toastr.success( return_data.message, 'Success!');
                        }
                        else{
                            toastr.error( return_data.message, 'Failed!');
                        }
                        var rowCount = $('.wishlist-table tbody tr').length;
                        if(rowCount == 0){
                            emptyTr();
                        }
                    }
                });
            });
        });
        function emptyTr(){
            var td = '<td colspan="6" style="text-align: center"> Wishlist is empty</td>';
            var tr = '<tr>' + td + '</tr>';
            $('.wishlist-table tbody').html(tr);
        }
    </script>
    <tr id="empty-remove">

    </tr>
@endsection
