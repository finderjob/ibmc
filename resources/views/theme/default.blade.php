<!doctype html>
<html class="no-js" lang="zxx">
    @include('theme.partials.head')
    <body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3"></script>
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->

    <!-- Main wrapper -->
    <div class="wrapper" id="wrapper">

    @include('theme.partials.header')
        @yield('content')
    @include('theme.partials.footer')
    </div>
    <!-- //Main wrapper -->

    @include('theme.partials.foot')
</body>
</html>
