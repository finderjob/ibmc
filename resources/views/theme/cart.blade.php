@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('cart');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--3" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Shopping Cart</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Shopping Cart</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- cart-main-area start -->
    <div class="cart-main-area section-padding--lg bg--white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 ol-lg-12">
                    <form action="#">
                        <div class="table-content wnro__table table-responsive">
                            <table id="cart_table">
                                <thead>
                                <tr class="title-top">
                                    <th class="product-thumbnail">Image</th>
                                    <th class="product-name">Product</th>
                                    <th class="product-price">Price</th>
                                    <th class="product-quantity">Quantity</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-remove">Remove</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $total  = 0;?>
                                @if(count($carts) == 0)
                                    <tr>
                                        <td colspan="6" style="text-align: center"> Cart is empty</td>
                                    </tr>
                                @endif
                                @foreach($carts as $cart)
                                    <?php
                                        $cover_images = products_cover_images($cart->product_id);
                                        $product_price = 0;
                                        if($cart->customer_type == 2){
                                            $product_price = $cart->school_price;
                                        }else{
                                            $product_price = $cart->normal_price - $cart->normal_discount;
                                        }
                                        $sub_total = $product_price * $cart->qty;
                                        $total += $sub_total;
                                    ?>
                                    <tr id="cart_id_{{ $cart->id }}" data-id="{{ $cart->id }}">
                                        <td class="product-thumbnail"><a href="#"><img src="{{ asset($cover_images->image_path) }}" alt="product img" style="max-width: 66%"></a></td>
                                        <td class="product-name"><a href="#">{{ $cart->name }}</a></td>

                                        <td class="product-price">
                                            <span class="amount">{{ final_price_format($product_price) }}</span>
                                            <input type="hidden" id="{{  $cart->product_id }}" value="{{ $product_price }}">
                                        </td>
                                        <td class="product-quantity">
                                            <input type="number" class="product_qty" min="1" step="1" data-cart="{{ $cart->id }}" data-id="{{ $cart->product_id }}" data-prev="{{ $cart->qty }}" value="{{ $cart->qty }}"></td>

                                        <td class="product-subtotal">
                                            <span class="cart_{{ $cart->id }}">{{ final_price_format($sub_total) }}</span>
                                        </td>
                                        <input type="hidden" class="cart_item_{{ $cart->id }}" value="{{ $sub_total }}">
                                        <td class="product-remove"><a href="#" class="remove" data-remove="{{ $cart->id }}">X</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="cartbox__btn">
                        <ul class="cart__btn__list d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between right">
                            <li><a href="{{ route('shop') }}">Update Cart</a></li>
                            @if(count($carts) != 0)
                                <li><a href="{{ route('checkout') }}">Check Out</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-6">
                    <div class="cartbox__total__area">

                        <div class="cart__total__amount">
                            <span>Sub Total</span>
                            <span><span class="grand-total">{{ final_price_format($total) }}</span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- cart-main-area end -->
    <script>
        $(document).ready(function(){
            var rowCount = $('#cart_table tbody tr').length;
            $('.remove').on('click', function (event) {
                event.preventDefault();
                let id = $(this).data('remove');
                minshoppingcart.remove(id, 2);

            });
            $('.product_qty').focusout(function (event) {
                $this = $(this);
                let qty = $this.val();
                let previous_qty = $this.data('prev');
                if(qty != previous_qty)
                {
                    let product_id = $(this).data('id');
                    let _token = $('input[name="_token"]').val();
                    let price = $('#' + product_id).val();
                    let subtotal = parseInt(qty) * parseFloat(price);
                    let cart_id = $(this).data('cart');
                    $.ajax({
                        url: "{{ route('cart.update') }}",
                        type: 'post',
                        data: {_token: _token, product_id:product_id, qty:qty, price: price},
                        success: function(data){
                            let return_data = jQuery.parseJSON(data);
                            if(return_data.status == 200)
                            {
                                $this.data('prev', qty);
                                $('.cart_' + cart_id).html(return_data.sub_total);
                                $('.cart_item_' + cart_id).val(subtotal);
                                $('.grand-total').html(return_data.total);
                                minshoppingcart.update();
                                toastr.success( return_data.message, 'Success!');
                            }
                            else{
                                toastr.error( return_data.message, 'Failed!');
                            }

                        }
                    });
                }
            });
        });
        function emptyTr(){
            var td = '<td colspan="6" style="text-align: center"> Cart is empty</td>';
            var tr = '<tr>' + td + '</tr>';
            $('#cart_table tbody').html(tr);
        }
    </script>
@endsection
