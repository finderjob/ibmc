@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('authors');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--5" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Authors</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Authors</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="wn_contact_area bg--white pt--80 pb--80">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <h2 style="text-align: center">{{ $author->name }}</h2>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="" style="text-align: center">
                        <div class="thumb author-data" data-url="{{ route('authors.details', [ $author->slug ]) }}">
                            <img src="{{ asset($author->image_path) }}" alt="{{ $author->slug }}">
                        </div>
                        <div class="content text-center">
                            <ul class="team__social__net" style="margin-top: 12px; margin-bottom: 12px;">
                                <?php $social_medias = config('status.social_media');
                                $value = '';
                                $social_medias_values = json_decode($author->social_media, true);
                                ?>
                                @foreach($social_medias as $social_media)
                                    @if(array_key_exists($social_media['value'], $social_medias_values))
                                        <li><a target="_blank" href="{{ $social_medias_values[$social_media['value']] }}"><i class="icon-social-{{ $social_media['value'] }} icons"></i></a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-12">
                    <div class="author-text">
                        <?php echo json_decode($author->description);?>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
