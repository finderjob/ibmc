@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('auth');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--6" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Login / Register</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="{{ route('home') }}">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Login / Register</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start My Account Area -->
    <section class="my_account_area pt--80 pb--55 bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="my__account__wrapper">
                        <div id="login-form-section">
                            <h3 class="account__title">Login</h3>
                            <form method="POST" action="{{ route('login') }}" class="customer-login-form">
                                @csrf
                                <div class="account__form">
                                    <div class="input__box">
                                        <label>Email address <span>*</span></label>
                                        <input type="email" name="email" autocomplete="off" class="input-icon {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" value="{{ old('email') }}" autofocus>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="input__box">
                                        <label>Password<span>*</span></label>
                                        <input type="password" name="password" id="password" class="input-icon {{ $errors->has('password') ? 'is-invalid' : '' }}">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form__btn">
                                        <button>Login</button>
                                        <label class="label-for-checkbox">
                                            <input id="remember" class="input-checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} type="checkbox">
                                            <span>Remember me</span>
                                        </label>
                                    </div>
                                    <a class="forget_pass" id="forget-pass" href="{{ route('password.reset.customer') }}">Lost your password?</a>
                                </div>
                            </form>
                        </div>
                        <div id="password-reset-form-section" style="display: none">
                            <h3 class="account__title">Password Reset</h3>
                            <form method="POST" action="{{ route('password.request.customer') }}">
                                @csrf
                                <div class="account__form">
                                    <div class="input__box">
                                        <label>Email address <span>*</span></label>
                                        <input type="email" name="email" class="input-icon {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" value="{{ old('email') }}" autofocus>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form__btn">
                                        <button>Send ResetLink</button>
                                    </div>
                                    <a class="forget_pass" id="back-to-login" href="">Back To Login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="my__account__wrapper">
                        <h3 class="account__title">Register</h3>
                        <form class="customer-register-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="account__form">
                                <div class="input__box">
                                    <label>Username <span>*</span></label>
                                    <input type="text" name="username" id="username" autocomplete="off"  class="input-icon {{ $errors->has('username') ? 'is-invalid' : '' }}">
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="input__box">
                                    <label>Email address <span>*</span></label>
                                    <input type="email" name="register_email" autocomplete="off" id="register_email" class="input-icon {{ $errors->has('register_email') ? 'is-invalid' : '' }}">
                                    @if ($errors->has('register_email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('register_email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="input__box">
                                    <label>Password<span>*</span></label>
                                    <input type="password" name="register_password" id="register_password" class="input-icon {{ $errors->has('register_password') ? 'is-invalid' : '' }}">
                                    @if ($errors->has('register_password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('register_password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="input__box">
                                    <label>Confirm Password<span>*</span></label>
                                    <input id="password-confirm" type="password" name="password_confirmation" class="input-icon {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form__btn">
                                    <button>Register</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End My Account Area -->
    <script>
        $(document).ready(function(){
            $('input').on('keypress',function(){
                $(this).removeClass('is-invalid');
            });

            $('#forget-pass').on('click', function (event) {
                //event.preventDefault();
                //$('#login-form-section').css('display', 'none');
                //$('#password-reset-form-section').css('display', 'block');
            });
            $('#back-to-login').on('click', function () {
                event.preventDefault();
                $('#password-reset-form-section').css('display', 'none');
                $('#login-form-section').css('display', 'block');
            });
            <?php
            if(session()->has('success_message')){
            $message = session()->get('success_message');
            ?>
            toastr.success( "<?php echo $message; ?>", 'Success!');
            <?php }
            if(session()->has('error_message')){
            $message = session()->get('error_message');
            ?>
            toastr.error("<?php echo $message; ?>", 'Failed!');
            <?php } ?>
        });
    </script>
@endsection
