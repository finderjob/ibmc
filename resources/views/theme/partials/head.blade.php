<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ \App\Http\Controllers\HomeController::$page_title }} | IBMC Bookshop</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <?php $favicon = favicon();?>
    @if($favicon)
        <link rel="shortcut icon" href="{{ asset($favicon) }}">
        <link rel="apple-touch-icon" href="{{ asset($favicon) }}">
    @else
    @endif
    <!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('theme/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}" />
    <!-- Cusom css -->
    <link rel="stylesheet" href="{{ asset('theme/css/custom.css') }}">


    <!-- Modernizer js -->
    <script src="{{ asset('theme/js/vendor/modernizr-3.5.0.min.js') }}"></script>
    <script src="{{ asset('theme/js/vendor/jquery-3.2.1.min.js') }}"></script>
</head>
