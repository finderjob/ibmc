<!-- JS Files -->
<script src="{{ asset('theme/js/popper.min.js') }}"></script>
<script src="{{ asset('theme/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme/js/plugins.js') }}"></script>
<script src="{{ asset('theme/js/active.js') }}"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script src="{{ asset('js/admin/login.js') }}"></script>
<script src="{{ asset('js/plugins/customer-validation.js') }}"></script>
<script>
    toastr.options = {
        progressBar: true,
        newestOnTop: false,
        closeButton: true,
        positionClass: "toast-bottom-right"
    };
</script>
