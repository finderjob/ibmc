@foreach($carts as $cart)
    <?php $cover_images = products_cover_images($cart->product_id);
        $product_price = 0;
        if($cart->customer_type == 2){
            $product_price = $cart->school_price;
        }else{
            $product_price = $cart->normal_price - $cart->normal_discount;
        }
        $sub_total = $product_price * $cart->qty;
    ?>
<div class="item01 d-flex">
    <div class="thumb">
        <a href=""><img src="{{ asset($cover_images->image_path) }}" alt="product images"></a>
    </div>
    <div class="content">
        <h6><a href="{{ route('product',[$cart->slug]) }}">{{ $cart->name }}</a></h6>
        <span class="prize">{{ final_price_format($sub_total) }}</span>
        <div class="product_prize d-flex justify-content-between">
            <span class="qun">Qty: {{ sprintf('%02d', $cart->qty) }}</span>
            <ul class="d-flex justify-content-end" style="margin-right: 16px">
                <li><a href="#" class="remove_product" data-id="{{ $cart->id }}" onclick="removeProduct({{ $cart->id }});"><i class="zmdi zmdi-delete"></i></a></li>
            </ul>
        </div>
    </div>
</div>
@endforeach

