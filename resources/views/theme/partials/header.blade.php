<!-- Header -->
<?php $route_name = \Request::route()->getName();
        $class = null;
        if($route_name != "home"){
            $class = "oth-page";
        }
?>
<header id="wn__header" class="{{ $class }} header__area header__absolute sticky__header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-6 col-lg-2">
                <div class="logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('theme/images/logo/logo.png') }}" alt="logo images">
                    </a>
                </div>
            </div>
            <div class="col-lg-8 d-none d-lg-block">
                <nav class="mainmenu__nav">
                    <ul class="meninmenu d-flex justify-content-start">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('about.us') }}">About us</a></li>
                        <li><a href="{{ route('shop') }}">Shop</a></li>
                        <li class="drop"><a href="#">Category</a>
                            <div class="megamenu dropdown">
                                <ul class="item item01">
                                    <?php $categories = get_all_categories(); ?>
                                    @for($i = 1; $i < count($categories); $i++)
                                        <li><a href="{{ route('shop.category', [$categories[$i]['slug']]) }}">{{ $categories[$i]['name'] }}</a></li>
                                    @endfor
                                </ul>
                            </div>
                        </li>
                        <li><a href="{{ route('authors') }}">Authors</a></li>
                        @if(false)
                        <li><a href="{{ route('blog') }}">Blog</a></li>
                        @endif
                        <li><a href="{{ route('contact.us') }}">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6 col-sm-6 col-6 col-lg-2">
                <ul class="header__sidebar__right d-flex justify-content-end align-items-center">
                    <li class="shop_search"><a class="search__active" href="#"></a></li>
                    <li class="wishlist"><a href="{{ route('wishlist') }}"></a></li>
                    @if(Auth::guard('customers')->user())
                        <li class="shopcart"><a class="cartbox_active" href="#"><span class="product_qun">0</span></a>
                            <!-- Start Shopping Cart -->
                            <div class="block-minicart minicart__active">
                                <div class="minicart-content-wrapper">
                                    <div class="micart__close">
                                        <span>close</span>
                                    </div>
                                    <div id="mini-checkout">
                                        <div class="items-total d-flex justify-content-between">
                                            <span><span id="total-count">0</span> items</span>
                                            <span>Cart Subtotal</span>
                                        </div>
                                        <div class="total_amount text-right">
                                            <span><span id="cart_sub_total">00.00</span></span>
                                        </div>
                                        <div class="mini_action checkout">
                                            <a class="checkout__btn" href="{{ route('checkout') }}">Go to Checkout</a>
                                        </div>
                                    </div>
                                    <div class="single__items">
                                        <div class="miniproduct">
                                        </div>
                                    </div>
                                    <div class="mini_action">
                                        <a class="cart__btn" href="{{ route('cart') }}">View and edit cart</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Shopping Cart -->
                        </li>
                        <li class="setting__bar__icon">
                            <a class="setting__active" href="#"></a>
                            <div class="searchbar__content setting__block">
                                <div class="content-inner">
                                    <div class="switcher-currency">
                                        <strong class="label switcher-label">
                                            <span>My Account</span>
                                        </strong>
                                        <div class="switcher-options">
                                            <div class="switcher-currency-trigger">
                                                <div class="setting__menu">
                                                    <span><a href="{{ route('profile') }}">My Account</a></span>
                                                    <span><a href="{{ route('cart') }}">My Cart <span class="badge badge-danger product_qun cart_items pull-right"></span></a></span>
                                                    <span><a href="{{ route('wishlist') }}">My Wishlist</a></span>
                                                    <span><a href="{{ route('orders') }}">My Orders</a></span>
                                                    <span><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">Sign Out</a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li style="margin-right: 5px">Hi</li>
                        <li>{{ Auth::guard('customers')->user()->name }}</li>
                    @else
                        <li class="setting__bar__icon"><a class="setting__active" href="#"></a>
                            <div class="searchbar__content setting__block">
                                <div class="content-inner">
                                    <div class="switcher-currency">
                                        <strong class="label switcher-label">
                                            <span>
                                                <ul>
                                                    <li> <a href="{{ route('login') }}">Sign In</a></li>
                                                    <li> <a href="{{ route('register') }}">Register</a></li>
                                                </ul>
                                            </span>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </li>

                    @endif
                </ul>
            </div>
        </div>
        <!-- Start Mobile Menu -->
        <div class="row d-none">
            <div class="col-lg-12 d-none">
                <nav class="mobilemenu__nav">
                    <ul class="meninmenu">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('about.us') }}">About us</a></li>
                        <li><a href="{{ route('shop') }}">Shop</a></li>
                        <li><a href="#">Category</a>
                            <ul>
                                <?php $categories = get_all_categories(); ?>
                                @for($i = 1; $i < count($categories); $i++)
                                    <li><a href="{{ route('shop',[$categories[$i]['slug']]) }}">{{ $categories[$i]['name'] }}</a></li>
                                @endfor
                            </ul>
                        </li>
                        <li><a href="{{ route('authors') }}">Authors</a></li>
                        @if(false)
                        <li><a href="{{ route('blog') }}">Blog</a></li>
                        @endif
                        <li><a href="{{ route('contact.us') }}">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- End Mobile Menu -->
        <div class="mobile-menu d-block d-lg-none">
            <input type="hidden" id="cart_count">
        </div>
        <!-- Mobile Menu -->
    </div>
    {{ csrf_field() }}
</header>
<script>
    var minshoppingcart = function () {
        var getShoppingData = function(){

            $.ajax({
                url: '{{ route('cart.get') }}',
                type: 'get',
                success: function(data){
                    if(data.status == 201){
                        toastr.error( data.message, 'Failed!');
                    }else{
                        $('.product_qun').html(data['count']);
                        $('#cart_count').val(data['count']);
                        $('#total-count').html(data['count']);
                        $('#cart_sub_total').html(data['total']);
                        $('.miniproduct').html(data['resource']);
                    }

                }
            });
        };

        var removeData = function(cart_id, status){
            let _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('cart.remove') }}",
                type: 'post',
                data: {_token: _token, row_id: cart_id},
                success: function(data){
                    let return_data = jQuery.parseJSON(data);
                    if(return_data.status == 200)
                    {
                        if(status == 2)
                        {
                            $('#cart_id_' + cart_id).remove();
                            $('.grand-total').html(return_data.total);
                        }
                        minshoppingcart.init();
                        toastr.success( return_data.message, 'Success!');
                    }
                    else{
                        toastr.error( return_data.message, 'Failed!');
                    }
                    if(status == 2){
                        var rowCount = $('#cart_table tbody tr').length;
                        console.log(rowCount);
                        if(rowCount == 0){
                            var td = '<td colspan="6" style="text-align: center"> Cart is empty</td>';
                            var tr = '<tr>' + td + '</tr>';
                            $('#cart_table tbody').html(tr);
                        }
                    }

                }
            });
        };
        var wishlist_add = function (product_id) {

            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('wishlist.add') }}",
                type: 'post',
                data: {_token: _token, product_id:product_id},
                success: function(data){
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 200)
                    {
                        minshoppingcart.init();
                        toastr.success( return_data.message, 'Success!');
                    }
                    else{
                        toastr.error( return_data.message, 'Failed!');
                    }

                }
            });
        };

        var cart_add = function(product_id){
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('cart.add') }}",
                type: 'post',
                data: {_token: _token, product_id:product_id},
                success: function(data){
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 200)
                    {
                        minshoppingcart.init();
                        toastr.success( return_data.message, 'Success!');
                    }
                    else{
                        toastr.error( return_data.message, 'Failed!');
                    }

                }
            });
        }
        return {
            //main function to initiate the module
            init: function () {
                @if(Auth::guard('customers')->user())
                    getShoppingData();
                @endif

            },
            remove: function(cart_id, status){
                removeData(cart_id, status);
            },
            update: function () {
            },
            add: function (product_id, type) {
                if(type == 'cart'){
                    cart_add(product_id);
                }else{
                    wishlist_add(product_id);
                }

            },
        };
    }();
    @if(Auth::guard('customers')->user())
        function removeProduct(id){
            event.preventDefault();
            minshoppingcart.remove(id, 1);
        }
    @endif

    $(document).ready(function(){
        $(document).ready(function () {
            minshoppingcart.init();
        });
        $('#subcribe-btn').on('click', function () {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var email = $('#subscribe').val();
            var _token = $('input[name="_token"]').val();

            if(email){
                if(filter.test(email)){
                    $.ajax({
                        url: "{{ route('subscribe') }}",
                        type: "post",
                        data: {_token: _token, email:email},
                        success: function (data) {
                            var return_data = jQuery.parseJSON(data);
                            if(return_data.status == 200)
                            {
                                $(this).val('');
                                $('.error-message').removeClass('invalid-feedback').html('');
                                toastr.success( return_data.message, 'Success!');
                            }
                            else if(return_data.status == 202){
                                var message = return_data.message.email;
                                $('.error-message').addClass('invalid-feedback').html(message[0]);
                                toastr.error( message[0], 'Failed!');
                            }
                            else{
                                toastr.error( return_data.message, 'Failed!');
                            }
                        }
                    });
                }else{
                    message = "Please enter valid subscribe email";
                    $('.error-message').addClass('invalid-feedback').html(message);
                    toastr.error(message , 'Failed!');
                }
            }else{
                message = "Please enter subscribe email";
                $('.error-message').addClass('invalid-feedback').html(message);
                toastr.error( message, 'Failed!');
            }

        });
    });
</script>
<!-- //Header -->
<!-- Start Search Popup -->
<div class="brown--color box-search-content search_active block-bg close__top">
    <form id="search_mini_form" class="minisearch" action="#">
        <div class="field__search">
            <input type="text" placeholder="Search entire store here...">
            <div class="action">
                <a href="#"><i class="zmdi zmdi-search"></i></a>
            </div>
        </div>
    </form>
    <div class="close__wrap">
        <span>close</span>
    </div>
</div>
<!-- End Search Popup -->
