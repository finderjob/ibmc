<div class="shop__sidebar">
    <aside class="wedget__categories poroduct--cat">
        <h3 class="wedget__title">Product Categories</h3>
        <ul class="set-category-list">
            <?php $categories = get_all_categories();?>
            @for($i = 0; $i < count($categories); $i++)
                @if($i == 0)
                    <li class="@if(!$slug) active @endif"><a href="{{ route('shop') }}" class="@if(!$slug) active @endif">{{ $categories[$i]['name'] }} <span class="count">({{ $categories[$i]['count'] }})</span></a></li>
                @else
                   <?php $get_sub_category = get_sub_category_by_using_category_id($categories[$i]['id']);?>
                    @if(count($get_sub_category) == 0)
                           <li><a href="{{ route('shop.category', $categories[$i]['slug']) }}" class="@if($categories[$i]['slug'] == $slug) active @endif">{{ $categories[$i]['name'] }} <span class="count">({{ $categories[$i]['count'] }})</span></a></li>
                    @else
                           <li class="category-list @if($slug == $categories[$i]['slug']) active set-border-bottom @endif"> {{ $categories[$i]['name'] }}
                               <span class="set-icon fa @if($slug == $categories[$i]['slug']) fa-angle-up @else fa-angle-down @endif"></span>
                               <span class="count">({{ $categories[$i]['count'] }})</span>
                               <ul id="{{ $categories[$i]['slug'] }}" class="@if($slug == $categories[$i]['slug']) open @endif">
                                   @for($j = 0; $j < count($get_sub_category); $j++)
                                       <li><a class="@if($sub_slug == $get_sub_category[$j]['slug']) active @endif" href="{{ route('shop.sub-category', [$categories[$i]['slug'], $get_sub_category[$j]['slug']]) }}">{{ $get_sub_category[$j]['name'] }}<span>({{ $get_sub_category[$j]['count'] }})</span></a></li>
                                   @endfor
                               </ul>
                           </li>
                    @endif
                @endif
            @endfor
        </ul>
    </aside>
    @if(!$remove_filter)
    <aside class="wedget__categories pro--range">
        <h3 class="wedget__title">Filter by price</h3>
        <div class="content-shopby">
            <div class="price_filter s-filter clear">
                <form action="#" method="GET">
                    <div id="slider-range"></div>
                    <div class="slider__range--output">
                        <div class="price__output--wrap">
                            <div class="price--output">
                                <span>Price :</span><input type="text" id="amount" readonly="">
                                <?php $amount = \Request::get('amount');
                                        $min = 0;
                                        $max = 0;
                                        if($amount)
                                        {
                                            $format = config('status.currency_format');
                                            $amount = explode('-', $amount);
                                            $min = ltrim(trim($amount[0]), $format);
                                            $max = ltrim(trim($amount[1]), $format);
                                            $min = floatval($min);
                                            $max = floatval($max);
                                        }
                                ?>
                                <input type="hidden" id="min_amount" value="{{ $min }}">
                                <input type="hidden" id="max_amount" value="{{ $max }}">
                            </div>
                            <div class="price--filter">
                                <a href="" id="filter_btn">Filter</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </aside>
    @endif
    <aside class="wedget__categories poroduct--tag">
        <h3 class="wedget__title">Product Tags</h3>
        <ul>
            @for($i = 0; $i < count($categories); $i++)
                @if($i == 0)
                    <li><a href="{{ route('shop') }}">{{ $categories[$i]['name'] }}</a></li>
                @else
                    <li><a href="{{ route('shop', $categories[$i]['slug']) }}">{{ $categories[$i]['name'] }}</a></li>
                @endif

            @endfor
        </ul>
    </aside>
    <aside class="wedget__categories sidebar--banner">
        <?php $bg_image_path = bg_images('sponsor');
        $path = false;
        if($bg_image_path){
            if(file_exists($bg_image_path)){
                $path = $bg_image_path;
            }
        }
        ?>
        @if($path)
                <img src="{{ asset($path) }}" alt="banner images">
        @else
                <img src="{{ asset('theme/images/others/banner_left.jpg') }}" alt="banner images">
                <div class="text">
                    <h2>new products</h2>
                    <h6>save up to <br> <strong>40%</strong>off</h6>
                </div>
        @endif

    </aside>
</div>
<script>
    $(document).ready(function () {
        $('.category-list').on('click', function (event) {
            var elm =$(event.target).closest('li').children("ul");
            var idName = elm.attr('id');
            $('.category-list').removeClass('set-border-bottom');
            if($('#' + idName).hasClass('open')){
                $(event.target).closest('li').find('span.set-icon').removeClass('fa-angle-up').addClass('fa-angle-down');
                $('#' + idName).removeClass('open');
            }else{
                $('.category-list span.set-icon').removeClass('fa-angle-up').addClass('fa-angle-down');
                $('.category-list ul').removeClass('open');
                $(event.target).closest('li').find('span.set-icon').removeClass('fa-angle-down').addClass('fa-angle-up');
                $('#' + idName).addClass('open');
                $(event.target).closest('li').addClass('set-border-bottom');
            }
        });
    });
</script>
