@foreach($products as $product)
    <?php $cover_images = products_cover_images($product->id);
    $url = route('product',[$product->slug]);
    ?>
    <!-- Start Single Product -->
    <div class="single__product">
        <div class="product product__style--3">
            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                <div class="product__thumb">
                    <a class="first__img" href="{{ $url }}"><img src="{{ asset($cover_images->image_path) }}" alt="{{ $product->name }}"></a>
                    <a class="second__img animation1" href="{{ $url }}"><img src="{{ asset($cover_images->image_path) }}" alt="{{ $product->name }}"></a>
                    <?php $checkBestSeller = checkBestSellerProduct($product->id); ?>
                    @if($checkBestSeller)
                    <div class="hot__box">
                        <span class="hot-label">BEST SALLER</span>
                    </div>
                    @endif
                </div>
                <div class="product__content content--center">
                    <h4><a href="{{ $url }}">{{ $product->name }}</a></h4>
                    <ul class="prize d-flex">
                        @if($customer_type == 2)
                            <li>{{ final_price_format($product->school_price) }}</li>
                            <li class="old_prize">{{ final_price_format($product->normal_price) }}</li>
                        @else
                            <li>{{ final_price_format($product->normal_price - $product->normal_discount) }}</li>
                            @if(($product->normal_price - $product->normal_discount) != $product->normal_price)
                                <li class="old_prize">{{ final_price_format($product->normal_price) }}</li>
                            @endif
                        @endif
                    </ul>
                    <div class="action">
                        <div class="actions_inner">
                            <ul class="add_to_links">
                                <li><a class="cart" onclick="addCart(this)" data-id="{{ $product->id }}" href="#"><i class="bi bi-shopping-cart-full" title="Cart"></i></a></li>
                                <li><a class="wishlists" onclick="addWishList(this)" href="#" data-id="{{ $product->id }}"><i class="bi bi-heart-beat" title="Wishlist"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="product__hover--content">
                        <ul class="rating d-flex">
                            <?php $avg_rating = get_product_rating_value($product->id);?>
                            @for($j = 1; $j <= 5; $j++)
                                @if($j <= $avg_rating)
                                    <li class="on"><i class="fa fa-star"></i></li>
                                @else
                                    <li><i class="fa fa-star-o"></i></li>
                                @endif
                            @endfor
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Single Product -->
@endforeach
