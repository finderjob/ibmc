<ul class="comment__list">
    @foreach($comments as $comment)
        <li>
            <div class="wn__comment">
                <div class="thumb">
                    <img src="{{ asset('theme/images/blog/comment/1.jpeg') }}" alt="comment images">
                </div>
                <div class="content" id="content-{{ $comment->id }}">
                    <div class="comnt__author d-block d-sm-flex">
                        <span><a href="#">{{ $comment->created_by }}</a> @if($comment->created_by_status == 1)<label class="badge btn-success">Post author </label> @elseif($comment->created_by_status == 2) <label class="badge btn-info">Admin</label> @else @endif</span>
                        <span>{{ date('F d, Y', strtotime($comment->created_at)) }}
                                at {{ date('g:i a', strtotime($comment->created_at)) }}
                        </span>
                        <div class="reply__btn">
                            <a href="#" onclick="replyThatComment({{$comment->id}}, {{ $comment->id }})">Reply</a>
                        </div>
                    </div>
                    <p>{{ json_decode($comment->comment) }}</p>
                </div>
            </div>
        </li>
        <?php $replies = \App\BlogComment::where('post_id', $comment->post_id)->where('comment_id','=', $comment->id)->where('status', 1)->get();?>
            @foreach($replies as $reply)
                <li class="comment_reply">
                    <div class="wn__comment">
                        <div class="thumb">
                            <img src="{{ asset('theme/images/blog/comment/1.jpeg') }}" alt="comment images">
                        </div>
                        <div class="content" id="content-{{ $reply->id }}">
                            <div class="comnt__author d-block d-sm-flex">
                                <span><a href="#">{{ $reply->created_by }}</a> @if($reply->created_by_status == 1)<label class="badge btn-success">Post author </label> @elseif($reply->created_by_status == 2) <label class="badge btn-info">Admin</label> @else @endif </span>
                                <span>{{ date('F d, Y', strtotime($reply->created_at)) }}
                                    at {{ date('g:i a', strtotime($reply->created_at)) }}</span>
                                <div class="reply__btn">
                                    <a href="#" onclick="replyThatComment({{$comment->id}}, {{ $reply->id }})">Reply</a>
                                </div>
                            </div>
                            <p>{{ json_decode($reply->comment) }}</p>
                        </div>
                    </div>
                </li>
            @endforeach
    @endforeach
</ul>
