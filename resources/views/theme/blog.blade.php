@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('blog');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--4" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Blog Page</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Blog</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start Blog Area -->
    <div class="page-blog bg--white section-padding--lg blog-sidebar right-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-12">
                    <div class="blog-page">
                        <div class="page__header">
                            <h2>Blog List</h2>
                        </div>
                        @foreach($blogs as $blog)
                            <!-- Start Single Post -->
                                <article class="blog__post d-flex flex-wrap">
                                    <div class="thumb">
                                        <a href="{{ route('blog', $blog->slug) }}">
                                            <img src="{{ asset($blog->image) }}" alt="blog images">
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h4><a href="{{ route('blog', $blog->slug) }}">{{ $blog->name }}</a></h4>
                                        <ul class="post__meta">
                                            <li>Posts by : <a href="#">{{ \App\User::find($blog->created_by)->name }}</a></li>
                                            <li class="post_separator">/</li>
                                            <li>{{ date('M d Y', strtotime($blog->created_at)) }}</li>
                                        </ul>
                                        <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Crastoup pretium arcu ex. Aenean posuere libero eu augue rhoncus Praesent ornare tortor amet.</p>
                                        <div class="blog__btn">
                                            <a href="{{ route('blog', $blog->slug) }}">read more</a>
                                        </div>
                                    </div>
                                </article>
                                <!-- End Single Post -->
                        @endforeach

                    </div>
                    @if($blogs->total() != 0)
                        <ul class="wn__pagination">
                            @if(1 != $blogs->currentPage())
                                <li><a href="{{ $blogs->url($blogs->currentPage() - 1) }}"><i class="zmdi zmdi-chevron-left"></i></a></li>
                            @endif
                            <li><a href="#"></a></li>
                            @for($i = 1; $i <= $blogs->lastPage(); $i++)
                                <li @if($i == $blogs->currentPage()) class="active" @endif><a @if($i != $blogs->currentPage()) href="{{ $blogs->url($i) }}"  @endif>{{ $i }}</a></li>
                            @endfor
                            @if($blogs->lastPage() != $blogs->currentPage())
                                <li><a href="{{ $blogs->nextPageUrl() }}" ><i class="zmdi zmdi-chevron-right"></i></a></li>
                            @endif
                        </ul>
                    @else
                        <div class="no-search-jobs-list">Opps!!!. There is no any records</div>
                    @endif
                </div>
                <div class="col-lg-3 col-12 md-mt-40 sm-mt-40">
                    <div class="wn__sidebar">
                        <!-- Start Single Widget -->
                        <aside class="widget search_widget">
                            <h3 class="widget-title">Search</h3>
                            <form action="#">
                                <div class="form-input">
                                    <input type="text" placeholder="Search...">
                                    <button><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </aside>
                        <!-- End Single Widget -->
                        <!-- Start Single Widget -->
                        <aside class="widget recent_widget">
                            <h3 class="widget-title">Recent</h3>
                            <div class="recent-posts">
                                <ul>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/1.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Blog image post</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/2.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Post with Gallery</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/3.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Post with Video</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/4.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Maecenas ultricies</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-wrapper d-flex">
                                            <div class="thumb">
                                                <a href="blog-details.html"><img src="images/blog/sm-img/5.jpg" alt="blog images"></a>
                                            </div>
                                            <div class="content">
                                                <h4><a href="blog-details.html">Blog image post</a></h4>
                                                <p>	March 10, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                        <!-- End Single Widget -->
                        <!-- Start Single Widget -->
                        <aside class="widget comment_widget">
                            <h3 class="widget-title">Comments</h3>
                            <ul>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>demo says:</p>
                                            <a href="#">Quisque semper nunc vitae...</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>Admin says:</p>
                                            <a href="#">Curabitur aliquet pulvinar...</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>Irin says:</p>
                                            <a href="#">Quisque semper nunc vitae...</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>Boighor says:</p>
                                            <a href="#">Quisque semper nunc vitae...</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper">
                                        <div class="thumb">
                                            <img src="images/blog/comment/1.jpeg" alt="Comment images">
                                        </div>
                                        <div class="content">
                                            <p>demo says:</p>
                                            <a href="#">Quisque semper nunc vitae...</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </aside>
                        <!-- End Single Widget -->
                        <!-- Start Single Widget -->
                        <aside class="widget category_widget">
                            <h3 class="widget-title">Categories</h3>
                            <ul>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Creative</a></li>
                                <li><a href="#">Electronics</a></li>
                                <li><a href="#">Kids</a></li>
                                <li><a href="#">Flower</a></li>
                                <li><a href="#">Books</a></li>
                                <li><a href="#">Jewelle</a></li>
                            </ul>
                        </aside>
                        <!-- End Single Widget -->
                        <!-- Start Single Widget -->
                        <aside class="widget archives_widget">
                            <h3 class="widget-title">Archives</h3>
                            <ul>
                                <li><a href="#">March 2015</a></li>
                                <li><a href="#">December 2014</a></li>
                                <li><a href="#">November 2014</a></li>
                                <li><a href="#">September 2014</a></li>
                                <li><a href="#">August 2014</a></li>
                            </ul>
                        </aside>
                        <!-- End Single Widget -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Blog Area -->
@endsection
