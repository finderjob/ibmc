@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('about_us');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--4" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">My Profile</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Profile</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <div class="cart-main-area section-padding--lg bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12">
                    @if(session()->has('success_message'))
                        <div style="margin: 5px">
                            <div class="alert alert-success" style="border-left: 5px solid #012d31">
                                <ul>
                                    <li>{{ session()->get('success_message') }}</li>
                                </ul>
                            </div>
                        </div>
                    @endif
                    @if(session()->has('error_message'))
                        <div style="margin: 5px">
                            <div class="alert alert-danger" style="border-left: 5px solid #012d31">
                                <ul>
                                    <li>{{ session()->get('error_message') }}</li>
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-12">
                    <form action="{{ route('profile.post') }}" method="post" id="customer_profile_form">
                        {{ csrf_field() }}
                        <div class="customer_details">
                            <h3>Basic Info</h3>
                            <div class="customar__field">
                                <div class="margin_between">
                                    <div class="input_box space_between">
                                        <label>User name <span>*</span></label>
                                        <input type="text" readonly value="{{ $customer->name }}">
                                    </div>
                                    <div class="input_box space_between">
                                        <label>Email <span>*</span></label>
                                        <input type="text" readonly value="{{ $customer->email }}">
                                    </div>
                                </div>
                                <div class="margin_between">
                                    <div class="input_box space_between">
                                        <label>First name <span>*</span></label>
                                        <input type="text" name="first_name" value="{{ $customer->first_name }}">
                                    </div>
                                    <div class="input_box space_between">
                                        <label>Last name <span>*</span></label>
                                        <input type="text" name="last_name" value="{{ $customer->last_name }}">
                                    </div>
                                </div>
                                <div class="input_box">
                                    <label>Address <span>*</span></label>
                                    <input type="text" name="address_line_1" placeholder="Address Line 1" value="{{ ($addresses) ? $addresses->address_line_1 : "" }}">
                                </div>
                                <div class="input_box">
                                    <input type="text" name="address_line_2" placeholder="Address Line 2" value="{{ ($addresses) ? $addresses->address_line_2 : "" }}">
                                </div>
                                <div class="input_box">
                                    <label>City <span>*</span></label>
                                    <input type="text" name="city" placeholder="City" value="{{ ($addresses) ? $addresses->city : "" }}">
                                </div>
                                <div class="input_box">
                                    <label>Postcode / ZIP <span>*</span></label>
                                    <input type="text" name="zip_code" placeholder="Postcode / Zip" value="{{ ($addresses) ? $addresses->zip_code : "" }}">
                                </div>
                                <div class="margin_between">
                                    <div class="input_box space_between">
                                        <label>Telephone</label>
                                        <input type="text" name="phone_number" class="phone_number" placeholder="Telephone number" value="{{ ($addresses) ? $addresses->phone_number : "" }}">
                                    </div>
                                    <div class="input_box space_between">
                                        <label>Mobile</label>
                                        <input type="text" name="mobile_number" class="mobile_number" placeholder="Mobile number" value="{{ ($addresses) ? $addresses->mobile_number : "" }}">
                                    </div>
                                </div>
                                <div class="input_box">
                                    <label><span>*</span> Telephone or Mobile is required</label>
                                </div>
                                <div class="">
                                    <button class="btn_profile">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
