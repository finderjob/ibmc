@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('about_us');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--4" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">My Orders</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">My Orders</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-blog bg--white section-padding--lg blog-sidebar right-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Order Id</th>
                                <th>Order Details</th>
                                <th>Sub total</th>
                                <th>Coupon</th>
                                <th>Order Total</th>
                                <th>Order status</th>
                                <th>Order Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->order_id }}</td>
                                <td><?php echo get_order_product($order->id); ?></td>
                                <td style="text-align: right">{{ final_price_format($order->sub_total) }}</td>
                                <td>
                                    <span>
                                        <?php $coupon = \App\Coupon::find($order->coupon_id);
                                            if($coupon){
                                                echo $coupon->name." - ";
                                            }else{
                                                echo " - ";
                                            }
                                        ?>
                                    </span>
                                    <span style="float: right">
                                        <?php
                                            if($coupon){
                                                echo final_price_format($order->coupon_value);
                                            }
                                        ?>
                                    </span>
                                </td>
                                <td style="text-align: right">{{ final_price_format($order->order_total) }}</td>
                                <td>
                                    <?php $status  = orderStatus($order->status); ?>
                                        <label class="label {{ $status[1] }}">{{ $status[0] }}</label>
                                </td>
                                <td>{{ $order->created_at }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if($orders->total() != 0)
                        <ul class="wn__pagination">
                            @if(1 != $orders->currentPage())
                                <li><a href="{{ $orders->url($orders->currentPage() - 1) }}"><i class="zmdi zmdi-chevron-left"></i></a></li>
                            @endif
                            <li><a href="#"></a></li>
                            @for($i = 1; $i <= $orders->lastPage(); $i++)
                                <li @if($i == $orders->currentPage()) class="active" @endif><a @if($i != $orders->currentPage()) href="{{ $orders->url($i) }}"  @endif>{{ $i }}</a></li>
                            @endfor
                            @if($orders->lastPage() != $orders->currentPage())
                                <li><a href="{{ $orders->nextPageUrl() }}" ><i class="zmdi zmdi-chevron-right"></i></a></li>
                            @endif
                        </ul>
                    @else
                        <div class="no-search-jobs-list">Opps!!!. There is no any records</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
