@extends('theme.default')

@section('content')
    <?php $bg_image_path = bg_images('product');
    $path = false;
    if($bg_image_path){
        if(file_exists($bg_image_path)){
            $path = $bg_image_path;
        }
    }
    ?>
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--4" @if($path) style="background-image: url('{{ $path }}');" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">{{ $product->name }}</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="{{ route('home') }}">Home</a>
                            <span class="brd-separetor">/</span>
                            @if($slug)
                                <span class="breadcrumb_item">Category</span>
                                <span class="brd-separetor">/</span>
                                <?php $category = \App\Category::select('name')->where('slug', $slug)->first();
                                    $name = "";
                                    if($category){
                                        $name = $category->name;
                                    }
                                ?>
                                <a href="{{ route('shop', [$slug]) }}" class="breadcrumb_item">{{ $name }}</a>
                                <span class="brd-separetor">/</span>
                                @if($sub_slug)
                                    <span class="breadcrumb_item">Sub category</span>
                                    <span class="brd-separetor">/</span>
                                    <a href="{{ route('shop.sub-category', [$slug, $sub_slug]) }}" class="breadcrumb_item">{{ $product->category_name }}</a>
                                    <span class="brd-separetor">/</span>
                                @endif
                            @else
                                <span class="breadcrumb_item">Category</span>
                                <span class="brd-separetor">/</span>
                                <a href="{{ route('shop') }}" class="breadcrumb_item ">All Books</a>
                                <span class="brd-separetor">/</span>
                            @endif
                            <span class="breadcrumb_item active">product</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start main Content -->
    <div class="maincontent bg--white pt--80 pb--55">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12 md-mt-40 sm-mt-40">
                    @include('theme.partials.widget')
                </div>
                <div class="col-lg-9 col-12">
                    <div class="wn__single__product">
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <div class="wn__fotorama__wrapper">
                                    <div class="fotorama wn__fotorama__action" data-nav="thumbs">
                                        <?php $products_images = products_other_images($product->id); ?>
                                        @foreach($products_images as $products_image)
                                                <a href="#"><img src="{{ asset($products_image->image_path) }}" alt=""></a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="product__info__main">
                                    <h1>{{ $product->name }}</h1>
                                    <div class="product-reviews-summary d-flex">
                                        <ul class="rating-summary d-flex">
                                            <?php $avg_rating = get_product_rating_value($product->id);?>
                                            @for($j = 1; $j <= 5; $j++)
                                                @if($j <= $avg_rating)
                                                        <li><i class="zmdi zmdi-star"></i></li>
                                                @else
                                                        <li><i class="zmdi zmdi-star-outline"></i></li>
                                                @endif
                                            @endfor
                                        </ul>
                                    </div>
                                    <div class="price-box">
                                        <span>
                                            @if($customer_type == 2)
                                                {{ final_price_format($product->school_price) }}
                                            @else
                                                {{ final_price_format(($product->normal_price - $product->normal_discount)) }}
                                            @endif
                                        </span>
                                    </div>
                                    <div class="product__overview">
                                        <?php echo json_decode($product->overview); ?>
                                    </div>
                                    <div class="box-tocart d-flex">
                                        <span>Qty</span>
                                        <input id="qty" class="input-text qty" name="qty" min="1" value="1" title="Qty" type="number">
                                        <input type="hidden" id="product-id" value="{{ $product->id }}">
                                        <div class="addtocart__actions">
                                            <button class="tocart" id="add-cart-btn" type="button" title="Add to Cart">Add to Cart</button>
                                        </div>
                                        <div class="product-addto-links clearfix">
                                            <a class="wishlist" id="product-wishlist" href="#"></a>
                                            <a class="compare" href="#"></a>
                                        </div>
                                    </div>
                                    <div class="product_meta">
											<span class="posted_in">Categories:
                                               <?php $categories = get_categories_by_product_id($product->id);
                                                    $comma = 0;
                                               ?>
                                                @foreach($categories as $category)
                                                    <a href="{{ route('shop', [$category->slug]) }}">{{ $category->name }}</a>
                                                    @if($comma != count($categories) -1) , @endif
                                                    <?php $comma++; ?>
                                                @endforeach
											</span>
                                    </div>
                                    <div class="product-share">
                                        <?php $product_url = route('product', [ $product->slug ]);
                                            $product_url = urlencode($product_url); ?>
                                        <ul>
                                            <li class="categories-title">Share :</li>
                                            <li>
                                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ $product_url }}&amp;src=sdkpreparse"
                                                   onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                                    <i class="icon-social-facebook icons"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product__info__detailed">
                        <div class="pro_details_nav nav justify-content-start" role="tablist">
                            <a class="nav-item nav-link @if($data == null || $data['status'] == 200)active @endif" data-toggle="tab" href="#nav-details" role="tab">Details</a>
                            <a class="nav-item nav-link @if($data != null && $data['status'] == 201)active @endif" data-toggle="tab" href="#nav-review" role="tab">Reviews</a>
                        </div>
                        <div class="tab__container">
                            <!-- Start Single Tab Content -->
                            <div class="pro__tab_label tab-pane fade @if($data == null || $data['status'] == 200) show  active @endif" id="nav-details" role="tabpanel">
                                <div class="description__attribute"><?php echo json_decode($product->description);?></div>
                            </div>
                            <!-- End Single Tab Content -->
                            <!-- Start Single Tab Content -->
                            <div class="pro__tab_label tab-pane fade @if($data != null && $data['status'] == 201) show  active @endif" id="nav-review" role="tabpanel">
                                <div class="review__attribute">
                                    <h1>Customer Reviews</h1>
                                    @foreach($product_reviews as $product_review)
                                    <div class="review-list">
                                        <h2>{{ $product_review->summary }}</h2>
                                        <div class="review__ratings__type d-flex">
                                            <div class="review-ratings">
                                                <div class="rating-summary d-flex">
                                                    <span>Quality</span>
                                                    <ul class="rating d-flex">
                                                        @for($i = 1; $i <= 5; $i++)
                                                            @if($i <= $product_review->quality)
                                                                <li><i class="zmdi zmdi-star"></i></li>
                                                            @else
                                                                <li class="off"><i class="zmdi zmdi-star"></i></li>
                                                            @endif
                                                        @endfor
                                                    </ul>
                                                </div>

                                                <div class="rating-summary d-flex">
                                                    <span>Price</span>
                                                    <ul class="rating d-flex">
                                                        @for($i = 1; $i <= 5; $i++)
                                                            @if($i <= $product_review->price)
                                                                <li><i class="zmdi zmdi-star"></i></li>
                                                            @else
                                                                <li class="off"><i class="zmdi zmdi-star"></i></li>
                                                            @endif
                                                        @endfor
                                                    </ul>
                                                </div>
                                                <div class="rating-summary d-flex">
                                                    <span>value</span>
                                                    <ul class="rating d-flex">
                                                        @for($i = 1; $i <= 5; $i++)
                                                            @if($i <= $product_review->value)
                                                                <li><i class="zmdi zmdi-star"></i></li>
                                                            @else
                                                                <li class="off"><i class="zmdi zmdi-star"></i></li>
                                                            @endif
                                                        @endfor
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="review-content">
                                                <p><?php echo json_decode($product_review->review)?></p>
                                                <p>Review by {{ $product_review->name }}</p>
                                                <p>Posted on {{ $product_review->created_at }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @if(Auth::guard('customers')->user())
                                <div class="review-fieldset">
                                    <h2>You're reviewing:</h2>
                                    <h3>Chaz Kangeroo Hoodie</h3>
                                    <form action="{{ route('rating.add') }}" method="post" id="rating-form">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <div class="review-field-ratings">
                                        <div class="product-review-table">
                                            <div class="review-field-rating d-flex">
                                                <span>Quality</span>
                                                <ul class="rating d-flex quality-rating">
                                                    <li class="quality-review-star quality-1 off" data-id="1"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="quality-review-star quality-2 off" data-id="2"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="quality-review-star quality-3 off" data-id="3"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="quality-review-star quality-4 off" data-id="4"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="quality-review-star quality-5 off" data-id="5"><i class="zmdi zmdi-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="review-field-rating d-flex">
                                                <span>Price</span>
                                                <ul class="rating d-flex price-rating">
                                                    <li class="price-review-star price-1 off" data-id="1"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="price-review-star price-2 off" data-id="2"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="price-review-star price-3 off" data-id="3"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="price-review-star price-4 off" data-id="4"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="price-review-star price-5 off" data-id="5"><i class="zmdi zmdi-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="review-field-rating d-flex">
                                                <span>Value</span>
                                                <ul class="rating d-flex value-rating">
                                                    <li class="value-review-star value-1 off" data-id="1"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="value-review-star value-2 off" data-id="2"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="value-review-star value-3 off" data-id="3"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="value-review-star value-4 off" data-id="4"><i class="zmdi zmdi-star"></i></li>
                                                    <li class="value-review-star value-5 off" data-id="5"><i class="zmdi zmdi-star"></i></li>
                                                </ul>
                                            </div>
                                            <input type="hidden" name="quality_review" id="quality-review" value="0"/>
                                            <input type="hidden" name="price_review" id="price-review" value="0"/>
                                            <input type="hidden" name="value_review" id="value-review" value="0"/>
                                        </div>
                                    </div>
                                    <div class="review_form_field">
                                        <div class="input__box">
                                            <span>Nickname</span>
                                            <input id="nickname_field" type="text" name="nickname" value="{{ Auth::guard('customers')->user()->name }}" readonly>
                                            @if ($errors->has('nickname'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('nickname') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input__box">
                                            <span>Summary</span>
                                            <input class="{{ $errors->has('summery') ? ' is-invalid' : '' }}" id="summery_field" type="text" name="summery">
                                            @if ($errors->has('summery'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('summery') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input__box">
                                            <span>Review</span>
                                            <textarea class="{{ $errors->has('review') ? ' is-invalid' : '' }}" name="review"></textarea>
                                            @if ($errors->has('review'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('review') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="review-form-actions">
                                            <button >Submit Review</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                @endif
                            </div>
                            <!-- End Single Tab Content -->
                        </div>
                    </div>
                    @if(count($related_products) != 0)
                    <div class="wn__related__product pt--80 pb--50">
                        <div class="section__title text-center">
                            <h2 class="title__be--2">Related Products</h2>
                        </div>
                        <div class="row mt--60">
                            <div class="productcategory__slide--2 arrows_style owl-carousel owl-theme">
                                @foreach($related_products as $related_product)
                                    <?php $cover_images = products_cover_images($related_product->id);
                                    if($slug){
                                        $url = route('product.with.category',[ $slug, $related_product->slug]);
                                    }else{
                                        $url = route('product',[$related_product->slug]);
                                    }
                                    ?>
                                    <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{ $url }}"><img src="{{ asset($cover_images->image_path) }}" alt="{{ $related_product->name }}"></a>
                                            <a class="second__img animation1" href="{{ $url }}"><img src="{{ asset($cover_images->image_path) }}" alt="{{ $related_product->name }}"></a>
                                            <?php $checkBestSeller = checkBestSellerProduct($related_product->id); ?>
                                            @if($checkBestSeller)
                                            <div class="hot__box">
                                                <span class="hot-label">BEST SALLER</span>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="product__content content--center">
                                            <h4><a href="{{ $url }}">{{ $related_product->name }}</a></h4>
                                            <ul class="prize d-flex">
                                                @if($customer_type == 2)
                                                    <li>{{ final_price_format($related_product->school_price) }}</li>
                                                    <li class="old_prize">{{ final_price_format($related_product->normal_price) }}</li>
                                                @else
                                                    <li>{{ final_price_format($related_product->normal_price - $related_product->normal_discount) }}</li>
                                                    @if(($related_product->normal_price - $related_product->normal_discount) != $related_product->normal_price)
                                                        <li class="old_prize">{{ final_price_format($related_product->normal_price) }}</li>
                                                    @endif
                                                @endif

                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        <li><a class="cart" data-id="{{ $related_product->id }}" href="#"><i class="bi bi-shopping-bag4"></i></a></li>
                                                        <li><a class="wishlists" href="#" data-id="{{ $related_product->id }}"><i class="bi bi-shopping-cart-full"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product__hover--content">
                                                <ul class="rating d-flex">
                                                    <?php $avg_rating = get_product_rating_value($related_product->id);?>
                                                    @for($j = 1; $j <= 5; $j++)
                                                        @if($j <= $avg_rating)
                                                            <li class="on"><i class="fa fa-star"></i></li>
                                                        @else
                                                            <li><i class="fa fa-star-o"></i></li>
                                                        @endif
                                                    @endfor
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(count($upsell_products) != 0)
                    <div class="wn__related__product">
                        <div class="section__title text-center">
                            <h2 class="title__be--2">upsell products</h2>
                        </div>
                        <div class="row mt--60">
                            <div class="productcategory__slide--2 arrows_style owl-carousel owl-theme">
                                @foreach($upsell_products as $upsell_product)
                                    <?php $cover_images = products_cover_images($upsell_product->id);
                                    if($slug){
                                        $url = route('product.with.category',[ $slug, $upsell_product->slug]);
                                    }else{
                                        $url = route('product',[$upsell_product->slug]);
                                    }
                                    ?>
                                    <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{ $url }}"><img src="{{ asset($cover_images->image_path) }}" alt="{{ $upsell_product->name }}"></a>
                                            <a class="second__img animation1" href="{{ $url }}"><img src="{{ asset($cover_images->image_path) }}" alt="{{ $upsell_product->name }}"></a>
                                            <?php $checkBestSeller = checkBestSellerProduct($upsell_product->id); ?>
                                            @if($checkBestSeller)
                                            <div class="hot__box">
                                                <span class="hot-label">BEST SALLER</span>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="product__content content--center">
                                            <h4><a href="{{ $url }}">{{ $upsell_product->name }}</a></h4>
                                            <ul class="prize d-flex">
                                                @if($customer_type == 2)
                                                    <li>{{ final_price_format($upsell_product->school_price) }}</li>
                                                    <li class="old_prize">{{ final_price_format($upsell_product->normal_price) }}</li>
                                                @else
                                                    <li>{{ final_price_format($upsell_product->normal_price - $upsell_product->normal_discount) }}</li>
                                                    @if(($upsell_product->normal_price - $upsell_product->normal_discount) != $upsell_product->normal_price)
                                                        <li class="old_prize">{{ final_price_format($upsell_product->normal_price) }}</li>
                                                    @endif
                                                @endif

                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        <li><a class="cart" data-id="{{ $upsell_product->id }}" href="#"><i class="bi bi-shopping-bag4"></i></a></li>
                                                        <li><a class="wishlists" href="#" data-id="{{ $upsell_product->id }}"><i class="bi bi-shopping-cart-full"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product__hover--content">
                                                <ul class="rating d-flex">
                                                    <?php $avg_rating = get_product_rating_value($upsell_product->id);?>
                                                    @for($j = 1; $j <= 5; $j++)
                                                        @if($j <= $avg_rating)
                                                            <li class="on"><i class="fa fa-star"></i></li>
                                                        @else
                                                            <li><i class="fa fa-star-o"></i></li>
                                                        @endif
                                                    @endfor
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- End main Content -->
    <script>
        $(document).ready(function(){

            @if($data != null && $data['status'] == 201)
                toastr.error( '{{ $data['message'] }}', 'Failed!');
            @endif
            @if($data != null && $data['status'] == 200)
                toastr.success( '{{ $data['message'] }}', 'Success!');
            @endif
            $('.cart').on('click', function (event) {
                event.preventDefault();
                var product_id = $(this).data('id');
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('cart.add') }}",
                    type: 'post',
                    data: {_token: _token, product_id:product_id},
                    success: function(data){
                        var return_data = jQuery.parseJSON(data);
                        if(return_data.status == 200)
                        {
                            minshoppingcart.init();
                            toastr.success( return_data.message, 'Success!');
                        }
                        else{
                            toastr.error( return_data.message, 'Failed!');
                        }

                    }
                });
            });

            $('.wishlists').on('click', function(){
                event.preventDefault();
                var product_id = $(this).data('id');
                minshoppingcart.add(product_id);
            });

            $('#add-cart-btn').on('click', function (event) {
                event.preventDefault();
                let product_id = $('#product-id').val();
                let qty = $('#qty').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('cart.update') }}",
                    type: 'post',
                    data: {_token: _token, product_id:product_id, qty:qty},
                    success: function(data){
                        var return_data = jQuery.parseJSON(data);
                        if(return_data.status == 200)
                        {
                            minshoppingcart.init();
                            toastr.success( return_data.message, 'Success!');
                        }
                        else{
                            toastr.error( return_data.message, 'Failed!');
                        }

                    }
                });
            });
            $('#product-wishlist').on('click', function(){
                event.preventDefault();
                let product_id = $('#product-id').val();
                minshoppingcart.add(product_id);
            });

            $('.quality-review-star').on('click', function(){
                $this = $(this);
                ratingsAdd($this,'quality-');
                ratingCount('quality-');
            });
            $('.price-review-star').on('click', function(){
                $this = $(this);
                ratingsAdd($this,'price-');
                ratingCount('price-');
            });
            $('.value-review-star').on('click', function(){
                $this = $(this);
                ratingsAdd($this,'value-');
                ratingCount('value-');
            });
        });

        function ratingsAdd($this, class_name){
            let id = $this.data('id');
            var check = 1;
            $('.' + class_name + 'rating li').each(function(){
                if(check <= id){
                    $(this).removeClass('off');
                }else{
                    $(this).addClass('off');
                }
                check++;
            });
        }

        function ratingCount(class_name){
            var count = 0;
            $('.' + class_name + 'rating li').each(function(){
                if(!$(this).hasClass('off')){
                    count++;
                }
            });
            $('#' + class_name + 'review').val(count);
        }
    </script>
@endsection

