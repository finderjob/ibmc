@extends('layouts.default')

@section('content')
    <h1 class="page-title">Sub Category</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Sub Category List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.sub-categories.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>Sub Category Id</th>
                                        <th> Name </th>
                                        <th> Slug</th>
                                        <th> Main Category</th>
                                        <th> Status </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->id }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->slug }}</td>
                                        <td>
                                            <?php $main_categories = \App\Category::where('id', $category->parent_id)->where('status',1)->get(); ?>
                                            @foreach($main_categories as $main_category)
                                                <span class="label label-sm label-info margin"> {{ $main_category->name }} </span>
                                            @endforeach
                                        </td>
                                        <td>
                                            <select id="category_status_{{ $category->id }}" data-id="{{ $category->id }}" data-prev="{{ $category->status }}" class="category_status form-control">
                                                <option value="1" @if($category->status == 1) selected @endif>Activate</option>
                                                <option value="0" @if($category->status == 0) selected @endif>Deactivate</option>
                                            </select>
                                        </td>
                                        <td>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="{{ route('admin.sub-category.edit.get',[$category->id]) }}" title="edit">
                                                <i class="icon-wrench"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.category_status').on('change',function(){
                var category_status = $(this).val();
                console.log(category_status);
                var category_id = $(this).data('id');
                console.log(category_id);
                var prev_status = $(this).data('prev');
                console.log(prev_status);
                changeCategoryStatus(category_id, category_status,prev_status);
            });
        });
        function changeCategoryStatus(category_id,category_status,prev_status){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('admin.category.change.categoryStatus') }}',
                type: "post",
                data: {_token: _token,category_id:category_id, category_status: category_status, sub_category: true},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#category_status_'+ category_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');

                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
