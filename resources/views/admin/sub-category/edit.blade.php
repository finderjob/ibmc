@extends('layouts.default')

@section('content')
    <h1 class="page-title">Sub Category</h1>
    <div class="row">
        <div class="col-md-6 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Sub Category Edit ID :: {{ $sub_category->id }}</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('admin.sub-category.edit.post',[$sub_category->id]) }}" method="post" role="form" class="sub-category-create-form">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Category</label>
                                     <select name="category" class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }}">
                                        <option value="" selected="true" disabled="true">Select Category</option>
                                         @foreach($categories as $category)
                                             <option value="{{ $category->id }}" @if($category->id == $sub_category->parent_id) selected="selected" @endif>{{ $category->name }}</option>
                                         @endforeach
                                    </select>
                                </span>
                                @if ($errors->has('category'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('category') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Name" value="{{ $sub_category->name }}">

                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Slug</label>
                                    <input type="text" name="slug" id="slug" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }} input-icon" placeholder="Slug" value="{{ $sub_category->slug }}">
                                </span>
                                @if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" id="hidden_slug" value="{{ $sub_category->slug }}">
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="{{ route('admin.sub-categories') }}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#slug').focusout(function ($e) {
                var keyword = $(this).val();
                var hidden_slug = $('#hidden_slug').val();
                if(keyword != hidden_slug)
                {
                    slugCreate(keyword);
                }
            });
        });
        function slugCreate(keyword){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('generate-slug') }}',
                type: "post",
                data: {_token: _token, keyword:keyword},
                success: function (data) {
                    console.log(data);
                    $('#slug').val(data);
                    $('.swing-preloader').css('display','none');
                    $('#slug').closest('.form-group').removeClass('has-error');
                    $('#slug-error').remove();
                }
            });
        }
    </script>
@endsection
