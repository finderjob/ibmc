@extends('layouts.default')

@section('content')
    <h1 class="page-title">Users Edit</h1>
    <div class="row">
        <div class="col-md-6 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">User Edit Form</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('admin.users.store') }}" method="post" role="form" class="user-create-form">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Username</label>
                                    <div class="input-group">
                                        <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Username">
                                        <span class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>First name</label>
                                    <div class="input-group">
                                        <input type="text" name="first_name" id="first_name" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }} input-icon" placeholder="First Name">
                                        <span class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </span>
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Last name</label>
                                    <div class="input-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }} input-icon" placeholder="Last Name">
                                        <span class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </span>
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Email Address</label>
                                    <div class="input-group">
                                        <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email Address">
                                        <span class="input-group-addon">
                                              <i class="fa fa-envelope"></i>
                                        </span>
                                    </div>
                                </span>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label for="exampleInputPassword1">Password</label>
                                    <div class="input-group">
                                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password">
                                        <span class="input-group-addon">
                                             <i class="fa fa-lock"></i>
                                        </span>
                                    </div>
                                </span>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>User type</label>
                                    <select name="user_type" class="form-control {{ $errors->has('user_type') ? ' is-invalid' : '' }}">
                                        <option value="" selected="true" disabled="true">Select user type</option>
                                        <option value="1">Super Admin</option>
                                        <option value="2">Admin</option>
                                        <option value="3">User</option>
                                    </select>
                                </span>
                                @if ($errors->has('user_type'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('user_type') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
