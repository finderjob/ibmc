@extends('layouts.default')

@section('content')
    <h1 class="page-title">Coupons</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Coupons List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.coupons.add.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>Coupon Id</th>
                                        <th> Name / Code </th>
                                        <th> Type </th>
                                        <td> Value </td>
                                        <th> Status </th>
                                        <th> Expire At </th>
                                        <th> Created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($coupons as $coupon)
                                        <?php
                                            $prefix = "";
                                            $postfix = "";
                                            $type = "";
                                            if($coupon->type == 1){
                                                $prefix = config('status.currency_format')." ";
                                                $type = "Apply fixed amount";
                                            }else if($coupon->type == 2){
                                                $postfix = "%";
                                                $type = "Apply percentage";
                                            }else if($coupon->type == 3){
                                                $prefix = "Rs. ";
                                                $type = "Apply fixed amount if subtotal more than " .$coupon->margin_value;
                                            }else{
                                                $postfix = "%";
                                                $type = "Apply percentage amount if subtotal more than " .$coupon->margin_value;
                                            }
                                        ?>
                                        <tr>
                                            <td>{{ $coupon->id }}</td>
                                            <td>{{ $coupon->name }}</td>
                                            <td>{{ $type }}</td>
                                            <td>{{ $prefix.number_format($coupon->value,2,'.',',').$postfix }}</td>
                                            <td>
                                                <select id="coupon_status_{{ $coupon->id }}" data-id="{{ $coupon->id }}" data-prev="{{ $coupon->status }}" class="coupon_status form-control">
                                                    <option value="1" @if($coupon->status == 1) selected @endif>Activate</option>
                                                    <option value="0" @if($coupon->status == 0) selected @endif>Deactivate</option>
                                                </select>
                                            </td>
                                            <td>{{ $coupon->expire_at }}</td>
                                            <td>{{ $coupon->created_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.coupon_status').on('change',function(){
                var coupon_status = $(this).val();
                var coupon_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeCouponStatus(coupon_id, coupon_status,prev_status);
            });
        });
        function changeCouponStatus(coupon_id, coupon_status, prev_status){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');

            $.ajax({
                url: '{{ route('admin.coupons.change.status') }}',
                type: "post",
                data: {_token: _token, coupon_status: coupon_status, coupon_id: coupon_id},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#coupon_status_'+ user_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');
                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
