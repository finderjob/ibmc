@extends('layouts.default')

@section('content')
    <h1 class="page-title">Coupon</h1>
    <div class="row">
        <div class="col-md-6 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Coupon Add</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('admin.coupons.add.post') }}" method="post" role="form" class="coupon-create-form">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Coupon name</label>
                                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Coupon Name">
                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Coupon type</label>
                                    <select name="coupon_type" id="coupon_type" class="form-control {{ $errors->has('coupon_type') ? ' is-invalid' : '' }}">
                                        <option value="1">Apply fixed amount</option>
                                        <option value="2">Apply percentage</option>
                                        <option value="3">Apply fixed amount if subtotal more than</option>
                                        <option value="4">Apply percentage amount if subtotal more than</option>
                                    </select>
                                </span>
                                @if ($errors->has('coupon_type'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('coupon_type') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group margin-value-section">
                                <span class="form-error">
                                    <label>Margin value</label>
                                     <input type="number" name="margin_value" min="1" step="0.01" id="margin_value" class="form-control {{ $errors->has('margin_value') ? ' is-invalid' : '' }} input-icon" placeholder="Margin Value">
                                </span>
                                @if ($errors->has('margin_value'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('margin_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Coupon value</label>
                                     <input type="number" name="coupon_value" min="0" step="0.01" id="coupon_value" class="form-control {{ $errors->has('coupon_value') ? ' is-invalid' : '' }} input-icon" placeholder="Coupon Value">
                                </span>
                                @if ($errors->has('coupon_value'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('coupon_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Expire Date </label>
                                    <input name="expire_date" class="form-control expire_date" size="16" type="text" value="" autocomplete="off">
                                </span>
                                @if ($errors->has('expire_date'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('expire_date') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="{{ route('admin.coupons') }}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            var coupon_type = $('#coupon_type').val();
            marginValue(coupon_type);
            $('#coupon_type').on('change', function(){
                coupon_type = $(this).val();
                marginValue(coupon_type);
            });
        });

        function marginValue(coupon_type){
            if(coupon_type >= 3){
                $('.margin-value-section').css('display', 'block');
            }else{
                $('.margin-value-section').css('display', 'none');
            }
        }
    </script>
@endsection
