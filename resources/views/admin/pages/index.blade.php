@extends('layouts.default')

@section('content')
    <h1 class="page-title">Home Page</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">
                    <form action="{{ route('admin.page.slider.upload') }}" method="post" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="home_slider" id="home-slider"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>

                                        @if ($errors->has('home_slider'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('home_slider') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10">
                                <span class="error-log hide">
                                    <span class="label label-danger">Error! </span>
                                    <span class="error-data"></span>
                                </span>
                                    <br>
                                    <span class="label label-warning">NOTE! </span>
                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="">
                                    <label>Text 1</label>
                                    <input type="text" name="text_1" id="text-1" class="form-control input-icon" placeholder="Text 1">
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="">
                                    <label>Color Text 1</label>
                                    <input type="text" name="color_text_1" id="color-text-1" class="form-control input-icon" placeholder="Color text 1">
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="">
                                    <label>Text 2</label>
                                    <input type="text" name="text_2" id="text-2" class="form-control input-icon" placeholder="Text 2">
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="">
                                    <label>Color Text 2</label>
                                    <input type="text" name="color_text_2" id="color-text-2" class="form-control input-icon" placeholder="Color text 2">
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="">
                                    <label>Text 3</label>
                                    <input type="text" name="text_3" id="text-3" class="form-control input-icon" placeholder="Text 3">
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="">
                                    <label>Color Text 3</label>
                                    <input type="text" name="color_text_3" id="color-text-3" class="form-control input-icon" placeholder="Color text 3">
                                </span>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green" id="btn-home-slider" disabled="true"> Submit </button>
                            <a href="javascript:;" data-section="change_avatar" class="cancel_btn btn default"> Cancel </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Image</th>
                                        <td>Text</td>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $get_home_slider_data = json_decode($pages->value, true);
                                        $home_sliders = array();
                                        $count = 0;
                                        if(!empty($get_home_slider_data) && array_key_exists('home_slider', $get_home_slider_data)){
                                            $home_sliders = $get_home_slider_data['home_slider'];
                                            if(array_key_exists('slider', $home_sliders)){
                                                $count = count($home_sliders['slider']['image']);
                                            }
                                        }
                                    ?>
                                    @for($i = 0; $i < $count; $i++)
                                        <tr>
                                            <td>{{ $i+1 }}</td>
                                            <td><img src="{{ asset($home_sliders['slider']['image'][$i]) }}" style="max-width: 400px;"></td>
                                            <td>
                                                <span> Row 01 : {{ $home_sliders['slider']['text'][$i][0][0] }} {{ $home_sliders['slider']['text'][$i][0][1] }}</span> <hr>
                                                <span> Row 02 : {{ $home_sliders['slider']['text'][$i][1][0] }} {{ $home_sliders['slider']['text'][$i][1][1] }}</span> <hr>
                                                <span> Row 03 : {{ $home_sliders['slider']['text'][$i][2][0] }} {{ $home_sliders['slider']['text'][$i][2][1] }}</span>
                                            </td>
                                            <td>
                                                <a class="btn btn-circle btn-icon-only btn-danger" title="Delete Image" href="{{ route('admin.page.slider.remove',[$i]) }}" onclick="event.preventDefault();
                                                    document.getElementById('home-slider-image-{{ $i }}').submit();">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                                <form id="home-slider-image-{{ $i }}" action="{{ route('admin.page.slider.remove',[$i]) }}" method="post" style="display: none;">
                                                    @csrf
                                                </form>
                                            </td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {
        var _URL = window.URL || window.webkitURL;
        var width = 1920;
        var height = 950;
        $("#home-slider").change(function (e) {
            var file, img;
            if ((file = this.files[0])) {
                img = new Image();
                img.onload = function () {
                    console.log(this.width, this.height);
                    if(this.width >= width && this.height >= height){
                        $('.error-log').removeClass('show');
                        $('.error-log').addClass('hide');
                        $('.error-data').html('');
                        $('#btn-home-slider').removeAttr('disabled');
                    }else{
                        $('.error-log').addClass('show');
                        $('.error-log').removeClass('hide');
                        $('.error-data').html('Image resolution should be ' + width +"X" +height);
                    }
                };
                img.src = _URL.createObjectURL(file);
            }
        });
    });
</script>
@endsection
