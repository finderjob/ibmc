@extends('layouts.default')

@section('content')
    <div class="row">
         <div class="col-md-12">
             <div class="error-code">
                {{ $error_code }}
             </div>
         </div>
        <div class="col-md-12">
            <div class="error-code-message">
                {{ $message }}
            </div>
        </div>
    </div>
@endsection
