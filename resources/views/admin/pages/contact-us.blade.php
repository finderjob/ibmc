@extends('layouts.default')

@section('content')
    <h1 class="page-title">Contact-us Page</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form action="{{ route('admin.pages.contact-us.post') }}" method="post" role="form">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label>Office info description</label>
                                <span class="form-error">
                                    <textarea name="office_info_content" id="office_info_content" class="form-control"><?php echo $data['content']['get_in_touch_content']; ?></textarea>
                                </span>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group">
                                <label>Get in touch description</label>
                                <span class="form-error">
                                     <textarea name="get_in_touch_content" id="get_in_touch_content" class="form-control"><?php echo $data['content']['office_info_content']; ?></textarea>
                                </span>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group">
                                <label>Address</label>
                                <span class="form-error">
                                     <input type="text" name="office_address" id="office_address" class="form-control" value="<?php echo $data['content']['office_address']; ?>">
                                </span>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group">
                                <label>Phone number</label>
                                <span class="form-error">
                                     <input type="text" name="office_phone" id="office_phone" class="form-control" value="<?php echo $data['content']['office_phone']; ?>">
                                </span>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group">
                                <label>Email address</label>
                                <span class="form-error">
                                    <input type="email" name="office_email" id="office_email" class="form-control" value="<?php echo $data['content']['office_email']; ?>">
                                </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="#" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
