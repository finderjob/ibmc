@extends('layouts.default')
@section('content')
    <h1 class="page-title">Settings</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body">
                    <form action="{{ route('admin.settings.post') }}" method="post" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="col-md-12">
                                <h4>Favicon</h4>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="favicon_image" id="home-slider"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>

                                            @if ($errors->has('home_slider'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('home_slider') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                        <span>Image should be PNG, ICO, GIF</span>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                    <span class="error-log hide">
                                        <span class="label label-danger">Error! </span>
                                        <span class="error-data"></span>
                                    </span>
                                        <br>
                                        <span class="label label-warning">NOTE! </span>
                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                @if($data['favicon'])
                                    <div class="col-md-4">
                                        @if(file_exists($data['favicon']))
                                            <img src="{{ asset($data['favicon']) }}" class="img-responsive" alt="">
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn green" id="btn-home-slider"> Submit </button>
                                <a href="javascript:;" data-section="change_avatar" class="cancel_btn btn default"> Cancel </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
