@extends('layouts.default')
@section('content')
    <h1 class="page-title">Team members</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">
                    <form action="{{ route('admin.pages.authors.team.members.post') }}" method="post" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput" id="image-div">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                         </div>
                                    <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="team_member_image" id="home-slider"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>

                                        @if ($errors->has('team_member_image'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('team_member_image') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span>Image minimum width: 300px <br> Image minimum height: 373px</span>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span class="error-log hide">
                                        <span class="label label-danger">Error! </span>
                                        <span class="error-data"></span>
                                    </span>
                                    <br>
                                    <span class="label label-warning">NOTE! </span>
                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member Name</label>
                                    <input type="text" name="team_member_name" class="form-control">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member Position</label>
                                    <input type="text" name="team_member_position" class="form-control">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member twitter url</label>
                                    <input type="text" name="team_member_twitter" class="form-control" placeholder="https://www.twitter.com/">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member tumblr url</label>
                                    <input type="text" name="team_member_tumblr" class="form-control" placeholder="https://www.tumblr.com/">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member facebook url</label>
                                    <input type="text" name="team_member_facebook" class="form-control" placeholder="https://www.facebook.com">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member linkedin url</label>
                                    <input type="text" name="team_member_linkedin" class="form-control" placeholder="https://www.linkedin.com/">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member pinterest url</label>
                                    <input type="text" name="team_member_pinterest" class="form-control" placeholder="https://www.pinterest.com/">
                                </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn green" id="btn-home-slider" disabled="true"> Submit </button>
                            <a href="javascript:;" data-section="change_avatar" class="cancel_btn btn default"> Cancel </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th style="max-width: 200px;">Image</th>
                                    <td>Team member name</td>
                                    <td>Team member position</td>
                                    <td>Team member social media</td>
                                    <th> Actions </th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($data))
                                        @for($i = 0; $i < count($data['team_member']); $i++)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>
                                                <?php $image = 'theme/images/default.png';
                                                    if(file_exists($data['team_member'][$i]['image'])){
                                                        $image = $data['team_member'][$i]['image'];
                                                    }
                                                ?>
                                                <img src="{{ asset($image) }}" style="max-width: 200px;"></td>
                                            <td>
                                                {{ $data['team_member'][$i]['name'] }}
                                            </td>
                                            <td>
                                                {{ $data['team_member'][$i]['position'] }}
                                            </td>
                                            <td>
                                                <li>Twitter : {{ $data['team_member'][$i]['twitter'] }}</li>
                                                <li>Tumblr : {{ $data['team_member'][$i]['tumblr'] }}</li>
                                                <li>Facebook : {{ $data['team_member'][$i]['facebook'] }}</li>
                                                <li>Linkedin : {{ $data['team_member'][$i]['linkedin'] }}</li>
                                                <li>Pinterest : {{ $data['team_member'][$i]['pinterest'] }}</li>
                                            </td>
                                            <td>
                                                <a class="btn btn-circle btn-icon-only btn-danger" title="Delete Team member" href="{{ route('admin.pages.team.members.remove',[$i]) }}" onclick="event.preventDefault();
                                                    document.getElementById('team-member-{{ $i }}').submit();">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                                <form id="team-member-{{ $i }}" action="{{ route('admin.pages.team.members.remove',[$i]) }}" method="post" style="display: none;">
                                                    @csrf
                                                </form>
                                                <a class="btn btn-circle btn-icon-only btn-primary" title="Edit Team member" data-id="{{ $i }}" href="" onclick="return editTeamMember(this);">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endfor
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="team-member-edit-modal" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('admin.pages.team.members.post.edit') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="team_member_index" id="team_member_id">
                    <input type="hidden" name="image_previous_url" id="home-slider-edit-file">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Team member Edit</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                        <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="team_member_image" id="home-slider-edit"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" id="remove-image" data-dismiss="fileinput"> Remove </a>

                                        @if ($errors->has('team_member_image'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('team_member_image') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span>Image minimum width: 300px <br> Image minimum height: 373px</span>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span class="error-log hide">
                                        <span class="label label-danger">Error! </span>
                                        <span class="error-data"></span>
                                    </span>
                                    <br>
                                    <span class="label label-warning">NOTE! </span>
                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member Name</label>
                                    <input type="text" name="team_member_name" id="team_member_name" class="form-control">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member Position</label>
                                    <input type="text" name="team_member_position" id="team_member_position" class="form-control">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member twitter url</label>
                                    <input type="text" name="team_member_twitter" id="team_member_twitter" class="form-control" placeholder="https://www.twitter.com/">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member tumblr url</label>
                                    <input type="text" name="team_member_tumblr" id="team_member_tumblr" class="form-control" placeholder="https://www.tumblr.com/">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member facebook url</label>
                                    <input type="text" name="team_member_facebook" id="team_member_facebook" class="form-control" placeholder="https://www.facebook.com">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member linkedin url</label>
                                    <input type="text" name="team_member_linkedin" id="team_member_linkedin" class="form-control" placeholder="https://www.linkedin.com/">
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Team member pinterest url</label>
                                    <input type="text" name="team_member_pinterest" id="team_member_pinterest" class="form-control" placeholder="https://www.pinterest.com/">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn blue" id="modal-save-btn">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var _URL = window.URL || window.webkitURL;
            var width = 300;
            var height = 373;
            $("#home-slider").change(function (e) {
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function () {
                        console.log(this.width, this.height);
                        if(this.width >= width && this.height >= height){
                            $('.error-log').removeClass('show');
                            $('.error-log').addClass('hide');
                            $('.error-data').html('');
                            $('#btn-home-slider').removeAttr('disabled');
                        }else{
                            $('.error-log').addClass('show');
                            $('.error-log').removeClass('hide');
                            $('.error-data').html('Image resolution should be ' + width +"X" +height);
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }
            });

            $("#home-slider-edit").change(function (e) {
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function () {
                        if(this.width >= width && this.height >= height){
                            $('#team-member-edit-modal .error-log').removeClass('show');
                            $('#team-member-edit-modal .error-log').addClass('hide');
                            $('#team-member-edit-modal .error-data').html('');
                            $('#team-member-edit-modal #modal-save-btn').removeAttr('disabled');
                            $('#team-member-edit-modal #home-slider-edit-file').val('');
                        }else{
                            $('#team-member-edit-modal .error-log').addClass('show');
                            $('#team-member-edit-modal .error-log').removeClass('hide');
                            $('#team-member-edit-modal .error-data').html('Image resolution should be ' + width +"X" +height);
                            $('#team-member-edit-modal #modal-save-btn').attr('disabled', 'disabled');
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }else{
                    $('#team-member-edit-modal #modal-save-btn').removeAttr('disabled');
                }
            });

            $('#remove-image').on('click', function(){
                $('#team-member-edit-modal #home-slider-edit-file').val('');
            });
        });

        function editTeamMember(elem){
            var index = $(elem).data('id');
            $.ajax({
                url: '{{ route('admin.pages.team.members.get') }}',
                data: { index: index},
                success: function (data) {
                    var data = jQuery.parseJSON(data);
                    if(data['team_member']['image']){
                        $('#team-member-edit-modal .fileinput').removeClass('fileinput-new');
                        $('#team-member-edit-modal .fileinput').addClass('fileinput-exists');
                        var image_preview = '<img src="' + data['team_member']['image'] + '" style="max-height: 140px;">';
                        $('#team-member-edit-modal .fileinput-preview > img').remove();
                        $('#team-member-edit-modal .fileinput-preview').html(image_preview);
                    }
                    $('#team-member-edit-modal #home-slider-edit-file').val(data['team_member']['image']);
                    $('#team-member-edit-modal #team_member_name').val(data['team_member']['name']);
                    $('#team-member-edit-modal #team_member_position').val(data['team_member']['position']);
                    $('#team-member-edit-modal #team_member_twitter').val(data['team_member']['twitter']);
                    $('#team-member-edit-modal #team_member_tumblr').val(data['team_member']['tumblr']);
                    $('#team-member-edit-modal #team_member_facebook').val(data['team_member']['facebook']);
                    $('#team-member-edit-modal #team_member_linkedin').val(data['team_member']['linkedin']);
                    $('#team-member-edit-modal #team_member_pinterest').val(data['team_member']['pinterest']);
                    $('#team-member-edit-modal #team_member_id').val(index);
                    $('#team-member-edit-modal').modal('show');
                }
            });

            return false;
        }
    </script>
@endsection
