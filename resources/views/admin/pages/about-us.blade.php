@extends('layouts.default')

@section('content')
    <h1 class="page-title">About-us Page</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form action="{{ route('admin.pages.about-us.post') }}" method="post" role="form">
                     {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label>Page Content Title</label>
                                <span class="form-error">
                                     <input type="text" class="form-control" name="about_us_title" value="{{ $data['content_title'] }}">
                                </span>
                            </div>
                            <div class="form-group">
                                <label>Summary</label>
                                <span class="form-error">
                                     <textarea name="about_us_page_content" id="about_us_page_content" class="form-control"><?php echo $data['summary']; ?></textarea>
                                </span>
                            </div>
                            @if(false)
                                <div class="form-group">
                                    <label>Our Vision</label>
                                    <span class="form-error">
                                         <textarea name="our_vision_content" id="our_vision_content" class="form-control"><?php echo $data['our_vision']; ?></textarea>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>Our Mission</label>
                                    <span class="form-error">
                                         <textarea name="our_mission_content" id="our_mission_content" class="form-control"><?php echo $data['our_mission']; ?></textarea>
                                    </span>
                                </div>
                            @endif
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="#" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
