@extends('layouts.default')

@section('content')
    <h1 class="page-title">Social Page</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form action="{{ route('admin.pages.social.media.post') }}" method="post" role="form">
                     {{ csrf_field() }}
                        <div class="form-body">
                            <?php $social_medias = config('status.social_media');?>
                            @foreach ($social_medias as $social_media)
                                <div class="form-group">
                                    <label>{{ $social_media['name'] }} url</label>
                                    <span class="form-error">
                                        <input type="text" class="form-control" name="{{ $social_media['value'] }}_url" value="{{ $data[$social_media['value']] }}" placeholder="https://www.{{ $social_media['value' ]}}.com/">
                                    </span>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="#" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
