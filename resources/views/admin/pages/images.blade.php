@extends('layouts.default')
@section('content')
    <h1 class="page-title">Images</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">
                    <form action="{{ route('admin.pages.image.post') }}" method="post" role="form" enctype="multipart/form-data" id="imagesForm">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="">
                                    <label>Select Page</label>
                                    <?php $pages = config('status.background_image.pages');?>
                                    <select class="form-control" name="page_name" id="page_name">
                                        @foreach($pages as $page)
                                            <option value="{{ $page['value'] }}">{{ $page['name'] }}</option>
                                        @endforeach
                                    </select>
                                    <span id="image-resolution"></span>
                                </span>
                            </div>
                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="background_image" id="home-slider"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>

                                        @if ($errors->has('home_slider'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('home_slider') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10">
                                <span class="error-log hide">
                                    <span class="label label-danger">Error! </span>
                                    <span class="error-data"></span>
                                </span>
                                    <br>
                                    <span class="label label-warning">NOTE! </span>
                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn green" id="btn-home-slider" disabled="true"> Submit </button>
                            <a href="javascript:;" data-section="change_avatar" class="cancel_btn btn default"> Cancel </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th style="max-width: 200px;">Image</th>
                                    <td>Page Name</td>
                                    <th> Actions </th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($data))
                                        @for($i = 0; $i < count($data['background_image']); $i++)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td><img src="{{ asset($data['background_image'][$i]['image']) }}" style="max-width: 200px;"></td>
                                            <td>
                                                <?php $pages = config('status.background_image.pages');
                                                        $page_name = "";
                                                      if(array_key_exists($data['background_image'][$i]['name'], $pages)){
                                                          $page_name = $pages[$data['background_image'][$i]['name']]['name'];
                                                      }
                                                ?>
                                                {{ $page_name }}
                                            </td>
                                            <td>
                                                <a class="btn btn-circle btn-icon-only btn-danger" title="Delete Image" href="{{ route('admin.pages.image.remove',[$i]) }}" onclick="event.preventDefault();
                                                    document.getElementById('bg-image-{{ $i }}').submit();">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                                <form id="bg-image-{{ $i }}" action="{{ route('admin.pages.image.remove',[$i]) }}" method="post" style="display: none;">
                                                    @csrf
                                                </form>
                                            </td>
                                        </tr>
                                        @endfor
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var _URL = window.URL || window.webkitURL;
            var width = 1920;
            var height = 950;
            $('#image-resolution').html('Image resolution should be ' + width +"X" +height);
            $('#page_name').on('change', function(){
                if($(this).val() === 'sponsor'){
                    width = 270;
                    height = 351;
                }else if($(this).val() === 'subscribe'){
                    width = 1920;
                    height = 660;
                }
                $('#image-resolution').html('Image resolution should be ' + width +"X" +height);
            });
            $("#home-slider").change(function (e) {
                var file, img;
                let page_name = $('#page_name').val();
                if(page_name === 'sponsor'){
                    width = 270;
                    height = 351;
                }else if(page_name === 'subscribe'){
                    width = 1920;
                    height = 660;
                }
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function () {
                        if(this.width >= width && this.height >= height){
                            $('.error-log').removeClass('show');
                            $('.error-log').addClass('hide');
                            $('.error-data').html('');
                            $('#btn-home-slider').removeAttr('disabled');
                        }else{
                            $('.error-log').addClass('show');
                            $('.error-log').removeClass('hide');
                            $('.error-data').html('Image resolution should be ' + width +"X" +height);
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }
            });
        });
    </script>
@endsection
