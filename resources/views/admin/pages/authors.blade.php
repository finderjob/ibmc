@extends('layouts.default')
@section('content')
    <h1 class="page-title">Authors</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">
                    <form action="{{ route('admin.pages.authors.post') }}" method="post" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="author_image" id="home-slider"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>

                                        @if ($errors->has('home_slider'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('home_slider') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span>Image minimum width: 300px <br> Image minimum height: 373px</span>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span class="error-log hide">
                                        <span class="label label-danger">Error! </span>
                                        <span class="error-data"></span>
                                    </span>
                                    <br>
                                    <span class="label label-warning">NOTE! </span>
                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="">
                                    <label>Author Name</label>
                                    <input type="text" name="author_name" class="form-control">
                                </span>
                            </div>
                            <?php $social_medias = config('status.social_media'); ?>
                            @foreach($social_medias as $social_media)
                                <div class="form-group">
                                <span class="">
                                    <label>Author {{ $social_media['value'] }} url</label>
                                    <input type="text" name="author_{{ $social_media['value'] }}" class="form-control" placeholder="https://www.{{ $social_media['value'] }}.com/">
                                </span>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn green" id="btn-home-slider" disabled="true"> Submit </button>
                            <a href="javascript:;" data-section="change_avatar" class="cancel_btn btn default"> Cancel </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th style="max-width: 200px;">Image</th>
                                    <td>Author Name</td>
                                    <td>Author social media</td>
                                    <th> Actions </th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($data))
                                        @for($i = 0; $i < count($data['author_list']); $i++)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>
                                                <?php $image = 'theme/images/default.png';
                                                if(file_exists($data['author_list'][$i]['image'])){
                                                    $image = $data['author_list'][$i]['image'];
                                                }
                                                ?>
                                                <img src="{{ asset($image) }}" style="max-width: 200px;"></td>
                                            <td>
                                                {{ $data['author_list'][$i]['name'] }}
                                            </td>
                                            <td>
                                                @foreach($social_medias as $social_media)
                                                    <li>{{ $social_media['name'] }} : {{ $data['author_list'][$i][$social_media['value']] }}</li>
                                                @endforeach
                                            </td>
                                            <td>
                                                <a class="btn btn-circle btn-icon-only btn-danger" title="Delete Author" href="{{ route('admin.pages.authors.remove',[$i]) }}" onclick="event.preventDefault();
                                                    document.getElementById('author-{{ $i }}').submit();">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                                <form id="author-{{ $i }}" action="{{ route('admin.pages.authors.remove',[$i]) }}" method="post" style="display: none;">
                                                    @csrf
                                                </form>
                                                <a class="btn btn-circle btn-icon-only btn-primary" title="Edit Author" data-id="{{ $i }}" href="" onclick="return editAuthor(this);">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endfor
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="author-edit-modal" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('admin.pages.authors.post.edit') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="author_index" id="author_id">
                    <input type="hidden" name="image_previous_url" id="home-slider-edit-file">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Team member Edit</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                        <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="author_image" id="home-slider-edit"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" id="remove-image" data-dismiss="fileinput"> Remove </a>

                                            @if ($errors->has('author_image'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('author_image') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                        <span>Image minimum width: 300px <br> Image minimum height: 373px</span>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                    <span class="error-log hide">
                                        <span class="label label-danger">Error! </span>
                                        <span class="error-data"></span>
                                    </span>
                                        <br>
                                        <span class="label label-warning">NOTE! </span>
                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                <span class="">
                                    <label>Author name</label>
                                    <input type="text" name="author_name" id="author_name" class="form-control">
                                </span>
                                </div>
                                @foreach($social_medias as $social_media)
                                    <div class="form-group">
                                        <span class="">
                                            <label>Author {{ $social_media['value'] }} url</label>
                                            <input type="text" name="author_{{ $social_media['value'] }}" id="author_{{ $social_media['value'] }}" class="form-control" placeholder="https://www.{{ $social_media['value'] }}.com/">
                                        </span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn blue" id="modal-save-btn">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var _URL = window.URL || window.webkitURL;
            var width = 300;
            var height = 373;
            $("#home-slider").change(function (e) {
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function () {
                        console.log(this.width, this.height);
                        if(this.width >= width && this.height >= height){
                            $('.error-log').removeClass('show');
                            $('.error-log').addClass('hide');
                            $('.error-data').html('');
                            $('#btn-home-slider').removeAttr('disabled');
                        }else{
                            $('.error-log').addClass('show');
                            $('.error-log').removeClass('hide');
                            $('.error-data').html('Image resolution should be ' + width +"X" +height);
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }
            });

            $("#home-slider-edit").change(function (e) {
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function () {
                        if(this.width >= width && this.height >= height){
                            $('#author-edit-modal .error-log').removeClass('show');
                            $('#author-edit-modal .error-log').addClass('hide');
                            $('#author-edit-modal .error-data').html('');
                            $('#author-edit-modal #modal-save-btn').removeAttr('disabled');
                            $('#author-edit-modal #home-slider-edit-file').val('');
                        }else{
                            $('#author-edit-modal .error-log').addClass('show');
                            $('#author-edit-modal .error-log').removeClass('hide');
                            $('#author-edit-modal .error-data').html('Image resolution should be ' + width +"X" +height);
                            $('#author-edit-modal #modal-save-btn').attr('disabled', 'disabled');
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }else{
                    $('#author-edit-modal #modal-save-btn').removeAttr('disabled');
                }
            });

            $('#remove-image').on('click', function(){
                $('#author-edit-modal #home-slider-edit-file').val('');
            });
        });

        function editAuthor(elem){
            var index = $(elem).data('id');
            $.ajax({
                url: '{{ route('admin.pages.authors.get') }}',
                data: { index: index},
                success: function (data) {
                    var data = jQuery.parseJSON(data);
                    console.log(data);
                    if(data['author_list']['image']){
                        $('#author-edit-modal .fileinput').removeClass('fileinput-new');
                        $('#author-edit-modal .fileinput').addClass('fileinput-exists');
                        var image_preview = '<img src="' + data['author_list']['image'] + '" style="max-height: 140px;">';
                        $('#author-edit-modal .fileinput-preview > img').remove();
                        $('#author-edit-modal .fileinput-preview').html(image_preview);
                    }
                    $('#author-edit-modal #home-slider-edit-file').val(data['author_list']['image']);
                    $('#author-edit-modal #author_name').val(data['author_list']['name']);
                    var social_media = data['author_list']['ids'];
                    for(var i = 0; i < social_media.length; i++){
                        $('#author-edit-modal #author_' + social_media[i]).val(data['author_list'][social_media[i]]);
                    }
                    $('#author-edit-modal #author_id').val(index);
                    $('#author-edit-modal').modal('show');
                }
            });

            return false;
        }
    </script>
@endsection
