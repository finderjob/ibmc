@extends('layouts.default')

@section('content')

    <h1 class="page-title">Settings</h1>
    <div class="row">
        <div class="col-md-8 ">
            <form action="{{ route('settings.add.post') }}" method="post" role="form" class="settings-create-form">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group">
                                <span class="form-error">
                                    <label>Facebook Url</label>
                                    <input type="text" name="facebook_url" id="facebook_url" class="form-control {{ $errors->has('facebook_url') ? ' is-invalid' : '' }} " placeholder="http://www.facebook.com" value="{{ $socail_url['facebook_url'] }}">
                                </span>
                        @if ($errors->has('facebook_url'))
                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('facebook_url') }}</strong>
                                     </span>
                        @endif
                    </div>
                    <div class="form-group">
                                <span class="form-error">
                                    <label>Twitter Url</label>
                                    <input type="text" name="twitter_url" id="twitter_url" class="form-control {{ $errors->has('twitter_url') ? ' is-invalid' : '' }} " placeholder="http://www.twitter.com" value="{{ $socail_url['twitter_url'] }}">
                                </span>
                        @if ($errors->has('twitter_url'))
                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('twitter_url') }}</strong>
                                     </span>
                        @endif
                    </div>
                    <div class="form-group">
                                <span class="form-error">
                                    <label>G-plus Url</label>
                                    <input type="text" name="g_plus_url" id="g_plus_url" class="form-control {{ $errors->has('g_plus_url') ? ' is-invalid' : '' }} " placeholder="http://www.gplus.com" value="{{ $socail_url['g_plus_url'] }}">
                                </span>
                        @if ($errors->has('g_plus_url'))
                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('g_plus_url') }}</strong>
                                     </span>
                        @endif
                    </div>
                    <div class="form-group">
                                <span class="form-error">
                                    <label>Linkedin Url</label>
                                    <input type="text" name="linkedin_url" id="linkedin_url" class="form-control {{ $errors->has('linkedin_url') ? ' is-invalid' : '' }} " placeholder="http://www.linkedin.com" value="{{ $socail_url['linkedin_url'] }}">
                                </span>
                        @if ($errors->has('linkedin_url'))
                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('linkedin_url') }}</strong>
                                     </span>
                        @endif
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{ route('settings.get') }}" class="btn default">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection