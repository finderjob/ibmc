@extends('layouts.default')

@section('content')
    <h1 class="page-title">Orders</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Order List</span>
                    </div>
                    @if(checkPermissionByName('order create'))
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.orders.add.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="order_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th> Id</th>
                                        <th> Order Id</th>
                                        <th> Customer Username</th>
                                        <th> Order Total</th>
                                        <th> Order Status</th>
                                        <th> Payment Status</th>
                                        <th> Order Date</th>
                                        @if(checkPermissionByName('order view'))
                                        <th> Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $order_statuses = config('status.orders');
                                    usort($order_statuses, 'sortByOrder');
                                    $order_status_cannot_backward = config('status.order_status_cannot_backward');
                                    ?>
                                    @foreach($orders as $order)
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>
                                            {{ $order->order_id }}
                                            @if($order->created_system == 3)
                                                <label class="label label-info">Mobile</label>
                                            @elseif($order->created_system == 2)
                                                <label class="label label-warning">System</label>
                                            @else
                                                <label class="label label-success">Web</label>
                                            @endif
                                        </td>
                                        <td>{{ get_customer_name($order->customer_id) }}</td>
                                        <td style="text-align: right">{{ final_price_format($order->order_total) }}</td>
                                        <td>
                                            <?php
                                                    $backward = false;
                                                    if(in_array($order->status, $order_status_cannot_backward)){
                                                        $backward = true;
                                                    }
                                            ?>
                                            <select id="order_status_{{ $order->id }}" data-id="{{ $order->id }}" data-prev="{{ $order->status }}" class="order_status form-control">
                                                @foreach($order_statuses as $order_status)
                                                    @if($backward)
                                                        @if(in_array($loop->iteration, $order_status_cannot_backward))
                                                            <option value="{{ $order_status['id'] }}" @if($order_status['id'] == $order->status) selected @endif>{{ $order_status['name'] }}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{ $order_status['id'] }}" @if($order_status['id'] == $order->status) selected @endif>{{ $order_status['name'] }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            @if($order->payment_status == 1)
                                                <label class="label label-success">Paid</label>
                                            @elseif($order->payment_status == 2)
                                                <label class="label label-warning">Refunded</label>
                                            @else
                                                <label class="label label-danger">Unpaid</label>
                                            @endif
                                        </td>
                                        <td>{{ $order->created_at }}</td>
                                        <td>
                                            @if(checkPermissionByName('order view'))
                                            <a class="btn btn-circle btn-icon-only btn-primary" href="{{ route('admin.orders.view',[$order->id]) }}" title="View Order">
                                                <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="<?php echo json_encode($order_status_cannot_backward); ?>" id="order-status-cannot-back"/>
    <script>
        $(document).ready(function(){
            $('.order_status').on('change',function(){
                var order_status = $(this).val();
                var order_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeOrderStatus(order_id, order_status,prev_status);
            });
        });
        function changeOrderStatus(order_id, order_status,prev_status){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            var order_status_cannot_back = $('#order-status-cannot-back').val();
            var oscb_data = jQuery.parseJSON(order_status_cannot_back);
            $.ajax({
                url: '{{ route('admin.orders.change.status') }}',
                type: "post",
                data: {_token: _token, order_status: order_status, order_id: order_id},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        if(oscb_data.indexOf(order_status) != -1){
                            $("#order_status_" + order_id+ " option").each(function(){
                                var val = parseInt($(this).val());
                                if(oscb_data.indexOf(val) == -1){
                                    console.log($(this));
                                    $(this).hide();
                                }
                            });
                        }
                        $("#order_status_" + order_id).data('prev', order_status);
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#order_status_'+ order_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');
                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
