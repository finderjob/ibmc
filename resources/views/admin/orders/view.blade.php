@extends('layouts.default')

@section('content')
    <h1 class="page-title">ID: {{ $order->id }} |
        <small>Order Details</small>
    </h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#info" data-toggle="tab">Order Info</a>
                                    </li>
                                    <li>
                                        <a href="#product" data-toggle="tab">Order Product</a>
                                    </li>
                                    <li>
                                        <a href="#payment" data-toggle="tab">Order Payment</a>
                                    </li>
                                    <li>
                                        <a href="#history" data-toggle="tab">Order History</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- PERSONAL INFO TAB -->
                                    <div class="tab-pane active" id="info">
                                        <div class="row margin-bottom-20">
                                            <div class="col-md-12">
                                                <span class="caption-subject font-blue-madison bold uppercase">Order</span>
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-5">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight">Order Id</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{ $order->order_id }}
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-5">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight">Customer Name</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{ get_customer_name($order->customer_id) }}
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-5">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight">Order Date</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{ $order->created_at }}
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-5">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight">Order Status</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <?php
                                                    $order_statuses = config('status.orders');
                                                    usort($order_statuses, 'sortByOrder');
                                                    $order_status_cannot_backward = config('status.order_status_cannot_backward');
                                                    $backward = false;
                                                    if(in_array($order->status, $order_status_cannot_backward)){
                                                        $backward = true;
                                                    }
                                                ?>
                                                @if($order->valid_status == 1)
                                                        <select id="order_status_{{ $order->id }}" data-id="{{ $order->id }}" data-prev="{{ $order->status }}" class="order_status form-control">
                                                            @foreach($order_statuses as $order_status)
                                                                @if($backward)
                                                                    @if(in_array($loop->iteration, $order_status_cannot_backward))
                                                                        <option value="{{ $order_status['id'] }}" @if($order_status['id'] == $order->status) selected @endif>{{ $order_status['name'] }}</option>
                                                                    @endif
                                                                @else
                                                                    <option value="{{ $order_status['id'] }}" @if($order_status['id'] == $order->status) selected @endif>{{ $order_status['name'] }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                @else
                                                        @foreach($order_statuses as $order_status)
                                                            @if($order->status == $order_status['id'])
                                                                <label class="label {{ $order_status['class'] }}"> {{ $order_status['name'] }}</label>
                                                            @endif
                                                        @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row margin-bottom-5">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight">Order Sub Total</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{ final_price_format($order->sub_total) }}
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-5">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight"> Coupon :
                                                <?php $coupon = \App\Coupon::find($order->coupon_id);
                                                    if($coupon){
                                                        echo $coupon->name." - ";
                                                    }
                                                ?>
                                                </span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <?php $coupon_value = 0;
                                                if($coupon){
                                                    $coupon_value = $order->coupon_value;
                                                }
                                                echo final_price_format($coupon_value);
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-5">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight">Order Total</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <span style="font-weight: 700">{{ final_price_format($order->order_total) }}</span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row margin-bottom-6">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight">Billing Address</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{ $order->billing_first_name }} {{ $order->billing_last_name }},
                                                <br>
                                                {{ $order->billing_address_line_1 }}
                                                <br>
                                                {{ $order->billing_address_line_2 }}
                                                <br>
                                                {{ $order->billing_city }}, {{ $order->billing_zip_code }}
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row margin-bottom-20">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <span class="weight">Shipping Address</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{ $order->shipping_first_name }} {{ $order->shipping_last_name }},
                                                <br>
                                                {{ $order->shipping_address_line_1 }}
                                                <br>
                                                {{ $order->shipping_address_line_2 }}
                                                <br>
                                                {{ $order->shipping_city }}, {{ $order->shipping_zip_code }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="product">
                                        <div class="row margin-bottom-20">
                                            <div class="col-md-12">
                                                <span class="caption-subject font-blue-madison bold uppercase">Products</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer list_table" role="grid" >
                                                    <thead>
                                                    <tr role="row">
                                                        <th> Product Id</th>
                                                        <th> Product Name</th>
                                                        <th> Product Price</th>
                                                        <th> Product Discount</th>
                                                        <th> Product Unit Price</th>
                                                        <th> Qty</th>
                                                        <th> Product Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($order_products as $order_product)
                                                        <tr>
                                                            <td>{{ $order_product->product_id }}</td>
                                                            <td>{{ $order_product->products_name }}</td>
                                                            <?php
                                                            if($order_product->user_type == 2){
                                                                $price = $order_product->school_price;
                                                                $discount = $order_product->school_discount;
                                                            }else{
                                                                $price = $order_product->normal_price;
                                                                $discount = $order_product->normal_discount;
                                                            }
                                                            $unit_price = $price - $discount;
                                                            $total = $unit_price * $order_product->qty;
                                                            ?>
                                                            <td style="text-align: right">{{ final_price_format($price) }}</td>
                                                            <td style="text-align: right">{{ final_price_format($discount) }}</td>
                                                            <td style="text-align: right">{{ final_price_format($unit_price) }}</td>
                                                            <td>{{ $order_product->qty }}</td>
                                                            <td style="text-align: right">{{ final_price_format($total) }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="payment">
                                        <div class="row margin-bottom-20">
                                            <div class="col-md-12">
                                                <span class="caption-subject font-blue-madison bold uppercase">Payments</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer list_table" role="grid" >
                                                    <thead>
                                                    <tr role="row">
                                                        <th> Id</th>
                                                        <th> Payment Id</th>
                                                        <th> Payment Value</th>
                                                        <th> Payment Type</th>
                                                        <th> Payment Status</th>
                                                        <th> Created User</th>
                                                        <th> Created Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($order_payments as $order_payment)
                                                        <tr>
                                                            <td>{{ $order_payment->id }}</td>
                                                            <td>{{ $order_payment->payment_id }}</td>
                                                            <td>{{ final_price_format($order_payment->payment) }}</td>
                                                            <td>
                                                                @if($order_payment->payment_type == 1)
                                                                    <label class="label label-info">Cash on delivery</label>
                                                                @else
                                                                    <label class="label label-info">Other</label>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($order_payment->status == 1)
                                                                    <label class="label label-info">Accepted</label>
                                                                @elseif($order_payment->status == 2)
                                                                    <label class="label label-danger">Declined</label>
                                                                @else
                                                                    <label class="label label-default">No Action Take</label>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{ \App\User::find($order_payment->user_id)->name }}
                                                            </td>
                                                            <td>
                                                                {{ $order_payment->created_at }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="history">
                                        <div class="row margin-bottom-20">
                                            <div class="col-md-12">
                                                <span class="caption-subject font-blue-madison bold uppercase">Histories</span>
                                            </div>
                                        </div>
                                        <div id="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer list_table" role="grid" >
                                                    <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <td>Section</td>
                                                        <td>Description</td>
                                                        <td>Data</td>
                                                        <td>Action User</td>
                                                        <td>Date</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $i =1; ?>
                                                    @foreach($order_histories as $order_history)
                                                        <?php $data = json_decode($order_history->data);
                                                        $user = \App\User::find($order_history->user_id);
                                                        ?>
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $order_history->section }}</td>
                                                            <td>{{ $order_history->description }}</td>
                                                            <td><?php echo $data; ?></td>
                                                            <td>{{ $user->name }}</td>
                                                            <td>{{ $order_history->created_at }}</td>
                                                        </tr>
                                                        <?php $i++; ?>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" value="<?php echo json_encode($order_status_cannot_backward); ?>" id="order-status-cannot-back"/>
    </div>
    <script>
        $(document).ready(function(){
            $('.order_status').on('change',function(){
                var order_status = $(this).val();
                var order_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeOrderStatus(order_id, order_status,prev_status);
            });
        });
        function changeOrderStatus(order_id, order_status,prev_status){
            var _token = $('input[name="_token"]').val();
            var order_status_cannot_back = $('#order-status-cannot-back').val();
            var oscb_data = jQuery.parseJSON(order_status_cannot_back);
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('admin.orders.change.status') }}',
                type: "post",
                data: {_token: _token, order_status: order_status, order_id: order_id},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        if(oscb_data.indexOf(order_status) != -1){
                            $("#order_status_" + order_id+ " option").each(function(){
                                var val = parseInt($(this).val());
                                if(oscb_data.indexOf(val) == -1){
                                    $(this).hide();
                                }
                            });
                        }
                        $("#order_status_" + order_id).data('prev', order_status);
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#order_status_'+ order_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');
                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
