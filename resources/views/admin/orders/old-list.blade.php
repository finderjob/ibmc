@extends('layouts.default')

@section('content')
    <h1 class="page-title">Orders</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Old Order List</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{ route('admin.orders.old.post') }}" method="post">
                        {{ csrf_field() }}
                        <?php
                        $order_statuses = config('status.orders');
                        usort($order_statuses, 'sortByOrder');
                        $payment_statuses = config('status.payment_status');
                        usort($payment_statuses, 'sortByOrder');
                        $order_payment_statuses = config('status.order_payment_status');
                        usort($order_payment_statuses, 'sortByOrder');
                        ?>
                        <div class="row margin-bottom-5">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Order Year</label>
                                    <select name="order_year" class="form-control">
                                        <option value="" disabled selected>select year</option>
                                        <option value="2017" @if($year == '2017') selected @endif>2017</option>
                                        <option value="2018" @if($year == '2018') selected @endif>2018</option>
                                        <option value="2019" @if($year == '2019') selected @endif>2019</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Order Status</label>
                                    <select name="order_status" class="form-control">
                                        <option value="" disabled selected>select status</option>
                                        <option value="-1">All</option>
                                        @foreach($order_statuses as $os)
                                            @if($os['close'])
                                                <option value="{{ $os['id'] }}" @if($os['id'] == $order_status) selected @endif>{{ $os['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Order payment status</label>
                                    <select name="order_payment_status" class="form-control">
                                        <option value="" disabled selected>select payment status</option>
                                        <option value="-1">All</option>
                                        @foreach($order_payment_statuses as $ops)
                                            <option value="{{ $ops['id'] }}" @if($ops['id'] === $order_payment_status) selected @endif>{{ $ops['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row margin-bottom-20">
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Filter</button>
                                <button type="reset"  class="btn btn-danger reset-btn">Reset</button>
                            </div>
                        </div>
                    </div>
                    </form>
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="order_list_table" role="grid" >
                                <thead>
                                <tr role="row">
                                    <th> Id</th>
                                    <th> Order Id</th>
                                    <th> Customer Username</th>
                                    <th> Order Total</th>
                                    <th> Order Status</th>
                                    <th> Payment Status</th>
                                    <th> Order Date</th>
                                    <th> Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>{{ $order->order_id }}</td>
                                        <td>{{ get_customer_name($order->customer_id) }}</td>
                                        <td style="text-align: right">{{ final_price_format($order->order_total) }}</td>
                                        <td>
                                            @foreach($order_statuses as $order_status)
                                                @if($order->status == $order_status['id'])
                                                    <label class="label {{ $order_status['class'] }}">{{ $order_status['name'] }}</label>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($order_payment_statuses as $order_payment_status)
                                                @if($order->payment_status == $order_payment_status['id'])
                                                    <label class="label {{ $order_payment_status['class'] }}">{{ $order_payment_status['name'] }}</label>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{ $order->created_at }}</td>
                                        <td>
                                            <a class="btn btn-circle btn-icon-only btn-primary" href="{{ route('admin.orders.old.view',[$order->id]) }}" title="View Order">
                                                <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.reset-btn').on('click', function () {
                var url = location.href;
                window.location.href = url;
            });
        });
    </script>
@endsection
