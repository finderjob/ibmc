@extends('layouts.default')

@section('content')
    <h1 class="page-title">Orders</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Order Add</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#info" data-toggle="tab">Customer Info</a>
                        </li>
                        <li class="disabled" id="product-info-tab">
                            <a href="#product" data-toggle="tab">Order Product</a>
                        </li>
                    </ul>
                    <form action="{{ route('admin.orders.add.post') }}" method="post" role="form" class="">
                        {{ csrf_field() }}
                        <div class="tab-content">
                            <div class="tab-pane active" id="info">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer name</label>
                                                    <select name="name" id="customer_select_name" class="form-control" style="width: 100%">
                                                        <option disabled="true">Select customer by name</option>
                                                    </select>
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer email</label>
                                                    <select name="name" id="customer_select_email" class="form-control" style="width: 100%">
                                                        <option disabled="true">Select customer by email</option>
                                                    </select>
                                                </span>
                                                @if ($errors->has('customer_email'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('customer_email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="customer-info-section">
                                        <input type="hidden" class="form-control" name="billing_address_id" id="billing_address_id">
                                        <input type="hidden" class="form-control" name="shipping_address_id" id="shipping_address_id">
                                        <div class="col-md-6 col-sm-12">
                                            <label style="font-weight: 700">Billing Info</label>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer billing first name</label>
                                                    <input type="text" class="form-control" name="billing_first_name" id="billing_first_name">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer billing last name</label>
                                                    <input type="text" class="form-control" name="billing_last_name" id="billing_last_name">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer billing address line 1</label>
                                                    <input type="text" class="form-control" name="billing_address_line_1" id="billing_address_line_1">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer billing address line 2</label>
                                                    <input type="text" class="form-control" name="billing_address_line_2" id="billing_address_line_2">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer billing city</label>
                                                    <input type="text" class="form-control" name="billing_city" id="billing_city">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer billing postcode/ zip</label>
                                                    <input type="text" class="form-control" name="billing_zip_code" id="billing_zip_code">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer billing telephone</label>
                                                    <input type="text" class="form-control" name="billing_phone_number" id="billing_phone_number">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer billing mobile</label>
                                                    <input type="text" class="form-control" name="billing_mobile_number" id="billing_mobile_number">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <label style="font-weight: 700">Shipping Info</label>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer shipping first name</label>
                                                    <input type="text" class="form-control" name="shipping_first_name" id="shipping_first_name">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer shipping last name</label>
                                                    <input type="text" class="form-control" name="shipping_last_name" id="shipping_last_name">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer shipping address line 1</label>
                                                    <input type="text" class="form-control" name="shipping_address_line_1" id="shipping_address_line_1">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer shipping address line 2</label>
                                                    <input type="text" class="form-control" name="shipping_address_line_2" id="shipping_address_line_2">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer shipping city</label>
                                                    <input type="text" class="form-control" name="shipping_city" id="shipping_city">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer shipping postcode/ zip</label>
                                                    <input type="text" class="form-control" name="shipping_zip_code" id="shipping_zip_code">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer shipping telephone</label>
                                                    <input type="text" class="form-control" name="shipping_phone_number" id="shipping_phone_number">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Customer shipping mobile</label>
                                                    <input type="text" class="form-control" name="shipping_mobile_number" id="shipping_mobile_number">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="product">
                                <table class="table table-bordered table-hover table-striped add-new-products-table">
                                    <thead class="order-table-header">
                                        <tr>
                                            <td>Product Name</td>
                                            <td>Product Qty</td>
                                            <td style="text-align: right">Product Price</td>
                                            <td style="text-align: right">Total</td>
                                            <td>
                                                Action
                                                <a class="btn btn-circle btn-icon-only btn-primary add-new-row" href="#" title="Add New Row">
                                                    <i class="fa fa-plus-circle"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot class="order-table-footer">
                                        <tr>
                                            <td colspan="3" style="text-align: right">Order Sub total</td>
                                            <td id="order-sub-total" style="text-align: right"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Coupon</td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text" name="coupon" id="coupon" class="form-control">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary" id="coupon-btn" type="button">Apply</button>
                                                    </span>
                                                </div>
                                            </td>
                                            <td style="text-align: right">Coupon total</td>
                                            <td id="coupon-total" style="text-align: right"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align: right">Order total</td>
                                            <td id="order-total" style="text-align: right"></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="form-actions">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <a href="{{ route('admin.orders') }}" class="btn default">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>

        function addNewRows(id){
            var tr = '<tr class="add-product-row" id="' + id +'">';
            tr += '<td><select name="product[]" onchange="selectProduct('+ id +')" data-id="'+ id +'" class="form-control product-name" style="width: 100%"></select></td>';
            tr += '<td><input type="number" onkeyup="updateQty();" onchange="updateQty();" name="qty[]" min="1" step="1" data-id="'+ id +'" class="form-control product-qty"></td>';
            tr += '<td style="text-align: right"><span id="product-price-'+ id +'" ></span></td>';
            tr += '<td style="text-align: right"><span id="product-sub-price-'+ id +'" ></span></td>';
            tr += '<td>';
            tr += '<a class="btn btn-circle btn-icon-only btn-danger" href="#" data-id="'+ id +'" title="Remove Row" onclick="return removeRow('+ id +')">';
            tr += '<i class="fa fa-minus-circle"></i>'
            tr += '</a>';
            tr += '</td>';
            tr += '</tr>';
            $('.add-new-products-table tbody').append(tr);
            var origin   = window.location.origin;
            var placeholder2 = "Select products";
            $(".add-new-products-table tbody #"+ id +" .product-name").select2({
                placeholder: placeholder2,
                minimumInputLength: 2,
                allowClear: true,
                ajax: {
                    url: origin + "/administrator/orders/product",
                    type: "GET",
                    quietMillis: 50,
                    data: function (term) {
                        var product_ids = new Array();
                        $('.add-new-products-table tbody .product-name').each(function(){
                            var id = $(this).val();
                            if(id){
                                product_ids.push(id);
                            }
                        });
                        return {
                            product_name: term,
                            product_ids: product_ids
                        };
                    },
                    processResults: function (data) {
                        obj = JSON.parse(data);
                        return {
                            results: $.map(obj, function (item) {
                                if(item){
                                    return {
                                        text: item.id + " : " + item.name,
                                        id: item.id
                                    }
                                }
                            })
                        };
                    }
                }
            });
        }
        function selectProduct(id){
            $('.add-new-products-table tbody #'+id+' .product-qty').val(1);
            updateData();
        }

        function updateQty(){
            updateData();
        }
        function updateData(){
            var products = new Array();
            $('.add-new-products-table tbody .product-name').each(function(){
                var product_id = $(this).val();
                var id = $(this).data('id');
                var qty = $('#'+id+' .product-qty').val();
                products.push({
                    id: product_id,
                    qty: qty,
                    row_id: id
                });
            });
            var customer_id = $('#customer_select_name').val();
            var coupon = $('#coupon').val();
            $.ajax({
                url: '{{ route('admin.order.price.data') }}',
                data: { customer_id: customer_id, products: products, coupon: coupon},
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    var products = data['products'];
                    for(var i = 0; i < products.length; i++){
                        var row_id = products[i]['row_id'];
                        var price = products[i]['price'];
                        var sub_total = products[i]['sub_total'];
                        $('#product-price-' + row_id).html(price);
                        $('#product-sub-price-' + row_id).html(sub_total);
                    }
                    $('#coupon-total').html(data['coupon']);
                    $('#order-sub-total').html(data['sub_total']);
                    $('#order-total').html(data['total']);
                }
            });
        }
        function removeRow(id){
            $('.add-new-products-table tbody #' + id).remove();
            updateData();
            return false;
        }
        $(document).ready(function(){
            $('.add-new-row').on('click', function (e) {
                e.preventDefault();
                var closet_row_id = $('table tbody tr:last').attr('id');
                if(!closet_row_id){
                    closet_row_id = 1;
                }else{
                    closet_row_id++;
                }
                addNewRows(closet_row_id);
            });

            $('#coupon-btn').on('click', function () {
                updateData();
            });

            $('#customer_select_name').on('change', function(){
                var customer_id = $(this).val();
                if(customer_id){
                    getCustomerInfos(customer_id);
                    $('#product-info-tab').removeClass('disabled');
                }else{
                    emptyCustomer();
                    $('#product-info-tab').addClass('disabled');
                }
            });
            $('#customer_select_email').on('change', function(){
                var customer_id = $(this).val();
                if(customer_id){
                    getCustomerInfos(customer_id);
                    $('#product-info-tab').removeClass('disabled');
                }else{
                    emptyCustomer();
                    $('#product-info-tab').addClass('disabled');
                }
            });
            $('#customer_select_name').on("select2:select", function () {
                $("#customer_select_email").attr('disabled', 'disabled');
            });
            $('#customer_select_name').on("select2:unselecting", function (e) {
                $("#customer_select_email").removeAttr('disabled');
            });
            $('#customer_select_email').on("select2:select", function () {
                $("#customer_select_name").attr('disabled', 'disabled');
            });
            $('#customer_select_email').on("select2:unselecting", function (e) {
                $("#customer_select_name").removeAttr('disabled');
            });

            var products = new Array();

            $('.add-new-products-table tbody .product-name').each(function(){
                var product_id = $(this).val();
                var id = $(this).data('id');
                var qty = $('#'+id+' .product-qty').val();
                products.push({
                    id: product_id,
                    qty: qty,
                    row_id: id
                });
            });
            updateData(products);
        });

        function emptyCustomer(){
            for(var i = 0; i < 2; i++){
                if(i == 0){
                    var id = '#billing_';
                }else{
                    var id = '#shipping_';
                }
                $(id + 'address_id').val(null);
                $(id + 'first_name').val("");
                $(id + 'last_name').val("");
                $(id + 'address_line_1').val("");
                $(id + 'address_line_2').val("");
                $(id + 'city').val("");
                $(id + 'zip_code').val("");
                $(id + 'phone_number').val("");
                $(id + 'mobile_number').val("");
            }
        }
        function getCustomerInfos(customer_id){
            $.ajax({
                url: '{{ route('admin.order.customer') }}',
                data: {customer_id: customer_id},
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    if(data.status == 1){
                        var set = data.data;
                        for(var i = 0; i < 2; i++){
                            if(i == 0){
                                var append_data = set['billing'];
                                var id = '#billing_';
                            }else{
                                var append_data = set['shipping'];
                                var id = '#shipping_';
                            }
                            $(id + 'address_id').val(append_data['address_id']);
                            $(id + 'first_name').val(append_data['first_name']);
                            $(id + 'last_name').val(append_data['last_name']);
                            $(id + 'address_line_1').val(append_data['address_line_1']);
                            $(id + 'address_line_2').val(append_data['address_line_2']);
                            $(id + 'city').val(append_data['city']);
                            $(id + 'zip_code').val(append_data['zip_code']);
                            $(id + 'phone_number').val(append_data['phone_number']);
                            $(id + 'mobile_number').val(append_data['mobile_number']);
                        }
                    }else{
                        emptyCustomer();
                    }
                }
            });
        }
    </script>
@endsection
