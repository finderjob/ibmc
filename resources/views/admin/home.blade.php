@extends('layouts.default')

@section('content')
    <h1 class="page-title"> Admin Dashboard
        <small>statistics, charts, recent events and reports</small>
    </h1>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ customer_count() }}
                    </div>
                    <div class="desc"> Customers </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span>{{ monthly_sales() }}</span></div>
                    <div class="desc"> This month sales </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span>{{ pending_order_count() }}</span>
                    </div>
                    <div class="desc"> Pending Orders </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <?php $counts = product_count(); ?>
                    <div class="number">
                        <span class="desc">Completed: </span> {{ $counts[0] }}
                        @if($counts[1] != 0)
                        <span class="desc">Not Complete: </span> {{ $counts[1] }}
                        @endif
                    </div>
                    <div class="desc"> Products </div>
                </div>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        @if($less_products)
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading" style="text-align: center;font-size: 20px;">
                    Less products table
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover table-bordered" id="less-product-table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Product name</th>
                            <th>Quantity</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($less_products as $less_product)
                            <tr>
                                <td>{{ $less_product->id }}</td>
                                <td>{{ $less_product->name }}</td>
                                <td>{{ $less_product->qty }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer" style="text-align: center">
                    <a href="{{ route('admin.products') }}" class="btn btn-primary">Go Products List</a>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection
