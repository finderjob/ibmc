@extends('layouts.default')

@section('content')
    <h1 class="page-title">Products</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Product Add</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('admin.products.add.post') }}" method="post" role="form" class="product-create-form">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Product Name</label>
                                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Product Name">
                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Slug</label>
                                    <input type="text" name="slug" id="slug" value="{{ old('slug') }}" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }} input-icon" placeholder="Slug">
                                </span>
                                @if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <?php $authors = \App\Author::where('status',1)->get();?>
                                <span class="form-error">
                                    <label>Author name</label>
                                    <select name="author" id="product_author" class="form-control">
                                            <option disabled="true" selected="true">Select author</option>
                                        @foreach($authors as $author)
                                            <option value="{{ $author->id }}" @if(old('author') == $author->id) selected="selected" @endif>{{ $author->name }}</option>
                                        @endforeach
                                    </select>
                                </span>
                                @if ($errors->has('author'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('author') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Qty</label>
                                    <input type="number" name="qty" id="qty" value="{{ old('qty') }}" class="form-control {{ $errors->has('qty') ? ' is-invalid' : '' }} input-icon" placeholder="Qty">
                                </span>
                                @if ($errors->has('qty'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('qty') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Product Overview</label>
                                    <textarea name="overview" id="product_overview" class="form-control">{{ old('overview') }}</textarea>
                                    @if ($errors->has('overview'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('overview') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Product Description</label>
                                     <textarea name="description" id="product_description" class="form-control">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Categories</label>
                                    <?php $categories = \App\Category::where('status',1)->where('parent_id', 0)->get();?>
                                    <select name="category" id="category" class="form-control">
                                        <option disabled="true" selected="true">Select Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" @if(old('category') == $category->id) selected="selected" @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('category') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Sub Categories</label>
                                    <select name="subcategory" id="subcategory" class="form-control">
                                        <option disabled="true">Select Sub category</option>
                                    </select>
                                    @if ($errors->has('subcategory'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('subcategory') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="{{ route('admin.products') }}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#slug').focusout(function ($e) {
                var keyword = $(this).val();
                slugCreate(keyword);
            });
            $('#name').focusout(function ($e) {
                var keyword = $(this).val();
                slugCreate(keyword);
            });
            var category_id = $("#category").val();
            if(category_id){
                getSubCategories(category_id);
            }
            $('#category').on('change', function () {
                var category_id = $(this).val();
                getSubCategories(category_id);
            });
        });
        function slugCreate(keyword){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('generate-slug') }}',
                type: "post",
                data: {_token: _token, keyword: keyword},
                success: function (data) {
                    $('#slug').val(data);
                    $('.swing-preloader').css('display','none');
                    $('#slug').closest('.form-group').removeClass('has-error');
                    $('#slug-error').remove();
                }
            });
        }

        function getSubCategories(category_id) {
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('admin.get.sub-categories.post') }}',
                type: "post",
                data: {_token: _token, category_id: category_id},
                success: function (data) {
                    var select = "";
                    for(var i = 0; i < data.length; i++){
                        var option = '<option value="'+ data[i][0] +'">'+ data[i][1] +'</option>';
                        select += option;
                    }
                   $('#subcategory').html(select);
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
