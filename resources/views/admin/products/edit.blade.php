@extends('layouts.default')

@section('content')
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title">Product</h1>
    <!-- END PAGE TITLE-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">{{ $product->name }} |<small>Product Details</small></span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="@if($section == null ||  $section == 'product_info') active @endif">
                                        <a href="#tab_1_1" data-toggle="tab">Product Info</a>
                                    </li>
                                    @if($show_hide['change_avatar'])
                                    <li class="@if($section == 'change_avatar') active @endif">
                                        <a href="#tab_1_2" data-toggle="tab">Product Images</a>
                                    </li>
                                    @endif
                                    @if($show_hide['change_price'])
                                    <li class="@if($section == 'change_price') active @endif">
                                        <a href="#tab_1_3" data-toggle="tab">Product Price</a>
                                    </li>
                                    @endif
                                    @if($show_hide['product_review'])
                                        <li class="@if($section == 'product_review') active @endif">
                                            <a href="#tab_1_5" data-toggle="tab">Product Review</a>
                                        </li>
                                    @endif
                                    <li class="@if($section == 'product_history') active @endif">
                                        <a href="#tab_1_4" data-toggle="tab">Product History</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- PERSONAL INFO TAB -->

                                    <div class="tab-pane @if($section == null ||  $section == 'product_info') active @endif" id="tab_1_1">
                                        <form role="form" action="{{ route('admin.products.edit.post',[ $product->id]) }}" method="post" class="update-product-info">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Product Name</label>
                                                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Product Name" value="{{ $product->name }}">
                                                </span>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Slug</label>
                                                    <input type="text" name="slug" id="slug" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }} input-icon" placeholder="Slug" value="{{ $product->slug }}">
                                                </span>
                                                @if ($errors->has('slug'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('slug') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <?php $authors = \App\Author::where('status',1)->get();?>
                                                <span class="form-error">
                                                    <label>Author name</label>
                                                    <select name="author" id="product_author" class="form-control">
                                                            <option disabled="true" selected="true">Select author</option>
                                                        @foreach($authors as $author)
                                                            <option value="{{ $author->id }}" @if($product->author_id == $author->id) selected="selected" @endif>{{ $author->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </span>
                                                @if ($errors->has('author_name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('author_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Qty</label>
                                                    <input type="number" name="qty" id="qty" class="form-control {{ $errors->has('qty') ? ' is-invalid' : '' }} input-icon" placeholder="Qty" value="{{ $product->qty }}">
                                                </span>
                                                @if ($errors->has('qty'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('qty') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Product Overview</label>
                                                     <textarea name="overview" id="product_overview" class="form-control">{{ json_decode($product->overview) }}</textarea>
                                                    @if ($errors->has('overview'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('overview') }}</strong>
                                                        </span>
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Product Description</label>
                                                     <textarea name="description" id="product_description" class="form-control">{{ json_decode($product->description) }}</textarea>
                                                    @if ($errors->has('description'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('description') }}</strong>
                                                        </span>
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Categories</label>
                                                    <?php $categories = \App\Category::where('status',1)->where('parent_id', 0)->get();?>
                                                    <select name="category" id="category" class="form-control">
                                                        <option disabled="true" selected="true">Select Category</option>
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->id }}" @if($category->id == $product_category) selected="selected" @endif>{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('category'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('category') }}</strong>
                                                        </span>
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Sub Categories</label>
                                                    <select name="subcategory" id="subcategory" class="form-control">
                                                        <option disabled="true">Select Sub category</option>
                                                    </select>
                                                    @if ($errors->has('subcategory'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('subcategory') }}</strong>
                                                        </span>
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Related Products</label>
                                                    <?php $all_products = \App\Product::select('id','name')->where('id','!=', $product->id)->where('status',1)->get();

                                                    ?>
                                                    <select name="related_product[]" id="related_product" class="form-control" multiple>
                                                        <option disabled="true">Select Category</option>
                                                        @foreach($all_products as $all_product)
                                                            <option value="{{ $all_product->id }}" @if(in_array($all_product->id, $related_products)) selected="selected" @endif>{{ $all_product->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </span>
                                            </div>
                                            <div class="margiv-top-10">
                                                <button type="submit" class="btn green"> Save Changes </button>
                                                <a href="{{ route('admin.products') }}" data-section="personal_info" class="cancel_btn btn default"> Cancel </a>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- END PERSONAL INFO TAB -->
                                    <!-- CHANGE AVATAR TAB -->
                                    <div class="tab-pane @if($section == 'change_avatar') active @endif" id="tab_1_2">
                                        <div class="col-md-12">
                                            <h5><label>Cover Image</label></h5>
                                            <hr>
                                        </div>
                                        <div class="row">
                                        @if($product_cover_images)
                                            <div class="col-md-4">

                                                    @if(file_exists($product_cover_images->image_path))
                                                        <img src="{{ asset($product_cover_images->image_path) }}" class="img-responsive" style="width: 66%;" alt="">
                                                    @else
                                                        <img src="{{ asset('images/users/profile_user.jpg') }}" class="img-responsive" alt="">
                                                    @endif

                                            </div>
                                        @endif
                                            <div class="col-md-8">
                                            <form action="{{ route('admin.products.edit.upload',[$product->id]) }}" method="post" role="form" enctype="multipart/form-data">
                                                <input type="hidden" name="user_id" value="#">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="cover_image">
                                                        </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">NOTE! </span>
                                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                    </div>
                                                </div>
                                                <div class="margin-top-10">
                                                    <button type="submit" class="btn green"> Submit </button>
                                                    <a href="javascript:;" data-section="change_avatar" class="cancel_btn btn default"> Cancel </a>
                                                </div>

                                            </form>

                                        </div>
                                        </div>
                                        <div class="col-md-12 other-image-block">
                                            <hr>
                                            <h5><label>Other Images (Max: 2 uploads)</label></h5>
                                            <hr>
                                        </div>

                                        <div class="row">
                                                @foreach($product_images as $product_image)
                                                    <div class="col-md-6">

                                                        @if(file_exists($product_image->image_path))
                                                            <img src="{{ asset($product_image->image_path) }}" class="img-responsive" style="width: 66%;" alt="">
                                                        @else
                                                            <img src="{{ asset('images/users/profile_user.jpg') }}" class="img-responsive" alt="">
                                                        @endif

                                                    </div>
                                                @endforeach
                                                <div class="col-md-8 image-upload-block">
                                                    <form action="{{ route('admin.products.edit.other.upload',[$product->id]) }}" method="post" role="form" enctype="multipart/form-data">
                                                        <input type="hidden" name="user_id" value="#">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="other_image_2">
                                                                </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="other_image_3">
                                                                </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix margin-top-10">
                                                                <span class="label label-danger">NOTE! </span>
                                                                <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                            </div>
                                                        </div>
                                                        <div class="margin-top-10">
                                                            <button type="submit" class="btn green"> Submit </button>
                                                            <a href="javascript:;" data-section="change_avatar" class="cancel_btn btn default"> Cancel </a>
                                                        </div>

                                                    </form>

                                                </div>
                                        </div>
                                    </div>
                                    <!-- END CHANGE AVATAR TAB -->
                                    <div class="tab-pane @if($section == 'change_price') active @endif" id="tab_1_3">
                                        <form role="form" action="{{ route('admin.products.edit.price', [$product->id]) }}" method="post" class="update-price-info">
                                            {{ csrf_field() }}
                                            <?php
                                                $normal_price = 0;$school_price = 0; $normal_discount = 0; $school_discount = 0;
                                                if($product_price){
                                                    $normal_price  = $product_price->normal_price;
                                                    $school_price = $product_price->school_price;
                                                    $normal_discount = $product_price->normal_discount;
                                                    $school_discount = $product_price->school_discount;
                                                }
                                            ?>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Product Normal Price</label>
                                                    <div class="input-group">
                                                        <input type="number" min="0" step="0.01" name="normal_price" id="normal_price" class="form-control {{ $errors->has('normal_price') ? ' is-invalid' : '' }} input-icon" placeholder="Product Normal Price" value="{{ $normal_price }}">
                                                        <span class="input-group-addon">
                                                             <i class="fa fa-dollar"></i>
                                                        </span>
                                                    </div>
                                                </span>
                                                @if ($errors->has('normal_price'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('normal_price') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Product school Price</label>
                                                    <div class="input-group">
                                                        <input type="number" min="0" step="0.01" name="school_price" id="school_price" class="form-control {{ $errors->has('school_price') ? ' is-invalid' : '' }} input-icon" placeholder="Product Normal Price" value="{{ $school_price }}">
                                                        <span class="input-group-addon">
                                                             <i class="fa fa-dollar"></i>
                                                        </span>
                                                    </div>
                                                </span>
                                                @if ($errors->has('school_price'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('school_price') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Product Normal Price Discount</label>
                                                    <div class="input-group">
                                                        <input type="number" min="0" step="0.01" name="normal_discount" id="normal_discount" class="form-control {{ $errors->has('normal_discount') ? ' is-invalid' : '' }} input-icon" placeholder="Product Normal Price" value="{{ $normal_discount }}">
                                                        <span class="input-group-addon">
                                                             <i class="fa fa-dollar"></i>
                                                        </span>
                                                    </div>
                                                </span>
                                                @if ($errors->has('normal_discount'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('normal_discount') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <span class="form-error">
                                                    <label>Product school Price Discount</label>
                                                    <div class="input-group">
                                                        <input type="number" min="0" step="0.01" name="school_discount" id="school_discount" class="form-control {{ $errors->has('school_discount') ? ' is-invalid' : '' }} input-icon" placeholder="Product Normal Price" value="{{ $school_discount }}">
                                                        <span class="input-group-addon">
                                                             <i class="fa fa-dollar"></i>
                                                        </span>
                                                    </div>
                                                </span>
                                                @if ($errors->has('school_discount'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('school_discount') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="margiv-top-10">
                                                <button type="submit" class="btn green"> Save Changes </button>
                                                <a href="{{ route('admin.products') }}" data-section="personal_info" class="cancel_btn btn default"> Cancel </a>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tab-pane @if($section == 'product_history') active @endif" id="tab_1_4">
                                        <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                                            <div class="row">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="product_history_table" role="grid" >
                                                    <thead>
                                                        <tr>
                                                            <td>Section</td>
                                                            <td>Description</td>
                                                            <td>Data</td>
                                                            <td>Action User</td>
                                                            <td>Date</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i =1; ?>
                                                        @foreach($product_history as $ph)
                                                            <?php $data = json_decode($ph->data);
                                                                $user = \App\User::find($ph->user_id);
                                                            ?>
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $ph->section }}</td>
                                                            <td>{{ $ph->description }}</td>
                                                            <td><?php echo $data->data; ?></td>
                                                            <td>{{ $user->name }}</td>
                                                            <td>{{ $ph->created_at }}</td>
                                                        </tr>
                                                            <?php $i++; ?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane @if($section == 'product_review') active @endif" id="tab_1_5">
                                        <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                                            <div class="row">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="product_review_table" role="grid" >
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Summary</th>
                                                        <th>Review</th>
                                                        <th>Review By</th>
                                                        <th>Ratings</th>
                                                        <th>Status</th>
                                                        <td>Date</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $i = 1; ?>
                                                    @foreach($product_reviews as $product_review)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $product_review->summary }}</td>
                                                            <td><?php echo json_decode($product_review->review); ?></td>
                                                            <td>{{ $product_review->name }}</td>
                                                            <td>
                                                                <?php $avg_rating = ($product_review->quality + $product_review->price + $product_review->value);
                                                                if($avg_rating != 0){
                                                                    $avg_rating = $avg_rating / 3;
                                                                    $avg_rating = intval(round($avg_rating));
                                                                }
                                                                ?>
                                                                <div class="overall">
                                                                    <span>Overall Rating</span>
                                                                    <ul class="rating-stars">
                                                                        @for($j = 1; $j <= 5; $j++)
                                                                            @if($j <= $avg_rating)
                                                                                <li class="on"><i class="fa fa-star"></i></li>
                                                                            @else
                                                                                <li><i class="fa fa-star-o"></i></li>
                                                                            @endif
                                                                        @endfor
                                                                    </ul>
                                                                </div>
                                                                <div class="">
                                                                    <span>Quality Rating</span>
                                                                    <ul class="rating-stars">
                                                                        @for($j = 1; $j <= 5; $j++)
                                                                            @if($j <= $product_review->quality)
                                                                                <li class="on"><i class="fa fa-star"></i></li>
                                                                            @else
                                                                                <li><i class="fa fa-star-o"></i></li>
                                                                            @endif
                                                                        @endfor
                                                                    </ul>
                                                                </div>
                                                                <div class="">
                                                                    <span>Price Rating</span>
                                                                    <ul class="rating-stars">
                                                                        @for($j = 1; $j <= 5; $j++)
                                                                            @if($j <= $product_review->price)
                                                                                <li class="on"><i class="fa fa-star"></i></li>
                                                                            @else
                                                                                <li><i class="fa fa-star-o"></i></li>
                                                                            @endif
                                                                        @endfor
                                                                    </ul>
                                                                </div>
                                                                <div class="">
                                                                    <span>Value Rating</span>
                                                                    <ul class="rating-stars">
                                                                        @for($j = 1; $j <= 5; $j++)
                                                                            @if($j <= $product_review->value)
                                                                                <li class="on"><i class="fa fa-star"></i></li>
                                                                            @else
                                                                                <li><i class="fa fa-star-o"></i></li>
                                                                            @endif
                                                                        @endfor
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <select id="review_status_{{ $product_review->id }}" data-id="{{ $product_review->id }}" data-prev="{{ $product_review->status }}" class="review_status form-control">
                                                                    <option value="0" selected disabled> Select Review status </option>
                                                                    <option value="1" @if($product_review->status == 1) selected @endif> Accept </option>
                                                                    <option value="2" @if($product_review->status == 2) selected @endif> Decline </option>
                                                                </select>
                                                            </td>
                                                            <td>{{ $product_review->created_at }}</td>
                                                        </tr>
                                                        <?php $i++; ?>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.cancel_btn').click(function() {
                var section = $(this).data('section');
                $.ajax({
                    url: '{{ route('admin.profile.session') }}',
                    type: 'get',
                    data: {profile_section : section},
                    success: function (data) {
                        location.reload();
                    }
                })
            });
            var  category_id = $('#category').val();
            getSubCategories(category_id);
            $('#category').on('change', function () {
                var  category_id = $(this).val();
                getSubCategories(category_id);
            });

            $('.review_status').on('change',function(){
                var review_status = $(this).val();
                var review_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeReviewStatus(review_id, review_status, prev_status);
            });
        });

        function changeReviewStatus(review_id, review_status, prev_status){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('admin.product.review.change.status') }}',
                type: "post",
                data: {_token: _token, review_id: review_id, review_status: review_status},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#review_status_'+ review_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');

                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
        function getSubCategories(category_id) {
            var _token = $('input[name="_token"]').val();
            var product_id = '{{ $product->id }}';
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('admin.get.sub-categories.post') }}',
                type: "post",
                data: {_token: _token, category_id: category_id, product_id: product_id},
                success: function (data) {
                    var select = "";
                    for(var i = 0; i < data.length; i++){
                        var selected = '';
                        if(data[i][2]){
                            selected = 'selected=selected'
                        }
                        var option = '<option value="'+ data[i][0] +'" '+selected+'>'+ data[i][1] +'</option>';
                        select += option;
                    }
                    $('#subcategory').html(select);
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
