@extends('layouts.default')

@section('content')
    <h1 class="page-title">Product</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Product List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.products.add.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="product_list_table" role="grid" >
                                <thead>
                                    <tr>
                                        <td>Id</td>
                                        <td>Name</td>
                                        <td>Category</td>
                                        <td>Subcategory</td>
                                        <td>Qty</td>
                                        <td>Complete</td>
                                        <td>Status</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                        <?php $product_category = \App\ProductCategory::where('product_id', $product->id)->where('status',1)->first();
                                              if($product_category){
                                                  $get_category = \App\Category::find($product_category->category_id);
                                                  if($get_category){
                                                      if($get_category->parent_id){
                                                          $sub_category = $get_category->name;
                                                          $get_category = \App\Category::find($get_category->parent_id);
                                                          if($get_category){
                                                              $category = $get_category->name;
                                                          }else{
                                                              $category = "None";
                                                          }
                                                      }else{
                                                          $category = $get_category->name;
                                                          $sub_category = "None";
                                                      }
                                                  }
                                              }
                                        ?>
                                        <tr>
                                            <td>{{ $product->id }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>
                                                @if($category == "None")
                                                    <span class="label label-sm label-danger margin"> {{ $category }} </span>
                                                @else
                                                    <span class="label label-sm label-info margin"> {{ $category }} </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($sub_category == "None")
                                                    <span class="label label-sm label-danger margin"> {{ $sub_category }} </span>
                                                @else
                                                    <span class="label label-sm label-info margin"> {{ $sub_category }} </span>
                                                @endif
                                            </td>
                                            <td>{{ $product->qty }}</td>
                                            <td>
                                                @if($product->complete_status != 3)
                                                    <span class="label label-sm label-danger"> Not Complete </span>
                                                @else
                                                    <span class="label label-sm label-success"> Completed </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if(false)
                                                    @if($product->status == 1)
                                                        <span class="label label-sm label-success"> Activate </span>
                                                    @else
                                                        <span class="label label-sm label-success"> Deactivate </span>
                                                    @endif
                                                @else
                                                    <select id="product_status_{{ $product->id }}" data-id="{{ $product->id }}" data-prev="{{ $product->status }}" class="product_status form-control">
                                                        <option value="1" @if($product->status == 1) selected @endif>Activate</option>
                                                        <option value="0" @if($product->status == 0) selected @endif>Deactivate</option>
                                                    </select>
                                                @endif
                                            </td>
                                            <td>
                                                @if($product->complete_status != 3)
                                                    <a class="btn btn-circle btn-icon-only btn-primary" href="{{ route('admin.products.edit.get',[$product->id]) }}" title="Complete">
                                                        <i class="fa fa-arrow-circle-right"></i>
                                                    </a>
                                                @else
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="{{ route('admin.products.edit.get',[$product->id]) }}" title="edit">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){

            $('.product_status').on('change',function(){
                var product_status = $(this).val();
                var product_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeProductStatus(product_id, product_status, prev_status);
            });

            function changeProductStatus(product_id, product_status, prev_status){
                var _token = $('input[name="_token"]').val();
                $('.swing-preloader').css('display','block');
                $.ajax({
                    url: '{{ route('admin.product.change.status') }}',
                    type: "post",
                    data: {_token: _token, product_status: product_status, product_id: product_id},
                    success: function (data) {
                        var return_data = jQuery.parseJSON(data);
                        if(return_data.status == 1){
                            toastr.success( return_data.message, 'Success!');
                        }else{
                            $('#product_status'+ product_id).val(prev_status);
                            toastr.error( return_data.message, 'Failed!');
                        }
                        $('.swing-preloader').css('display','none');
                    },
                    error: function(xhr, error){
                        $('#product_status'+ product_id).val(prev_status);
                        toastr.error( error , 'Failed!');
                        $('.swing-preloader').css('display','none');
                    },
                });
            }
        });
    </script>
@endsection
