@extends('layouts.default')

@section('content')
    <h1 class="page-title">Customers</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Customer List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.customers.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>Customer Id</th>
                                        <th> Email </th>
                                        <th> First name </th>
                                        <th> Last Name</th>
                                        <th> Customer type</th>
                                        <th> Contact number</th>
                                        <th> Status </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($customers as $customer)
                                    <tr>
                                        <td>{{ $customer->id }}</td>
                                        <td><a href="mailto:{{ $customer->email }}">{{ $customer->email }}</a></td>
                                        <td>{{ $customer->first_name }}</td>
                                        <td>{{ $customer->last_name }}</td>
                                        <td>
                                            @if($customer->customer_type == 1)
                                                Personal
                                            @elseif($customer->customer_type == 2)
                                                School
                                            @else
                                                Author
                                            @endif
                                        </td>
                                        <td>
                                            <ul>
                                                @if($customer->phone_number)
                                                    <li>Telephone : {{ $customer->phone_number }}</li>
                                                @endif
                                                @if($customer->mobile_number)
                                                    <li>Mobile : {{ $customer->mobile_number }}</li>
                                                @endif
                                            </ul>
                                        </td>
                                        <td>
                                            <select id="customer_status_{{ $customer->id }}" data-id="{{ $customer->id }}" data-prev="{{ $customer->status }}" class="customer_status form-control">
                                                <option value="1" @if($customer->status == 1) selected @endif>Activate</option>
                                                <option value="0" @if($customer->status == 0) selected @endif>Deactivate</option>
                                            </select>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.customer_status').on('change',function(){
                var customer_status = $(this).val();
                var customer_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeCustomerStatus(customer_id, customer_status,prev_status);
            });
        });
        function changeCustomerStatus(customer_id, customer_status,prev_status){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('admin.customers.change.status') }}',
                type: "post",
                data: {_token: _token,customer_id: customer_id, customer_status: customer_status},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#customer_status_'+ customer_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');

                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
