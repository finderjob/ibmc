@extends('layouts.default')
@section('content')
    <h1 class="page-title">Customers</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Message List</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                <tr role="row">
                                    <th>Id</th>
                                    <th>Customer Name</th>
                                    <th>Customer EMail</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($complains as $complain)
                                    <tr class="@if($complain->status == 0) new-message @endif">
                                        <td>{{ $complain->id }}</td>
                                        <td>{{ $complain->name }}</td>
                                        <td><a href="mailto:{{ $complain->email }}">{{ $complain->email }}</a></td>
                                        <td>{{ $complain->subject }}</td>
                                        <td>
                                            {{ json_decode($complain->message) }}
                                        </td>
                                        <td>
                                            <select data-id="{{ $complain->id }}" class="form-control message_status">
                                                <option value="0" selected="selected" disabled>No Action</option>
                                                <option value="1" @if($complain->status == 1) selected="selected" @endif>Only Read</option>
                                                <option value="2" @if($complain->status == 2) selected="selected" @endif>Ready & Reply</option>
                                            </select>
                                            <form id="message-status-{{ $complain->id }}" action="{{ route('admin.message.status.get', [$complain->id]) }}" method="POST" style="display: none;">
                                                @csrf
                                                <input type="hidden" name="message_status" id="text-{{ $complain->id }}">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.message_status').on('change', function () {
                var message_id = $(this).data('id');
                var status = $(this).val();
                $('#text-' + message_id).val(status);
                $('#message-status-' + message_id).submit();
            });
        });
    </script>
@endsection

