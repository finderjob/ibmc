@extends('layouts.default')

@section('content')
    <h1 class="page-title">Customers</h1>
    <div class="row">
        <div class="col-md-6 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Customer Add</span>
                        <span class="" id="icon-cat-prev"></span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('admin.customers.post') }}" method="post" role="form" class="customer-create-form">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Username</label>
                                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Username">
                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Email address</label>
                                    <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} input-icon" placeholder="Email address">
                                </span>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>First name</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }} input-icon" placeholder="First name">
                                </span>
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Last name</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }} input-icon" placeholder="Last name">
                                </span>
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Password</label>
                                    <input type="text" name="password" id="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} input-icon" placeholder="Password">
                                </span>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Customer type</label>
                                    <select class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} " name="customer_type">
                                        <option value="0" selected="selected" disabled >Select customer type</option>
                                        <option value="1">Personal</option>
                                        <option value="2">School</option>
                                        <option value="3">Authors</option>
                                        <option value="4">Bookshop</option>
                                    </select>
                                </span>
                                @if ($errors->has('customer_type'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('customer_type') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="{{ route('admin.customers') }}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#slug').focusout(function ($e) {
                var keyword = $(this).val();
                slugCreate(keyword);
            });
            $('#name').focusout(function ($e) {
                var keyword = $(this).val();
                slugCreate(keyword);
            });

        });
        function slugCreate(keyword){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('generate-slug') }}',
                type: "post",
                data: {_token: _token, keyword:keyword},
                success: function (data) {
                    console.log(data);
                    $('#slug').val(data);
                    $('.swing-preloader').css('display','none');
                    $('#slug').closest('.form-group').removeClass('has-error');
                    $('#slug-error').remove();
                }
            });
        }
    </script>
@endsection
