<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse" aria-expanded="false" style="height: 0px;">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <?php $route_name = \Request::route()->getName();
                    $user = Auth::guard('admins')->user();
                    $roles = $user->getRoleNames();
            ?>
            <?php  $dashboard_routes_names = array('admin.home');?>
            <li class="nav-item <?php echo (in_array($route_name, $dashboard_routes_names)) ? "start active open" : ""?>">
                <a href="{{ route('admin.home') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <?php echo (in_array($route_name, $dashboard_routes_names)) ? "<span class='selected'></span>" : ""?>
                </a>
            </li>
            @if($user->hasAnyPermission('user list'))
                <li class="heading">
                    <h3 class="uppercase">User Management</h3>
                </li>
                <?php  $user_routes_names = array('admin.list','admin.users.add');?>
                <li class="nav-item <?php echo (in_array($route_name, $user_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.list') }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Users</span>
                        <?php echo (in_array($route_name, $user_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                @if($roles[0] == 'super-admin')
                <?php  $role_routes_names = array('admin.role');?>
                <li class="nav-item <?php echo (in_array($route_name, $role_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.role') }}" class="nav-link nav-toggle">
                        <i class="fa fa-user-secret"></i>
                        <span class="title">Roles</span>
                        <?php echo (in_array($route_name, $role_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                @endif
            @endif
                <li class="heading">
                    <h3 class="uppercase">Customer Management</h3>
                </li>
                <?php  $customer_routes_names = array('admin.customers', 'admin.customers.get', 'admin.customers.edit.get');?>
                <li class="nav-item <?php echo (in_array($route_name, $customer_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.customers') }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Customers</span>
                        <?php echo (in_array($route_name, $customer_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $author_routes_names = array('admin.authors', 'admin.authors.add.get', 'admin.authors.edit.get');?>
                <li class="nav-item <?php echo (in_array($route_name, $author_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.authors') }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Authors</span>
                        <?php echo (in_array($route_name, $author_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $customer_routes_names = array('admin.message.get');?>
                <li class="nav-item <?php echo (in_array($route_name, $customer_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.message.get') }}" class="nav-link nav-toggle">
                        <i class="fa fa-envelope"></i>
                        <span class="title">Message</span>
                        <?php echo (in_array($route_name, $customer_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <li class="heading">
                    <h3 class="uppercase">Product Management</h3>
                </li>
                <?php  $category_routes_names = array('admin.category');?>
                <li class="nav-item <?php echo (in_array($route_name, $category_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.category') }}" class="nav-link nav-toggle">
                        <i class="fa fa-list"></i>
                        <span class="title">Categories</span>
                        <?php echo (in_array($route_name, $category_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $subcategory_routes_names = array('admin.sub-categories','admin.sub-categories.get');?>
                <li class="nav-item <?php echo (in_array($route_name, $subcategory_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.sub-categories') }}" class="nav-link nav-toggle">
                        <i class="fa fa-list-alt"></i>
                        <span class="title">Sub Categories</span>
                        <?php echo (in_array($route_name, $subcategory_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $product_routes_names = array('admin.products');?>
                <li class="nav-item <?php echo (in_array($route_name, $product_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.products') }}" class="nav-link nav-toggle">
                        <i class="fa fa-book"></i>
                        <span class="title">Products</span>
                        <?php echo (in_array($route_name, $product_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $coupon_routes_names = array('admin.coupons','admin.coupons.add.get');?>
                <li class="nav-item <?php echo (in_array($route_name, $coupon_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.coupons') }}" class="nav-link nav-toggle">
                        <i class="fa fa-tags"></i>
                        <span class="title">Coupons</span>
                        <?php echo (in_array($route_name, $coupon_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                @if($user->hasAnyPermission('order access'))
                <li class="heading">
                    <h3 class="uppercase">Order Management</h3>
                </li>
                <?php  $order_routes_names = array('admin.orders','admin.orders.view');?>
                <li class="nav-item <?php echo (in_array($route_name, $order_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.orders') }}" class="nav-link nav-toggle">
                        <i class="fa fa-circle"></i>
                        <span class="title">Orders</span>
                        <?php echo (in_array($route_name, $order_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $order_routes_names = array('admin.orders.old','admin.orders.old.view');?>
                <li class="nav-item <?php echo (in_array($route_name, $order_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.orders.old') }}" class="nav-link nav-toggle">
                        <i class="fa fa-circle"></i>
                        <span class="title">Old Orders</span>
                        <?php echo (in_array($route_name, $order_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                @endif
                <?php  $payment_routes_names = array('admin.payments');?>
                <li class="nav-item <?php echo (in_array($route_name, $payment_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.payments') }}" class="nav-link nav-toggle">
                        <i class="fa fa-money"></i>
                        <span class="title">Payments</span>
                        <?php echo (in_array($route_name, $payment_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <li class="heading">
                    <h3 class="uppercase">Settings</h3>
                </li>
                <?php  $pages_routes_names = array('admin.settings');?>
                <li class="nav-item <?php echo (in_array($route_name, $pages_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.settings') }}" class="nav-link nav-toggle">
                        <i class="fa fa-gears"></i>
                        <span class="title">Settings</span>
                        <?php echo (in_array($route_name, $pages_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $pages_routes_names = array('admin.pages');?>
                <li class="nav-item <?php echo (in_array($route_name, $pages_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.pages') }}" class="nav-link nav-toggle">
                        <i class="fa fa-circle"></i>
                        <span class="title">Home Page</span>
                        <?php echo (in_array($route_name, $pages_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $pages_routes_names = array('admin.pages.about-us');?>
                <li class="nav-item <?php echo (in_array($route_name, $pages_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.pages.about-us') }}" class="nav-link nav-toggle">
                        <i class="fa fa-circle"></i>
                        <span class="title">About us Page</span>
                        <?php echo (in_array($route_name, $pages_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $pages_routes_names = array('admin.pages.contact-us');?>
                <li class="nav-item <?php echo (in_array($route_name, $pages_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.pages.contact-us') }}" class="nav-link nav-toggle">
                        <i class="fa fa-circle"></i>
                        <span class="title">Contact us Page</span>
                        <?php echo (in_array($route_name, $pages_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $pages_routes_names = array('admin.pages.image');?>
                <li class="nav-item <?php echo (in_array($route_name, $pages_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.pages.image') }}" class="nav-link nav-toggle">
                        <i class="fa fa-image"></i>
                        <span class="title">Image</span>
                        <?php echo (in_array($route_name, $pages_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $pages_routes_names = array('admin.pages.team.members');?>
                <li class="nav-item <?php echo (in_array($route_name, $pages_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.pages.team.members') }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Team Members</span>
                        <?php echo (in_array($route_name, $pages_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
                <?php  $pages_routes_names = array('admin.pages.social.media');?>
                <li class="nav-item <?php echo (in_array($route_name, $pages_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.pages.social.media') }}" class="nav-link nav-toggle">
                        <i class="icon-bell"></i>
                        <span class="title">Social Media</span>
                        <?php echo (in_array($route_name, $pages_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>

                <li class="heading">
                    <h3 class="uppercase">Blog</h3>
                </li>
                <?php  $blogs_routes_names = array('admin.blogs','admin.blogs.edit.get');?>
                <li class="nav-item <?php echo (in_array($route_name, $blogs_routes_names)) ? "start active open" : ""?>">
                    <a href="{{ route('admin.blogs') }}" class="nav-link nav-toggle">
                        <i class="fa fa-circle"></i>
                        <span class="title">Blogs</span>
                        <?php echo (in_array($route_name, $blogs_routes_names)) ? "<span class='selected'></span>" : ""?>
                    </a>
                </li>
        </ul>
    </div>
</div>
