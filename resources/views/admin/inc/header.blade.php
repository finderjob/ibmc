<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ route('admin.home') }}">
                <img src="{{ asset('images/logo3.png') }}" alt="{{ config('app.name', 'Finder.lk') }}" class="logo-default"> </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="#" class="dropdown-toggle" dropdown-menu-hover data-toggle="dropdown" data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-default" id="notification-count"  hidden></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                Notifications</h3>
                            <a href="{{ route('admin.notification.list') }}">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: auto;" data-handle-color="#637283" id="notification-list">
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                        @if(file_exists(Auth::guard('admins')->user()->pro_pic_path))
                            <img src="{{ asset(Auth::guard('admins')->user()->pro_pic_path) }}" class="img-circle" alt="">
                        @else
                            <img src="{{ asset('images/users/profile_user.jpg') }}" class="img-circle" alt="">
                        @endif
                        <span class="username username-hide-on-mobile"> {{ Auth::guard('admins')->user()->name }} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{ route('admin.profile') }}">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="{{ route('admin.logout.post') }}" class="" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="icon-logout"></i> Log out
                            </a>
                            <form id="logout-form" action="{{ route('admin.logout.post') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('admin.logout.post') }}" class="dropdown-toggle" onclick="event.preventDefault();
                                                     document.getElementById('logout-form-2').submit();">
                        <i class="icon-logout"></i>
                    </a>
                    <form id="logout-form-2" action="{{ route('admin.logout.post') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>
<input type="hidden" id="last_notification_id">
<script>
    $(document).ready(function(){
        notification();
        setInterval(notification, 30000);
    });

    function notification() {
        var last_id = $('#last_notification_id').val();
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: '{{ route('admin.notification.get') }}',
            data: {last_id: last_id, _token: _token},
            success: function (data) {
                var dataObj = jQuery.parseJSON(data);
                $('#last_notification_id').val(dataObj.last_id);
                if(dataObj.count == 0){
                    $('#notification-count').hide();
                }else{
                    $('#notification-count').html(dataObj.count).show();
                }
                var ul = $('#notification-list');
                if(dataObj.notications){
                    if(last_id){
                        $('#notify-' + last_id).before(dataObj.notications);
                    }else{
                        ul.append(dataObj.notications);
                    }
                }
            }
        });
    }
</script>
