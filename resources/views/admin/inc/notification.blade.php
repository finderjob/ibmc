@foreach($notifications as $notification)
    <li id="notify-{{$notification->id}}" data-id="{{ $notification->id }}" class="notification {{ ($notification->read_status == 0)? 'unread': '' }}">
        <?php   if($notification->read_status == 0){
            $url = route('admin.notification.read', [$notification->id]);
        }else{
            $url = $notification->url;
        }
        ?>
        <a href="{{ $url }}">
            <span class="time">{{ notificationTimeDiff($notification->created_at) }}</span>
            <span class="details">
                    <?php echo json_decode($notification->data); ?>
            </span>
        </a>
    </li>
@endforeach
