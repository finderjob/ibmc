@extends('layouts.default')

@section('content')
    <h1 class="page-title">Users</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Users List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.users.add') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>User Id</th>
                                        <th> Username </th>
                                        <th> First Name </th>
                                        <th> Last Name </th>
                                        <th> Email </th>
                                        <th> User type </th>
                                        <th> Status </th>
                                        <th> Joined </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user_list as $ul)
                                        <tr>
                                            <td><a href="{{ route('admin.profile.id',[$ul->id]) }}">{{ $ul->id }}</a> </td>
                                            <td><a href="{{ route('admin.profile.id',[$ul->id]) }}">{{ $ul->name }}</a> </td>
                                            <td>{{ $ul->first_name }}</td>
                                            <td>{{ $ul->last_name }}</td>
                                            <td><a href="mailto:{{ $ul->email }}"> {{ $ul->email }}</a></td>
                                            <td>
                                                @if($ul->id == Auth::guard('admins')->user()->id)
                                                    @if($ul->user_type == 1)
                                                        <span class="label label-sm label-success"> Super Admin </span>
                                                    @else
                                                        <span class="label label-sm label-success"> Admin </span>
                                                    @endif
                                                @else
                                                <select id="user_type_{{ $ul->id }}" data-id="{{ $ul->id }}" data-prev="{{ $ul->user_type }}" class="user_type form-control">
                                                    <option value="1" @if($ul->user_type == 1) selected @endif>Super Admin</option>
                                                    <option value="2" @if($ul->user_type == 2) selected @endif>Admin</option>
                                                    <option value="3" @if($ul->user_type == 3) selected @endif>User</option>
                                                </select>
                                                @endif
                                            </td>
                                            <td>
                                                @if($ul->id == Auth::guard('admins')->user()->id)
                                                    @if($ul->status)
                                                        <span class="label label-sm label-success"> Activate </span>
                                                    @else
                                                        <span class="label label-sm label-danger"> Deactivate </span>
                                                    @endif
                                                @else
                                                    <select id="user_status_{{ $ul->id }}" data-id="{{ $ul->id }}" data-prev="{{ $ul->status }}" class="user_status form-control">
                                                        <option value="1" @if($ul->status == 1) selected @endif>Activate</option>
                                                        <option value="0" @if($ul->status == 0) selected @endif>Deactivate</option>
                                                    </select>
                                                @endif
                                            </td>
                                            <td>{{ date("Y-m-d", strtotime($ul->created_at)) }}</td>
                                            <td>
                                                    <a class="btn btn-circle btn-icon-only btn-primary" title="Reset Password" href="{{ route('admin.users.reset-password',$ul->id) }}" onclick="event.preventDefault();
                                                            document.getElementById('user-reset-password-{{ $ul->id }}').submit();">
                                                        <i class="fa fa-arrow-circle-right"></i>
                                                    </a>
                                                    <form id="user-reset-password-{{ $ul->id }}" action="{{ route('admin.users.reset-password',$ul->id) }}" method="get" style="display: none;">
                                                        @csrf
                                                    </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.user_type').on('change',function(){
                var user_type = $(this).val();
                var user_id = $(this).data('id');
                var prev_type = $(this).data('prev');
                changeUserType(user_id, user_type,prev_type);
            });
            $('.user_status').on('change',function(){
                var user_status = $(this).val();
                var user_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeUserStatus(user_id, user_status,prev_status);
            });
            function changeUserType(user_id, user_type,prev_type){
                var _token = $('input[name="_token"]').val();
                $('.swing-preloader').css('display','block');

                $.ajax({
                    url: '{{ route('admin.users.change.userType') }}',
                    type: "post",
                    data: {_token: _token, user_type: user_type, user_id: user_id},
                    success: function (data) {
                        var return_data = jQuery.parseJSON(data);
                        if(return_data.status == 1){
                            toastr.success( return_data.message, 'Success!');
                        }else{
                            $('#user_type_'+ user_id).val(prev_status);
                            toastr.error( return_data.message, 'Failed!');
                        }
                        $('.swing-preloader').css('display','none');
                    }
                });
            }

            function changeUserStatus(user_id, user_status,prev_status){
                var _token = $('input[name="_token"]').val();
                $('.swing-preloader').css('display','block');
                $.ajax({
                    url: '{{ route('admin.users.change.userStatus') }}',
                    type: "post",
                    data: {_token: _token, user_status: user_status, user_id: user_id},
                    success: function (data) {
                        var return_data = jQuery.parseJSON(data);
                        if(return_data.status == 1){
                            toastr.success( return_data.message, 'Success!');
                        }else{
                            $('#user_status_'+ user_id).val(prev_status);
                            toastr.error( return_data.message, 'Failed!');
                        }
                        $('.swing-preloader').css('display','none');
                    }
                });
            }
        });
    </script>
@endsection
