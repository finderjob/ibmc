@extends('layouts.default')

@section('content')
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title">{{ $user->name }} |
            <small>User Profile</small>
        </h1>
        <!-- END PAGE TITLE-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light profile-sidebar-portlet ">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            @if(file_exists($user->pro_pic_path))
                                <img src="{{ asset($user->pro_pic_path) }}" class="img-responsive" alt="">
                            @else
                                <img src="{{ asset('images/users/profile_user.jpg') }}" class="img-responsive" alt="">
                            @endif
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> {{ $user->first_name }} {{ $user->last_name }}</div>
                            <div class="profile-usertitle-job">  </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="active">
                                    <a href="#">
                                        <i class="icon-settings"></i> Account Settings </a>
                                </li>
                            </ul>
                        </div>
                        @if($user->hasAnyPermission('user list'))
                            <a href="{{ route('admin.list') }}" class="btn green">Back to Users List</a>
                        @endif
                        <!-- END MENU -->
                    </div>
                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light ">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="@if($section == null || $section == 'personal_info') active @endif">
                                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                        </li>
                                        <li class="@if($section == 'change_avatar') active @endif">
                                            <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                        </li>
                                        @if(Auth::guard('admins')->user()->id == $user->id)
                                        <li class="@if($section == 'change_password') active @endif">
                                            <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <!-- PERSONAL INFO TAB -->

                                        <div class="tab-pane @if($section == null || $section == 'personal_info') active @endif" id="tab_1_1">
                                            <form role="form" action="{{ route('admin.profile.update',[$user->id]) }}" method="post" class="update-profile-info">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <span class="form-error">
                                                        <label class="control-label">First Name *</label>
                                                        <div class="input-group">
                                                            <input type="text" name="first_name" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="First Name" value="{{ $user->first_name }}">
                                                            <span class="input-group-addon">
                                                                 <i class="fa fa-user"></i>
                                                            </span>
                                                        </div>
                                                    </span>
                                                    @if ($errors->has('first_name'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('first_name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                        <span class="form-error">
                                                                <label class="control-label">Last Name *</label>
                                                                <div class="input-group">
                                                                    <input type="text" name="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="Last Name" value="{{ $user->last_name }}">
                                                                    <span class="input-group-addon">
                                                                         <i class="fa fa-user"></i>
                                                                    </span>
                                                                </div>
                                                            </span>
                                                        @if ($errors->has('last_name'))
                                                            <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group">
                                                    <span class="form-error">
                                                        <label class="control-label">Email *</label>
                                                        <div class="input-group">
                                                            <input type="text" name="email" readonly class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Last Name" value="{{ $user->email }}">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-envelope"></i>
                                                            </span>
                                                        </div>
                                                    </span>
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <span class="form-error">
                                                        <label class="control-label">Birth Day </label>
                                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="0d">
                                                            <input type="text" name="bod" class="form-control {{ $errors->has('bod') ? ' is-invalid' : '' }}" placeholder="Birth Day" value="{{ $user->bod }}">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-birthday-cake"></i>
                                                            </span>
                                                        </div>
                                                    </span>
                                                    @if ($errors->has('bod'))
                                                        <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('bod') }}</strong>
                                                                </span>
                                                    @endif
                                                </div>
                                                <div class="margiv-top-10">
                                                    <button type="submit" class="btn green"> Save Changes </button>
                                                    <a href="javascript:;" data-section="personal_info" class="cancel_btn btn default"> Cancel </a>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- END PERSONAL INFO TAB -->
                                        <!-- CHANGE AVATAR TAB -->
                                        <div class="tab-pane @if($section == 'change_avatar') active @endif" id="tab_1_2">
                                            <form action="{{ route('admin.profile.propic', [$user->id ]) }}" method="post" role="form" enctype="multipart/form-data">
                                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                                <span class="fileinput-new"> Select image </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="propic"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">NOTE! </span>
                                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                    </div>
                                                </div>
                                                <div class="margin-top-10">
                                                    <button type="submit" class="btn green"> Submit </button>
                                                    <a href="javascript:;" data-section="change_avatar" class="cancel_btn btn default"> Cancel </a>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- END CHANGE AVATAR TAB -->
                                        @if(Auth::guard('admins')->user()->id == $user->id)
                                        <!-- CHANGE PASSWORD TAB -->
                                        <div class="tab-pane @if($section == 'change_password') active @endif" id="tab_1_3">
                                            <form action="{{ route('admin.users.change.password') }}" method="post" class="change-password-form">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <span class="form-error">
                                                        <label for="exampleInputPassword1">Current Password *</label>
                                                        <div class="input-group">
                                                            <input type="password" name="current_password" class="form-control {{ $errors->has('current_password') ? ' is-invalid' : '' }}" placeholder="Current Password">
                                                            <span class="input-group-addon">
                                                                 <i class="fa fa-lock"></i>
                                                            </span>
                                                        </div>
                                                    </span>
                                                    @if ($errors->has('current_password'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('current_password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <span class="form-error">
                                                        <label for="exampleInputPassword1">New Password *</label>
                                                        <div class="input-group">
                                                            <input type="password" id="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="New Password">
                                                            <span class="input-group-addon">
                                                                 <i class="fa fa-lock"></i>
                                                            </span>
                                                        </div>
                                                    </span>
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <span class="form-error">
                                                        <label for="exampleInputPassword1">Confirm New Password *</label>
                                                        <div class="input-group">
                                                            <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="Confirm New Password">
                                                            <span class="input-group-addon">
                                                                 <i class="fa fa-lock"></i>
                                                            </span>
                                                        </div>
                                                    </span>
                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="margin-top-10">
                                                    <button type="submit" class="btn green"> Change Password </button>
                                                    <a href="javascript:;" data-section="change_password" class="cancel_btn btn default"> Cancel </a>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- END CHANGE PASSWORD TAB -->
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
    <script>
        $('.cancel_btn').click(function() {
            var section = $(this).data('section');
            $.ajax({
                url: '{{ route('admin.profile.session') }}',
                type: 'get',
                data: {profile_section : section},
                success: function (data) {
                    location.reload();
                }
            })

        });
    </script>
@endsection
