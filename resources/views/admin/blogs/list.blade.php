@extends('layouts.default')

@section('content')
    <h1 class="page-title">Blogs</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Blog List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.blogs.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>Blog Id</th>
                                        <th> Title </th>
                                        <th> Created By</th>
                                        <th> Created At</th>
                                        <th> Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($blogs as $blog)
                                        <tr>
                                            <td>{{ $blog->id }}</td>
                                            <td>{{ $blog->name }}</td>
                                            <td>{{ \App\User::find($blog->created_by)->name }}</td>
                                            <td>{{ $blog->created_at }}</td>
                                            <td>
                                                <a class="btn btn-circle btn-icon-only btn-default" href="{{ route('admin.blogs.edit.get',[$blog->id]) }}" title="edit">
                                                    <i class="icon-wrench"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.coupon_status').on('change',function(){
                var coupon_status = $(this).val();
                var coupon_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeCouponStatus(coupon_id, coupon_status,prev_status);
            });
        });
        function changeCouponStatus(coupon_id, coupon_status, prev_status){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');

            $.ajax({
                url: '{{ route('admin.coupons.change.status') }}',
                type: "post",
                data: {_token: _token, coupon_status: coupon_status, coupon_id: coupon_id},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#coupon_status_'+ user_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');
                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
