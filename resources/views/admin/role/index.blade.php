@extends('layouts.default')

@section('content')
    <h1 class="page-title">Users</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">User Role</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{ route('admin.assign.permission') }}" method="post" role="form" class="">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <span class="form-error">
                                        <label>User Role</label>
                                        <?php $user = session()->get('user_type')?>
                                        <select id="user_type" name="user_type" class="form-control {{ $errors->has('user_type') ? ' is-invalid' : '' }}">
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}" @if($role->id == $user) selected @endif>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </span>
                                    @if ($errors->has('user_type'))
                                        <span class="invalid-feedback">
                                                <strong>{{ $errors->first('user_type') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <span class="form-error">
                                        <label>Permissions List</label>
                                        <ul class="checkboxes permission-list">
                                                @foreach($permissions as $permission)
                                                    <li>
                                                        <input id="permission-{{ $permission->id }}" class="check-permission"  type="checkbox" name="check_permssion[]" value="{{ $permission->id }}">
                                                        <label class="m-checkbox m-checkbox--solid" for="permission-{{ $permission->id }}">{{ $permission->name }}</label>
                                                    </li>
                                                @endforeach
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .permission-list{
            left: 10px;
            padding: 5px;
            overflow-y: scroll;
            max-height: 500px;
        }
        .permission-list li{
            list-style: none;
        }
        .permission-list li label{
            margin-left: 20px;
        }
        .checkboxes input[type=checkbox] {
            display: none;
        }
        .checkboxes input[type=checkbox]:checked + label:before {
            content: "";
            font-family: "FontAwesome";
            font-size: 12px;
            color: #fff;
            text-align: center;
            line-height: 15px;
            background-color: #505050;
            margin-right: 4px;
            border: 2px solid transparent;
        }
        .checkboxes label:before {
            content: "";
            display: inline-block;
            width: 19px;
            height: 19px;
            margin-right: 10px;
            position: absolute;
            left: 15px;
            background-color: #fff;
            border: 2px solid #d0d0d0;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            border-radius: 3px;
        }
    </style>
    <script>
        $(document).ready(function(){
            var user_type = $('#user_type').val();
            getAssignPermission(user_type);
            $('#user_type').on('change',function(){
                var user_type = $(this).val();
                getAssignPermission(user_type);
            });
        });

        function getAssignPermission(user_type){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('role.permission') }}',
                type: "post",
                data: {_token: _token, user_type:user_type},
                success: function (data) {
                    $.each($(".check-permission"), function(){
                        console.log($.inArray(parseInt(this.value), data));
                        if($.inArray(parseInt(this.value), data) != -1) {
                            this.checked = true;
                        }else{
                            this.checked = false;
                        }
                    });
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
