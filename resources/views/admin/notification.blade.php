@extends('layouts.default')

@section('content')
    <h1 class="page-title">Notifications</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                <tr role="row">
                                    <th>Notification</th>
                                    <th> Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($notifications as $notification)
                                        <tr class="{{ ($notification->read_status == 0)? 'unread': '' }}">
                                            <td>
                                                <?php   if($notification->read_status == 0){
                                                            $url = route('admin.notification.read', [$notification->id]);
                                                        }else{
                                                            $url = $notification->url;
                                                        }
                                                ?>
                                                <a href="{{ $url }}">
                                                <?php echo json_decode($notification->data)?>
                                                </a>
                                            </td>
                                            <td>{{ notificationTimeDiff($notification->created_at) }}</td>

                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
