@extends('layouts.default')

@section('content')
    <h1 class="page-title">Payment</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Payment List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.payments.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="order_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th> Payment Id</th>
                                        <th> Payment Reference </th>
                                        <th> Order Id </th>
                                        <th> Order Reference </th>
                                        <th> Payment </th>
                                        <th> Payment Type</th>
                                        <th> Status </th>
                                        <th> Create User</th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $payment_statuses = config('status.payment_status');
                                        usort($payment_statuses, 'sortByOrder');
                                        $payment_types = config('status.payment_types');
                                        usort($payment_types, 'sortByOrder');
                                    ?>
                                    @foreach($payments as $payment)
                                        <tr>
                                            <td>{{ $payment->id }}</td>
                                            <td>{{ $payment->payment_id }}</td>
                                            <td>{{ $payment->order_id }}</td>
                                            <td>{{ $payment->order_reference }}</td>
                                            <td>{{ final_price_format($payment->payment) }}</td>
                                            <td>
                                                @foreach($payment_types as $payment_type)
                                                    @if($payment->payment_type == $payment_type['id'])
                                                        <label class="label {{ $payment_type['class'] }}">{{ $payment_type['name'] }}</label>
                                                    @endif
                                                @endforeach
                                            </td>
                                            <td>
                                                @if($payment->payment_type == 1 && $payment->status == 0)
                                                    <select id="payment_status_{{ $payment->id }}" data-id="{{ $payment->id }}" data-prev="{{ $payment->status }}" class="payment_status form-control">
                                                        <option value="0" selected disabled> Select Payment status </option>
                                                        @foreach($payment_statuses as $payment_status)
                                                            <option value="{{ $payment_status['id'] }}" @if($payment->status == $payment_status['id']) selected @endif> {{ $payment_status['name'] }} </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    @foreach($payment_statuses as $payment_status)
                                                        @if($payment_status['id'] == $payment->status)
                                                            <label class="label {{ $payment_status['class'] }}">{{ $payment_status['name'] }}</label>
                                                        @endif
                                                    @endforeach
                                                @endif

                                            </td>
                                            <td>{{ \App\User::find($payment->user_id)->name }}</td>
                                            <td>
                                                @if($payment->payment_type == 1)
                                                    <a class="btn btn-circle btn-icon-only btn-danger" title="Delete" href="{{ route('admin.payment.deactivate',$payment->id) }}"onclick="event.preventDefault();
                                                        document.getElementById('payment-deactivate-{{ $payment->id }}').submit();">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                    <form id="payment-deactivate-{{ $payment->id }}" action="{{ route('admin.payment.deactivate',$payment->id) }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.payment_status').on('change',function(){
                var payment_status = $(this).val();
                var payment_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changePaymentStatus(payment_id, payment_status,prev_status);
            });
        });
        function changePaymentStatus(payment_id, payment_status,prev_status){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('admin.payment.change.status') }}',
                type: "post",
                data: {_token: _token,payment_id:payment_id, payment_status: payment_status},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#payment_status_'+ payment_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');

                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
