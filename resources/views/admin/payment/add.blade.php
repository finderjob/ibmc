@extends('layouts.default')

@section('content')
    <h1 class="page-title">Payments</h1>
    <div class="row">
        <div class="col-md-6 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Payments Add</span>
                        <span class="" id="icon-cat-prev"></span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('admin.payments.post') }}" method="post" role="form" class="payment-create-form">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Order Id</label>
                                    <select class="form-control {{ $errors->has('order_id') ? ' is-invalid' : '' }}" name="order_id" id="order_id">
                                        @foreach($orders as $order)
                                            <option value="{{ $order->id }}">{{ $order->order_id }}</option>
                                        @endforeach
                                    </select>
                                </span>
                                @if ($errors->has('order_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('order_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Order total :</label> <span id="order_total"> {{ final_price_format(0) }}</span><br>
                                <label>Paid :</label> <span id="paid_total"> {{ final_price_format(0) }}</span><br>
                                <label>Remaining Payment :</label> <span id="remaining_payment"> {{ final_price_format(0) }}</span><br>
                                <label class="refund">Maximum Refund Payment :</label> <span class="refund" id="refund_payment"> {{ final_price_format(0) }}</span>
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Payment value</label>
                                    <input type="number" min="0.01" step="0.01" name="payment_value" id="payment_value" class="form-control {{ $errors->has('payment_value') ? ' is-invalid' : '' }} input-icon" placeholder="Value">
                                </span>
                                @if ($errors->has('payment_value'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('payment_value') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="{{ route('admin.payments') }}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
               var order_id = $('#order_id').val();
                remaining(order_id);
               $('#order_id').on('change', function () {
                   var order_id = $(this).val();
                   remaining(order_id);
               });
        });

        function remaining(order_id){
            $('.swing-preloader').css('display','block');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: '{{ route('admin.payments.remain.post') }}',
                type: "post",
                data: {_token: _token,order_id:order_id},
                success: function (data) {
                    $('#order_total').html(data[0]);
                    $('#remaining_payment').html(data[1]);
                    $('#refund_payment').html(data[2]);
                    $('#paid_total').html(data[3]);
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
