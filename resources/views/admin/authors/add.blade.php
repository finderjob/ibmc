@extends('layouts.default')

@section('content')
    <h1 class="page-title">Authors</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Author Add</span>
                        <span class="" id="icon-cat-prev"></span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('admin.authors.add.post') }}" method="post" role="form" class="author-create-form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Name">
                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Slug</label>
                                    <input type="text" name="slug" id="slug" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }} input-icon" placeholder="Slug">
                                </span>
                                @if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="author_image" id="home-slider"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>

                                        @if ($errors->has('author_image'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('author_image') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span>Image minimum width: 300px <br> Image minimum height: 373px</span>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span class="error-log hide">
                                        <span class="label label-danger">Error! </span>
                                        <span class="error-data"></span>
                                    </span>
                                    <br>
                                    <span class="label label-warning">NOTE! </span>
                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                </div>
                            </div>
                            <?php $social_medias = config('status.social_media'); ?>
                            @foreach($social_medias as $social_media)
                                <div class="form-group">
                                <span class="">
                                    <label>Author {{ $social_media['value'] }} url</label>
                                    <input type="text" name="author_{{ $social_media['value'] }}" class="form-control" placeholder="https://www.{{ $social_media['value'] }}.com/">
                                </span>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="submitBtn" class="btn blue" disabled="true">Submit</button>
                            <a href="{{ route('admin.category') }}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#slug').focusout(function ($e) {
                var keyword = $(this).val();
                slugCreate(keyword);
            });
            $('#name').focusout(function ($e) {
                var keyword = $(this).val();
                slugCreate(keyword);
            });
            var _URL = window.URL || window.webkitURL;
            var width = 300;
            var height = 373;
            $("#home-slider").change(function (e) {
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function () {
                        if(this.width >= width && this.height >= height){
                            $('.error-log').removeClass('show');
                            $('.error-log').addClass('hide');
                            $('.error-data').html('');
                            $('#submitBtn').removeAttr('disabled');
                        }else{
                            $('.error-log').addClass('show');
                            $('.error-log').removeClass('hide');
                            $('.error-data').html('Image resolution should be ' + width +"X" +height);
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }
            });
        });
        function slugCreate(keyword){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('generate-slug') }}',
                type: "post",
                data: {_token: _token, keyword:keyword},
                success: function (data) {
                    console.log(data);
                    $('#slug').val(data);
                    $('.swing-preloader').css('display','none');
                    $('#slug').closest('.form-group').removeClass('has-error');
                    $('#slug-error').remove();
                }
            });
        }
    </script>
@endsection
