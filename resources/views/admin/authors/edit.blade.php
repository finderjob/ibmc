@extends('layouts.default')

@section('content')
    <h1 class="page-title">Author</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Author Edit :: {{ $author->id }}</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('admin.authors.edit.post',[$author->id]) }}" method="post" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="image_previous_url" id="home-slider-edit-file" value="{{ $author->image_path }}">
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" value="{{ $author->name }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Name">
                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Slug</label>
                                    <input type="text" name="slug" id="slug" value="{{ $author->slug }}" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }} input-icon" placeholder="Slug">
                                </span>
                                @if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control">{{ json_decode($author->description) }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                            <div class="form-group">
                                <div class="fileinput @if($author->image_path) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                        @if($author->image_path) <img src="{{ asset($author->image_path) }}" style="max-height: 140px;"> @endif
                                    </div>
                                    <div>
                                         <span class="btn default btn-file">
                                             <span class="fileinput-new"> Select image </span>
                                             <span class="fileinput-exists"> Change </span>
                                             <input type="file" name="author_image" id="home-slider">
                                         </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" id="remove-image" data-dismiss="fileinput"> Remove </a>
                                        @if ($errors->has('author_image'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('author_image') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span>Image minimum width: 300px <br> Image minimum height: 373px</span>
                                </div>
                                <div class="clearfix margin-top-10">
                                    <span class="error-log hide">
                                        <span class="label label-danger">Error! </span>
                                        <span class="error-data"></span>
                                    </span>
                                    <br>
                                    <span class="label label-warning">NOTE! </span>
                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                </div>
                            </div>
                            <?php $social_medias = config('status.social_media');
                                $social_media_values = json_decode($author->social_media, true);
                            ?>
                            @foreach($social_medias as $social_media)
                                <?php  $value = '';
                                if(array_key_exists($social_media['value'], $social_media_values)){
                                    $value = $social_media_values[$social_media['value']];
                                } ?>
                                <div class="form-group">
                                <span class="">
                                    <label>Author {{ $social_media['value'] }} url</label>
                                    <input type="text" name="author_{{ $social_media['value'] }}" value="{{ $value }}" class="form-control" placeholder="https://www.{{ $social_media['value'] }}.com/">
                                </span>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="submitBtn" class="btn blue" @if(empty($author->image_path)) disabled="true" @endif>Submit</button>
                            <a href="{{ route('admin.category') }}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#slug').focusout(function ($e) {
                var keyword = $(this).val();
                var hidden_slug = $('#hidden_slug').val();
                if(keyword != hidden_slug)
                {
                    slugCreate(keyword);
                }
            });
            var _URL = window.URL || window.webkitURL;
            var width = 300;
            var height = 373;
            $("#home-slider").change(function (e) {
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function () {
                        console.log(this.width, this.height);
                        if(this.width >= width && this.height >= height){
                            $('.error-log').removeClass('show');
                            $('.error-log').addClass('hide');
                            $('.error-data').html('');
                            $('#btn-home-slider').removeAttr('disabled');
                            $('#home-slider-edit-file').val('');
                        }else{
                            $('.error-log').addClass('show');
                            $('.error-log').removeClass('hide');
                            $('.error-data').html('Image resolution should be ' + width +"X" +height);
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }
            });
            $('#remove-image').on('click', function(){
                $('#submitBtn').attr('disabled', 'disabled');
                $('#home-slider-edit-file').val('');
            });
        });
        function slugCreate(keyword){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('generate-slug') }}',
                type: "post",
                data: {_token: _token, keyword:keyword},
                success: function (data) {
                    console.log(data);
                    $('#slug').val(data);
                    $('.swing-preloader').css('display','none');
                    $('#slug').closest('.form-group').removeClass('has-error');
                    $('#slug-error').remove();
                }
            });
        }
    </script>
@endsection
