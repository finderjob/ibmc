@extends('layouts.default')

@section('content')
    <h1 class="page-title">Authors</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Author List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('admin.authors.add.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>Author Id</th>
                                        <th> Name </th>
                                        <th> Slug</th>
                                        <th> Status </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($authors as $author)
                                    <tr>
                                        <td>{{ $author->id }}</td>
                                        <td>{{ $author->name }}</td>
                                        <td>{{ $author->slug }}</td>
                                        <td>
                                            <select id="author_status_{{ $author->id }}" data-id="{{ $author->id }}" data-prev="{{ $author->status }}" class="author_status form-control">
                                                <option value="1" @if($author->status == 1) selected @endif>Activate</option>
                                                <option value="0" @if($author->status == 0) selected @endif>Deactivate</option>
                                            </select>
                                        </td>
                                        <td>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="{{ route('admin.authors.edit.get', [$author->id]) }}" title="edit">
                                                <i class="icon-wrench"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.author_status').on('change',function(){
                var author_status = $(this).val();
                var author_id = $(this).data('id');
                var prev_status = $(this).data('prev');
                changeAuthorStatus(author_id, author_status,prev_status);
            });
        });
        function changeAuthorStatus(author_id, author_status,prev_status){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('admin.author.change.status') }}',
                type: "post",
                data: {_token: _token,author_id:author_id, author_status: author_status},
                success: function (data) {
                    var return_data = jQuery.parseJSON(data);
                    if(return_data.status == 1){
                        toastr.success( return_data.message, 'Success!');
                    }else{
                        $('#author_status_'+ author_id).val(prev_status);
                        toastr.error( return_data.message, 'Failed!');

                    }
                    $('.swing-preloader').css('display','none');
                }
            });
        }
    </script>
@endsection
