@extends('layouts.admin')

@section('content')
<div class="login">
    <div class="container">
        <div class="logo-content">
            <img src="{{ asset('images/logo2.png') }}" alt="{{ config('app.name', 'Finder.lk') }}">
        </div>
        <div class="content">
            <form class="password-reset-form" action="{{ route('admin.users.password.reset') }}" method="post">
                {{ csrf_field() }}
                <h3 class="form-title">Password Reset</h3>
                @if(session()->has('error_message'))
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span> {{ session()->get('error_message') }} </span>
                    </div>
                @endif
                <div class="alert alert-danger hidden">
                    <button class="close" data-close="alert"></button>
                    <span> Enter email and password. </span>
                </div>
                <input type="hidden" name="token" value="{{ json_encode($token) }}">
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label hidden">Email</label>
                    <input class="form-control input-icon{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" autocomplete="off" placeholder="Email" name="email" value="{{ old('email') }}" required/>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label hidden">Password</label>
                    <input class="form-control input-icon{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" autocomplete="off" placeholder="Password" id="password" name="password" required/>
                    @if ($errors->has('password'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label hidden">Confirm Password</label>
                    <input class="form-control input-icon{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" type="password" autocomplete="off" placeholder="Confirm Password" name="password_confirmation" required/>
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn default-btn uppercase">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
