<?php
?>

    <!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
        .body-frame{
            width: 100%;
            height: auto;
        }
        .div-frame{
            width: 800px;
            margin-left: auto;
            margin-right: auto;
            position: relative;
            height: auto;
            background-color: #0000000f;
        }
        .content-frame{
            padding: 0 50px 25px 50px;
            position: relative;
            float: left;
            width: 700px;
            background-color: #0000000f;
        }
        .footer-frame{
            padding: 25px 50px 25px 50px;
            background-color: #08a6d5;
            position: relative;
            width: 700px;
            float: left;
            color:white;
            text-align: center
        }
        .content-frame p{
            color: #000000;
        }
        .head-title{
            margin-bottom: 40px;
            margin-top: -30px;
        }
        .head-title h1{
            text-align: center;
            color: #d50824;
        }
        .order-details{
            width: 100%;
            margin-bottom: 20px;
            padding-bottom: 40px;
        }
        .order-details .number{
            float: left;
            width: 50%;
            font-size: 19px;
        }
        .order-details .date{
            float: right;
            width: 50%;
            text-align: right;
            font-size: 19px;
        }
        .summary{
            width: 100%;
            margin-bottom: 10px;
        }
        .address{
            width: 100%;
            float: left;
            padding-bottom: 10px;
        }
        .billing{
            width: 50%;
            float: left;
        }
        .shipping{
            width: 50%;
            float: left;
        }
        .order-product{
            width: 100%;
        }
        .order-product table{
            width: 100%;
            border-collapse: collapse;
        }
        .order-product table tr{
            width: 100%;
        }
        .order-product table tr th{
            height: 30px;
        }
        table, th, td {
            border: 1px solid black;
        }
        .product{
            padding: 5px;
        }
        .product img{
            width: 100px;
        }
        .product span{}
        .footer-frame a{
            color: white;
        }
    </style>
</head>
<body>
<div class="body-frame">
    <div class="div-frame">
        <div class="header-frame">
            <img src="{{ asset('email/email_header.png') }}">
        </div>
        <div class="content-frame">
            <div class="head-title">
                <h1>Thank you for your order</h1>
            </div>
            <div class="order-details">
                <div class="number">
                    <strong>Order number:</strong> {{ $order->order_id }}
                </div>
                <div class="date">
                    <strong>Order date:</strong> {{ date('jS F Y', strtotime($order->created_at)) }}
                </div>
            </div>
            <div class="">
                <span>Hi {{ $order->billing_first_name }} {{ $order->billing_last_name }},</span>
            </div>
            <div class="summary">
                <p>Thank you for shopping with IBMC Publication, your order number is {{ $order->order_id }}. Your order will be dispatched from our warehouse soon, and we'll let you know once it's on its way to you.</p>
            </div>
            <div class="address">
                <div class="billing">
                    <p>
                        <strong>Customer details: </strong><br>
                        {{ $order->billing_first_name }} {{ $order->billing_last_name }} <br>
                        Category: {{ $customer_type }}<br>
                        {{ $order->billing_address_line_1 }} <br>
                        {{ $order->billing_address_line_2 }} <br>
                        {{ $order->billing_city }} <br>
                        {{ $order->billing_zip_code }}
                    </p>
                </div>
                <div class="shipping">
                    <p>
                        <strong>Delivery address: </strong><br>
                        {{ $order->shipping_first_name }} {{ $order->shipping_last_name }}<br>
                        {{ $order->shipping_address_line_1 }} <br>
                        {{ $order->shipping_address_line_2 }} <br>
                        {{ $order->shipping_city }} <br>
                        {{ $order->shipping_zip_code }}
                    </p>
                </div>
            </div>
            <div class="order-product">
                <table cellspacing="0">
                    <tr>
                        <th width="400">Order Details</th>
                        <th>Qty</th>
                        <th>Item price</th>
                        <th>Amount</th>
                    </tr>
                    @foreach($order_products as $order_product)
                        <tr>
                            <td>
                                <div class="product">
                                    <div style="width: 100px;position: relative;float: left;margin-bottom: 7px;">
                                        <img style="vertical-align: middle;" src="{{ asset($order_product->image_path) }}">
                                    </div>

                                    <div style="width: 250px; position: relative;float: left;padding: 12px 0 0 12px;">
                                        <p>
                                            {{ $order_product->products_name }} <br>
                                            By {{ $order_product->author_name }}
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td align="center">{{ $order_product->qty }}</td>
                            <td align="right" style="padding-right: 5px">
                                <?php
                                $amount = 0;
                                if($order_product->user_type == 2){
                                    $amount = $order_product->school_price - $order_product->school_discount;
                                }else{
                                    $amount = $order_product->normal_price - $order_product->normal_discount;
                                }
                                ?>
                                {{ final_price_format($amount) }}
                            </td>
                            <td align="right" style="padding-right: 5px">{{ final_price_format($amount * $order_product->qty) }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="3" style="padding-right: 5px;" align="right">Sub Total</th>
                        <th style="padding-right: 5px;" align="right">{{ final_price_format($order->sub_total) }}</th>
                    </tr>
                    <tr>
                        <th colspan="3" style="padding-right: 5px;" align="right">Discount</th>
                        <th style="padding-right: 5px;" align="right">{{ final_price_format($order->coupon_value) }}</th>
                    </tr>
                    <tr>
                        <th colspan="3" style="padding-right: 5px;" align="right">Total</th>
                        <th style="padding-right: 5px;" align="right">{{ final_price_format($order->order_total) }}</th>
                    </tr>
                </table>
            </div>

            <div class="">
                <p>Thank you for shopping with us</p>
                <p>IBMC Publication Customer Service Team</p>
            </div>
        </div>
        <div class="footer-frame" style="color:white; text-align: center">
            <p>To make sure our email updates are delivered to your inbox
                <br>Please add support@ibmcbooks.lk to your e-mail address book</p>
            <p>
                <span >Follow us
                    <img src="https://img.icons8.com/color/48/000000/facebook-circled.png" style="vertical-align: middle;">
                    <img src="https://img.icons8.com/color/48/000000/twitter-circled.png" style="vertical-align: middle;">
                    <img src="https://img.icons8.com/color/48/000000/instagram-new.png" style="vertical-align: middle;">
                </span>

            </p>
            <p><a href="">Contact us</a> | <a href="#">Terms & Conditions</a></p>
            <p>© IBMC Publication, IBMC Lanka (Pvt.) Ltd. All Rights Reserved</p>
        </div>
    </div>
</div>
</body>
</html>
