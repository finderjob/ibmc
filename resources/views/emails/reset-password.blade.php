<?php
?>

    <!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
        .body-frame{
            width: 100%;
            height: 500px
        }
        .div-frame{
            margin-left: auto;
            margin-right: auto;
            background-color: #0000000f;
            padding-top: 30px;
        }
        .header-frame{
            text-align: center;
        }
        .content-frame{
            padding-top: 25px;
            text-align: center;
        }
        .content-frame p {
            text-align: center;
        }
        .action-frame{
            padding-top: 25px;
            text-align: center;
            padding-bottom: 25px;
        }
        .action-frame a{
            cursor: pointer;
            padding: 10px 20px;
            display: inline-block;
            text-transform: uppercase;
            font-weight: 600;
            font-size: 14px;
            outline: none;
            overflow: hidden;
            position: relative;
            z-index: 10;
            color: #fff;
            background-color: #26ae61;
            border: none;
            -webkit-transition: all 0.2s ease-in-out;
            -moz-transition: all 0.2s ease-in-out;
            -ms-transition: all 0.2s ease-in-out;
            -o-transition: all 0.2s ease-in-out;
            transition: all 0.2s ease-in-out;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="body-frame">
    <div class="div-frame">
        <div class="header-frame">
            <a href="{{ url('/') }}">
                <img src="data:image/png;base64,<?php echo base64_encode(file_get_contents(asset('images/logo2.png')))?>" alt="IBMC.lk">
            </a>
        </div>
        <div class="content-frame">
           <p>Hi {{ $user }},</p>
            <p>
                You can reset password using below link
            </p>
            <p>Thanks,
                <br>
               IBMC Team
            </p>
        </div>
        <div class="action-frame">
            <a href="{{ $link }}">Click Link</a>
        </div>
    </div>
</div>
</body>
</html>
