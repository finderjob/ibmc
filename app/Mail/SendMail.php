<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = env('MAIL_FROM_ADDRESS');
        $name = env('MAIL_FROM_NAME');
        $subject = $this->data["subject"];
        if($this->data['type'] == 1){
            return $this->view('emails.reset-password')
                ->subject($subject)
                ->with(['user' => $this->data['user'],'subject' => $subject, 'link' => $this->data['link']]);
        }

        return $this->view('emails.reset-password')
            ->subject($subject)
            ->with(['user' => $this->data['user'],'subject' => $subject]);
    }
}
