<?php

namespace App\Console\Commands;

use App\Customer;
use App\Order;
use App\OrderMail;
use App\OrderProduct;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class OrderEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:order-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = ['subject' => "Order confirmation"];
        $order_emails = OrderMail::where('status', 0)->get();
        foreach ($order_emails as $order_email){
            $customer = Customer::where('id', $order_email->customer_id)->first();
            if($customer){
                $to = $customer->email;
                $order_id = $order_email->order_id;
                $customer_type = "Personal";
                $user = Auth::guard('customers')->user();
                if($user)
                {
                    if($user->customer_type == 2){
                        $customer_type = "School";
                    }else if($user->customer_type == 3){
                        $customer_type = "Authors";
                    }else if($user->customer_type == 4){
                        $customer_type = "Bookshop";
                    }
                }
                $order = Order::where('id', $order_id)->first();
                $order_products = OrderProduct::select('order_products.*', 'product_images.image_path', 'products.author_name')
                    ->leftJoin('product_images', 'order_products.product_id', '=', 'product_images.product_id')
                    ->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                    ->where('order_products.order_id', $order_id)
                    ->where('product_images.type', 1)
                    ->where('product_images.status', 1)
                    ->get();
                $data['order'] = $order;
                $data['order_products'] = $order_products;
                $data['customer_type'] = $customer_type;
                \Mail::to($to)->send(new \App\Mail\SendOrderMail($data));
                if( count(\Mail::failures()) > 0 ) {
                }else{
                    OrderMail::where('id', $order_email->id)->update(['status' => 1]);
                }
            }else{
                OrderMail::where('id', $order_email->id)->update(['status' => 2]);
            }

        }
        var_dump("finished");
    }
}
