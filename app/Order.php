<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'orders';

    public function scopeAddCustomerFilter($query,$year, $order_status, $order_payment_status)
    {
        if($year){
            $start_date = $year."-01-01 00:00:00";
            $end_date = $year."-12-31 23:59:59";
            $query->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
        }

        if($order_status){
            if($order_status > 0){
                $query->where('status', $order_status);
            }
        }
        if($order_payment_status){
            if($order_payment_status > 0){
                $query->where('payment_status', $order_payment_status);
            }
        }

        return $query;
    }

    public function scopeTimePeriods($query, $from_date, $to_date){
        if($from_date && $to_date){
            return $query->where('o.created_at', '<=', $to_date)->where('o.created_at', '>=', $from_date);
        }

        return $query;
    }

    public function scopePayment($query, $payment_id){

        if($payment_id > 0){
            return $query->where('o.status', '=', $payment_id);
        }
        return $query->whereIn('o.status', array(4, 7));
    }

    public function scopeCategory($query, $category_id){
        if($category_id > 0){
            return $query->where('c.customer_type', '=', $category_id);
        }
        return $query;
    }

    public function scopeCustomer($query, $customer_id){
        if($customer_id > 0){
            return $query->where('c.id', $customer_id);
        }

        return $query;
    }

    public function scopeUser($query, $user_id){
        if($user_id > 0){
            return $query->where('o.user_id', $user_id);
        }else if($user_id == -2){
            return $query->where('o.created_system', 1)->orWhere('o.created_system', 2)->whereNull('o.user_id');
        }
        return $query;
    }

    public function scopeOrderStatus($query, $order_status){

        if($order_status > 0){
            return $query->where('o.status', $order_status);
        }
        return $query;
    }
}
