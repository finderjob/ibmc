<?php
/**
 * Created by PhpStorm.
 * User: Danuja Fernando
 * Date: 8/2/2018
 * Time: 10:16 AM
 */


if(!function_exists('send_email'))
{
    function send_email($to,$cc = null,$subject,$user, $type = 0, $link = null)
    {
        $data = ['user' => $user,'subject' => $subject, 'type' => $type, 'link' => $link];
        if(!is_null($cc))
        {
            \Mail::to($to)->cc($cc)->send(new \App\Mail\SendMail($data));
        }
        else
        {
            \Mail::to($to)->send(new \App\Mail\SendMail($data));
        }

    }
}

if(!function_exists('get_all_categories')){

    function get_all_categories($remove_empty = false){

        $categories = \App\Category::where('parent_id',0)->where('status', 1)->get();
        $category_list = array();

        $total = \App\Product::where('complete_status',3)->where('status', 1)->count();
        $category_list[0] = array('name' => "All books", 'slug' => '', 'count' => $total);
        foreach ($categories as $category){
            $data = array();
            $data['id'] = $category->id;
            $data['name'] = $category->name;
            $data['slug'] = $category->slug;
            $sub_category = \App\Category::where('parent_id', $category->id)->where('status', 1)->pluck('id')->toArray();
            array_push($sub_category, $category->id);
            $count = \App\Product::select(\Illuminate\Support\Facades\DB::raw('count(*) as counts'))
                                    ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
                                    ->where('products.status', 1)
                                    ->where('products.complete_status', 3)
                                    ->where('product_categories.status', 1)
                                    ->whereIn('product_categories.category_id', $sub_category)
                                    ->groupBy('products.id')
                                    ->get();
            $data['count'] = count($count);
            if($remove_empty && count($count) == 0){
            }else{
                array_push($category_list, $data);
            }
        }
        return $category_list;
    }
}

if(!function_exists('get_sub_category_by_using_category_id')){

    function get_sub_category_by_using_category_id($id){

        $categories = \App\Category::where('parent_id', $id)->where('status', 1)->get();
        $category_list = array();

        //$total = \App\Product::where('complete_status',3)->where('status', 1)->count();
        foreach ($categories as $category){
            $data = array();
            $data['id'] = $category->id;
            $data['name'] = $category->name;
            $data['slug'] = $category->slug;
            $count = \App\Product::join('product_categories', 'products.id', '=', 'product_categories.product_id')
                ->where('products.status', 1)
                ->where('products.complete_status', 3)
                ->where('product_categories.status', 1)
                ->where('product_categories.category_id', $category->id)
                ->count();
            $data['count'] = $count;
            array_push($category_list, $data);
        }
        return $category_list;
    }
}

if(!function_exists('get_categories_by_product_id')){

    function get_categories_by_product_id($id){
        $categories = \App\Category::select('categories.*')
                        ->join('product_categories', 'categories.id', '=', 'product_categories.category_id')
                        ->where('product_categories.product_id', $id)
                        ->get();

        return $categories;
    }
}

if(!function_exists('products_cover_images')){

    function products_cover_images($id){

        return \App\ProductImage::select('image_path')->where('type', 1)->where('product_id',$id)->first();
    }
}

if(!function_exists('products_other_images')){

    function products_other_images($id){

        return \App\ProductImage::select('image_path')->where('type','!=', 1)->where('product_id',$id)->get();
    }
}

if(!function_exists('products_price')){

    function products_price($id){
        return \App\ProductPrice::where('status', 1)->where('product_id',$id)->first();
    }
}

if(!function_exists('get_product_rating_value')){

    function get_product_rating_value($id){
        $ratings = \App\ProductsReviews::where('product_id', $id)->where('status', 1)->get();
        $total_rating = 0;
        $avg_rating = 0;
        foreach ($ratings as $rating){
            $rating_count = $rating->quality + $rating->price + $rating->value;
            $total_rating += $rating_count;
        }
        if(count($ratings) != 0){
            $avg_rating = $total_rating / (count($ratings) * 3);
            $avg_rating = intval(round($avg_rating));
        }
        return $avg_rating;
    }
}
if(!function_exists('sub_total')){
    function sub_total($user){
        $carts = \App\Cart::select('carts.*','products.name','product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('products', 'carts.product_id','=','products.id')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('carts.user_id', $user->id)
            ->where('product_prices.status', 1)
            ->get();
        $sub_total = 0;
        foreach ($carts as $cart){
            $product_price = 0;
            if($cart->customer_type == 2){
                $product_price = $cart->school_price;
            }else{
                $product_price = $cart->normal_price - $cart->normal_discount;
            }
            $sub_total += ($product_price * $cart->qty);
        }
        return $sub_total;
    }
}
if(!function_exists('apply_coupon_code')){
    function apply_coupon_code($coupon, $sub_total){
        $magin_value = $coupon->margin_value;
        $value = $coupon->value;
        $data =  array();
        $data['value'] = 0;
        $data['status'] = 0;
        $data['sub_total'] = $sub_total;
        switch ($coupon->type){
            case 1:
                $data['value'] = $value;
                $data['status'] = 1;
               break;
            case 2:
                $data['value'] = ($sub_total* $value)/100;
                $data['status'] = 1;
                break;
            case 3:
                if($sub_total > $magin_value){
                    $data['value'] = $value;
                    $data['status'] = 1;
                }
                break;
            case 4:
                if($sub_total > $magin_value){
                    $data['value'] = ($sub_total* $value)/100;
                    $data['status'] = 1;
                }
                break;
            default:
        }

        return $data;
    }
}

if(!function_exists('home_page_slider')){

    function home_page_slider(){
        $default_slider = array();
        $default_slider['slider']['image'][0] = 'theme/images/bg/1.jpg';
        $default_slider['slider']['text'][0][0] = array('Buy', 'your');
        $default_slider['slider']['text'][0][1] = array('favourite','Book');
        $default_slider['slider']['text'][0][2] = array('from', 'Here');
        $default_slider['slider']['image'][1] = 'theme/images/bg/7.jpg';
        $default_slider['slider']['text'][1][0] = array('Buy7', 'your');
        $default_slider['slider']['text'][1][1] = array('favourite7','Book');
        $default_slider['slider']['text'][1][2] = array('from7', 'Here');

        \Illuminate\Support\Facades\Cache::forget('home_slider');
        $has_slider = \Illuminate\Support\Facades\Cache::has('home_slider');
        if(!$has_slider){
            $get_home_slider = \App\Page::where('name', 'home')->first();
            if($get_home_slider){
                $get_home_slider_data = json_decode($get_home_slider->value, true);
                if(!empty($get_home_slider_data) && array_key_exists('home_slider', $get_home_slider_data)){
                    $home_slider_data = $get_home_slider_data['home_slider'];
                    if(array_key_exists('slider', $home_slider_data)){
                        $home_slider_data = $home_slider_data;
                    }else{
                        $home_slider_data = $default_slider;
                    }
                }else{
                    $home_slider_data = $default_slider;
                }
            }else{
                $home_slider_data = $default_slider;
            }
            \Illuminate\Support\Facades\Cache::forever('home_slider', $home_slider_data);
        }

        $home_slider = \Illuminate\Support\Facades\Cache::get('home_slider');
        return array($home_slider, $default_slider);
    }
}

if(!function_exists('post_archives')){

    function post_archives(){
        $get_oldest_post = \App\Blog::orderBy('id', 'ASC')->first();
        if(!$get_oldest_post){
            return false;
        }
        $created_at = $get_oldest_post->created_at;
        $now = now();
        $start = new DateTime($created_at);
        $start->modify('first day of this month');
        $end = new DateTime($now);
        $end->modify('first day of next month');

        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $data = array();
        foreach ($period as $pd){
            $data[] = $pd->format('F Y');
        }
        return array_reverse($data);
    }
}
if(!function_exists('get_order_product')){
    function get_order_product($order_id){
        $order_products = \App\OrderProduct::where('order_id', $order_id)->get();
        $ul = "<ul>";
        foreach($order_products as $order_product){
            $li = "<li>".$order_product->products_name." x ".$order_product->qty."</li>";
            $ul .= $li;
        }
        $ul .= "</ul>";

        return $ul;
    }
}

if(!function_exists('get_customer_name')){
    function get_customer_name($customer_id){
        $customer = \App\Customer::find($customer_id);
        if($customer){
            return $customer->first_name." ".$customer->last_name;
        }
        return "";
    }
}

if(!function_exists('expire_orders')){
    function expire_orders(){
        $orders = \App\Order::where('status', '>', 3)->where('valid_status', 1)->get();
        foreach ($orders as $order){
            $time_period = (config('status.order_completed_time_period')) ? config('status.order_completed_time_period') : 14 ;
            $expire_order = strtotime($order->updated_at) + 60 * 60 * 24 * $time_period;
            $now = strtotime("now");
            if($now > $expire_order){
                \App\Order::where('id', $order->id)->update(array('valid_status' => 0));
            }
        }
    }
}
if(!function_exists('createOrderHistory')){
    function createOrderHistory($data){
        $order_history = new \App\OrderHistory();
        $order_history->order_id = $data['order_id'];
        $order_history->section = $data['section'];
        $order_history->description = $data['description'];
        $order_history->data = $data['data'];
        $order_history->user_id = Auth::guard('admins')->user()->id;
        $order_history->save();
    }
}
if(!function_exists('createNotifications')){
    function createNotifications($data){
        $notification = new \App\Notification();
        $notification->user_id = $data['user_id'];
        $notification->data = json_encode($data['data']);
        $notification->url = $data['url'];
        $notification->save();
    }
}

if(!function_exists('notificationTimeDiff')){
    function notificationTimeDiff($created_at){
        $today = new DateTime('now');
        $created_at = new DateTime($created_at);
        $days_until_appt = $created_at->diff($today);
        $years = $days_until_appt->d;
        $months = $days_until_appt->m;
        $day = $days_until_appt->d;
        $hour = $days_until_appt->h;
        $min = $days_until_appt->i;
        if($years > 0){
            if($years == 1){
                $time = "1 year ago";
            }else{
                $time = $years." years ago";
            }
        }else if($months > 0){
            if($months == 1){
                $time = "1 month ago";
            }else{
                $time = $months." month ago";
            }
        }else if($day > 0){
            if($day == 1){
                $time = "1 day ago";
            }else{
                $time = $day." days ago";
            }
        }else if($hour > 0){
            if($hour == 1){
                $time = "1 hour ago";
            }else{
                $time = $hour." hours ago";
            }
        }else if($min > 0){
            if($min == 1){
                $time = "1 minute ago";
            }else{
                $time = $min." minute ago";
            }
        }else{
            $time = "Just now";
        }
        return $time;
    }
}
if(!function_exists('bestSellerProducts')){
    function bestSellerProducts(){
        $data = bestSellerProductIds();
        if(count($data) != 8){
            return array();
        }
        $products  = \App\Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                                    ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                                    ->where('products.complete_status',3)
                                    ->where('products.status', 1)
                                    ->where('product_prices.status', 1)
                                    ->whereIn('products.id', $data)
                                    ->get();
        return $products;
    }
}

if(!function_exists('addBillingAddress')){
    function addBillingAddress($request, $customer_id){

        $address = \App\Address::where('customer_id', $customer_id)->where('address_type', 1)->first();
        if(!$address){
            $address = new \App\Address();
            $address->address_type = 1;
        }
        $address->customer_id = $customer_id;
        $address->first_name = $request->get('billing_first_name');
        $address->last_name = $request->get('billing_last_name');
        $address->address_line_1 = $request->get('billing_address_line_1');
        $address->address_line_2 = $request->get('billing_address_line_2');
        $address->city = $request->get('billing_city');
        $address->zip_code = $request->get('billing_zip_code');
        $address->phone_number = $request->get('billing_phone_number');
        $address->mobile_number = $request->get('billing_mobile_number');
        $address->save();

        $customer = \App\Customer::where('complete_status','!=',2)->where('id', $customer_id)->first();
        if($customer){
            $customer->first_name = $request->get('billing_first_name');
            $customer->last_name = $request->get('billing_last_name');
            $customer->phone_number = $request->get('billing_phone_number');
            $customer->mobile_number = $request->get('billing_mobile_number');
            $customer->complete_status = 1;
            $customer->save();
        }
    }
}

if(!function_exists('addShippingAddress')){
    function addShippingAddress($request, $customer_id){
        $address = \App\Address::where('customer_id', $customer_id)->where('address_type', 2)->first();
        if(!$address){
            $address = new \App\Address();
            $address->address_type = 2;
        }
        $address->customer_id = $customer_id;
        $ship_to_different_address = $request->get('ship_to_different_address');
        if($ship_to_different_address){
            $address->first_name = $request->get('shipping_first_name');
            $address->last_name = $request->get('shipping_last_name');
            $address->address_line_1 = $request->get('shipping_address_line_1');
            $address->address_line_2 = $request->get('shipping_address_line_2');
            $address->city = $request->get('shipping_city');
            $address->zip_code = $request->get('shipping_zip_code');
            $address->phone_number = $request->get('shipping_phone_number');
            $address->mobile_number = $request->get('shipping_mobile_number');
        }else{
            $address->first_name = $request->get('billing_first_name');
            $address->last_name = $request->get('billing_last_name');
            $address->address_line_1 = $request->get('billing_address_line_1');
            $address->address_line_2 = $request->get('billing_address_line_2');
            $address->city = $request->get('billing_city');
            $address->zip_code = $request->get('billing_zip_code');
            $address->phone_number = $request->get('billing_phone_number');
            $address->mobile_number = $request->get('billing_mobile_number');
        }
        $address->save();
    }
}

function generateOrderId($customer_id){
    $order_id = $customer_id;
    $order_id .= date('d').date('n');
    $length = strlen($order_id);
    $remaining = 10 - $length;
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    for ($i = 0; $i < $remaining; $i++) {
        $order_id .= $characters[rand(0, $charactersLength - 1)];
    }
    return $order_id;
}

if(!function_exists('createOrder')){
    function createOrder($request, $customer_id, $data){

        $order_id = generateOrderId($customer_id);
        $order = new \App\Order();
        $order->order_id = $order_id;
        $order->customer_id = $customer_id;
        $order->coupon_id = $data['coupon_id'];
        $order->sub_total = $data['sub_total'];
        $order->coupon_value = $data['coupon_value'];
        $order->order_total = $data['order_total'];
        $order->status = 1;
        $order->billing_first_name = $request->get('billing_first_name');
        $order->billing_last_name = $request->get('billing_last_name');
        $order->billing_address_line_1 = $request->get('billing_address_line_1');
        $order->billing_address_line_2 = $request->get('billing_address_line_2');
        $order->billing_city = $request->get('billing_city');
        $order->billing_zip_code = $request->get('billing_zip_code');
        $order->billing_phone_number = $request->get('billing_phone_number');
        $order->billing_mobile_number = $request->get('billing_mobile_number');
        $ship_to_different_address = $request->get('ship_to_different_address');
        if($ship_to_different_address){
            $order->shipping_first_name = $request->get('shipping_first_name');
            $order->shipping_last_name = $request->get('shipping_last_name');
            $order->shipping_address_line_1 = $request->get('shipping_address_line_1');
            $order->shipping_address_line_2 = $request->get('shipping_address_line_2');
            $order->shipping_city = $request->get('shipping_city');
            $order->shipping_zip_code = $request->get('shipping_zip_code');
            $order->shipping_phone_number = $request->get('shipping_phone_number');
            $order->shipping_mobile_number = $request->get('shipping_mobile_number');
        }else{
            $order->shipping_first_name = $request->get('billing_first_name');
            $order->shipping_last_name = $request->get('billing_last_name');
            $order->shipping_address_line_1 = $request->get('billing_address_line_1');
            $order->shipping_address_line_2 = $request->get('billing_address_line_2');
            $order->shipping_city = $request->get('billing_city');
            $order->shipping_zip_code = $request->get('billing_zip_code');
            $order->shipping_phone_number = $request->get('billing_phone_number');
            $order->shipping_mobile_number = $request->get('billing_mobile_number');
        }
        if(array_key_exists('created_system', $data)){
            $order->created_system = $data['created_system'];
        }
        if(array_key_exists('user_id', $data)){
            $order->user_id = $data['user_id'];
        }
        $order->save();
        orderEmail($order->id, $customer_id);
        return $order->id;
    }
}

if(!function_exists('orderEmail')){
    function orderEmail($order_id, $customer_id){
        $order_email = new \App\OrderMail();
        $order_email->order_id = $order_id;
        $order_email->customer_id = $customer_id;
        $order_email->save();
    }
}

if(!function_exists('sortByOrder')){
    function sortByOrder($a, $b) {
        return $a['sort_by'] - $b['sort_by'];
    }
}

if(!function_exists('checkPermissionByName')){
    function checkPermissionByName($name){
        $permission = \Spatie\Permission\Models\Permission::findByName($name);

        if($permission){
            $user = \Illuminate\Support\Facades\Auth::guard('admins')->user();
            if($user){
                $role = \Spatie\Permission\Models\Role::find($user->user_type);
                if($role){
                    return $role->hasPermissionTo($name);
                }
            }
        }
        return false;

    }
}

if(!function_exists('monthly_sales')){
    function monthly_sales(){
        $month_start = "2019-05-01 00:00:00";
        $month_end = "2019-05-31 23:59:59";
        $order_status = config('status.orders');
        $order_status_complete = $order_status['completed']['id'];
        $orders = \App\Order::where('status', '=', $order_status_complete)
                            ->where('created_at', '>=', $month_start)
                            ->where('created_at', '<=', $month_end)
                            ->get();

        $sales = 0;
        foreach ($orders as $order){
            $sales += $order->order_total;
        }
        return final_price_format($sales);
    }
}

if(!function_exists('customer_count')){
    function customer_count(){
        $count = \App\Customer::where('status', 1)->count();
        return $count;
    }
}

if(!function_exists('product_count')){
    function product_count(){
        $completed_count = \App\Product::where('status', 1)->where('complete_status', 3)->count();
        $notcomplted_count = \App\Product::where('status', 1)->where('complete_status', '!=', 3)->count();
        return array($completed_count, $notcomplted_count);
    }
}

if(!function_exists('pending_order_count')){
    function pending_order_count(){
        $order_statuses = config('status.orders');
        $order_status = array();
        foreach ($order_statuses as $os) {
            if(!$os['payment'] && !$os['close'] && !$os['return_qty']){
                array_push($order_status, $os['id']);
            }
        }
        $orders = \App\Order::whereIn('status', $order_status)->count();
        return $orders;
    }
}

if(!function_exists('bg_images')){

    function bg_images($page_value){
        $page = "bg-images";
        $has_bg_images = \Illuminate\Support\Facades\Cache::has('bg_images');
        $image_path = false;
        if(!$has_bg_images){
            $get_bg_images = \App\Page::where('name', $page)->first();
            $get_bg_images_data = array();
            if($get_bg_images){
                $get_bg_images_data = json_decode($get_bg_images->value, true);
            }
            \Illuminate\Support\Facades\Cache::forever('bg_images', $get_bg_images_data);
        }else{
            $get_bg_images_data = \Illuminate\Support\Facades\Cache::get('bg_images');
        }
        if(!empty($get_bg_images_data) && array_key_exists('background_image', $get_bg_images_data)){
            foreach ($get_bg_images_data['background_image'] as $key => $value){
                if($value['name'] == $page_value){
                    $image_path = $value['image'];
                }
            }

        }
        return $image_path;
    }
}

if(!function_exists('author_list')){
    function author_list(){
        $page = "authors";
        $has_authors = \Illuminate\Support\Facades\Cache::has('author_list');
        $data = array();
        if(!$has_authors){
            $get_author_list = \App\Page::where('name', $page)->first();
            $get_author_list_data = array();
            if($get_author_list){
                $get_author_list_data = json_decode($get_author_list->value, true);
            }

            \Illuminate\Support\Facades\Cache::forever('author_list', $get_author_list_data);
        }
        $data = \Illuminate\Support\Facades\Cache::get('author_list');
        return $data;
    }
}

if(!function_exists('team_members')){
    function team_members(){
        $page = "team_members";
        $has_authors = \Illuminate\Support\Facades\Cache::has('team_member');
        $data = array();
        if(!$has_authors){
            $get_team_member = \App\Page::where('name', $page)->first();
            $get_team_member_data = array();
            if($get_team_member){
                $get_team_member_data = json_decode($get_team_member->value, true);
            }
            \Illuminate\Support\Facades\Cache::forever('team_member', $get_team_member_data);
        }
        $data = \Illuminate\Support\Facades\Cache::get('team_member');
        return $data;
    }
}
if(!function_exists('social_media')){
    function social_media(){
        $page = "social_media";
        $has_social_media = \Illuminate\Support\Facades\Cache::has('social_media');
        $data = array();
        if(!$has_social_media){
            $get_social_media = \App\Page::where('name', $page)->first();
            $get_social_media_data = array();
            if($get_social_media){
                $get_social_media_data = json_decode($get_social_media->value, true);
            }
            \Illuminate\Support\Facades\Cache::forever('social_media', $get_social_media_data);
        }
        $data = \Illuminate\Support\Facades\Cache::get('social_media');
        return $data;
    }
}

if(!function_exists('favicon')){
    function favicon(){
        $page = "settings";
        $has_favicon = \Illuminate\Support\Facades\Cache::has('favicon');
        $favicon = null;
        if(!$has_favicon){
            $get_settings = \App\Page::where('name', $page)->first();
            if($get_settings){
                $get_settings_data = json_decode($get_settings->value, true);
                if($get_settings_data && array_key_exists('favicon', $get_settings_data)){
                    $favicon = $get_settings_data['favicon']['image'];
                }
            }
            \Illuminate\Support\Facades\Cache::forever('favicon', $favicon);
        }
        $favicon = \Illuminate\Support\Facades\Cache::get('favicon');
        return $favicon;
    }
}

if(!function_exists('order_email_send')){
    function order_email_send(){
        $data = ['subject' => "Order confirmation"];
        $order_emails = \App\OrderMail::where('status', 0)->get();
        foreach ($order_emails as $order_email){
            $customer = \App\Customer::where('id', $order_email->customer_id)->first();
            if($customer){
                $to = $customer->email;
                $order_id = $order_email->order_id;
                $order = \App\Order::find($order_id);
                $customer_type = "Personal";
                $user = \Illuminate\Support\Facades\Auth::guard('customers')->user();
                if($user)
                {
                    if($user->customer_type == 2){
                        $customer_type = "School";
                    }else if($user->customer_type == 3){
                        $customer_type = "Authors";
                    }else if($user->customer_type == 4){
                        $customer_type = "Bookshop";
                    }
                }
                $order_products = \App\OrderProduct::select('order_products.*', 'product_images.image_path', 'products.author_id')
                    ->leftJoin('product_images', 'order_products.product_id', '=', 'product_images.product_id')
                    ->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                    ->where('order_products.order_id', $order_id)
                    ->where('product_images.type', 1)
                    ->where('product_images.status', 1)
                    ->get();
                $data['order'] = $order;
                $data['order_products'] = $order_products;
                $data['customer_type'] = $customer_type;
                \Mail::to($to)->send(new \App\Mail\SendOrderMail($data));
                if( count(\Mail::failures()) > 0 ) {
                }else{
                    \App\OrderMail::where('id', $order_email->id)->update(['status' => 1]);
                }
                $email = env('MAIL_FROM_ADDRESS');
                if($email){
                    \Mail::to($to)->send(new \App\Mail\SendOrderMail($data));
                }

            }else{
                \App\OrderMail::where('id', $order_email->id)->update(['status' => 2]);
            }
        }
    }
}

if(!function_exists('bestSellerProductIds')){
    function bestSellerProductIds(){
        $orders = \App\Order::where('status', '!=', 5)->where('status', '!=', 6)->get();
        $data = array();
        foreach ($orders as $order){
            $order_products = \App\OrderProduct::where('order_id', $order->id)->get();
            foreach ($order_products as $order_product){
                $product = \App\Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                    ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                    ->where('products.complete_status',3)
                    ->where('products.status', 1)
                    ->where('product_prices.status', 1)
                    ->where('products.id', $order_product->id)
                    ->orderBy('id','DESC')
                    ->first();
                if($product){
                    if(array_key_exists($order_product->product_id, $data)){
                        $data[$order_product->product_id] = $data[$order_product->product_id] + 1;
                    }else{
                        $data[$order_product->product_id] = 1;
                    }
                }

            }
        }
        $y = 10;
        $data = array_filter($data, function ($x) use ($y){ return $x > $y; });
        arsort($data);
        $data_key = array_keys($data);
        $data = array_slice($data_key, 0, 8);
        return $data;
    }
}

if(!function_exists('checkBestSellerProduct')){
    function checkBestSellerProduct($product_id){
        $best_seller_products = bestSellerProductIds();
        if(in_array($product_id, $best_seller_products)){
            return true;
        }
        return false;
    }
}

if(!function_exists('final_price_format')){
    function final_price_format($price){
        $format = config('status.currency_format');
        $price = $format." ".number_format($price, 2, '.', ',');
        return $price;
    }
}


if(!function_exists('checkCartItems')){
    function checkCartItems($user){
        $carts = \App\Cart::where('carts.user_id', $user->id)->get();
        if($carts){
            return true;
        }
        return false;
    }
}

if(!function_exists('orderStatus')){
    function orderStatus($id){
        $order_statuses = config('status.orders');
        $set = array();
        foreach ($order_statuses as $od){
            if($od['id'] == $id){
               array_push($set, $od['name'], $od['class']);
               break;
            }
        }
        return $set;
    }
}



