<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions)
    {
        $permissions_list = explode("|", $permissions);
        $status = false;
        $count = 0;
        foreach ($permissions_list as $permission){
            if(checkPermissionByName($permission)){
                $count++;
            }
        }
        if($count == count($permissions_list)){
            $status = true;
        }
        if(!$status){
            $message = 'User does not have the right permissions.';
            $error_code = 403;
           // abort(403, $message);
            return new Response(view('admin.pages.error', compact('message', 'error_code')));
        }
        return $next($request);
    }
}
