<?php

namespace App\Http\Middleware;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Closure;

class CheckCustomerProfileComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $customer = Auth::guard('customers')->user();

        if($customer){
            switch($customer->complete_status) {
                case 1:
                    //return  redirect()->to(route('profile'));
                    break;
                case 2:
                    break;
                default:
            }

        }
        return $next($request);
    }
}
