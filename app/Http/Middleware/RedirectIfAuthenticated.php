<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //dd($request,$next,$guard,Auth::guard($guard)->check());
        switch ($guard)
        {
            case 'admins':
                if (\Auth::guard('admins')->check())
                {

                    return redirect()->route('admin.home');
                }
                break;
            case 'customers':
                if (\Auth::guard('customers')->check())
                {

                    return redirect()->route('home');
                }
                break;
            default:
                break;
        }

        return $next($request);
    }
}
