<?php

namespace App\Http\Controllers\Auth;

use App\Customer;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    //

    public function passwordReset(){
        HomeController::$page_title = "Password Reset";
        return view('theme.password-reset');
    }

    public function passwordRequest(Request $request){
        $rules = [
          'email' => 'required|email|exists:customers,email'
        ];
        $messages = [
            'email.required' => 'Email is required',
            'email.email' => 'Email should be valid',
            'email.exists' => "Email doesn't exists"
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            session()->flash('error_message','Password request is incomplete.');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $customer = Customer::where('email', $request->get('email'))->first();
        $token = bin2hex(random_bytes(32));
        $date = now();
        DB::table('password_resets')->insert(array(
            'email' => $customer->email,
            'token' => $token,
            'created_at' => $date
        ));
        $password_reset_url = route('reset.password.customer',[$token]);
        $subject = "Password Reset";

        send_email($customer->email,null,$subject,$customer->name, 1, $password_reset_url);

        session()->flash('success_message', 'Password reset link sent to your email');
        return redirect()->back();
    }

    public function resetPassword($token){
        HomeController::$page_title = "Reset Password";
        $get_token_details = DB::table('password_resets')->where('token', $token)->first();
        $created_at = strtotime($get_token_details->created_at) + 2*60*60;
        $now = strtotime(now());
        if($now > $created_at)
        {
            session()->flash('error_message','This token already expired, try again');
            return redirect()->to(route('login'));
        }
        else{
            if(!$get_token_details){
                session()->flash('error_message',"This token doesn't exists");
                return redirect()->to(route('login'));
            }
            return view('theme.reset-password', compact('token'));
        }
    }

    public function resetPasswordRequest($token, Request $request){

        $rules = [
            'email' => 'required|email',
            'password' => 'required|string|max:255|min:8|confirmed',
        ];

        $message = [
            'email.required' => 'The email field is required',
            'email.email' => 'The email should be valid email',
            'email.unique' => 'This email already exists',
            'name.required' => 'The name field is required',
            'password.required' =>'The password field is required',
            'password.string' => 'The password should be string',
            'password.max' => 'Maximum characters is 255',
            'password.min' => 'Minimum characters is 8',
            'confirmed' => 'The password is not match'
        ];
        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','Password change unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if($token != $request->get('password_token')){
            session()->flash('error_message','Password change unsuccessfully');
            return redirect()->back()
                ->withInput();
        }
        $get_token_details = DB::table('password_resets')->where('token', $token)->first();
        if($get_token_details){
            $created_at = strtotime($get_token_details->created_at) + 2*60*60;
            $now = strtotime(now());
            if($now > $created_at){
                session()->flash('error_message','Password changed link is not available.');
                return redirect()->to(route('admin.login.get'));

            }
            $email = $get_token_details->email;
            $customer_email = $request->get('email');
            if($email != $customer_email){
                session()->flash('error_message','Password change unsuccessfully');
                return redirect()->back()->withInput();
            }
            $customer = Customer::where('email', $email)->first();
            if(!$customer){
                session()->flash('error_message','Password change unsuccessfully');
                return redirect()->to(route('admin.login.get'));
            }
            $customer->password = Hash::make($request->get('password'));
            $customer->save();
            DB::table('password_resets')->where('email', $email)->where('token', $token)->delete();
            session()->flash('success_message','Password changed successfully. please log in.');
            return redirect()->to(route('login'));
        }else{
            session()->flash('error_message','Password change unsuccessfully');
            return redirect()->to(route('admin.login.get'));
        }
    }
}
