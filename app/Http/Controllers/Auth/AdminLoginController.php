<?php

namespace App\Http\Controllers\Auth;


use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;


class AdminLoginController extends Controller
{

    use AuthenticatesUsers;

    protected $guard = 'admins';

    protected $username = null;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/administrator';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest:admins')->except('logout');

    }

    public function guard()
    {
        return Auth::guard('admins');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {

        return view('auth.admin.login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        if(filter_var($this->username, FILTER_VALIDATE_EMAIL)){
            return 'email';
        }else{
            return 'name';
        }

    }

    protected function validateLogin(Request $request)
    {
        $username = $request->get('email');
        $this->username = $username;
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function login(Request $request)
    {
        $username = $request->get('email');
        $this->username = $username;
        if (Auth::guard('admins')->attempt([$this->username() => $request->email, 'password' => $request->password, 'status'=> 1], $request->get('remember'))) {
            return redirect()->to(route('admin.home'));
        }else{

            $user = User::whereEmail($request->email)->first();

            if($user){
                $password = Hash::check($request->password, $user->password);
                if($password){
                    if(!$user->status){
                        $error = $this->sendFailedLoginResponseForStatus($request);
                    }
                }else{
                    $error = $this->sendFailedLoginResponse($request);
                }

            }else{
                $error = $this->sendFailedLoginResponse($request);
            }
            return back()->withInput($request->only('email', 'remember'))->withErrors($error->errors());
        }

    }

    public function sendFailedLoginResponseForStatus($request){
        return ValidationException::withMessages([
            $this->username() => [trans('auth.status')],
        ]);
    }
    public function sendFailedLoginResponse($request){
        return ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }
    public function logout(Request $request)
    {
        if(Auth::guard('admins')->check()){
            Auth::guard('admins')->logout();
        }
        //$request->session()->invalidate();

        return redirect()->to($this->redirectTo);
    }
}
