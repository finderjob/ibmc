<?php

namespace App\Http\Controllers\Auth;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('theme.auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'register_email' => 'required|email|unique:customers,email',
            'username' => 'required|string|max:255',
            'register_password' => 'required|string|max:255|min:8',
        ];

        $message = [
            'register_email.required' => 'The email field is required',
            'register_email.email' => 'The email should be valid email',
            'register_email.unique' => 'This email already exists',
            'username.required' => 'The username field is required',
            'username.string' => 'The username should be string',
            'username.max' => 'Maximum characters is 255',
            'register_password.required' =>'The password field is required',
            'register_password.string' => 'The password should be string',
            'register_password.max' => 'Maximum characters is 255',
            'register_password.min' => 'Minimum characters is 8',
        ];
        return Validator::make($data, $rules, $message);
    }

    protected function registered(Request $request)
    {
        session()->put('authenticated_success', 'register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $types = array(1,2,3);
        $customer_type = 1;
        $status = 1;
        if($customer_type == 3){
            $status = 0;
        }
        return Customer::create([
            'name' => $data['username'],
            'email' => $data['register_email'],
            'password' => Hash::make($data['register_password']),
            'customer_type' => $customer_type,
            'status' => $status
        ]);
    }

    protected function guard()
    {
        return \Auth::guard('customers');
    }
}
