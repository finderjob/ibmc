<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customers')->except('logout');
    }

    protected function guard()
    {
        return \Auth::guard('customers');
    }

    protected function authenticated(Request $request, $user)
    {
        session()->put('authenticated_success', 'login');
    }

    public function showLoginForm(){

        return view('theme.auth');
    }
    public function logout(Request $request)
    {
        if(\Auth::guard('customers')->check()){
            \Auth::guard('customers')->logout();
        }

        $request->session()->invalidate();

        return redirect()->to($this->redirectTo);
    }
}
