<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Breadcrumbs;

class CouponController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        array_push(Breadcrumbs::$breadcrumb,array('Users',''));

        $coupons = Coupon::orderBy('created_at','DESC')->get();
        return view('admin.coupons.list', compact('coupons'));
    }

    public function add(){
        return view('admin.coupons.add');
    }

    public function store(Request $request){

        $rules = [
            'name' => 'required|string|max:6|min:6',
            'coupon_type' => 'required|integer',
            'coupon_value' => 'required',
            'expire_date' => 'required|date'
        ];

        $message = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 6',
            'name.min' => 'Minimum characters is 6',
            'coupon_type.required' => 'The coupon type field is required',
            'coupon_type.integer' => 'The coupon type field should be number',
            'coupon_value.required' => 'The coupon value field is required',
            'coupon_value.number' => 'The coupon value field should be price',
            'expire_date.required' => 'The expire date field is required',
            'expire_date.date' => 'The expire date field should be date',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','Coupon create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $coupon = new Coupon();
        $coupon->name = $request->get('name');
        $coupon->type = $request->get('coupon_type');
        if($request->get('coupon_type') < 2){
           $coupon->margin_value = $request->get('margin_value');
        }
        $coupon->value = $request->get('coupon_value');
        $coupon->expire_at = $request->get('expire_date');
        $coupon->save();

        session()->flash('success_message','Coupon created successfully');
        return redirect()->back();
    }

    public function changeCouponsStatus(Request $request){

        $coupon_status = $request->get('coupon_status');
        $coupon_id = $request->get('coupon_id');
        $coupon = Coupon::find($coupon_id);
        $data = array();
        if($coupon){

            $coupon->status = $coupon_status;
            $coupon->save();

            $data['status'] = 1;
            $data['message'] = 'Coupon status changed successfully';
        }else{
            $data['status'] = 0;
            $data['message'] = 'This coupon doesn\'t exists';
        }

        return json_encode($data);
    }
}
