<?php

namespace App\Http\Controllers\Admin;

use App\AllSlug;
use App\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Breadcrumbs;

class AuthorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    public function list(){
        array_push(Breadcrumbs::$breadcrumb,array('Authors',''));
        $authors = Author::all();
        return view('admin.authors.list', compact('authors'));
    }
    public function add(){
        array_push(Breadcrumbs::$breadcrumb,array('Authors','admin.authors'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        return view('admin.authors.add');
    }

    public function store(Request $request){

        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string',
            'author_image' => 'required|max:20000'
        ];

        $messages = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string',
            'author_image.required' => 'The Author image is required',
            'author_image.max' => 'Maximum size is 2MB'
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            session()->flash('error_message','Author add unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $author_image = $request->file('author_image');
        $ext = $author_image->getClientOriginalExtension();
        $t=time();
        $fileName = "author_image_".$t.'.'.$ext;
        $location = public_path('images/author/');
        if(!is_dir($location)){
            mkdir($location);
            chmod($location, 0777);
        }
        $author_image->move($location, $fileName);
        $author_name_path = 'images/author/'.$fileName;
        $slug = AllSlug::generateSlug($request->get('slug'));
        $author  = new Author();
        $author->name = $request->get('name');
        $author->slug = $slug;
        $author->image_path = $author_name_path;
        $author->description = json_encode($request->get('description'));
        $social_medias = config('status.social_media');
        $data = array();
        foreach($social_medias as $social_media){
            $data[$social_media['value']] = $request->get('author_'.$social_media['value']);
        }
        $author->social_media = json_encode($data);
        $author->save();
        AllSlug::addSlug($slug);
        session()->flash('success_message','Author saved successfully');
        return redirect()->back();
    }

    public function edit($id){

        $author = Author::find($id);
        if(!$author){
            session()->flash('error_message','Author doesn\'t exist');
            return redirect()->to(route('admin.authors'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Authors','admin.authors'));
        array_push(Breadcrumbs::$breadcrumb,array('Edit',''));

        return view('admin.authors.edit', compact('author'));
    }

    public function update($id, Request $request){

        $author = Author::find($id);
        if(!$author){
            session()->flash('error_message','Author doesn\'t exist');
            return redirect()->to(route('admin.authors'));
        }
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string',
            'author_image' => 'nullable|max:20000'
        ];

        $messages = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string',
            'author_image.max' => 'Maximum size is 2MB'
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            session()->flash('error_message','Author update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $image_previous_url = $request->get('image_previous_url');
        if(empty($image_previous_url)){
            $author_image = $request->file('author_image');
            if($author_image){
                $author_image = $request->file('author_image');
                $ext = $author_image->getClientOriginalExtension();
                $t=time();
                $fileName = "author_image_".$t.'.'.$ext;
                $location = public_path('images/author/');
                if(!is_dir($location)){
                    mkdir($location);
                    chmod($location, 0777);
                }
                $author_image->move($location, $fileName);
                $author_name_path = 'images/author/'.$fileName;
                $author->image_path = $author_name_path;
            }
        }
        $author->name = $request->get('name');
        $author_slug = $author->slug;
        if($author_slug !=  $request->get('slug'))
        {
            $slug = AllSlug::generateSlug($request->get('slug'));
            if($author_slug != $slug)
            {
                $author->slug = $slug;
                AllSlug::addSlug($slug);
            }
        }

        $author->description = json_encode($request->get('description'));
        $social_medias = config('status.social_media');
        $data = array();
        foreach($social_medias as $social_media){
            $data[$social_media['value']] = $request->get('author_'.$social_media['value']);
        }
        $author->social_media = json_encode($data);
        $author->save();
        session()->flash('success_message','Author updated successfully');
        return redirect()->back();
    }

    public function changeAuthorStatus(Request $request){
        $author_status = $request->get('author_status');
        $author_id = $request->get('author_id');
        $author = Author::find($author_id);
        $data = array();
        if($author){
            $author->status = $author_status;
            $author->save();

            $data['status'] = 1;
            $message = 'Author status changed successfully';
            $data['message'] = $message;
        }else{
            $data['status'] = 0;
            $message = 'This author doesn\'t exists';
            $data['message'] = $message;
        }

        return json_encode($data);
    }
}
