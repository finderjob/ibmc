<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Breadcrumbs;
use Illuminate\Support\Facades\Cache;

class PageController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Pages - Home',''));
        $pages = Page::where('name', 'home')->first();
        return view('admin.pages.index', compact('pages'));
    }

    public function homeSliderUpload(Request $request){

        $rules = [
            'home_slider' => 'required|max:20000|mimes:png,jpeg,jpg|dimensions:min_width=1920,min_height=950'
        ];

        $message = [
            'home_slider.required' => 'The image field is required',
            'home_slider.max' => 'The image max size 2MB',
            'home_slider.mimes' => 'This image allows only png,jpeg,jpg',
            'home_slider.dimensions' => 'This image resolution should be 1920x950'
        ];
        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Home sliders add unsuccessfully');
            return redirect()->back()
                ->withErrors($validator);
        }
        $text1 = $request->get('text_1');
        $color_text1 = $request->get('color_text_1');
        $text2 = $request->get('text_2');
        $color_text2 = $request->get('color_text_2');
        $text3 = $request->get('text_3');
        $color_text3 = $request->get('color_text_3');
        $home_slider_pic = $request->file('home_slider');
        $ext = $home_slider_pic->getClientOriginalExtension();
        $t=time();
        $fileName = "product_cover_image_".$t.'.'.$ext;
        $location = public_path('images/bg/');
        $home_slider_pic->move($location, $fileName);
        $home_slider_pic_path = 'images/bg/'.$fileName;
        $index = 0;
        $home_slider = Page::where('name', 'home')->first();
        $get_home_slider_data = json_decode($home_slider->value, true);
        if(!empty($get_home_slider_data) && array_key_exists('home_slider', $get_home_slider_data)) {
            $home_slider_data = $get_home_slider_data['home_slider'];
            if (array_key_exists('slider', $home_slider_data)) {
                $index = count($home_slider_data['slider']['image']);
            }
        }
        $get_home_slider_data['home_slider']['slider']['image'][$index] = $home_slider_pic_path;
        $get_home_slider_data['home_slider']['slider']['text'][$index][0] = array($text1, $color_text1);
        $get_home_slider_data['home_slider']['slider']['text'][$index][1] = array($text2, $color_text2);
        $get_home_slider_data['home_slider']['slider']['text'][$index][2] = array($text3, $color_text3);
        $home_slider->value = json_encode($get_home_slider_data);
        $home_slider->save();

        if(Cache::has('home_slider')){
            Cache::forget('home_slider');
        }
        session()->flash('success_message','Home sliders added successfully');
        return redirect()->back();
    }

    public function homeSliderRemove($index){

        $home_slider = Page::where('name', 'home')->first();
        $get_home_slider_data = json_decode($home_slider->value, true);
        if(!empty($get_home_slider_data) && array_key_exists('home_slider', $get_home_slider_data)){

            unset($get_home_slider_data['home_slider']['slider']['image'][$index]);
            unset($get_home_slider_data['home_slider']['slider']['text'][$index][0]);
            unset($get_home_slider_data['home_slider']['slider']['text'][$index][1]);
            unset($get_home_slider_data['home_slider']['slider']['text'][$index][2]);

            $count = count($get_home_slider_data['home_slider']['slider']['image']);
            if($count == 0){
                unset($get_home_slider_data['home_slider']['slider']);
            }
            $reindex = 0;
            $reindex_data['home_slider']['slider']['image'] = array();
            foreach ($get_home_slider_data['home_slider']['slider']['image'] as $key => $value){
                $reindex_data['home_slider']['slider']['image'][$reindex] = $value;
                $reindex_data['home_slider']['slider']['text'][$reindex][0] = $get_home_slider_data['home_slider']['slider']['text'][$key][0];
                $reindex_data['home_slider']['slider']['text'][$reindex][1] = $get_home_slider_data['home_slider']['slider']['text'][$key][1];
                $reindex_data['home_slider']['slider']['text'][$reindex][2] = $get_home_slider_data['home_slider']['slider']['text'][$key][2];
                $reindex++;
            }
            $get_home_slider_data = $reindex_data;
            if(Cache::has('home_slider')){
                Cache::forget('home_slider');
            }
            $home_slider->value = json_encode($get_home_slider_data);
            $home_slider->save();

        }
        session()->flash('success_message','Home sliders remove successfully');
        return redirect()->back();
    }

    public function store(Request $request){
         $page = "home";

         $pages = Page::where('name', $page)->first();
         $data = $this->homePage();
         if($pages){
            $pages->value = $data;
         }else{
             $pages = new Page();
             $pages->name = "home";
             $pages->value = $data;
         }
         $pages->save();
    }

    public function homePage(){
        $home_page_details = array();
        $home_page_details['home_slider']['slider']['image'][0] = 'theme/images/bg/9.jpg';
        $home_page_details['home_slider']['slider']['text'][0][0] = array('Buy9', 'your');
        $home_page_details['home_slider']['slider']['text'][0][1] = array('favourite9','Book');
        $home_page_details['home_slider']['slider']['text'][0][2] = array('from9', 'Here');
        $home_page_details['home_slider']['slider']['image'][1] = 'theme/images/bg/8.jpg';
        $home_page_details['home_slider']['slider']['text'][1][0] = array('Buy8', 'your');
        $home_page_details['home_slider']['slider']['text'][1][1] = array('favourite8','Book');
        $home_page_details['home_slider']['slider']['text'][1][2] = array('from8', 'Here');
        $home_page_details['home_slider']['slider']['image'][2] = 'theme/images/bg/1.jpg';
        $home_page_details['home_slider']['slider']['text'][2][0] = array('Buy1', 'your');
        $home_page_details['home_slider']['slider']['text'][2][1] = array('favourite1','Book');
        $home_page_details['home_slider']['slider']['text'][2][2] = array('from1', 'Here');
        $home_page_details['home_slider']['slider']['image'][3] = 'theme/images/bg/7.jpg';
        $home_page_details['home_slider']['slider']['text'][3][0] = array('Buy7', 'your');
        $home_page_details['home_slider']['slider']['text'][3][1] = array('favourite7','Book');
        $home_page_details['home_slider']['slider']['text'][3][2] = array('from7', 'Here');

        if(Cache::has('home_slider')){
            Cache::forget('home_slider');
        }
        Cache::forever('home_slider', $home_page_details['home_slider']);

        return json_encode($home_page_details);
    }

    public function aboutUs(){
        array_push(Breadcrumbs::$breadcrumb,array('Pages - About us',''));
        $page = "about-us";
        $pages = Page::where('name', $page)->first();
        $data = array();
        $data['content_title'] = null;
        $data['summary'] = null;
        $data['our_vision'] = null;
        $data['our_mission'] = null;
        if($pages){
            $content = json_decode($pages->value, true);
            if(array_key_exists('content', $content)){
                $data['content_title'] = $content['content']['content_title'];
                $data['summary'] = $content['content']['summary'];
              //  $data['our_vision'] = $content['content']['our_vision'];
              //  $data['our_mission'] = $content['content']['our_mission'];
            }
        }
        return view('admin.pages.about-us', compact('data'));
    }

    public function aboutUsStore(Request $request)
    {
        /*
        $rules = [
            'about_us_title' => 'required|string|max:255',
            'about_us_page_content' => 'required|string',
            'our_vision_content' => 'required|string',
            'our_mission_content' => 'required|string',
        ];

        $messages = [
            'about_us_title.required' => 'Page content title is required',
            'about_us_title.string'   => 'Page content should be string',
            'about_us_title.max'      => 'Maximum characters is 255',
            'about_us_page_content.required' => 'Summary is required',
            'about_us_page_content.string'  => 'Summary should be string',
            'our_vision_content.required' => 'Our vision is required',
            'our_vision_content.string'  => 'Our vision should be string',
            'our_mission_content.required' => 'Our mission is required',
            'our_mission_content.string'  => 'Our mission should be string',
        ];
        */
        $content_title = $request->get('about_us_title');
        $summary = $request->get('about_us_page_content');
        if (false) {
            $our_vision = $request->get('our_vision_content');
            $our_mission = $request->get('our_mission_content');
        }

        $page = "about-us";
        $pages = Page::where('name', $page)->first();
        $data = array();

        if (!$pages) {
            $pages = new Page();
            $pages->name = $page;
        } else {
            $content = json_decode($pages->value);
            if (array_key_exists('slider', $content)) {
                $data['slider']['image'] = $content['slider']['image'];
            }
        }
        $data['content']['content_title'] = $content_title;
        $data['content']['summary'] = $summary;
        if (false){
            $data['content']['our_vision'] = $our_vision;
            $data['content']['our_mission'] = $our_mission;
        }
        $pages->value = json_encode($data);
        $pages->save();
        session()->flash('success_message','About us page content added successfully');
        return redirect()->back();
    }

    public function contactUs(){
        array_push(Breadcrumbs::$breadcrumb,array('Pages - Contact us',''));
        $page = "contact-us";
        $pages = Page::where('name', $page)->first();
        $data = array();
        if($pages){
            $data = json_decode($pages->value, true);
        }else{
            $data['content']['office_info_content'] = "";
            $data['content']['get_in_touch_content'] = "";
            $data['content']['office_address'] = "";
            $data['content']['office_phone'] = "";
            $data['content']['office_email'] = "";
        }
        return view('admin.pages.contact-us', compact('data'));
    }

    public function contactUsStore(Request $request){

        $office_info_content = $request->get('office_info_content');
        $get_in_touch_content = $request->get('get_in_touch_content');
        $office_address = $request->get('office_address');
        $office_phone = $request->get('office_phone');
        $office_email = $request->get('office_email');

        $page = "contact-us";
        $pages = Page::where('name', $page)->first();
        $data = array();
        if(!$pages){
            $pages = new Page();
            $pages->name = $page;
        }

        $data['content']['office_info_content'] = $office_info_content;
        $data['content']['get_in_touch_content'] = $get_in_touch_content;
        $data['content']['office_address'] = $office_address;
        $data['content']['office_phone'] = $office_phone;
        $data['content']['office_email'] = $office_email;
        $pages->value = json_encode($data);
        $pages->save();

        session()->flash('success_message','Contact us page content added successfully');
        return redirect()->back();
    }

    public function backgroundImage(){
        array_push(Breadcrumbs::$breadcrumb,array('Pages - Images',''));
        $data = array();
        $page = "bg-images";
        $pages = Page::where('name', $page)->first();
        if($pages){
            $data = json_decode($pages->value, true);
        }

        return view('admin.pages.images', compact('data'));
    }

    public function backgroundImageStore(Request $request){
        $page = "bg-images";
        $page_name = $request->get('page_name');
        $bg_image = $request->file('background_image');
        $ext = $bg_image->getClientOriginalExtension();
        $t=time();
        $fileName = "bg_image_".$t.'.'.$ext;
        $location = public_path('images/bg/');
        if(!is_dir($location)){
            mkdir($location);
            chmod($location, 0777);
        }
        $bg_image->move($location, $fileName);
        $bg_image_path = 'images/bg/'.$fileName;
        $pages = Page::where('name', $page)->first();
        $data = array();
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('background_image', $content)){
                $i = 0;
                $index = -1;
                foreach ($content['background_image'] as $key => $value){
                    if($value['name'] != $page_name){
                        $data['background_image'][$i]['name'] = $value['name'];
                        $data['background_image'][$i]['image'] = $value['image'];
                    }else{
                        $index = $i;
                    }
                    $i++;
                }
                if($index != -1){
                    $i = $index;
                }
                $data['background_image'][$i]['name'] = $page_name;
                $data['background_image'][$i]['image'] = $bg_image_path;
            }else{
                $data['background_image'][0]['name'] = $page_name;
                $data['background_image'][0]['image'] = $bg_image_path;
            }
        }else{
            $pages = new Page();
            $pages->name = $page;
            $data['background_image'][0]['name'] = $page_name;
            $data['background_image'][0]['image'] = $bg_image_path;
        }

        $pages->value = json_encode($data);
        $pages->save();
        if(Cache::has('bg_images')){
            Cache::forget('bg_images');
        }
        session()->flash('success_message','A Page background added successfully');
        return redirect()->back();
    }

    public function backgroundImageRemove($index){
        $page = "bg-images";
        $pages = Page::where('name', $page)->first();
        $data = array();
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('background_image', $content)) {
                $i = 0;
                foreach ($content['background_image'] as $key => $value){
                    if($key != $index){
                        $data['background_image'][$i]['name'] = $value['name'];
                        $data['background_image'][$i]['image'] = $value['image'];
                        $i++;
                    }
                }
                $pages->value = json_encode($data);
                $pages->save();
                if(Cache::has('bg_images')){
                    Cache::forget('bg_images');
                }
            }
        }
        session()->flash('success_message','A Page background remove successfully');
        return redirect()->back();
    }

    public function authors(){
        array_push(Breadcrumbs::$breadcrumb,array('Pages - Authors',''));
        $data = array();
        $page = "authors";
        $pages = Page::where('name', $page)->first();
        if($pages){
            $data = json_decode($pages->value, true);
        }

        return view('admin.pages.authors', compact('data'));
    }

    public function authorsStore(Request $request){
        $rules = [
            'author_name' => 'required|string|max:255',
            'author_image' => 'required|max:20000|mimes:png,jpeg,jpg|dimensions:min_width=200,min_height=200'
        ];

        $message = [
            'author_name.required'  => 'The author name is required',
            'author_name.string'    => 'The author name should be string',
            'author_name.max'       => ' Maximum characters is 255',
            'author_image.required' => 'The image field is required',
            'author_image.max' => 'The image max size 2MB',
            'author_image.mimes' => 'This image allows only png,jpeg,jpg',
            'author_image.dimensions' => 'This image resolution should be 200x200'
        ];
        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Author add unsuccessfully');
            return redirect()->back()
                ->withErrors($validator);
        }
        $page = "authors";
        $author_name = $request->get('author_name');
        $author_image = $request->file('author_image');
        $ext = $author_image->getClientOriginalExtension();
        $t=time();
        $fileName = "author_image_".$t.'.'.$ext;
        $location = public_path('images/author/');
        if(!is_dir($location)){
            mkdir($location);
            chmod($location, 0777);
        }
        $author_image->move($location, $fileName);
        $author_name_path = 'images/author/'.$fileName;
        $social_medias = config('status.social_media');
        $pages = Page::where('name', $page)->first();
        $data = array();
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('author_list', $content)){
                $i = 0;
                $index = -1;
                foreach ($content['author_list'] as $key => $value){
                    $data['author_list'][$i]['name'] = $value['name'];
                    $data['author_list'][$i]['image'] = $value['image'];
                    foreach($social_medias as $social_media){
                        if(array_key_exists($social_media['value'], $value)){
                            $data['author_list'][$i][$social_media['value']] = $value[$social_media['value']];
                        }else{
                            $data['author_list'][$i][$social_media['value']] = null;
                        }
                    }
                    $i++;
                }
                $data['author_list'][$i]['name'] = $author_name;
                $data['author_list'][$i]['image'] = $author_name_path;
                foreach($social_medias as $social_media){
                    $data['author_list'][$i][$social_media['value']] = $request->get('author_'.$social_media['value']);
                }
            }else{
                $data['author_list'][0]['name'] = $author_name;
                $data['author_list'][0]['image'] = $author_name_path;
                foreach($social_medias as $social_media){
                    $data['author_list'][0][$social_media['value']] = $request->get('author_'.$social_media['value']);
                }
            }
        }else{
            $pages = new Page();
            $pages->name = $page;
            $data['author_list'][0]['name'] = $author_name;
            $data['author_list'][0]['image'] = $author_name_path;
            foreach($social_medias as $social_media){
                $data['author_list'][0][$social_media['value']] = $request->get('author_'.$social_media['value']);
            }
        }

        $pages->value = json_encode($data);
        $pages->save();
        if(Cache::has('author_list')){
            Cache::forget('author_list');
        }
        session()->flash('success_message','Author added successfully');
        return redirect()->back();
    }

    public function authorsRemove($index){
        $page = "authors";
        $pages = Page::where('name', $page)->first();
        $data = array();
        $social_medias = config('status.social_media');
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('author_list', $content)) {
                $i = 0;
                foreach ($content['author_list'] as $key => $value){
                    if($key != $index){
                        $data['author_list'][$i]['name'] = $value['name'];
                        $data['author_list'][$i]['image'] = $value['image'];
                        foreach($social_medias as $social_media){
                            if(array_key_exists($social_media['value'], $value)){
                                $data['author_list'][$i][$social_media['value']] = $value[$social_media['value']];
                            }else{
                                $data['author_list'][$i][$social_media['value']] = null;
                            }
                        }
                        $i++;
                    }
                }
                $pages->value = json_encode($data);
                $pages->save();
                if(Cache::has('author_list')){
                    Cache::forget('author_list');
                }
            }
        }
        session()->flash('success_message','Author remove successfully');
        return redirect()->back();
    }

    public function getAuthors(Request $request){
        $page = "authors";
        $index = $request->get('index');
        $pages = Page::where('name', $page)->first();
        $data = array();
        $social_medias = config('status.social_media');
        $data['author_list']['ids'] = array_column($social_medias, 'value');
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('author_list', $content)) {
                foreach ($content['author_list'] as $key => $value){
                    if($key == $index){
                        $data['author_list']['name'] = $value['name'];
                        $image = null;
                        if($value['image']){
                            $path = asset($value['image']);
                            $image = asset($path);
                        }
                        $data['author_list']['image'] = $image;
                        foreach($social_medias as $social_media){
                            if(array_key_exists($social_media['value'], $value)){
                                $data['author_list'][$social_media['value']] = $value[$social_media['value']];
                            }else{
                                $data['author_list'][$social_media['value']] = null;
                            }
                        }
                    }
                }
            }
        }
        return json_encode($data);
    }

    public function editAuthors(Request $request){
        $rules = [
            'author_name' => 'required|string|max:255',
            'author_image' => 'nullable|max:20000|mimes:png,jpeg,jpg|dimensions:min_width=200,min_height=200'
        ];

        $message = [
            'author_name.required'  => 'The author name is required',
            'author_name.string'    => 'The author name should be string',
            'author_name.max'       => ' Maximum characters is 255',
            'author_image.max' => 'The image max size 2MB',
            'author_image.mimes' => 'This image allows only png,jpeg,jpg',
            'author_image.dimensions' => 'This image resolution should be 200x200'
        ];
        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Author edit unsuccessfully');
            return redirect()->back()
                ->withErrors($validator);
        }
        $social_medias = config('status.social_media');
        $page = "authors";
        $index = $request->get('team_member_index');
        $image_previous_url = $request->get('image_previous_url');
        $author_name = $request->get('author_name');
        $author_name_path = null;
        $author_image = $request->file('author_image');
        if($author_image){
            $ext = $author_image->getClientOriginalExtension();
            $t=time();
            $fileName = "author_image_".$t.'.'.$ext;
            $location = public_path('images/author/');
            if(!is_dir($location)){
                mkdir($location);
                chmod($location, 0777);
            }
            $author_image->move($location, $fileName);
            $author_name_path = 'images/author/'.$fileName;
        }
        $pages = Page::where('name', $page)->first();
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('author_list', $content)) {
                foreach ($content['author_list'] as $key => $value){
                    if($key == $index){
                        $content['author_list'][$key]['name'] = $author_name;
                        if(empty($image_previous_url)){
                            $content['author_list'][$key]['image'] = $author_name_path;
                        }
                        foreach ($social_medias as $social_media){
                            $content['author_list'][$key][$social_media['value']] = $request->get('author_'.$social_media['value']);
                        }
                    }
                }
            }
            $pages->value = json_encode($content);
            $pages->save();
        }
        if(Cache::has('author_list')){
            Cache::forget('author_list');
        }
        session()->flash('success_message','Author updated successfully');
        return redirect()->back();
    }

    public function teamMembers(){
        array_push(Breadcrumbs::$breadcrumb,array('Pages - Team members',''));
        $data = array();
        $page = "team_members";
        $pages = Page::where('name', $page)->first();
        if($pages){
            $data = json_decode($pages->value, true);
        }
        //dd($data);
        return view('admin.pages.team-members', compact('data'));
    }

    public function teamMembersStore(Request $request){
        $rules = [
            'team_member_name' => 'required|string|max:255',
            'team_member_position' => 'required|string|max:255',
            'team_member_image' => 'required|max:20000|mimes:png,jpeg,jpg|dimensions:min_width=200,min_height=200'
        ];

        $message = [
            'team_member_name.required'  => 'The team member name is required',
            'team_member_name.string'    => 'The team member should be string',
            'team_member_name.max'       => ' Maximum characters is 255',
            'team_member_position.required'  => 'The team member position is required',
            'team_member_position.string'    => 'The team member position should be string',
            'team_member_position.max'       => ' Maximum characters is 255',
            'team_member_image.required' => 'The image field is required',
            'team_member_image.max' => 'The image max size 2MB',
            'team_member_image.mimes' => 'This image allows only png,jpeg,jpg',
            'team_member_image.dimensions' => 'This image resolution should be 200x200'
        ];
        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Team member add unsuccessfully');
            return redirect()->back()
                ->withErrors($validator);
        }
        $page = "team_members";
        $team_member_name = $request->get('team_member_name');
        $team_member_position = $request->get('team_member_position');
        $team_member_twitter = $request->get('team_member_twitter');
        $team_member_tumblr = $request->get('team_member_tumblr');
        $team_member_facebook = $request->get('team_member_facebook');
        $team_member_linkedin = $request->get('team_member_linkedin');
        $team_member_pinterest = $request->get('team_member_pinterest');

        $team_member_image = $request->file('team_member_image');
        $ext = $team_member_image->getClientOriginalExtension();
        $t=time();
        $fileName = "team_member_image".$t.'.'.$ext;
        $location = public_path('images/team_member/');
        if(!is_dir($location)){
            mkdir($location);
            chmod($location, 0777);
        }
        $team_member_image->move($location, $fileName);
        $team_member_path = 'images/team_member/'.$fileName;

        $pages = Page::where('name', $page)->first();
        $data = array();
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('team_member', $content)){
                $i = 0;
                $index = -1;
                foreach ($content['team_member'] as $key => $value){
                    $data['team_member'][$i]['name'] = $value['name'];
                    $data['team_member'][$i]['image'] = $value['image'];
                    $data['team_member'][$i]['position'] = $value['position'];
                    $data['team_member'][$i]['twitter'] = $value['twitter'];
                    $data['team_member'][$i]['tumblr'] = $value['tumblr'];
                    $data['team_member'][$i]['facebook'] = $value['facebook'];
                    $data['team_member'][$i]['linkedin'] = $value['linkedin'];
                    $data['team_member'][$i]['pinterest'] = $value['pinterest'];
                    $i++;
                }
                if($index != -1){
                    $i = $index;
                }
                $data['team_member'][$i]['name'] = $team_member_name;
                $data['team_member'][$i]['image'] = $team_member_path;
                $data['team_member'][$i]['position'] = $team_member_position;
                $data['team_member'][$i]['twitter'] = $team_member_twitter;
                $data['team_member'][$i]['tumblr'] = $team_member_tumblr;
                $data['team_member'][$i]['facebook'] = $team_member_facebook;
                $data['team_member'][$i]['linkedin'] = $team_member_linkedin;
                $data['team_member'][$i]['pinterest'] = $team_member_pinterest;

            }else{
                $data['team_member'][0]['name'] = $team_member_name;
                $data['team_member'][0]['image'] = $team_member_path;
                $data['team_member'][0]['position'] = $team_member_position;
                $data['team_member'][0]['twitter'] = $team_member_twitter;
                $data['team_member'][0]['tumblr'] = $team_member_tumblr;
                $data['team_member'][0]['facebook'] = $team_member_facebook;
                $data['team_member'][0]['linkedin'] = $team_member_linkedin;
                $data['team_member'][0]['pinterest'] = $team_member_pinterest;
            }
        }else{
            $pages = new Page();
            $pages->name = $page;
            $data['team_member'][0]['name'] = $team_member_name;
            $data['team_member'][0]['image'] = $team_member_path;
            $data['team_member'][0]['position'] = $team_member_position;
            $data['team_member'][0]['twitter'] = $team_member_twitter;
            $data['team_member'][0]['tumblr'] = $team_member_tumblr;
            $data['team_member'][0]['facebook'] = $team_member_facebook;
            $data['team_member'][0]['linkedin'] = $team_member_linkedin;
            $data['team_member'][0]['pinterest'] = $team_member_pinterest;
        }

        $pages->value = json_encode($data);
        $pages->save();
        if(Cache::has('team_member')){
            Cache::forget('team_member');
        }
        session()->flash('success_message','Team member added successfully');
        return redirect()->back();
    }

    public function teamMembersRemove($index){
        $page = "team_members";
        $pages = Page::where('name', $page)->first();
        $data = array();
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('team_member', $content)) {
                $i = 0;
                foreach ($content['team_member'] as $key => $value){
                    if($key != $index){
                        $data['team_member'][$i]['name'] = $value['name'];
                        $data['team_member'][$i]['image'] = $value['image'];
                        $data['team_member'][$i]['position'] = $value['position'];
                        $data['team_member'][$i]['twitter'] = $value['twitter'];
                        $data['team_member'][$i]['tumblr'] = $value['tumblr'];
                        $data['team_member'][$i]['facebook'] = $value['facebook'];
                        $data['team_member'][$i]['linkedin'] = $value['linkedin'];
                        $data['team_member'][$i]['pinterest'] = $value['pinterest'];
                        $i++;
                    }
                }
                $pages->value = json_encode($data);
                $pages->save();
                if(Cache::has('team_member')){
                    Cache::forget('team_member');
                }
            }
        }
        session()->flash('success_message','Team member remove successfully');
        return redirect()->back();
    }

    public function getTeamMembers(Request $request){
        $page = "team_members";
        $index = $request->get('index');
        $pages = Page::where('name', $page)->first();
        $data = array();
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('team_member', $content)) {
                foreach ($content['team_member'] as $key => $value){
                    if($key == $index){
                        $data['team_member']['name'] = $value['name'];
                        $image = null;
                        if($value['image']){
                            $path = asset($value['image']);
                            $image = asset($path);
                        }

                        $data['team_member']['image'] = $image;
                        $data['team_member']['position'] = $value['position'];
                        $data['team_member']['twitter'] = $value['twitter'];
                        $data['team_member']['tumblr'] = $value['tumblr'];
                        $data['team_member']['facebook'] = $value['facebook'];
                        $data['team_member']['linkedin'] = $value['linkedin'];
                        $data['team_member']['pinterest'] = $value['pinterest'];
                    }
                }
            }
        }

        return json_encode($data);
    }

    public function editTeamMembers(Request $request){

        $rules = [
            'team_member_name' => 'required|string|max:255',
            'team_member_position' => 'required|string|max:255',
            'team_member_image' => 'nullable|max:20000|mimes:png,jpeg,jpg|dimensions:min_width=200,min_height=200'
        ];

        $message = [
            'team_member_name.required'  => 'The team member name is required',
            'team_member_name.string'    => 'The team member should be string',
            'team_member_name.max'       => ' Maximum characters is 255',
            'team_member_position.required'  => 'The team member position is required',
            'team_member_position.string'    => 'The team member position should be string',
            'team_member_position.max'       => ' Maximum characters is 255',
            'team_member_image.max' => 'The image max size 2MB',
            'team_member_image.mimes' => 'This image allows only png,jpeg,jpg',
            'team_member_image.dimensions' => 'This image resolution should be 200x200'
        ];
        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Team member edit unsuccessfully');
            return redirect()->back()
                ->withErrors($validator);
        }
        $page = "team_members";
        $team_member_name = $request->get('team_member_name');
        $team_member_position = $request->get('team_member_position');
        $team_member_twitter = $request->get('team_member_twitter');
        $team_member_tumblr = $request->get('team_member_tumblr');
        $team_member_facebook = $request->get('team_member_facebook');
        $team_member_linkedin = $request->get('team_member_linkedin');
        $team_member_pinterest = $request->get('team_member_pinterest');
        $index = $request->get('team_member_index');
        $image_previous_url = $request->get('image_previous_url');
        $team_member_image = $request->file('team_member_image');
        $team_member_path = null;
        if($team_member_image){
            $ext = $team_member_image->getClientOriginalExtension();
            $t=time();
            $fileName = "team_member_image".$t.'.'.$ext;
            $location = public_path('images/team_member/');
            if(!is_dir($location)){
                mkdir($location);
                chmod($location, 0777);
            }
            $team_member_image->move($location, $fileName);
            $team_member_path = 'images/team_member/'.$fileName;
        }
        $pages = Page::where('name', $page)->first();
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('team_member', $content)) {
                foreach ($content['team_member'] as $key => $value){
                    if($key == $index){
                        $content['team_member'][$key]['name'] = $team_member_name;
                        if(empty($image_previous_url)){
                            $content['team_member'][$key]['image'] = $team_member_path;
                        }
                        $content['team_member'][$key]['position'] = $team_member_position;
                        $content['team_member'][$key]['twitter'] = $team_member_twitter;
                        $content['team_member'][$key]['tumblr'] = $team_member_tumblr;
                        $content['team_member'][$key]['facebook'] = $team_member_facebook;
                        $content['team_member'][$key]['linkedin'] = $team_member_linkedin;
                        $content['team_member'][$key]['pinterest'] = $team_member_pinterest;
                    }
                }
            }
            $pages->value = json_encode($content);
            $pages->save();
        }
        if(Cache::has('team_member')){
            Cache::forget('team_member');
        }
        session()->flash('success_message','Team member update successfully');
        return redirect()->back();
    }

    public function socialMedia(){
        array_push(Breadcrumbs::$breadcrumb,array('Pages - Social Media',''));
        $page = "social_media";
        $pages = Page::where('name', $page)->first();
        $data = array();
        $social_medias = config('status.social_media');
        foreach ($social_medias as $social_media){
            $data[$social_media['value']] = null;
        }
        if($pages){
            $content = json_decode($pages->value, true);
            if(!empty($content) && array_key_exists('social_media', $content)) {
                foreach ($social_medias as $social_media){
                    if(array_key_exists($social_media['value'], $content['social_media'])){
                        $data[$social_media['value']] = $content['social_media'][$social_media['value']];
                    }
                }
            }
        }
        return view('admin.pages.social-media', compact('data'));
    }

    public function socialMediaStore(Request $request){

        $social_medias = config('status.social_media');
        foreach ($social_medias as $social_media){
            $data['social_media'][$social_media['value']] = $request->get($social_media['value'].'_url');
        }
        $page = "social_media";
        $pages = Page::where('name', $page)->first();
        if(!$pages){
            $pages = new Page();
            $pages->name = $page;
        }
        $pages->value = json_encode($data);
        $pages->save();
        if(Cache::has('social_media')){
            Cache::forget('social_media');
        }
        session()->flash('success_message','Social media added/updated successfully');
        return redirect()->back();
    }

    public function setting(){
        array_push(Breadcrumbs::$breadcrumb,array('Pages - Settings',''));
        $page = "settings";
        $pages = Page::where('name', $page)->first();
        $data = array();
        $data['favicon'] = null;
        if($pages){
            $content = json_decode($pages->value, true);
            if($content && array_key_exists('favicon', $content)){
                $data['favicon'] = $content['favicon']['image'];
            }
        }
        return view('admin.pages.setting', compact('data'));
    }

    public function storeSetting(Request $request){
        $page = "settings";
        $pages = Page::where('name', $page)->first();
        $favicon_image = $request->file('favicon_image');
        $favicon_image_path = null;
        if($favicon_image){
            $ext = $favicon_image->getClientOriginalExtension();
            $t=time();
            $fileName = "favicon_image".$t.'.'.$ext;
            $location = public_path('images/');
            if(!is_dir($location)){
                mkdir($location);
                chmod($location, 0777);
            }
            $favicon_image->move($location, $fileName);
            $favicon_image_path = 'images/'.$fileName;
        }
        $data = array();
        if(!$pages){
            $pages = new Page();
            $pages->name = $page;
        }else{
            $data = json_decode($pages->value, true);
        }
        $data['favicon']['image'] = $favicon_image_path;
        $pages->value = json_encode($data);
        $pages->save();
        if(Cache::has('favicon')){
            Cache::forget('favicon');
        }
        session()->flash('success_message','Settings added/updated successfully');
        return redirect()->back();
    }
}
