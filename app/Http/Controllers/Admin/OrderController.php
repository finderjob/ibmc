<?php

namespace App\Http\Controllers\Admin;

use App\Address;
use App\Coupon;
use App\Customer;
use App\Order;
use App\OrderHistory;
use App\OrderProduct;
use App\Payment;
use App\Product;
use App\ProductPrice;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Breadcrumbs;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
        expire_orders();
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Orders','#'));
        $orders = Order::where('status','!=', 0)->where('valid_status','=',1)->orderBy('id', 'DESC')->get();
        return view('admin.orders.list', compact('orders'));
    }//

    public function add(){
        array_push(Breadcrumbs::$breadcrumb,array('Orders', 'admin.orders'));
        array_push(Breadcrumbs::$breadcrumb,array('Add', ''));

        return view('admin.orders.add');
    }

    public function view($id){
        $order = Order::find($id);
        if(!$order){
            session()->flash('error_message','Order doesn\'t exist');
            return redirect()->to(route('admin.orders'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Orders', 'admin.orders'));
        array_push(Breadcrumbs::$breadcrumb,array('Order Details', ''));
        $order_products = OrderProduct::where('order_id', $order->id)->get();
        $order_payments = Payment::where('order_id', $order->id)->get();
        $order_histories = OrderHistory::where('order_id', $order->id)->get();
        return view('admin.orders.view', compact('order', 'order_products', 'order_payments', 'order_histories'));
    }

    public function changeOrderStatus(Request $request){
        $order_status = $request->get('order_status');
        $order_id = $request->get('order_id');
        $order_status_lists = config('status.orders');
        $predefined = array_column($order_status_lists, 'id');
        $order_status_cannot_change = config('status.order_status_cannot_backward');

        $data = array();
        $order_history = array();
        $order_history['section'] = "Order List";
        $order_history['order_id'] = $order_id;
        $order_history['description'] = "Order status changed";
        if(!in_array($order_status, $predefined)){
            $data['status'] = 0;
            $message = 'Order status change unsuccessfully';
            $data['message'] = $message;
        }else{
            $order = Order::find($order_id);
            if($order){
                if(in_array($order->status, $order_status_cannot_change) && !in_array($order_status, $order_status_cannot_change)){
                    $data['status'] = 0;
                    $message = 'Order status change unsuccessfully';
                    $data['message'] = $message;
                }else{
                    foreach ($order_status_lists as $order_status_list){
                        if($order_status_list['id'] == $order_status){
                            $change_status = true;
                            if($order_status_list['payment']){
                                if(!$order->payment_status){
                                    $change_status = false;
                                }
                            }
                            if($change_status){
                                $order->status = $order_status;
                                if($order_status_list['return_qty']){
                                    $order->payment_status = 2;
                                    $this->orderRefundOrCancel($order->id);
                                }
                                $order->save();

                                $status = '<li> Status: ';
                                $status .= $order_status_list['name'];
                                $status .= '</li>';
                                $order_history['data'] = json_encode($status);
                                createOrderHistory($order_history);

                                $user_lists = User::where('user_type', '!=', 3)
                                    //->where('id','!=', Auth::guard('admins')->user()->id)
                                    ->get();
                                foreach ($user_lists as $user_list){
                                    $notification['user_id'] = $user_list->id;
                                    $od = orderStatus($order_status);
                                    $notification['data'] = Auth::guard('admins')->user()->name." changed status as ".$od[0]." for order id: ".$order->order_id;
                                    $notification['url'] = route('admin.orders.view', [$order->id]);
                                    createNotifications($notification);
                                }
                                $data['status'] = 1;
                                $message = 'Order status changed successfully';
                                $data['message'] = $message;
                            }else{
                                $data['status'] = 0;
                                $message = 'Order payment not complete';
                                $data['message'] = $message;
                            }
                        }
                    }
                }
            }else{
                $data['status'] = 0;
                $message = 'This order doesn\'t exists';
                $data['message'] = $message;
            }
        }
        return json_encode($data);
    }

    protected function orderRefundOrCancel($order_id){

        $order_products = OrderProduct::where('order_id', $order_id)->get();
        foreach ($order_products as $order_product){
            $product = Product::find($order_product->product_id);
            if($product){
                $product->qty = $product->qty + $order_product->qty;
                $product->save();
            }
        }
    }

    public function indexOld(Request $request){

        $year = $request->get('order_year');
        $order_status = $request->get('order_status');
        $order_payment_status = $request->get('order_payment_status');
        array_push(Breadcrumbs::$breadcrumb,array('Old Orders','#'));
        $orders = Order::where('valid_status','=',0)->addCustomerFilter($year, $order_status, $order_payment_status)
                        ->orderBy('id', 'DESC')
                        ->get();

        return view('admin.orders.old-list', compact('orders', 'year', 'order_status', 'order_payment_status'));
    }

    public function viewOld($id){
        $order = Order::find($id);
        if(!$order){
            session()->flash('error_message','Order doesn\'t exist');
            return redirect()->to(route('admin.orders'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Old Orders', 'admin.orders'));
        array_push(Breadcrumbs::$breadcrumb,array('Order Details', ''));
        $order_products = OrderProduct::where('order_id', $order->id)->get();
        $order_payments = Payment::where('order_id', $order->id)->get();
        $order_histories = OrderHistory::where('order_id', $order->id)->get();
        return view('admin.orders.view', compact('order', 'order_products', 'order_payments', 'order_histories'));
    }

    public function customer(Request $request){

        $name = $request->get('customer_name');
        $name = $name['term'];
        $email = $request->get('customer_email');
        $email = $email['term'];
        $json = array();
        $json['result'] = [];
        if($name && empty($emial)){
            $customers = Customer::where('status', 1)
                                    ->where('name', 'like', $name."%")
                                    ->orWhere('first_name', 'like', $name."%")
                                    ->orWhere('last_name', 'like', $name."%")
                                    ->get();
        }elseif($email && empty($name)){
            $customers = Customer::where('status', 1)
                                    ->where('email', 'like', $email.'%')
                                    ->get();
        }else{
            $customers = Customer::where('status', 1)
                ->where('complete_status', 2)
                ->where('first_name', 'like', $name."%")
                ->orWhere('last_name', 'like', $name."%")
                ->get();
        }
        $return[] = null;
        foreach ($customers as $customer){
            $set = array();
            $set['id'] = $customer->id;
            if($customer->first_name || $customer->last_name){
                if(!$customer->last_name && $customer->first_name){
                    $set['name'] = $customer->first_name;
                }elseif($customer->last_name && !$customer->first_name){
                    $set['name'] = $customer->last_name;
                }else{
                    $set['name'] = $customer->first_name." ".$customer->last_name;
                }
            }else{
                $set['name'] = $customer->name;
            }
            $set['email'] = $customer->email;
            $return[] = $set;
        }
        return json_encode($return);
    }

    public function customerInfos(Request $request){
        $customer_id = $request->get('customer_id');
        $json = array();
        $customer = Customer::find($customer_id);
        if($customer){
            $addresses = Address::where('customer_id', $customer_id)->get();

            if(count($addresses) == 0){
                $json['status'] = 0;
                $json['message'] = "The customer has no address";
            }else{
                foreach ($addresses as $address){
                    $set = array();
                    if($address->address_type == 1){
                        $type = 'billing';
                    }else{
                        $type = "shipping";
                    }
                    $set['address_id'] = $address->id;
                    $set['first_name'] = $address->first_name;
                    $set['last_name'] = $address->last_name;
                    $set['address_line_1'] = $address->address_line_1;
                    $set['address_line_2'] = $address->address_line_2;
                    $set['city'] = $address->city;
                    $set['zip_code'] = $address->zip_code;
                    $set['phone_number'] = $address->phone_number;
                    $set['mobile_number'] = $address->mobile_number;
                    $json['data'][$type] = $set;
                }

                if(count($json['data']) == 1){
                    $set['address_id'] = null;
                    $set['first_name'] = null;
                    $set['last_name'] = null;
                    $set['address_line_1'] = null;
                    $set['address_line_2'] = null;
                    $set['city'] = null;
                    $set['zip_code'] = null;
                    $set['phone_number'] = null;
                    $set['mobile_number'] = null;
                    $json['data']['shipping'] = $set;
                }
                $json['count'] = count($addresses);
                $json['status'] = 1;
                $json['message'] = "There is address";
            }
        }else{
            $json['status'] = 0;
            $json['message'] = "The customer doesn't exists";
        }

        return json_encode($json);
    }

    public function productsInfos(Request $request){
        $product_name = $request->get('product_name');
        $product_name = $product_name['term'];
        $product_ids = $request->get('product_ids');

        if($product_ids == null){
            $products = Product::where('name', 'like', $product_name.'%')->where('status', 1)->where('complete_status', 3)->get();
        }else{
            $products = Product::where('name', 'like', $product_name.'%')
                                ->where('status', 1)
                                ->where('complete_status', 3)
                                ->whereNotIn('id', $product_ids)
                                ->get();
        }
        $return[] = null;
        foreach ($products as $product) {
            $set = array();
            $set['id'] = $product->id;
            $set['name'] = $product->name;
            $return[] = $set;
        }

        return json_encode($return);
    }

    public function priceData(Request $request){

        $customer_id = $request->get('customer_id');
        $products = $request->get('products');
        $coupon_code = $request->get('coupon');
        $customer = Customer::find($customer_id);
        $customer_type = 1;
        if($customer){
            $customer_type = $customer->customer_type;
        }

        $json = array();
        $data = [];
        $total = 0;
        $sub_total = 0;
        if($products){
            foreach ($products as $product){
                $price = ProductPrice::where('product_id', $product['id'])->where('status', 1)->first();
                if($customer_type == 2){
                    $price = $price->school_price - $price->school_discount;
                }else{
                    $price = $price->normal_price - $price->normal_discount;
                }
                $product['price'] = "LKR ".number_format($price, 2, '.', ',');
                $amount = $price * $product['qty'];
                $product['sub_total'] = "LKR ".number_format($amount, 2,'.',',');
                $sub_total += $amount;
                $data[] = $product;
            }
        }
        $coupon_value = 0;
        $coupon = Coupon::where('name', $coupon_code)->first();
        if($coupon){
            $expire = $coupon->expire_at;
            $expire = strtotime($expire);
            $now = time();
            if($expire > $now){
                $coupon_data = apply_coupon_code($coupon, $sub_total);
                if($coupon_data){
                    if($coupon_data['status']){
                        $coupon_value = -1 * $coupon_data['value'];
                    }
                }
            }
        }
        $total = $sub_total + $coupon_value;
        $json['coupon'] = "LKR ".number_format($coupon_value, 2,'.',',');
        $json['products'] = $data;
        $json['sub_total'] = "LKR ".number_format($sub_total, 2,'.',',');
        $json['total'] = "LKR ".number_format($total, 2,'.',',');
        return json_encode($json);
    }

    public function store(Request $request){
        $user = $request->get('name');
        $customer = Customer::find($user);
        $customer_type = 1;
        if($customer){
            $customer_type = $customer->customer_type;
        }
        $products = $request->get('product');
        $qty = $request->get('qty');
        $sub_total = 0;
        for($i = 0; $i < count($products); $i++){
            $price = ProductPrice::where('product_id', $products[$i])->where('status', 1)->first();
            if($customer_type == 2){
                $price = $price->school_price - $price->school_discount;
            }else{
                $price = $price->normal_price - $price->normal_discount;
            }
            $sub_total += $price * $qty[$i];
        }
        $coupon_code = $request->get('coupon');
        $coupon_id = null;
        $coupon_value = 0;
        $coupon = Coupon::where('name', $coupon_code)->first();
        if($coupon){
            $expire = $coupon->expire_at;
            $expire = strtotime($expire);
            $now = time();
            if($expire > $now){
                $coupon_data = apply_coupon_code($coupon, $sub_total);
                if($coupon_data){
                    if($coupon_data['status']){
                        $coupon_value = -1 * $coupon_data['value'];
                        $coupon_id =  $coupon->id;
                    }
                }
            }
        }
        $data['created_system'] = 2;
        $data['user_id'] = Auth::guard('admins')->user()->id;
        $data['coupon_id'] = $coupon_id;
        $data['coupon_value'] = $coupon_value;
        $data['sub_total'] = $sub_total;
        $data['order_total'] = $data['sub_total'] + $data['coupon_value'];
        addBillingAddress($request, $user);
        addShippingAddress($request, $user);
        $order_id = createOrder($request, $user, $data);
        $this->orderProducts($order_id, $customer_type, $products, $qty);
        $order_history = array();
        $order_history['section'] = "Order Add";
        $order_history['order_id'] = $order_id;
        $order_history['description'] = "Order creation";
        $order_history['data'] = json_encode("<li>New order created through system</li>");
        createOrderHistory($order_history);
        order_email_send();
        session()->flash('success_message','Order has been created.');
        return redirect()->back();
    }

    public function orderProducts($order_id, $customer_type, $products, $qty){
        for($i = 0; $i < count($products); $i++){
            $product_price = ProductPrice::where('product_id', $products[$i])->orderBy('id', 'DESC')->first();
            $product = Product::find($products[$i]);
            $order_product = new OrderProduct();
            $order_product->order_id = $order_id;
            $order_product->product_id = $products[$i];
            $order_product->products_name = $product->name;
            $order_product->qty = $qty[$i];
            $order_product->user_type = $customer_type;
            $order_product->normal_price = $product_price->normal_price;
            $order_product->normal_discount = $product_price->normal_discount;
            $order_product->school_price = $product_price->school_price;
            $order_product->school_discount = $product_price->school_discount;
            $order_product->save();
            $product->qty = $product->qty - $qty[$i];
            $product->save();
        }
    }
}
