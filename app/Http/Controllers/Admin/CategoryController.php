<?php

namespace App\Http\Controllers\Admin;

use App\AllSlug;
use App\Category;
use App\Http\Controllers\Breadcrumbs;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Category','#'));
        $categories = Category::where('parent_id', 0)->get();
        return view('admin.category.list', compact('categories'));
    }

    public function subIndex(){
        array_push(Breadcrumbs::$breadcrumb,array('Category','#'));
        $categories = Category::where('parent_id','!=', 0)->get();
        return view('admin.sub-category.list', compact('categories'));
    }

    public function subAdd()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Sub category','admin.sub-categories'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        $categories = Category::where('parent_id', 0)->get();
        return view('admin.sub-category.add', compact('categories'));
    }

    public function subStore(Request $request)
    {
        $validator = $this->categoryFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Sub Category create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $slug = AllSlug::generateSlug($request->get('slug'));
        $category = new Category();
        $category->parent_id = $request->get('category');
        $category->name = $request->get('name');
        $category->slug = $slug;
        $category->save();
        AllSlug::addSlug($slug);
        session()->flash('success_message','Sub Category created successfully');
        return redirect()->back();
    }

    public function add()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Category','admin.category'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        return view('admin.category.add');
    }

    public function store(Request $request)
    {
        $validator = $this->categoryFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Category create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $slug = AllSlug::generateSlug($request->get('slug'));
        $category = new Category();
        $category->name = $request->get('name');
        $category->slug = $slug;
        $category->save();
        AllSlug::addSlug($slug);
        session()->flash('success_message','Category created successfully');
        return redirect()->back();
    }

    public function edit($id){

        $category = Category::find($id);
        if(!$category)
        {
            session()->flash('error_message','Category doesn\'t exist');
            return redirect()->to(route('admin.category'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Category','admin.category'));
        array_push(Breadcrumbs::$breadcrumb,array('Edit',''));
        return view('admin.category.edit')->with('category', $category);
    }

    public function subEdit($id){

        $sub_category = Category::find($id);
        if(!$sub_category)
        {
            session()->flash('error_message','Sub category doesn\'t exist');
            return redirect()->to(route('admin.sub-categories'));
        }

        array_push(Breadcrumbs::$breadcrumb,array('Sub category','admin.sub-categories'));
        array_push(Breadcrumbs::$breadcrumb,array('Edit',''));
        $categories = Category::where('parent_id', 0)->get();
        return view('admin.sub-category.edit', compact('sub_category','categories'));
    }

    public function subUpdate($id, Request $request)
    {
        $sub_category = Category::find($id);
        if(!$sub_category)
        {
            session()->flash('error_message','Sub category doesn\'t exist');
            return redirect()->to(route('admin.sub-categories'));
        }
        $validator = $this->categoryFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Sub category update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category_slug = $sub_category->slug;
        if($category_slug !=  $request->get('slug'))
        {
            $slug = AllSlug::generateSlug($request->get('slug'));
            if($category_slug != $slug)
            {
                $sub_category->slug = $slug;
                AllSlug::addSlug($slug);
            }
        }
        $sub_category->name = $request->get('name');
        $sub_category->parent_id = $request->get('category');
        $sub_category->save();

        session()->flash('success_message','Sub category updated successfully');
        return redirect()->back();
    }

    public function update($id, Request $request)
    {
        $category = Category::find($id);
        if(!$category)
        {
            session()->flash('error_message','Category doesn\'t exist');
            return redirect()->to(route('category.list.get'));
        }
        $validator = $this->categoryFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Category update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category_slug = $category->slug;
        if($category_slug !=  $request->get('slug'))
        {
            $slug = AllSlug::generateSlug($request->get('slug'));
            if($category_slug != $slug)
            {
                $category->slug = $slug;
                AllSlug::addSlug($slug);
            }
        }
        $category->name = $request->get('name');
        $category->save();

        session()->flash('success_message','Category updated successfully');
        return redirect()->back();
    }

    public function categoryFormValidation($request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string'
        ];

        $message = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        return $validator;
    }

    public function changeCategoryStatus(Request $request){
        $category_status = $request->get('category_status');
        $category_id = $request->get('category_id');
        $sub_category = $request->get('sub_category');
        $category = Category::find($category_id);
        $data = array();
        if($category){
            $category->status = $category_status;
            $category->save();

            $data['status'] = 1;
            $message = ($sub_category) ? 'Sub category status changed successfully' : 'Category status changed successfully';
            $data['message'] = $message;
        }else{
            $data['status'] = 0;
            $message = ($sub_category) ? 'This category doesn\'t exists' : 'This category doesn\'t exists';
            $data['message'] = $message;
        }

        return json_encode($data);
    }

    public function getSubCategories(Request $request){
        $category_id = $request->get('category_id');
        $product_id = $request->get('product_id');
        $sub_categories = Category::where('parent_id', $category_id)->where('status', 1)->get();
        $data = array();
        foreach ($sub_categories as $sub_category){
            $product_category = ProductCategory::where('category_id', $sub_category->id)
                                                ->where('product_id', $product_id)
                                                ->where('status', 1)
                                                ->first();
            $status = false;
            if($product_category){
                $status =  true;
            }
            $set = array($sub_category->id, $sub_category->name, $status);
            array_push($data, $set);
        }

        return $data;
    }
}
