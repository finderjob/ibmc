<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Breadcrumbs;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Payments','#'));
        $payments = Payment::where('isDelete', 0)->get();
        return view('admin.payment.list', compact('payments'));
    }//

    public function add(){
        array_push(Breadcrumbs::$breadcrumb,array('Payments','admin.payments'));
        array_push(Breadcrumbs::$breadcrumb,array('Add','#'));

        $orders = Order::select('id', 'order_id', 'order_total')->where('status', 3)->get();
        return view('admin.payment.add', compact('orders'));
    }

    public function store(Request $request){

        $rules = [
            'order_id' => 'required|numeric',
            'payment_value' => 'required|numeric'
        ];

        $message = [
            'order_id.required' => 'The order id is required',
            'order_id.numeric' => 'The order id should be string',
            'payment_value.required' => 'The payment value field is required',
            'payment_value.numeric' => 'The payment value should be string'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Payment create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $order_id = $request->get('order_id');
        $order = Order::find($order_id);
        $payment = new Payment();
        $payment->payment_id = $this->randomString();
        $payment->order_id = $order->id;
        $payment->order_reference = $order->order_id;
        $payment->payment_type = 1;
        $payment->payment = $request->get('payment_value');
        $payment->user_id = Auth::guard('admins')->user()->id;
        $payment->save();

        $order_history = array();
        $order_history['section'] = "Payment List";
        $order_history['order_id'] = $order->id;
        $order_history['description'] = "Payment Creation";
        $description = "<li> Payment Id: ". $payment->payment_id."</li>";
        $description .= "<li>Payment Type: Cash on delivery</li>";
        $description .= "<li>Value: LKR".number_format($request->get('payment_value'), 2,'.',',')."</li>";
        $order_history['data'] = json_encode($description);
        createOrderHistory($order_history);
        $user_lists = User::where('user_type', '!=', 3)->get();
        foreach ($user_lists as $user_list){
            $notification['user_id'] = $user_list->id;
            $notification['data'] = Auth::guard('admins')->user()->name." has been created payment for order id: ".$order->order_id;
            $notification['url'] = route('admin.payments');
            createNotifications($notification);
        }

        session()->flash('success_message','Payment created successfully');
        return redirect()->back();
    }

    public function changePaymentStatus(Request $request){
        $payment_status = $request->get('payment_status');
        $payment_id = $request->get('payment_id');

        $payment = Payment::find($payment_id);
        if($payment){
            if($payment->status == 0){
                $order_total = 0;
                $payment_total = 0;
                $order = Order::find($payment->order_id);
                $status = 1;
                $total_payment = $this->payment($payment->order_id);
                $payment_total = $total_payment;
                $order_total = $order->order_total;
                if($payment_status == 1){
                    if($order){
                        $remaining = $order->order_total - $total_payment;
                        if($remaining != 0){
                            $order->payment_status = 1;
                            $order->save();
                            $status = 0;
                        }else{
                            $status = 1;
                        }
                    }
                }else if($payment_status == 3){
                    $refund_percentage = config('status.refund_percentage');
                    $refund_funds = ($order_total *  $refund_percentage) /100;
                    if($payment_total >= $refund_funds){
                        $refund_funds = $payment_total - $refund_funds;
                    }else{
                        $refund_funds = 0;
                    }
                    if($refund_funds >= $payment->payment){
                        if($order){
                            $order->status = 7;
                            $order->payment_status = 3;
                            $order->save();
                            $status = 0;
                        }
                    }else{
                        $status = 2;
                    }

                }else{
                    $status = 0;
                }
                if(!$status){
                    $payment->status = $payment_status;
                    $payment->save();
                    $order_history = array();
                    $order_history['section'] = "Payment List";
                    $order_history['order_id'] = $payment->order_id;
                    $change_status = ($payment_status)? "Accepted" : "Declined";
                    $order_history['description'] = "Payment Status";
                    $status = '<li> Status: ';
                    $status .= $change_status;
                    $status .= '</li>';
                    $status .= "<li>Order total :".$order_total."</li>";
                    $status .= "<li>Payment total :".$payment_total."</li>";
                    $order_history['data'] = json_encode($status);
                    createOrderHistory($order_history);
                    $notification['user_id'] = $payment->id;
                    $notification['data'] = Auth::guard('admins')->user()->name." ".$change_status." payment id: ".$payment->payment_id;
                    $notification['url'] = route('admin.payments');
                    createNotifications($notification);
                    $data['status'] = 1;
                    $message = 'This payment status changed successfully';
                    $data['message'] = $message;
                }else{
                    $data['status'] = 0;
                    if($status == 2){
                        $message = 'This payment cannot be refund';
                    }else{
                        $message = 'This payment cannot be approved';
                    }

                    $data['message'] = $message;
                }
            }else{
                $data['status'] = 0;
                $message = 'This payment status already changed';
                $data['message'] = $message;
            }
        }else{
            $data['status'] = 0;
            $message = 'This payment doesn\'t exists';
            $data['message'] = $message;
        }
        return json_encode($data);
    }

    public function remain(Request $request){
        $order_id = $request->get('order_id');
        $remaining = 0;
        $order = Order::find($order_id);
        $total = 0;
        $refund_funds = 0;
        $payment_value = 0;
        if($order){
            $total = $order->order_total;
            $payment_value = $this->payment($order_id);
            $remaining = $total - $payment_value;
            $refund_percentage = config('status.refund_percentage');
            $refund_funds = ($total *  $refund_percentage) /100;
            if($payment_value > $refund_funds){
                $refund_funds = $payment_value - $refund_funds;
            }else{
                $refund_funds = 0;
            }
        }
        $remaining = final_price_format($remaining);
        $total = final_price_format($total);
        $refund_funds = final_price_format($refund_funds);
        return array($total, $remaining, $refund_funds, $payment_value);
    }

    public function payment($order_id){
        $payments = Payment::where('order_id', $order_id)->get();
        $payment_value = 0;
        foreach ($payments as $payment){
            if($payment->status == 1){
                $payment_value += $payment->payment;
            }

        }
        return $payment_value;
    }

    function randomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }

    public function deactivate($id){
        $payment = Payment::find($id);
        if($payment){
            if($payment->status != 1){
                $payment->isDelete = 1;
                $payment->save();
                session()->flash('success_message','Payment delete successfully');
            }else{
                session()->flash('error_message','Payment already accepted');
            }
        }else{
            session()->flash('error_message','Payment doesn\'t exists');
        }
        return redirect()->back();
    }
}
