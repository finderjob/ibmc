<?php

namespace App\Http\Controllers\Admin;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Breadcrumbs;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $last_id = ($request->get('last_id')) ? $request->get('last_id') : 0;
        $user_id = Auth::guard('admins')->user()->id;
        $notification_unread_count = Notification::where('user_id', $user_id)->where('read_status', 0)->count();
        $notifications = Notification::where('id', '>', $last_id)->where('user_id', $user_id)->orderBy('id', 'DESC')->limit(10)->get();
        $data['count'] = $notification_unread_count;
        $i = 1;
        $resource = null;
        if($notifications){
            foreach ($notifications as $notification){
                if($i == 1){
                    $last_id = $notification->id;
                }
                $i++;
            }

            $resource = view('admin.inc.notification', compact('notifications'))->render();
        }

        $data['notications'] = $resource;
        $data['last_id'] = $last_id;
        return json_encode($data);
    }

    public function list(){

        array_push(Breadcrumbs::$breadcrumb,array('Notifications','#'));
        $user_id = Auth::guard('admins')->user()->id;
        $notifications = Notification::where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        return view('admin.notification', compact('notifications'));
    }

    public function read($id){

        $notifiction = Notification::find($id);
        if($notifiction){
            $notifiction->read_status = 1;
            $notifiction->save();

            return redirect()->to($notifiction->url);
        }else{
            return redirect()->to(route('admin.notification.list'));
        }
    }
}
