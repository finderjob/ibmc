<?php

namespace App\Http\Controllers\Admin;

use App\ContactUs;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Breadcrumbs;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Customers','#'));
        $customers = Customer::all();
        return view('admin.customers.list', compact('customers'));
    }//

    public function changeCustomerStatus(Request $request){
        $customer_status = $request->get('customer_status');
        $customer_id = $request->get('customer_id');
        $customer = Customer::find($customer_id);
        $data = array();
        if($customer){
            $customer->status = $customer_status;
            $customer->save();

            $data['status'] = 1;
            $message = 'Customer status changed successfully';
            $data['message'] = $message;
        }else{
            $data['status'] = 0;
            $message = 'This customer doesn\'t exists';
            $data['message'] = $message;
        }

        return json_encode($data);
    }

    public function add(){
        array_push(Breadcrumbs::$breadcrumb,array('Customers','admin.customers'));
        array_push(Breadcrumbs::$breadcrumb,array('Add','#'));
        return view('admin.customers.add');
    }

    public function store(Request $request){

        $validator = $this->customerFromValidation($request);

        if ($validator->fails()) {
            session()->flash('error_message','Customer create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $customer = new Customer();
        $customer->name = $request->get('name');
        $customer->email = $request->get('email');
        $customer->first_name = $request->get('first_name');
        $customer->last_name = $request->get('last_name');
        $customer->password = $request->get('password');
        $customer->customer_type = $request->get('customer_type');
        $customer->complete_status = 2;
        $customer->created_by = Auth::guard('admins')->user()->id;
        $customer->save();

        session()->flash('success_message','Customer created unsuccessfully');
        return redirect()->back();
    }

    public function edit($id){

        $customer = Customer::find($id);
        if(!$customer){
            session()->flash('error_message',"Customer doesn't exists");
            return redirect()->back();
        }

        return view('admin.customers.edit', compact('customer'));
    }

    public function update($id, Request $request){

        $customer = Customer::find($id);
        if(!$customer){
            session()->flash('error_message',"Customer doesn't exists");
            return redirect()->back();
        }
        $validator = $this->customerFromValidation($request, 1);
        if ($validator->fails()) {
            session()->flash('error_message','Customer update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $customer->name = $request->get('name');
        $customer->email = $request->get('email');
        $customer->first_name = $request->get('first_name');
        $customer->last_name = $request->get('last_name');
        $customer->password = $request->get('password');
        $customer->customer_type = $request->get('customer_type');
        $customer->complete_status = 2;
        $customer->save();

        session()->flash('success_message','Customer updated unsuccessfully');
        return redirect()->back();
    }

    protected function customerFromValidation($request, $status = 0){
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:customers,email',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'customer_type' => 'required|integer'
        ];
        $message = [
            'name.required' => 'Username is required',
            'name.string'   => 'Username must be string',
            'name.max'      => 'Maximum characters is 255',
            'email.required'    => 'Email address is required',
            'email.email'   => 'Email should be valid email address',
            'email.unique'  => 'This email address is already taken',
            'first_name.required' => 'First name is required',
            'first_name.string'   => 'First name must be string',
            'first_name.max'      => 'First name characters is 255',
            'last_name.required' => 'Last name is required',
            'last_name.string'   => 'Last name must be string',
            'last_name.max'      => 'Last name characters is 255',
            'customer_type.required' => 'Customer type is required',
            'customer_type.integer' => 'Customer should be integer'
        ];
        if(!$status){
            $rules = [
                'password' => 'required|string|max:255|min:8'
            ];
            $message = [
                'password.required' => 'Password is required',
                'password.string'   => 'Password should be string',
                'password.max'      => 'Maximum characters is 255',
                'password.min'      => 'Minimum characters is 8',
            ];
        }
        $validator = \Validator::make($request->all(), $rules, $message);
        return $validator;
    }

    public function complains(){
        array_push(Breadcrumbs::$breadcrumb,array('Customers','admin.customers'));
        array_push(Breadcrumbs::$breadcrumb,array('Messages','#'));

        $complains = ContactUs::orderBy('id', 'DESC')->get();
        return view('admin.customers.contact', compact('complains'));
    }

    public function complainsStatus($id, Request $request){
        $complain = ContactUs::find($id);
        if($complain){
            $complain->status = $request->get('message_status');
            $complain->save();
            session()->flash('success_message','Message status change successfully');
        }else{
            session()->flash('error_message','This Message doesn\'t exist');
        }
        return redirect()->back();
    }
}
