<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Breadcrumbs;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
        $role = Role::find(1);
        $role->givePermissionTo(Permission::all());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb, array('Role', ''));
        $roles = Role::all();
        $permissions = Permission::all();
        return view('admin.role.index', compact('roles', 'permissions'));
    }

    public function assign(Request $request){
        $user_role = $request->get('user_type');
        $permissions = $request->get('check_permssion');
        $role = Role::find($user_role);
        if($role->name == 'super-admin'){
            $role->givePermissionTo(Permission::all());
        }else{
            $permissions = Permission::whereIn('id',$permissions)->get();
            $role->revokePermissionTo(Permission::all());
            $role->givePermissionTo($permissions);
        }
        session()->flash('success_message','Permission assigned successfully');
        session()->flash('user_type',$user_role);
        return redirect()->back()->with( ['user_type' => $user_role] );
    }

    public function getAssignPermission(Request $request){
        $user_role = $request->get('user_type');
        $rolePermissions  = DB::table("role_has_permissions")->where("role_id",$user_role)->get();
        $data = array();
        foreach ($rolePermissions as $rolePermission){
            array_push($data, (int)$rolePermission->permission_id);
        }

        return $data;
    }
}
