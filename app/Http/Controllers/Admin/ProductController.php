<?php

namespace App\Http\Controllers\Admin;

use App\AllSlug;
use App\Category;
use App\Http\Controllers\Breadcrumbs;
use App\Product;
use App\ProductCategory;
use App\ProductHistory;
use App\ProductImage;
use App\ProductPrice;
use App\ProductsReviews;
use App\RelatedProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Product','#'));
        $products = Product::all();
        return view('admin.products.list', compact('products'));
    }

    public function add(){
        array_push(Breadcrumbs::$breadcrumb,array('Category','admin.products'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        return view('admin.products.add');
    }

    public function store(Request $request){

        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string',
            'description' => 'required|string',
            'overview' => 'required|string',
            'category' => 'required',
            'subcategory' => 'required',
        ];
        $message = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string',
            'description.required' => 'The Product description is required',
            'description.string' => 'The Product description should be string',
            'overview.required' => 'The Product overview is required',
            'overview.string' => 'The Product overview should be string',
            'category.required' => 'The category is required',
            'subcategory.required' => 'The sub category is required'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Product add unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $description  = json_encode($request->get('description'));
        $slug = AllSlug::generateSlug($request->get('slug'));
        $products = new Product();
        $products->name = $request->get('name');
        $products->slug = $slug;
        $products->author_id = $request->get('author');
        $products->description = $description;
        $products->overview = json_encode($request->get('overview'));
        $products->qty = ($request->get('qty')) ? $request->get('qty') : 0;
        $products->complete_status = 1;
        $products->save();
        AllSlug::addSlug($slug);
        $category = $request->get('category');
        $sub_category = $request->get('subcategory');
        $data_category = $this->productCategory($products->id,$category,$sub_category);

        $data = '<li> Product Name: '.$request->get('name').'</li>';
        $data .= '<li> Product Slug: '.$slug.'</li>';
        $data .= '<li> Product Qty: '.$request->get('qty').'</li>';
        $data .= '<li>Product Category: '.$data_category.'</li>';
        $history_description = array();
        $history_description['data'] = $data;
        $status = "Product Added";
        $this->productHistory('Product Info', $history_description, $status, $products->id);

        session()->put('product_section','change_avatar');
        session()->flash('success_message','Product created successfully, please update other details');
        return redirect()->to(route('admin.products.edit.get', $products->id));
    }

    public function edit($id){

        $product = Product::find($id);
        $section = null;
        if(!$product)
        {
            session()->flash('error_message','Product doesn\'t exist');
            return redirect()->to(route('admin.products'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Product','admin.products'));
        array_push(Breadcrumbs::$breadcrumb,array('Edit',''));
        $product_cover_images = ProductImage::where('product_id',$id)->where('type','=',1)->first();
        $product_images = ProductImage::where('product_id',$id)->where('type','!=', 1)->get();
        $product_price = ProductPrice::where('product_id', $id)->where('status', 1)->first();

        $category = ProductCategory::where('product_id', $id)->where('status',1)->first();
        $product_category = null;
        if($category){
            $category_data = Category::find($category->category_id);
            if($category_data->parent_id){
                $product_category = $category_data->parent_id;
            }else{
                $product_category = $category_data->id;
            }
        }
        $related = RelatedProduct::where('product_id', $id)->where('status',1)->get();
        $related_products = array_pluck($related, 'related_id');
        $product_history = ProductHistory::where('product_id', $id)->orderBy('id', 'DESC')->get();
        $product_reviews = ProductsReviews::where('product_id', $id)->orderBy('id', 'DESC')->get();
        if(session()->has('product_section')){
            $section = session()->get('product_section');
            session()->forget('product_section');
        }

        $show_hide = $this->setSection($product->complete_status);
        return view('admin.products.edit', compact('product','section','product_images','product_cover_images','product_price','show_hide','product_category','related_products','product_history','product_reviews'));
    }

    public function setSection($complete_status){
        switch ($complete_status){
            case 1:
                $show_hide['product_info'] = true;
                $show_hide['change_avatar'] = true;
                $show_hide['change_price'] = false;
                $show_hide['product_review'] = false;
                break;
            case 2:
                $show_hide['product_info'] = true;
                $show_hide['change_avatar'] = true;
                $show_hide['change_price'] = true;
                $show_hide['product_review'] = false;
                break;
            default:
                $show_hide['product_info'] = true;
                $show_hide['change_avatar'] = true;
                $show_hide['change_price'] = true;
                $show_hide['product_review'] = true;
        }

        return $show_hide;
    }

    public function update($id,Request $request){

        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string',
            'description' => 'required|string',
            'overview' => 'required|string',
            'category' => 'required',
        ];
        $message = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string',
            'description.required' => 'The Product description is required',
            'description.string' => 'The Product description should be string',
            'overview.required' => 'The Product overview is required',
            'overview.string' => 'The Product overview should be string',
            'category.required' => 'The category is required'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Product update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $product = Product::find($id);
        if(!$product)
        {
            session()->flash('error_message','Product doesn\'t exist');
            return redirect()->to(route('admin.products'));
        }
        if($product->slug != $request->get('slug')){
            $slug = AllSlug::generateSlug($request->get('slug'));
            $product->slug = $slug;
            AllSlug::addSlug($slug);
        }
        $description = json_encode($request->get('description'));
        $product->name = $request->get('name');
        $product->author_id = $request->get('author');
        $product->description = $description;
        $product->overview = json_encode($request->get('overview'));
        $product->qty = $request->get('qty');
        $product->save();
        $category = $request->get('category');
        $sub_category = $request->get('subcategory');
        $data_category = $this->productCategory($id,$category,$sub_category);
        $related_products = $request->get('related_product');
        $related_data = $this->relatedProduct($id, $related_products);


        $data = '<li> Product Name: '.$request->get('name').'</li>';
        $data .= '<li> Product Slug: '.$product->slug.'</li>';
        $data .= '<li> Product Qty: '.$request->get('qty').'</li>';
        $data .= '<li>Product Categories: '.$data_category.'</li>';
        $data .= '<li>Related Products: '.$related_data.'</li>';
        $history_description = array();
        $history_description['data'] = $data;
        $status = "Product Info Updated";
        $this->productHistory('Product Info', $history_description, $status, $id);
        $this->checkProductComplete($id);
        session()->put('product_section','product_info');
        session()->flash('success_message','Product updated successfully');
        return redirect()->back();
    }

    public function storeRelatedProduct($id, $related_id, $type = 1){
        if($type){
            $related_product = RelatedProduct::where('product_id', $id)->where('related_id', $related_id)->first();
            if($related_product){
                if(!$related_product->status){
                    $related_product->status = 1;
                    $related_product->save();
                }
            }else{
                $related_products = new RelatedProduct();
                $related_products->product_id = $id;
                $related_products->related_id = $related_id;
                $related_products->save();
            }
        }else{
            RelatedProduct::where('related_id',$related_id)->where('product_id', $id)->update(['status' => 0]);
            RelatedProduct::where('related_id', $id)->where('product_id', $related_id)->update(['status' => 0]);
        }

    }

    public function relatedProduct($id, $products){

        $data = '';
        if($products){
            for($i = 0; $i < count($products); $i++){
                $this->storeRelatedProduct($id, $products[$i]);
                $this->storeRelatedProduct($products[$i],$id);
                $product = Product::find($products[$i]);
                if($product){
                    $data .= '<li>id: '.$product->id.', name: '.$product->name.'</li>';
                }
            }
            $not_related_products = RelatedProduct::select('related_id')->whereNotIn('related_id',$products)->where('product_id', $id)->get();
            foreach($not_related_products as $not_related){
                $this->storeRelatedProduct($id, $not_related->related_id, 0);
                $this->storeRelatedProduct($not_related->related_id,$id, 0);
            }
            $ul = ($data)? '<ul>'.$data.'</ul>' : 'none';
        }else{
            $ul = "";
            $not_related_products = RelatedProduct::select('related_id')->where('product_id', $id)->get();
            foreach($not_related_products as $not_related){
                $this->storeRelatedProduct($id, $not_related->related_id, 0);
                $this->storeRelatedProduct($not_related->related_id,$id, 0);
            }
            $ul = ($data)? '<ul>'.$data.'</ul>' : 'none';
        }
        return $ul;
    }

    public function productCategory($id, $category, $sub_category){

        $data = "";
        ProductCategory::whereNotIn('category_id',[$category, $sub_category])->where('product_id', $id)->update(['status' => 0]);
        if($sub_category){
            $set_id = $sub_category;
        }else{
            $set_id = $category;
        }
        $set = Category::find($set_id);
        $sub_cat_name = ($set) ? $set->name: 'None';
        $set = Category::find($category);
        $cat_name = ($set) ? $set->name: 'None';
        $data .= '<li>category name: '. $cat_name .', subcategory name: '. $sub_cat_name .' </li>';
        $product_category = ProductCategory::where('product_id', $id)->where('category_id', $set_id)->first();
        if($product_category){
            $product_category->status = 1;
            $product_category->save();
        }else{
            $product_category = new ProductCategory();
            $product_category->product_id = $id;
            $product_category->category_id = $set_id;
            $product_category->save();
        }

        $ul = ($data)? '<ul>'.$data.'</ul>' : 'none';
        return $ul;
    }

    public function uploadCoverImages($id,Request $request){
        $product_cover = $request->file('cover_image');
        $product = Product::find($id);
        if(!$product){
            session()->flash('error_message','Product doesn\'t exist');
            return redirect()->to(route('admin.products'));
        }
        session()->put('product_section','change_avatar');
        if(!$product_cover){
            session()->flash('error_message','Product cover photo inserted unsuccessfully');
            return redirect()->back();
        }
        $ext = $product_cover->getClientOriginalExtension();
        $t=time();
        $fileName = "product_cover_image_".$id."_".$t.'.'.$ext;
        $location = public_path('images/product/'.$id.'/');
        $product_cover->move($location, $fileName);
        $product_cover_path = 'images/product/'.$id.'/'.$fileName;
        $product_image = ProductImage::where('product_id',$id)->where('type',1)->first();
        if($product_image){
            //unlink($product_image->image_path);
            $product_image->image_path = $product_cover_path;
            $product_image->save();
            session()->flash('success_message','Product cover photo updated successfully');
        }else{
            $product_image = new ProductImage();
            $product_image->product_id = $id;
            $product_image->image_path = $product_cover_path;
            $product_image->type = 1;
            $product_image->save();
            session()->flash('success_message','Product cover photo inserted successfully');
        }
        $data_image = '<img src='.asset($product_cover_path).' class="img-responsive " style="width: 40%;" alt=""/>';

        $data = '<li> Product Cover Image: </li>';
        $data .= $data_image;
        $history_description = array();
        $history_description['data'] = $data;
        $status = "Product Cover Image";
        $this->productHistory('Product Images', $history_description, $status, $id);
        $this->checkProductComplete($id);
        return redirect()->back();
    }

    public function uploadOtherImages($id,Request $request){
        $other_image_2 = $request->file('other_image_2');
        $other_image_3 = $request->file('other_image_3');
        $product = Product::find($id);
        if(!$product){
            session()->flash('error_message','Product doesn\'t exist');
            return redirect()->to(route('admin.products'));
        }
        session()->put('product_section','change_avatar');
        if(empty($other_image_2) && empty($other_image_3)){
            session()->flash('error_message','Product other photo inserted unsuccessfully');
            return redirect()->back();
        }
        $t=time();
        $success_message =array();
        if($other_image_2){
            $ext = $other_image_2->getClientOriginalExtension();
            $fileName = "product_other_image_".$id.'_'.$t.'.'.$ext;
            $location = public_path('images/product/'.$id.'/');
            $other_image_2->move($location, $fileName);
            $other_image_2_path = 'images/product/'.$id.'/'.$fileName;
            $product_image = ProductImage::where('product_id',$id)->where('type',2)->first();

            if($product_image){
                //unlink($product_image->image_path);
                $product_image->image_path = $other_image_2_path;
                $product_image->save();
                array_push($success_message, 'Product other photo 2 updated successfully');
            }else{
                $product_image = new ProductImage();
                $product_image->product_id = $id;
                $product_image->image_path = $other_image_2_path;
                $product_image->type = 2;
                $product_image->save();
                $product->complete_status = 2;
                array_push($success_message, 'Product other photo 2 inserted successfully');
            }
            $data_image = '<img src='.asset($other_image_2_path).' class="img-responsive " style="width: 40%;" alt=""/>';

            $data = '<li> Product Image 2: </li>';
            $data .= $data_image;
            $history_description = array();
            $history_description['data'] = $data;
            $status = "Product Image 2";
            $this->productHistory('Product Images', $history_description, $status, $id);
        }
        if($other_image_3){
            $ext = $other_image_3->getClientOriginalExtension();
            $fileName = "product_other_image_".$id.'_'.$t.'.'.$ext;
            $location = public_path('images/product/'.$id.'/');
            $other_image_3->move($location, $fileName);
            $other_image_3_path = 'images/product/'.$id.'/'.$fileName;
            $product_image = ProductImage::where('product_id',$id)->where('type',3)->first();
            if($product_image){
                //unlink($product_image->image_path);
                $product_image->image_path = $other_image_3_path;
                $product_image->save();
                array_push($success_message, 'Product other photo 3 updated successfully');
            }else{
                $product_image = new ProductImage();
                $product_image->product_id = $id;
                $product_image->image_path = $other_image_3_path;
                $product_image->type = 3;
                $product_image->save();
                $product->complete_status = 2;
                array_push($success_message, 'Product other photo 3 inserted successfully');
            }
            $data_image = '<img src='.asset($other_image_3_path).' class="img-responsive " style="width: 40%;" alt=""/>';

            $data = '<li> Product Image 3: </li>';
            $data .= $data_image;
            $history_description = array();
            $history_description['data'] = $data;
            $status = "Product Image 3";
            $this->productHistory('Product Images', $history_description, $status, $id);
        }
        $product->save();
        $this->checkProductComplete($id);
        session()->flash('success_message',$success_message);
        return redirect()->back();
    }

    public function productPrice($id, Request $request){


        $product = Product::find($id);
        if(!$product){
            session()->flash('error_message','Product doesn\'t exist');
            return redirect()->to(route('admin.products'));
        }
        session()->put('product_section','change_price');
        $product_price = ProductPrice::where('product_id', $id)->where('status',1)->first();
        if($product_price){
            if($this->checkPreviousValues($product_price, $request)){
                session()->flash('error_message','New product prices are not different with previous prices.');
                return redirect()->back();
            }
            $product_price->status = 0;
            $product_price->save();
        }

        $price = new ProductPrice();
        $price->product_id = $id;
        $price->normal_price = $request->get('normal_price');
        $price->school_price = $request->get('school_price');
        $price->normal_discount = $request->get('normal_discount');
        $price->school_discount = $request->get('school_discount');
        $price->status = 1;
        $price->save();
        $data = '<li> Normal Price: '.$request->get('normal_price').'</li>';
        $data .= '<li> School Price: '.$request->get('school_price').'</li>';
        $data .= '<li> Normal Discount: '.$request->get('normal_discount').'</li>';
        $data .= '<li> School Discount: '.$request->get('school_discount').'</li>';
        $history_description = array();
        $history_description['data'] = $data;
        $status = "Product Price Updated";
        $this->checkProductComplete($id);
        $this->productHistory('Product Price', $history_description, $status, $id);
        session()->flash('success_message','Product price created successfully');
        return redirect()->back();

    }

    public function checkPreviousValues($product_price, $request){

        $normal_price = ($product_price->normal_price != $request->get('normal_price'))? true : false;
        $school_price = ($product_price->school_price != $request->get('school_price'))? true : false;
        $normal_discount = ($product_price->normal_discount != $request->get('normal_discount'))? true : false;
        $school_discount = ($product_price->school_discount != $request->get('school_discount'))? true : false;

        if($normal_price || $school_price || $normal_discount || $school_discount){
            return false;
        }else{
            return  true;
        }
    }

    public function productHistory($section, $description, $status, $product_id){

        $product_history = new ProductHistory();
        $product_history->product_id = $product_id;
        $product_history->section = $section;
        $product_history->description = $status;
        $product_history->data = json_encode($description);
        $product_history->user_id = Auth::guard('admins')->user()->id;

        $product_history->save();
    }

    public function changeProductStatus(Request $request){

        $product_status = $request->get('product_status');
        $product_id = $request->get('product_id');
        $product = Product::find($product_id);
        $data = array();
        if($product){
            $product->status = $product_status;
            $product->save();
            $status = '<li> Status: ';
            $status .= ($product_status)? 'Activate' : 'Deactivate';
            $status .= '</li>';
            $description = array();
            $description['data'] = $status;
            $this->productHistory("Product List", $description, "Product Status changed", $product_id);
            $data['status'] = 1;
            $data['message'] = 'Product status changed successfully';
        }else{
            $data['status'] = 0;
            $data['message'] = 'This product doesn\'t exists';
        }
        return json_encode($data);
    }

    public function changeProductReviewStatus(Request $request){

        $review_status = $request->get('review_status');
        $review_id = $request->get('review_id');
        $review = ProductsReviews::find($review_id);
        $data = array();
        if($review){
            $review->status = $review_status;
            $review->save();
            $status = '<li> Status: ';
            $status .= ($review_status)? 'Accepted' : 'Declined';
            $status .= '</li>';
            $description = array();
            $description['data'] = $status;
            $this->productHistory("Product List", $description, "Product review status changed", $review_id);
            $data['status'] = 1;
            $data['message'] = 'The Review status changed successfully';
        }else{
            $data['status'] = 0;
            $data['message'] = 'This review doesn\'t exists';
        }
        return json_encode($data);
    }

    public function checkProductComplete($product_id){

        $products_images = ProductImage::where('status', 1)->where('product_id', $product_id)->get();
        $has_cover_image = false;
        $has_other_image = false;
        $has_price = false;
        $product_complete = 3;
        foreach ($products_images as $products_image){
            if($products_image->type == 1){
                $has_cover_image = true;
            }
            if($products_image->type == 2 || $products_image->type == 3){
                $has_other_image = true;
            }
        }
        $product_price = ProductPrice::where('status', 1)->where('product_id', $product_id)->first();
        if($product_price){
            $has_price = true;
        }

        if(!$has_other_image || !$has_cover_image){
            $product_complete = 1;
        }else if(!$has_price){
            $product_complete = 2;
        }

        $product = Product::find($product_id);
        $product->complete_status = $product_complete;
        $product->save();
    }
}
