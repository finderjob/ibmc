<?php
/**
 * Created by PhpStorm.
 * User: Danuja Fernando
 * Date: 3/12/2019
 * Time: 11:05 AM
 */

namespace App\Http\Controllers\Admin;
use App\AllSlug;
use App\Product;
use App\Rules\PasswordCheck;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Breadcrumbs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins')->except('resetPassword');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Dashboard',''));
        $less_product_quantity_count = (config('status.less_product_quantity_count')) ? config('status.less_product_quantity_count') : 10;
        $less_products = Product::where('complete_status', 3)->where('status', 1)->where('qty', '<=', $less_product_quantity_count)->get();

        return view('admin.home', compact('less_products'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Users',''));
        $user_edit = checkPermissionByName('user edit');
        if($user_edit){
            $user_list = User::all();
        }else{
            $user_id = Auth::guard('admins')->user()->id;
            $user_list = User::where('id', $user_id)->get();
        }
        return view('admin.users.list')->with('user_list',$user_list);
    }

    public function add()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Users','admin.list'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        $roles = Role::get();
        return view('admin.users.add', compact( 'roles'));
    }

    public function store(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'password' => 'required|string|max:255|min:8',
            'user_type' => 'required|integer'
        ];

        $message = [
            'email.required' => 'The email field is required',
            'email.email' => 'The email should be valid email',
            'email.unique' => 'This email already exists',
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'first_name.required' => 'The first name field is required',
            'first_name.string' => 'The first name should be string',
            'first_name.max' => 'Maximum characters is 255',
            'last_name.required' => 'The first name field is required',
            'last_name.string' => 'The first name should be string',
            'last_name.max' => 'Maximum characters is 255',
            'password.required' =>'The password field is required',
            'password.string' => 'The password should be string',
            'password.max' => 'Maximum characters is 255',
            'password.min' => 'Minimum characters is 8',
            'user_type.required' => 'The user type is required',
            'user_type.integer' => 'User type must be valid'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','User create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = new User();
        $user->name = $request->get('name');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->user_type = $request->get('user_type');
        $user->save();

        $user->assignRole($request->get('user_type'));

        session()->flash('success_message','User created successfully');
        return redirect()->back();
    }

    public function generateSlug(Request $request)
    {
        $keyword = $request->get('keyword');
        if($keyword != '' && !empty($keyword))
        {
            $slug = AllSlug::generateSlug($keyword);
            return $slug;
        }

        return '';
    }

    public function forgetPassword($id){

        $user = User::find($id);
        if($user)
        {
            $token = bin2hex(random_bytes(32));
            $date = now();
            DB::table('password_resets')->insert(array(
                'email' => $user->email,
                'token' => $token,
                'created_at' => $date
            ));
            $password_reset_url = route('admin.users.reset-password-url',[$token]);
            $subject = "Password Reset";

            send_email($user->email,null,$subject,$user->name, 1, $password_reset_url);
            session()->flash('success_message', 'Password reset link sent to user email');
        }
        else
        {
            session()->flash('error_message', 'This user doesn\'t exists');
        }
        return redirect()->back();
    }

    public function resetPassword($token){
        $get_token_details = DB::table('password_resets')->where('token', $token)->first();
        $user_email = $get_token_details->email;
        $created_at = strtotime($get_token_details->created_at) + 2*60*60;
        $now = strtotime(now());

        if($now > $created_at)
        {
            return redirect()->to(route('admin.login.get'));
        }
        else{
            return view('auth.admin.password-reset')->with('token',$token);
        }
    }

    public function passwordReset(Request $request){


        $email = $request->get('email');
        $token = json_decode($request->get('token'));
        $password = $request->get('password');

        $rules = [
            'email' => 'required|email',
            'password' => 'required|string|max:255|min:8|confirmed',
        ];

        $message = [
            'email.required' => 'The email field is required',
            'email.email' => 'The email should be valid email',
            'email.unique' => 'This email already exists',
            'name.required' => 'The name field is required',
            'password.required' =>'The password field is required',
            'password.string' => 'The password should be string',
            'password.max' => 'Maximum characters is 255',
            'password.min' => 'Minimum characters is 8',
            'confirmed' => 'The password is not match'
        ];
        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','Password change unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $get_token_details = DB::table('password_resets')->where('token', $token)->first();
        $user_email = $get_token_details->email;
        if($user_email != $email){
            $status = 1;
        }
        $created_at = strtotime($get_token_details->created_at) + 2*60*60;
        $now = strtotime(now());

        if($now > $created_at){
            session()->flash('error_message','Password changed link is not available.');
            return redirect()->to(route('admin.login.get'));

        }
        $user = User::where('email', $email)->first();
        if(!$user){
            $status = 1;
        }

        if($status){
            session()->flash('error_message','Password change unsuccessfully');
            return redirect()->back()
                ->withInput();
        }

        $user->password = Hash::make($request->get('password'));
        $user->save();

        DB::table('password_resets')->where('email', $email)->delete();
        session()->flash('success_message','Password changed successfully. please log in.');
        return redirect()->to(route('admin.login.get'));
    }

    public function profile($id = null){

        if($id){
            $user_id = $id;
        }else{
            $user_id = Auth::guard('admins')->user()->id;
        }

        $user = User::find($user_id);
        $section = null;
        if(session()->has('profile_section')){
            $section = session()->get('profile_section');
            session()->forget('profile_section');
        }

        array_push(Breadcrumbs::$breadcrumb,array('My Profile','#'));
        return view('admin.users.profile', compact('user','section'));
    }

    public function updatePropic($id, Request $request){

        $rules = [
            'propic' => 'required|max:20000|mimes:png,jpeg,jpg'
        ];

        $message = [
            'propic.required' => 'The image field is required',
            'propic.max' => 'The image max size 2MB',
            'propic.mimes' => 'This image allows only png,jpeg,jpg',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        session()->put('profile_section','change_avatar');
        if ($validator->fails()) {
            session()->flash('error_message','Profile Pic change unsuccessfully');
            return redirect()->back()
                ->withErrors($validator);
        }
        $user_pic = $request->file('propic');
        $fileName = $user_pic->getClientOriginalName();
        $location = public_path('images/users/');
        $user_pic->move($location, $fileName);
        $user_pic_path = 'images/users/'.$fileName;

        $user = User::find($id);
        if($user){
            $user->pro_pic_path = $user_pic_path;
            $user->save();
        }
        else{
            session()->flash('error_message', 'This user doesn\'t exists');
            return redirect()->to(route('admin.list'));
        }
        session()->flash('success_message','User profile pic updated successfully');
        return redirect()->back();
    }

    public function changeUserType(Request $request){
        $user_type = $request->get('user_type');
        $user_id = $request->get('user_id');
        $user = User::find($user_id);
        $data = array();
        $logged_user = Auth::guard('admins')->user()->id;
        if($user){
            if($user->id == $logged_user){
                $data['status'] = 0;
                $data['message'] = 'You cannot change your user type';
            }else{
                $user->user_type = $user_type;
                $user->save();
                $user->assignRole($user_type);

                $data['status'] = 1;
                $data['message'] = 'User type changed successfully';
            }
        }else{
            $data['status'] = 0;
            $data['message'] = 'This user doesn\'t exists';
        }

        return json_encode($data);
    }

    public function changeUserStatus(Request $request){
        $user_status = $request->get('user_status');
        $user_id = $request->get('user_id');
        $user = User::find($user_id);
        $data = array();
        $logged_user = Auth::guard('admins')->user()->id;
        if($user){
            if($user->id == $logged_user){
                $data['status'] = 0;
                $data['message'] = 'You cannot change your user status';
            }else{
                $user->status = $user_status;
                $user->save();

                $data['status'] = 1;
                $data['message'] = 'User status changed successfully';
            }
        }else{
            $data['status'] = 0;
            $data['message'] = 'This user doesn\'t exists';
        }

        return json_encode($data);
    }

    public function changeUserPassword(Request $request){
        $rules = [
            'current_password' => ['required','string','max:255', new PasswordCheck()],
            'password' => 'required|string|max:255|min:6|different:current_password',
            'password_confirmation' =>'required|same:password'
        ];

        $message = [
            'current_password.required' =>'The current password field is required',
            'current_password.string' => 'The current password should be string',
            'current_password.max' => 'Maximum characters is 255',
            'password.required' =>'The password field is required',
            'password.string' => 'The password should be string',
            'password.max' => 'Maximum characters is 255',
            'password.min' => 'Minimum characters is 8',
        ];

        session()->put('profile_section', 'change_password');
        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','Password change unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $logged_user = Auth::guard('admins')->user()->id;
        $user = User::find($logged_user);
        $user->password = Hash::make($request->get('password'));
        $user->save();
        session()->flash('success_message','Password changed successfully');
        return redirect()->back();
    }

    public function updateProfile($id,Request $request){

        $rules = [
            'email' => 'required|email',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'bod' => 'date'
        ];

        $message = [
            'first_name.required' => 'The first name field is required',
            'first_name.string' => 'The first name should be string',
            'first_name.max' => 'Maximum characters is 255',
            'last_name.required' => 'The first name field is required',
            'last_name.string' => 'The first name should be string',
            'last_name.max' => 'Maximum characters is 255',
            'bod' => 'Birth day should be date'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','User create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($id);
        if($user){
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->bod = $request->get('bod');
            $user->save();
            session()->flash('success_message','Profile info updated successfully');
        }else{
            session()->flash('error_message', 'This user doesn\'t exists');
        }
        return redirect()->back();
    }

    public function setSession(Request $request){

        $section = $request->get('profile_section');
        session()->put('profile_section', $section);
        return "ok";
    }

    public function checkMail(){

        $messageBody = "<p>Hi Danuja ,</p><br>";
        $messageBody .= "<p>You can reset password using below link</p>\n";
        $messageBody .= "<p>Thanks,\n IBMC Team</p>";

        $user  = "Danuja";
        $link = route('admin.users.reset-password-url',[123]);
        return view('emails.reset-password', compact('user','link'));
    }


}
