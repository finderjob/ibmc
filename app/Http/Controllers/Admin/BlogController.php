<?php

namespace App\Http\Controllers\Admin;

use App\AllSlug;
use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Breadcrumbs;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Blog','#'));
        $blogs = Blog::orderBy('id', 'DESC')->get();
        return view('admin.blogs.list', compact('blogs'));
    }//

    public function add()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Blog','admin.blogs'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        return view('admin.blogs.add');
    }

    public function store(Request $request){

        $validator = $this->blogFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Blog create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $blog_image = $request->file('blog_image');
        $blog_image_path = null;
        if($blog_image) {
            $t=time();
            $ext = $blog_image->getClientOriginalExtension();
            $fileName = "blog-image" . '_' . $t . '.' . $ext;
            $location = public_path('images/blog/');
            $blog_image->move($location, $fileName);
            $blog_image_path = 'images/blog/' . $fileName;
        }
        $slug = AllSlug::generateSlug($request->get('slug'));
        $blog = new Blog();
        $blog->name = $request->get('name');
        $blog->slug = $slug;
        $blog->content = json_encode($request->get('content'));
        $blog->image = $blog_image_path;
        $blog->created_by = Auth::guard('admins')->user()->id;
        $blog->save();
        session()->flash('success_message','Blog created successfully');
        return redirect()->back();
    }

    public function edit($id){

        $blog = Blog::find($id);
        if(!$blog)
        {
            session()->flash('error_message','Blog doesn\'t exist');
            return redirect()->to(route('admin.category'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Blog','admin.blogs'));
        array_push(Breadcrumbs::$breadcrumb,array('Edit',''));
        return view('admin.blogs.edit', compact('blog'));
    }

    public function update($id, Request $request){

        $blog = Blog::find($id);
        if(!$blog)
        {
            session()->flash('error_message','Blog doesn\'t exist');
            return redirect()->to(route('admin.category'));
        }

        $validator = $this->blogFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Blog update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $blog_image = $request->file('blog_image');
        $blog_image_path = null;
        if($blog_image) {
            $t=time();
            $ext = $blog_image->getClientOriginalExtension();
            $fileName = "blog-image" . '_' . $t . '.' . $ext;
            $location = public_path('images/blog/');
            $blog_image->move($location, $fileName);
            $blog_image_path = 'images/blog/' . $fileName;
            $blog->image = $blog_image_path;
        }
        $blog_slug = $blog->slug;
        if($blog_slug !=  $request->get('slug'))
        {
            $slug = AllSlug::generateSlug($request->get('slug'));
            if($blog_slug != $slug)
            {
                $blog->slug = $slug;
                AllSlug::addSlug($slug);
            }
        }
        $blog->name = $request->get('name');
        $blog->content = json_encode($request->get('content'));
        $blog->save();
        session()->flash('success_message','Blog updated successfully');
        return redirect()->back();
    }

    public function blogFormValidation($request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string',
            'content' => 'required|string'
        ];

        $message = [
            'name.required' => 'The title field is required',
            'name.string' => 'The title should be string',
            'name.max' => 'Maximum characters is 255',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string',
            'content.required' => 'The content field is required',
            'content.string' => 'The content should be string'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        return $validator;
    }
}
