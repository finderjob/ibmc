<?php

namespace App\Http\Controllers;

use App\Address;
use App\Author;
use App\Blog;
use App\BlogComment;
use App\Cart;
use App\Category;
use App\ContactUs;
use App\Coupon;
use App\Customer;
use App\EmailSubscribe;
use App\Order;
use App\OrderProduct;
use App\Page;
use App\Product;
use App\ProductCategory;
use App\ProductPrice;
use App\ProductsReviews;
use App\User;
use App\WishList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    public static $page_title;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        HomeController::$page_title = "Home";
        //$this->middleware('auth:customers');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::guard('customers')->user();
        if(!$user){
            $user_id = mt_rand(100000, 999999);
        }else{
            $user_id = $user->id;
        }
        if(!session()->has('wishlist_user_id')){
            session()->put('wishlist_user_id', $user_id);
        }else{
            $this->setUserWishList(session()->get('wishlist_user_id'));
        }
        $customer_type = null;
        if($user)
        {
            $customer_type = $user->customer_type;
        }
        $new_products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('products.complete_status',3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->orderBy('id','DESC')
            ->limit(8)
            ->get();

        $best_sellers = bestSellerProducts();
        return view('theme.index', compact('new_products','customer_type', 'best_sellers'));
    }

    public function product_list(Request $request){
        $customer_type = null;
        $user = Auth::guard('customers')->user();
        if($user)
        {
            $customer_type = $user->customer_type;
        }
        $category_id = $request->get('category_id');
        if($category_id == null || $category_id == 0 || empty($category_id)){
            $products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                ->where('products.complete_status',3)
                ->where('products.status', 1)
                ->where('product_prices.status', 1)
                ->orderBy('id','ASC')
                ->get();
        }else{
            $categories = Category::where('parent_id', $category_id)->get();
            $categories = array_pluck($categories, 'id');
            array_push($categories, $category_id);

            $products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
                ->whereIn('product_categories.category_id', $categories)
                ->where('products.complete_status',3)
                ->where('products.status', 1)
                ->where('product_prices.status', 1)
                ->where('product_categories.status', 1)
                ->orderBy('id','ASC')
                ->get();
        }

        $resource = view('theme.partials.products', compact('products','customer_type'))->render();
        return $resource;
    }

    public function shopWithCategory($slug = null, Request $request){

        $sub_slug = null;
        $order_by = $request->get('order_by');
        $order = $request->get('order');
        $amount = $request->get('amount');
        $customer_type = null;
        $user = Auth::guard('customers')->user();
        if($user)
        {
            $customer_type = $user->customer_type;
        }
        $category = Category::where('slug', $slug)->where('status', 1)->first();
        if($category){
            $get_subs = Category::where('parent_id', $category->id)->where('status', 1)->get()->toArray();
            $get_subs = array_column($get_subs, 'id');
            if(empty($get_subs)){
                $get_subs = [$category->id];
            }
        }else{
            return redirect()->to(route('shop'));
        }
        $products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->whereIn('categories.id', $get_subs)
            ->where('products.complete_status','=', 3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->where('product_categories.status', 1)
            ->ofSort($order, $order_by, $customer_type,$amount)
            ->paginate(9);

        $category = Category::where('slug','=', $slug)->first();
        if($category){
            HomeController::$page_title = $category->name;
        }
        $sub_category = null;
        $remove_filter = false;
        return view('theme.shop', compact('category','sub_category','products','slug','sub_slug','customer_type','remove_filter'));
    }

    public function shopWithSubcategory($slug, $sub_slug, Request $request){
        $order_by = $request->get('order_by');
        $order = $request->get('order');
        $amount = $request->get('amount');
        $customer_type = null;
        $user = Auth::guard('customers')->user();
        if($user)
        {
            $customer_type = $user->customer_type;
        }

        $products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('categories.slug','=', $sub_slug)
            ->where('products.complete_status','=', 3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->where('product_categories.status', 1)
            ->ofSort($order, $order_by, $customer_type,$amount)
            ->paginate(9);

        $category = Category::where('slug','=', $slug)->first();
        $sub_category = Category::where('slug','=', $sub_slug)->first();
        if($sub_category){
            HomeController::$page_title = $category->name;
        }
        $remove_filter = false;
        return view('theme.shop', compact('sub_category','category','products','slug','sub_slug','customer_type','remove_filter'));
    }

    public function shop(Request $request){

        $slug = null;
        $sub_slug = null;
        $order_by = $request->get('order_by');
        $order = $request->get('order');
        $amount = $request->get('amount');
        $customer_type = null;
        $user = Auth::guard('customers')->user();
        if($user)
        {
            $customer_type = $user->customer_type;
        }
        HomeController::$page_title = "Shop";
        $category = null;
        $sub_category = null;
        $products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('products.complete_status',3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->ofSort($order, $order_by, $customer_type,$amount)
            ->paginate(9);

        $remove_filter = false;
        return view('theme.shop', compact('category','sub_category','products','slug','sub_slug','customer_type','remove_filter'));
    }

    public function product($product_slug){
        $slug = null;
        $sub_slug = null;
        $customer_type = null;
        $user = Auth::guard('customers')->user();
        if($user)
        {
            $customer_type = $user->customer_type;
        }
        $data = array();
        if(session()->has('error_message') || session()->has('success_message')){
            if(session()->has('error_message')){
                $data['status'] = 201;
                $data['message'] = session()->get('error_message');
            }
            if(session()->has('success_message')){
                $data['status'] = 200;
                $data['message'] = session()->get('success_message');
            }
        }
        $product = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                    ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                    ->where('products.complete_status',3)
                    ->where('products.status', 1)
                    ->where('product_prices.status', 1)
                    ->where('products.slug', $product_slug)
                    ->first();
        if(!$product){
            return redirect()->to(route('shop'));
        }
        HomeController::$page_title = $product->name;
        $product_id = $product->id;
        $related_products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                    ->join('related_products', 'products.id', '=', 'related_products.product_id')
                    ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                    ->where('products.complete_status',3)
                    ->where('products.status', 1)
                    ->where('product_prices.status', 1)
                    ->where('related_products.related_id', $product_id)
                    ->get();
        $upsell_products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                                ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                                ->where('products.complete_status',3)
                                ->where('products.status', 1)
                                ->where('product_prices.status', 1)
                                ->where('products.id','!=',$product_id)
                                ->get();
        $remove_filter = true;
        $product_reviews = ProductsReviews::where('product_id', $product_id)->where('status', 1)->orderBy('id', 'DESC')->get();

        return view('theme.product', compact('product','related_products','customer_type','product_reviews','slug','sub_slug','remove_filter','upsell_products', 'data'));
    }

    public function productWithCategory($slug, $product_slug){

        $customer_type = null;
        $sub_slug = null;
        $user = Auth::guard('customers')->user();
        if($user)
        {
            $customer_type = $user->customer_type;
        }
        $product = Product::select('products.*', 'categories.name as category_name','product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->where('products.complete_status',3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->where('categories.slug', $slug)
            ->where('products.slug', $product_slug)
            ->first();
        if(!$product){
            return redirect()->to(route('shop'));
        }
        HomeController::$page_title = $product->name;
        $product_id = $product->id;
        $related_products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('related_products', 'products.id', '=', 'related_products.product_id')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('products.complete_status',3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->where('related_products.related_id', $product_id)
            ->get();

        $upsell_products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('products.complete_status',3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->where('products.id','!=',$product_id)
            ->get();
        $remove_filter = true;
        $data = array();
        if(session()->has('error_message') || session()->has('success_message')){
            if(session()->has('error_message')){
                $data['status'] = 201;
                $data['message'] = session()->get('error_message');
            }
            if(session()->has('success_message')){
                $data['status'] = 200;
                $data['message'] = session()->get('success_message');
            }
        }
        $product_reviews = ProductsReviews::where('product_id', $product_id)->where('status', 1)->orderBy('id', 'DESC')->get();

        return view('theme.product', compact('product','related_products','customer_type','product_reviews','slug','sub_slug','remove_filter','upsell_products','data'));
    }

    public function productWithSubcategory($slug, $sub_slug, $product_slug){

        $customer_type = null;
        $user = Auth::guard('customers')->user();
        if($user)
        {
            $customer_type = $user->customer_type;
        }
        $product = Product::select('products.*', 'categories.name as category_name','product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->where('products.complete_status',3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->where('categories.slug', $sub_slug)
            ->where('products.slug', $product_slug)
            ->first();
        if(!$product){
            return redirect()->to(route('shop'));
        }
        HomeController::$page_title = $product->name;
        $product_id = $product->id;
        $related_products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('related_products', 'products.id', '=', 'related_products.product_id')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('products.complete_status',3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->where('related_products.related_id', $product_id)
            ->get();

        $upsell_products = Product::select('products.*', 'product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('products.complete_status',3)
            ->where('products.status', 1)
            ->where('product_prices.status', 1)
            ->where('products.id','!=',$product_id)
            ->get();
        $remove_filter = true;
        $data = array();
        if(session()->has('error_message') || session()->has('success_message')){
            if(session()->has('error_message')){
                $data['status'] = 201;
                $data['message'] = session()->get('error_message');
            }
            if(session()->has('success_message')){
                $data['status'] = 200;
                $data['message'] = session()->get('success_message');
            }
        }
        $product_reviews = ProductsReviews::where('product_id', $product_id)->where('status', 1)->orderBy('id', 'DESC')->get();

        return view('theme.product', compact('product','related_products','customer_type','product_reviews','slug','sub_slug','remove_filter','upsell_products','data'));
    }

    public function cart()
    {
        $user = Auth::guard('customers')->user();
        if(!$user)
        {
           return redirect()->to(route('login'));
        }
        HomeController::$page_title = "Cart";
        $carts = Cart::select('carts.*','products.name','product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                        ->join('products', 'carts.product_id','=','products.id')
                        ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                        ->where('carts.user_id', $user->id)
                        ->where('product_prices.status', 1)
                        ->get();
        return view('theme.cart', compact('carts'));
    }

    public function addCart(Request $request){

        $user = Auth::guard('customers')->user();
        $data = array();
        if(!$user)
        {
            $data['status'] = 201;
            $data['message'] = "User doesn't log in";
        }else{
            $product_id = $request->get('product_id');
            $product = Product::find($product_id);
            if(!$product)
            {
                $data['status'] = 201;
                $data['message'] = "Product doesn't exists";
            }else{
                if(session()->has('customer_coupon_code')){
                    session()->forget('customer_coupon_code');
                }
                $cart  = Cart::where('user_id',$user->id)->where('product_id', $product_id)->first();
                if($cart)
                {
                    $cart->qty = $cart->qty + 1;
                    $cart->save();
                }else{
                    $cart = new Cart();
                    $cart->user_id = $user->id;
                    $cart->product_id = $product_id;
                    $cart->qty = 1;
                    $cart->user_type = $user->customer_type;
                    $cart->save();
                }
                $row_id = $request->get('row_id');
                if($row_id){
                    WishList::where('user_id',$user->id)->where('id', $row_id)->delete();
                }
                $data['status'] = 200;
                $data['message'] = "Product added to cart successfully.";
            }

        }

        return json_encode($data);
    }

    public function updateCartQty(Request $request)
    {
        $user = Auth::guard('customers')->user();
        $data = array();
        if(!$user)
        {
            $data['status'] = 201;
            $data['message'] = "User doesn't log in";
        }else{
            $product_id = $request->get('product_id');
            $qty = $request->get('qty');
            $price = $request->get('price');
            if($qty == 0)
            {
                $data['status'] = 201;
                $data['message'] = "Product Qty should be more than 0";
            }else{
                $product = Product::find($product_id);
                if(!$product)
                {
                    $data['status'] = 201;
                    $data['message'] = "Product doesn't exists";
                }else{
                    if(session()->has('customer_coupon_code')){
                        session()->forget('customer_coupon_code');
                    }
                    $cart  = Cart::where('user_id',$user->id)->where('product_id', $product_id)->first();
                    if($cart)
                    {
                        $cart->qty = $qty;
                        $cart->save();
                    }else{
                        $cart = new Cart();
                        $cart->user_id = $user->id;
                        $cart->product_id = $product_id;
                        $cart->user_type = $user->customer_type;
                        $cart->qty = $qty;
                        $cart->save();
                    }
                    $sub_total = $price * $qty;
                    $total = sub_total($user);
                    $data['status'] = 200;
                    $data['total'] = final_price_format($total);
                    $data['sub_total'] = final_price_format($sub_total);
                    $data['message'] = "Product updated to cart successfully.";
                }
            }
        }

        return json_encode($data);
    }

    public function removeCart(Request $request){
        $user = Auth::guard('customers')->user();
        $data = array();
        if(!$user)
        {
            $data['status'] = 201;
            $data['message'] = "User doesn't log in";
        }else{
            $row_id = $request->get('row_id');
            Cart::where('user_id',$user->id)->where('id', $row_id)->delete();
            $total = sub_total($user);
            $data['status'] = 200;
            $data['total'] = final_price_format($total);
            $data['message'] = "Product removed in cart successfully.";
        }

        return json_encode($data);
    }

    public function getCart(){
        $data = array();
        try{
            $user = Auth::guard('customers')->user();
            if($user)
            {
                $carts = Cart::select('carts.*','products.name','products.slug','product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
                    ->join('products', 'carts.product_id','=','products.id')
                    ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
                    ->where('carts.user_id', $user->id)
                    ->where('product_prices.status', 1)
                    ->get();
                $data['count'] = count($carts);
                $total = 0;
                foreach ($carts as $cart){

                    if($cart->user_type == 2){
                        $sub_total = $cart->school_price * $cart->qty;
                    }else{
                        $sub_total = ($cart->normal_price - $cart->normal_discount) * $cart->qty;
                    }
                    $total += $sub_total;
                }
                $resource = view('theme.partials.cart', compact('carts'))->render();
                $data['resource'] = $resource;
                $data['total'] = final_price_format($total);
                $data['status'] = 200;
            }
        }catch (\Exception $e){
            $data['status'] = 201;
            $data['message'] = $e->getMessage();
        }


        return $data;
    }

    public function wishlist(){
        $user = Auth::guard('customers')->user();
        if(!$user){
            $user_id = session()->get('wishlist_user_id');
        }else{
            $user_id = $user->id;
        }
        HomeController::$page_title = "Wish List";
        $wishlists = WishList::select('wishlists.*','products.name', 'products.qty as products_qty','product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('products', 'wishlists.product_id','=','products.id')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('wishlists.user_id', $user_id)
            ->where('product_prices.status', 1)
            ->get();
        return view('theme.wishlist', compact('wishlists'));
    }

    public function addWishlist(Request $request){

        $product_id = $request->get('product_id');
        $user_id = session()->get('wishlist_user_id');

        $wishlist = WishList::where('user_id', $user_id)->where('product_id',$product_id)->first();
        if(!$wishlist){
            $wishlist = new WishList();
            $wishlist->user_id = $user_id;
            $wishlist->product_id = $product_id;
            $wishlist->qty = 1;
            $wishlist->save();
        }

        $this->setUserWishList($user_id);
        $data['status'] = 200;
        $data['message'] = "Product added to wishlist successfully.";

        return json_encode($data);
    }

    public function removeWishlist(Request $request){
        $row_id = $request->get('row_id');
        $user = Auth::guard('customers')->user();
        if(!$user){
            $user_id = session()->get('wishlist_user_id');
        }else{
            $user_id = $user->id;
        }
        WishList::where('user_id',$user_id)->where('id', $row_id)->delete();
        $data['status'] = 200;
        $data['message'] = "Product removed in wishlists successfully.";
        return json_encode($data);
    }

    public function setUserWishList($user_id){

        $user = Auth::guard('customers')->user();
        if($user){
            if($user_id != $user->id){
                WishList::where('user_id',$user_id)
                    ->update([
                        'user_id' => $user->id,
                        'user_type' => $user->customer_type
                    ]);
                session()->put('wishlist_user_id', $user->id);
            }

        }
    }

    public function addRating(Request $request){

        $rules = [
            'review' => 'required|string',
            'summery' => 'required|string|max:255',
            'nickname' => 'required|string|max:255',
        ];
        $message = [
            'summery.required' => 'The summery field is required',
            'summery.string' => 'The summery should be string',
            'summery.max' => 'Maximum characters is 255',
            'review.required' => 'The review field is required',
            'review.string' => 'The review should be string',
            'nickname.required' => 'The nickname field is required',
            'nickname.string' => 'The nickname should be string',
            'nickname.max' => 'Maximum characters is 255',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            session()->flash('error_message','Your review form is incomplete.');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $review = $request->get('review');
        $summery = $request->get('summery');
        $nickname = $request->get('nickname');
        $value_review = ($request->get('value_review')) ? $request->get('value_review') : 0;
        $price_review = ($request->get('price_review')) ? $request->get('price_review') : 0;
        $quality_review = ($request->get('quality_review')) ? $request->get('quality_review') : 0;
        $product_id = $request->get('product_id');
        $user = Auth::guard('customers')->user();
        $product_review = new ProductsReviews();
        $product_review->customer_id = $user->id;
        $product_review->product_id = $product_id;
        $product_review->quality = $quality_review;
        $product_review->price = $price_review;
        $product_review->value = $value_review;
        $product_review->name = $nickname;
        $product_review->summary = $summery;
        $product_review->review = json_encode($review);
        $product_review->status = 0;
        $product_review->save();

        $user_lists = User::where('user_type', '!=', 3)->get();
        foreach ($user_lists as $user_list){
            $notification['user_id'] = $user_list->id;
            $notification['data'] = "A customer has been added review";
            $notification['url'] = route('admin.products.edit.get', [$product_id]);
            createNotifications($notification);
        }

        session()->flash('success_message','Review Added successfully');
        return redirect()->back();
    }

    public function checkout(){
        $user = Auth::guard('customers')->user();
        if(!$user)
        {
            return redirect()->to(route('login'));
        }
        HomeController::$page_title = "Checkout";
        $carts = Cart::select('carts.*','products.name','product_prices.normal_price','product_prices.normal_discount','product_prices.school_price','product_prices.school_discount')
            ->join('products', 'carts.product_id','=','products.id')
            ->join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->where('carts.user_id', $user->id)
            ->where('product_prices.status', 1)
            ->get();
        $coupon_code = null;
        $coupon_value = 0;
        if(session()->has('customer_coupon_code')){
            $coupon_code = session()->get('customer_coupon_code');
            $coupon_value = session()->get('customer_coupon_value');
        }
        $addresses = Address::where('customer_id', $user->id)->where('address_type', 1)->first();
        $shipping_addresses = Address::where('customer_id', $user->id)->where('address_type', 2)->first();
        return view('theme.checkout', compact('carts','coupon_code','coupon_value','addresses','shipping_addresses'));
    }

    public function coupon(Request $request){

        $coupon_code = $request->get('coupon_code');
        $data = array();
        $now = time();
        $coupon = Coupon::where('name', $coupon_code)->first();
        $user = Auth::guard('customers')->user();
        if(!$user){
            $data['status'] = 202;
            $data['message'] = "User doesn't log in";
        }
        $sub_total = sub_total($user);
        if($coupon){
            $expire = $coupon->expire_at;
            $expire = strtotime($expire);
            if($expire < $now){
                $data['status'] = 201;
                $data['message'] = "Coupon was expired";
                $data['coupon_total_text'] = final_price_format(0);
                $data['grand_total_text'] = final_price_format($sub_total);
            }else{
                $coupon_data = apply_coupon_code($coupon, $sub_total);
                if($coupon_data){
                    if($coupon_data['status']){
                        session()->put('customer_coupon_code', $coupon_code);
                        session()->put('customer_coupon_value', $coupon_data['value']);
                        $data['status'] = 200;
                        $data['message'] = "Coupon applied successfully";
                        $data['coupon_total'] = $coupon_data['value'];
                        $data['sub_total'] = $coupon_data['sub_total'];
                        $data['grand_total'] = $coupon_data['sub_total'] - $coupon_data['value'];
                        $data['coupon_total_text'] = final_price_format((-1) *$coupon_data['value']);
                        $data['sub_total_text'] = final_price_format($coupon_data['sub_total']);
                        $data['grand_total_text'] = final_price_format($coupon_data['sub_total'] - $coupon_data['value']);
                    }else{
                        $data['status'] = 201;
                        $data['message'] = "Coupon apply unsuccessfully";
                        $data['coupon_total_text'] = final_price_format(0);
                        $data['grand_total_text'] = final_price_format($coupon_data['sub_total'] - $coupon_data['value']);
                    }

                }else{
                    $data['status'] = 201;
                    $data['message'] = "Coupon apply unsuccessfully";
                    $data['coupon_total_text'] = final_price_format(0);
                    $data['grand_total_text'] = final_price_format($coupon_data['sub_total'] - $coupon_data['value']);
                }
            }

        }else{
            $data['status'] = 201;
            $data['message'] = "Coupon doesn't exists";
            $data['coupon_total_text'] = final_price_format(0);
            $data['grand_total_text'] = final_price_format($sub_total);
        }

        return json_encode($data);
    }

    public function subscribe(Request $request){

        $rules = [
            'email' => 'required|email',
        ];

        $message = [
            'email.required' => 'The email field is required',
            'email.email' => 'The email should be valid email',
        ];
        $data = array();
        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            $error = $validator->errors();
            $data['status'] = 202;
            $data['message'] = $error;
        }
        $email = $request->get('email');
        $subscribe = EmailSubscribe::where('email',$email)->first();
        if($subscribe){
            if($subscribe->status == 0){
                $subscribe->status = 1;
                $subscribe->save();
                $data['status'] = 200;
                $data['message'] = "Thank you for subscribe us again";
            }else{
                $data['status'] = 200;
                $data['message'] = "You already subscribed us";
            }
        }else{
            $subscribe = new EmailSubscribe();
            $subscribe->email = $email;
            $subscribe->save();
            $data['status'] = 200;
            $data['message'] = "Thank you for subscribe us";
        }
        return json_encode($data);
    }

    public function blog($slug = null){

        if($slug){
            $blog = Blog::where('slug', $slug)->first();
            if($blog){
                return view('theme.blog-details', compact('blog'));
            }
        }else{
            $blogs = Blog::orderBy('id', 'DESC')->paginate(10);
            return view('theme.blog', compact('blogs'));
        }
    }

    public function comment(Request $request){

        $post_id = $request->get('post_id');
        $comment_id = $request->get('comment_id');
        $comment = $request->get('comment');
        $email = $request->get('email');
        $name = $request->get('name');

        $new_comment = new BlogComment();
        $new_comment->post_id = $post_id;
        $new_comment->comment_id = $comment_id;
        $new_comment->comment = json_encode($comment);
        $new_comment->created_by = $name;
        $new_comment->created_by_email = $email;
        $new_comment->created_by_status = 3;
        $new_comment->save();


    }

    public function getComment(Request $request){

        $post_id = $request->get('post_id');
        $post = Blog::find($post_id);
        $count = 0;
        $resource = 0;
        if($post){
            $comments = BlogComment::where('post_id', $post_id)->where('comment_id','=', null)->where('status', 1)->orderBy('id', 'DESC')->get();
            $count = BlogComment::where('post_id', $post_id)->where('status', 1)->count();
            $resource = view('theme.partials.comment', compact('comments'))->render();
        }

        $data['resource'] = $resource;
        $data['count'] = $count;
        return $data;
    }

    public function profile(){
        $user = Auth::guard('customers')->user();
        $customer = Customer::find($user->id);
        $addresses = Address::where('customer_id', $user->id)->where('address_type', 1)->first();
        return view('theme.profile', compact('customer','addresses'));
    }

    public function updateProfile(Request $request){

        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'address_line_1' => 'required',
            'zip_code' => 'required',
        ];

        $message = [
            'first_name.required' => 'The First name field is required',
            'last_name.required' => 'The Last name field is required',
            'address_line_1.required' => 'The address aine 1 field is required',
            'zip_code.required' => 'The postalcode / zip  field is required',
        ];
        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','Customer profile update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::guard('customers')->user();
        $customer = Customer::find($user->id);

        if(!$customer){
            return redirect()->to(route('login'));
        }

        $customer->first_name = $request->get('first_name');
        $customer->last_name = $request->get('last_name');
        $customer->phone_number = $request->get('phone_number');
        $customer->mobile_number = $request->get('mobile_number');
        $customer->complete_status = 2;
        $customer->save();

        $addresses = Address::where('customer_id', $user->id)->where('address_type', 1)->first();
        if(!$addresses){
            $addresses = new Address();
        }
        $addresses->customer_id = $user->id;
        $addresses->first_name = $request->get('first_name');
        $addresses->last_name = $request->get('last_name');
        $addresses->address_type = 1;
        $addresses->address_line_1 = $request->get('address_line_1');
        $addresses->address_line_2 = $request->get('address_line_2');
        $addresses->city = $request->get('city');
        $addresses->zip_code = $request->get('zip_code');
        $addresses->phone_number = $request->get('phone_number');
        $addresses->mobile_number = $request->get('mobile_number');
        $addresses->save();
        session()->flash('success_message','Customer profile updated successfully');
        return redirect()->back();
    }

    public function checkoutComplete(Request $request){
        $user = Auth::guard('customers')->user();
        $customer = Customer::find($user->id);

        if(!$customer){
            return redirect()->to(route('login'));
        }
        $rules = [
            'billing_first_name' => 'required|string|max:255',
            'billing_last_name' => 'required|string|max:255',
            'billing_address_line_1' => 'required|string|max:255',
            'billing_address_line_2' => 'string|max:255',
            'billing_city' => 'required|string|max:255',
            'billing_zip_code' => 'required|string|max:255',
            'billing_phone_number' => 'nullable|string|max:255',
            'billing_mobile_number' => 'nullable|string|max:255'
        ];

        $message = [
            'billing_first_name.required' => 'The billing first name field is required',
            'billing_first_name.string' => 'The billing first name should be string',
            'billing_first_name.max' => 'The billing first name is maximum 255 characters',
            'billing_last_name.required' => 'The billing last name field is required',
            'billing_last_name.string' => 'The billing last name should be string',
            'billing_last_name.max' => 'The billing last name is maximum 255 characters',
            'billing_address_line_1.required' => 'The billing address line 1 field is required',
            'billing_address_line_1.string' => 'The billing address line 1 should be string',
            'billing_address_line_1.max' => 'The billing address line 1 is maximum 255 characters',
            'billing_address_line_2.string' => 'The billing address line 2 should be string',
            'billing_address_line_2.max' => 'The billing address line 2 is maximum 255 characters',
            'billing_city.required' => 'The billing city field is required',
            'billing_city.string' => 'The billing city should be string',
            'billing_city.max' => 'The billing city is maximum 255 characters',
            'billing_zip_code.required' => 'The billing postalcode / zip  field is required',
            'billing_zip_code.string' => 'The billing postalcode / zip should be string',
            'billing_zip_code.max' => 'The billing postalcode / zip is maximum 255 characters',
            'billing_phone_number.required_if' => 'The billing phone number or The billing mobile number is required',
            'billing_phone_number.string' => 'The billing phone number should be string',
            'billing_phone_number.max' => 'The billing phone number is maximum 255 characters',
            'billing_mobile_number.required_if' => 'The billing phone number or The billing mobile number is required',
            'billing_mobile_number.string' => 'The billing mobile number should be string',
            'billing_mobile_number.max' => 'The billing mobile number is maximum 255 characters',
        ];
        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','Order place unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if(!checkCartItems($user)){
            return redirect()->to(route('cart'));
        }
        $coupon_id = null;
        $coupon_value = 0;
        if(session()->has('customer_coupon_code')){
            $coupon_code = session()->get('customer_coupon_code');
            $coupon = Coupon::where('name', $coupon_code)->first();
            if($coupon){
                $coupon_id = $coupon->id;
                $coupon_value = session()->get('customer_coupon_value');
            }
        }
        $data['coupon_id'] = $coupon_id;
        $data['coupon_value'] = $coupon_value;
        $data['sub_total'] = sub_total($user);
        $data['order_total'] = $data['sub_total'] - $data['coupon_value'];
        addBillingAddress($request, $user->id);
        addShippingAddress($request, $user->id);
        $order_id = createOrder($request, $user->id, $data);
        $this->orderProducts($order_id, $user->id);
        order_email_send();
        session()->flash('success_message','Order has been completed.');
        return redirect()->to(route('home'));
    }

    protected function createOrder($request, $customer_id, $data){

        $order_id = $this->generateOrderId($customer_id);
        $order = new Order();
        $order->order_id = $order_id;
        $order->customer_id = $customer_id;
        $order->coupon_id = $data['coupon_id'];
        $order->sub_total = $data['sub_total'];
        $order->coupon_value = $data['coupon_value'];
        $order->order_total = $data['order_total'];
        $order->status = 1;
        $order->billing_first_name = $request->get('billing_first_name');
        $order->billing_last_name = $request->get('billing_last_name');
        $order->billing_address_line_1 = $request->get('billing_address_line_1');
        $order->billing_address_line_2 = $request->get('billing_address_line_2');
        $order->billing_city = $request->get('billing_city');
        $order->billing_zip_code = $request->get('billing_zip_code');
        $order->billing_phone_number = $request->get('billing_phone_number');
        $order->billing_mobile_number = $request->get('billing_mobile_number');
        $ship_to_different_address = $request->get('ship_to_different_address');
        if($ship_to_different_address){
            $order->shipping_first_name = $request->get('shipping_first_name');
            $order->shipping_last_name = $request->get('shipping_last_name');
            $order->shipping_address_line_1 = $request->get('shipping_address_line_1');
            $order->shipping_address_line_2 = $request->get('shipping_address_line_2');
            $order->shipping_city = $request->get('shipping_city');
            $order->shipping_zip_code = $request->get('shipping_zip_code');
            $order->shipping_phone_number = $request->get('shipping_phone_number');
            $order->shipping_mobile_number = $request->get('shipping_mobile_number');
        }else{
            $order->shipping_first_name = $request->get('billing_first_name');
            $order->shipping_last_name = $request->get('billing_last_name');
            $order->shipping_address_line_1 = $request->get('billing_address_line_1');
            $order->shipping_address_line_2 = $request->get('billing_address_line_2');
            $order->shipping_city = $request->get('billing_city');
            $order->shipping_zip_code = $request->get('billing_zip_code');
            $order->shipping_phone_number = $request->get('billing_phone_number');
            $order->shipping_mobile_number = $request->get('billing_mobile_number');
        }
        $order->save();
        return $order->id;
    }

    protected function orderProducts($order_id, $customer_id){

        $carts = Cart::where('user_id', $customer_id)->get();
        foreach ($carts as $cart){
            $product_price = ProductPrice::where('product_id', $cart->product_id)->orderBy('id', 'DESC')->first();
            $product = Product::find($cart->product_id);
            $order_product = new OrderProduct();
            $order_product->order_id = $order_id;
            $order_product->product_id = $cart->product_id;
            $order_product->products_name = $product->name;
            $order_product->qty = $cart->qty;
            $order_product->user_type = $cart->user_type;
            $order_product->normal_price = $product_price->normal_price;
            $order_product->normal_discount = $product_price->normal_discount;
            $order_product->school_price = $product_price->school_price;
            $order_product->school_discount = $product_price->school_discount;
            $order_product->save();
            $product->qty = $product->qty - $cart->qty;
            $product->save();
        }
        Cart::where('user_id', $customer_id)->delete();
    }

    protected function generateOrderId($customer_id){
        $order_id = $customer_id;
        $order_id .= date('d').date('n');
        $length = strlen($order_id);
        $remaining = 10 - $length;
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        for ($i = 0; $i < $remaining; $i++) {
            $order_id .= $characters[rand(0, $charactersLength - 1)];
        }
        return $order_id;
    }

    protected function addBillingAddress($request, $customer_id){

        $address = Address::where('customer_id', $customer_id)->where('address_type', 1)->first();
        if(!$address){
            $address = new Address();
            $address->address_type = 1;
        }
        $address->customer_id = $customer_id;
        $address->first_name = $request->get('billing_first_name');
        $address->last_name = $request->get('billing_last_name');
        $address->address_line_1 = $request->get('billing_address_line_1');
        $address->address_line_2 = $request->get('billing_address_line_2');
        $address->city = $request->get('billing_city');
        $address->zip_code = $request->get('billing_zip_code');
        $address->phone_number = $request->get('billing_phone_number');
        $address->mobile_number = $request->get('billing_mobile_number');
        $address->save();

        $customer = Customer::where('complete_status','!=',2)->where('id', $customer_id)->first();
        if($customer){
            $customer->first_name = $request->get('billing_first_name');
            $customer->last_name = $request->get('billing_last_name');
            $customer->phone_number = $request->get('billing_phone_number');
            $customer->mobile_number = $request->get('billing_mobile_number');
            $customer->complete_status = 1;
            $customer->save();
        }
    }

    protected function addShippingAddress($request, $customer_id){

        $address = Address::where('customer_id', $customer_id)->where('address_type', 2)->first();
        if(!$address){
            $address = new Address();
            $address->address_type = 2;
        }
        $address->customer_id = $customer_id;
       $ship_to_different_address = $request->get('ship_to_different_address');
       if($ship_to_different_address){
           $address->first_name = $request->get('shipping_first_name');
           $address->last_name = $request->get('shipping_last_name');
           $address->address_line_1 = $request->get('shipping_address_line_1');
           $address->address_line_2 = $request->get('shipping_address_line_2');
           $address->city = $request->get('shipping_city');
           $address->zip_code = $request->get('shipping_zip_code');
           $address->phone_number = $request->get('shipping_phone_number');
           $address->mobile_number = $request->get('shipping_mobile_number');
       }else{
           $address->first_name = $request->get('billing_first_name');
           $address->last_name = $request->get('billing_last_name');
           $address->address_line_1 = $request->get('billing_address_line_1');
           $address->address_line_2 = $request->get('billing_address_line_2');
           $address->city = $request->get('billing_city');
           $address->zip_code = $request->get('billing_zip_code');
           $address->phone_number = $request->get('billing_phone_number');
           $address->mobile_number = $request->get('billing_mobile_number');
       }
       $address->save();
    }

    public function orders(){
        $user = Auth::guard('customers')->user();
        $customer = Customer::find($user->id);

        if(!$customer){
            return redirect()->to(route('login'));
        }
        $orders = Order::where('customer_id', $user->id)->orderBy('id', 'DESC')->paginate(10);
        return view('theme.order', compact('orders'));
    }

    public function aboutUs(){
        HomeController::$page_title = "About us";
        $page = "about-us";
        $pages = Page::where('name', $page)->first();
        $data = array();
        $data['content']['office_info_content'] = "";
        $data['content']['get_in_touch_content'] = "";
        $data['content']['office_address'] = "";
        $data['content']['office_phone'] = "";
        $data['content']['office_email'] = "";
        if($pages){
            $content = json_decode($pages->value, true);
            if(array_key_exists('content', $content)){
                $data['content']['content_title'] = $content['content']['content_title'];
                $data['content']['summary'] = $content['content']['summary'];
              //  $data['content']['our_vision'] = $content['content']['our_vision'];
               // $data['content']['our_mission'] = $content['content']['our_mission'];

            }
        }
        return view('theme.about-us', compact('data'));
    }

    public function contactUs(){
        HomeController::$page_title = "Contact us";
        $page = "contact-us";
        $pages = Page::where('name', $page)->first();
        $data = array();
        $data['content']['office_info_content'] = "";
        $data['content']['get_in_touch_content'] = "";
        $data['content']['office_address'] = "";
        $data['content']['office_phone'] = "";
        $data['content']['office_email'] = "";
        if($pages){
            $content = json_decode($pages->value, true);
            if(array_key_exists('content', $content)){
                $data['content']['office_info_content'] = $content['content']['office_info_content'];
                $data['content']['get_in_touch_content'] = $content['content']['get_in_touch_content'];
                $data['content']['office_address'] = $content['content']['office_address'];
                $data['content']['office_phone'] = $content['content']['office_phone'];
                $data['content']['office_email'] = $content['content']['office_email'];
            }
        }
        return view('theme.contact-us', compact('data'));
    }

    public function contactUsStore(Request $request){
        $rules = [
            'name'  => 'required|string|max:255',
            'email' => 'required|email',
            'subject' => 'required|string|max:255',
            'message' => 'required|string'
        ];

        $messages = [
            'name.required' => 'Name is required',
            'name.string'   => 'Name should be string',
            'name.max'      => 'Maximum characters is 255',
            'email.required' => 'Email address is required',
            'email.email'   => 'Email should be string',
            'subject.required' => 'Subject is required',
            'subject.string' => 'Subject should be string',
            'subject.max'   => 'Maximum characters is 255',
            'message.required' => 'Message is required',
            'message.string'    => 'Message should be string'
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            session()->flash('error_message','Contact form is uncomplete.');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $contact_us = new ContactUs();
        $contact_us->name = $request->get('name');
        $contact_us->email = $request->get('email');
        $contact_us->subject = $request->get('subject');
        $contact_us->message = json_encode($request->get('message'));
        $contact_us->save();

        session()->flash('success_message','Thank you contact us! we will be touch you soon');
        return redirect()->back();
    }

    public function passowrdResetLink(Request $request){

        return $request->get('email');
    }

    public function authors(){
        HomeController::$page_title = "Authors";
        $authors = Author::where('status', 1)->get();
        return view('theme.authors', compact('authors'));
    }

    public function authorDetails($slug){
        $author = Author::where('slug', $slug)->where('status', 1)->first();
        if($author){
            return view('theme.author-details', compact('author'));
        }else{
            abort(404);
        }
    }
    public function orderEmail(){
        $order_id = 6;
        $order = Order::find($order_id);
        $customer_type = "Personal";
        $user = Auth::guard('customers')->user();
        if($user)
        {
            if($user->customer_type == 2){
                $customer_type = "School";
            }else if($user->customer_type == 3){
                $customer_type = "Authors";
            }else if($user->customer_type == 4){
                $customer_type = "Bookshop";
            }
        }
        $order_products = OrderProduct::select('order_products.*', 'product_images.image_path', 'products.author_name')
                            ->leftJoin('product_images', 'order_products.product_id', '=', 'product_images.product_id')
                            ->leftJoin('products', 'order_products.product_id', '=', 'products.id')
                            ->where('order_products.order_id', $order_id)
                            ->where('product_images.type', 1)
                            ->where('product_images.status', 1)
                            ->get();
        return view('emails.test', compact('order', 'customer_type', 'order_products'));
    }
}
