<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    //
    protected $table = 'products';


    public function scopeOfSort($query, $order, $order_by, $customer_type,$amount)
    {

        if(!$order_by){
            $order_by = 'ASC';
        }
        $order_field = 'products.name';
        if(!$customer_type){
            $customer_type = 1;
        }
        if($order == 'price')
        {
            if($customer_type == 2){
                $order_field = DB::raw('school_price - school_discount');
            }else{
                $order_field = DB::raw('normal_price - normal_discount');
            }
        }
        $format = config('status.currency_format');
        if($amount){
            $amount = explode('-', $amount);
            $min = ltrim(trim($amount[0]), $format);
            $max = ltrim(trim($amount[1]), $format);
            $min = floatval($min);
            $max = floatval($max);
            if($customer_type == 2){
                $query->where(DB::raw('school_price - school_discount'),'>=',$min)->where(DB::raw('school_price - school_discount'), '<', $max);
            }else{
                $query->where(DB::raw('normal_price - normal_discount'),'>=',$min)->where(DB::raw('normal_price - normal_discount'), '<', $max);
            }

        }

        $query->orderBy($order_field, $order_by);

        return $query;
    }
}
