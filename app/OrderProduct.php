<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    //

    public function scopeTimePeriods($query, $from_date, $to_date){
        if($from_date && $to_date){
            return $query->where('op.created_at', '<=', $to_date)->where('op.created_at', '>=', $from_date);
        }

        return $query;
    }

    public function scopePayment($query, $payment_id){

        if($payment_id > 0){
            return $query->where('o.status', '=', $payment_id);
        }
        return $query->whereIn('o.status', array(4, 7));
    }
}
