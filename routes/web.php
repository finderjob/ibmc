<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('password/reset', 'Auth\CustomerController@passwordReset')->name('password.reset.customer');
Route::post('password/reset', 'Auth\CustomerController@passwordRequest')->name('password.request.customer');
Route::get('reset/password/{token}', 'Auth\CustomerController@resetPassword')->name('reset.password.customer');
Route::post('reset/password/{token}', 'Auth\CustomerController@resetPasswordRequest')->name('reset.password.customer.request');

Route::group(['middleware' => ['profile']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('blog/{slug?}', 'HomeController@blog')->name('blog');
    Route::get('products/', 'HomeController@product_list')->name('products');
    Route::get('shop', 'HomeController@shop')->name('shop');
    Route::get('shop/category/{slug}', 'HomeController@shopWithCategory')->name('shop.category');
    Route::get('shop/category/{slug}/sub-category/{sub_category}', 'HomeController@shopWithSubcategory')->name('shop.sub-category');
    Route::get('shop/product/{product_slug}', 'HomeController@product')->name('product');
    Route::get('shop/category/{slug}/product/{product_slug}', 'HomeController@productWithCategory')->name('product.with.category');
    Route::get('shop/category/{slug}/sub-category/{sub_slug}/product/{product_slug}', 'HomeController@productWithSubcategory')->name('product.with.sub-category');
    Route::get('my-cart', 'HomeController@cart')->name('cart');
    Route::get('cart/min', 'HomeController@getCart')->name('cart.get');
    Route::get('my-wishlist','HomeController@wishlist')->name('wishlist');
    Route::get('checkout', 'HomeController@checkout')->name('checkout');
    Route::get('blog/comment', 'HomeController@getComment')->name('blog.comment.get');
    Route::get('my-orders', 'HomeController@orders')->name('orders');
    Route::get('authors', 'HomeController@authors')->name('authors');
    Route::get('authors/{slug}', 'HomeController@authorDetails')->name('authors.details');
});

Route::post('cart', 'HomeController@addCart')->name('cart.add');
Route::post('cart/qty', 'HomeController@updateCartQty')->name('cart.update');
Route::post('cart/remove', 'HomeController@removeCart')->name('cart.remove');
Route::post('wishlist', 'HomeController@addWishlist')->name('wishlist.add');
Route::post('wishlist/remove', 'HomeController@removeWishlist')->name('wishlist.remove');
Route::post('rating', 'HomeController@addRating')->name('rating.add');
Route::post('coupon', 'HomeController@coupon')->name('coupon');
Route::post('email/subscribe', 'HomeController@subscribe')->name('subscribe');
Route::post('blog/comment', 'HomeController@comment')->name('blog.comment');
Route::get('my-profile', 'HomeController@profile')->name('profile');
Route::post('customer/profile', 'HomeController@updateProfile')->name('profile.post');
Route::post('checkout', 'HomeController@checkoutComplete')->name('checkout.post');
Route::get('about-us', 'HomeController@aboutUs')->name('about.us');
Route::get('contact-us', 'HomeController@contactUs')->name('contact.us');
Route::post('contact-us-with', 'HomeController@contactUsStore')->name('contact.us.post');

Route::get('order-email','HomeController@orderEmail');
/*
 * Admin panel routes
 */

Route::group(['prefix'=>'administrator'], function() {
    /*
     * Login, logout routes,
     * */
    Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login.get');
    Route::post('login', 'Auth\AdminLoginController@login')->name('admin.login.post');
    Route::post('logout', 'Auth\AdminLoginController@logout')->name('admin.logout.post');
    Route::get('/', 'Admin\AdminController@index')->name('admin.home');

    Route::get('users','Admin\AdminController@show')->name('admin.list')->middleware('permission:user list');
    Route::get('users/add','Admin\AdminController@add')->name('admin.users.add')->middleware('permission:user list|user create');
    Route::post('users/add','Admin\AdminController@store')->name('admin.users.store')->middleware('permission:user list|user create');
    Route::post('users/change/status','Admin\AdminController@changeUserStatus')->name('admin.users.change.userStatus')->middleware('permission:user list');
    Route::get('users/forget-password/{id}','Admin\AdminController@forgetPassword')->name('admin.users.reset-password');
    Route::post('users/change/type','Admin\AdminController@changeUserType')->name('admin.users.change.userType');
    Route::post('users/change/password','Admin\AdminController@changeUserPassword')->name('admin.users.change.password');
    Route::get('user/profile','Admin\AdminController@profile')->name('admin.profile');
    Route::get('user/profile/{id}','Admin\AdminController@profile')->name('admin.profile.id');
    Route::post('user/update/profile/{id}','Admin\AdminController@updateProfile')->name('admin.profile.update');
    Route::post('user/update/propic/{id}','Admin\AdminController@updatePropic')->name('admin.profile.propic');
    Route::get('user/session','Admin\AdminController@setSession')->name('admin.profile.session');

    Route::get('roles','Admin\RoleController@index')->name('admin.role');
    Route::post('assign-permission','Admin\RoleController@assign')->name('admin.assign.permission');
    Route::post('role-permission','Admin\RoleController@getAssignPermission')->name('role.permission');

    Route::get('reset-password/{token}','Admin\AdminController@resetPassword')->name('admin.users.reset-password-url');
    Route::post('reset-password/','Admin\AdminController@passwordReset')->name('admin.users.password.reset');
    Route::get('users/forget-password/{id}','Admin\AdminController@forgetPassword')->name('admin.users.reset-password');

   // Route::get('checkMail','Admin\AdminController@checkMail');

    Route::post('slug-generate','Admin\AdminController@generateSlug')->name('generate-slug');

    Route::get('categories', 'Admin\CategoryController@index')->name('admin.category');
    Route::get('categories/add', 'Admin\CategoryController@add')->name('admin.category.get');
    Route::post('categories/add', 'Admin\CategoryController@store')->name('admin.category.post');
    Route::get('categories/edit/{id}', 'Admin\CategoryController@edit')->name('admin.category.edit.get');
    Route::post('categories/edit/{id}', 'Admin\CategoryController@update')->name('admin.category.edit.post');
    Route::post('categories/change/status','Admin\CategoryController@changeCategoryStatus')->name('admin.category.change.categoryStatus');

    Route::get('sub-categories', 'Admin\CategoryController@subIndex')->name('admin.sub-categories');
    Route::get('sub-categories/add', 'Admin\CategoryController@subAdd')->name('admin.sub-categories.get');
    Route::post('sub-categories/add', 'Admin\CategoryController@subStore')->name('admin.sub-categories.post');
    Route::get('sub-categories/edit/{id}', 'Admin\CategoryController@subEdit')->name('admin.sub-category.edit.get');
    Route::post('sub-categories/edit/{id}', 'Admin\CategoryController@subUpdate')->name('admin.sub-category.edit.post');

    Route::post('sub-categories/get', 'Admin\CategoryController@getSubCategories')->name('admin.get.sub-categories.post');

    Route::get('products', 'Admin\ProductController@index')->name('admin.products');
    Route::get('products/add', 'Admin\ProductController@add')->name('admin.products.add.get');
    Route::post('products/add', 'Admin\ProductController@store')->name('admin.products.add.post');
    Route::get('products/edit/{id}', 'Admin\ProductController@edit')->name('admin.products.edit.get');
    Route::post('products/edit/{id}', 'Admin\ProductController@update')->name('admin.products.edit.post');
    Route::post('products/edit/upload/{id}', 'Admin\ProductController@uploadCoverImages')->name('admin.products.edit.upload');
    Route::post('products/edit/upload/others/{id}', 'Admin\ProductController@uploadOtherImages')->name('admin.products.edit.other.upload');
    Route::post('products/edit/price/{id}', 'Admin\ProductController@productPrice')->name('admin.products.edit.price');
    Route::post('products/change/status','Admin\ProductController@changeProductStatus')->name('admin.product.change.status');
    Route::post('products/review/change/status','Admin\ProductController@changeProductReviewStatus')->name('admin.product.review.change.status');

    Route::get('coupons', 'Admin\CouponController@index')->name('admin.coupons');
    Route::get('coupons/add', 'Admin\CouponController@add')->name('admin.coupons.add.get');
    Route::post('coupons/add', 'Admin\CouponController@store')->name('admin.coupons.add.post');
    Route::post('coupons/change/status','Admin\CouponController@changeCouponsStatus')->name('admin.coupons.change.status');

    Route::get('pages/home','Admin\PageController@index')->name('admin.pages');
    Route::post('home-slider/upload','Admin\PageController@homeSliderUpload')->name('admin.page.slider.upload');
    Route::post('home-slider/remove/{index}','Admin\PageController@homeSliderRemove')->name('admin.page.slider.remove');
    Route::get('pages/home/add','Admin\PageController@store')->name('admin.pages.get');

    Route::get('pages/about-us','Admin\PageController@aboutUs')->name('admin.pages.about-us');
    Route::post('pages/about-us','Admin\PageController@aboutUsStore')->name('admin.pages.about-us.post');
    Route::get('pages/contact-us','Admin\PageController@contactUs')->name('admin.pages.contact-us');
    Route::post('pages/contact-us','Admin\PageController@contactUsStore')->name('admin.pages.contact-us.post');
    Route::get('pages/image', 'Admin\PageController@backgroundImage')->name('admin.pages.image');
    Route::post('pages/image', 'Admin\PageController@backgroundImageStore')->name('admin.pages.image.post');
    Route::post('pages/image/remove/{id}', 'Admin\PageController@backgroundImageRemove')->name('admin.pages.image.remove');
    Route::get('pages/team-members', 'Admin\PageController@teamMembers')->name('admin.pages.team.members');
    Route::post('pages/team-members', 'Admin\PageController@teamMembersStore')->name('admin.pages.authors.team.members.post');
    Route::post('pages/team-members/remove/{id}', 'Admin\PageController@teamMembersRemove')->name('admin.pages.team.members.remove');
    Route::get('pages/team-members/edit', 'Admin\PageController@getTeamMembers')->name('admin.pages.team.members.get');
    Route::post('pages/team-members/edit', 'Admin\PageController@editTeamMembers')->name('admin.pages.team.members.post.edit');
    Route::get('pages/social-media', 'Admin\PageController@socialMedia')->name('admin.pages.social.media');
    Route::post('pages/social-media', 'Admin\PageController@socialMediaStore')->name('admin.pages.social.media.post');
    Route::get('settings', 'Admin\PageController@setting')->name('admin.settings');
    Route::post('settings', 'Admin\PageController@storeSetting')->name('admin.settings.post');

    Route::get('blogs', 'Admin\BlogController@index')->name('admin.blogs');
    Route::get('blogs/add', 'Admin\BlogController@add')->name('admin.blogs.get');
    Route::post('blogs/add', 'Admin\BlogController@store')->name('admin.blogs.post');
    Route::get('blogs/edit/{id}', 'Admin\BlogController@edit')->name('admin.blogs.edit.get');
    Route::post('blogs/edit/{id}', 'Admin\BlogController@update')->name('admin.blogs.edit.post');

    Route::get('orders', 'Admin\OrderController@index')->name('admin.orders')->middleware('permission:order access');
    Route::post('orders/change/status','Admin\OrderController@changeOrderStatus')->name('admin.orders.change.status')->middleware('permission:order access|order change status');
    Route::get('orders/add', 'Admin\OrderController@add')->name('admin.orders.add.get')->middleware('permission:order access|order create');
    Route::post('orders/add', 'Admin\OrderController@store')->name('admin.orders.add.post')->middleware('permission:order access|order create');
    Route::get('orders/old', 'Admin\OrderController@indexOld')->name('admin.orders.old')->middleware('permission:order access');
    Route::get('orders/old/{id}', 'Admin\OrderController@viewOld')->name('admin.orders.old.view')->middleware('permission:order access|order view');
    Route::get('orders/new/{id}', 'Admin\OrderController@view')->name('admin.orders.view')->middleware('permission:order access|order view');
    Route::post('orders/old', 'Admin\OrderController@indexOld')->name('admin.orders.old.post');
    Route::get('orders/customer/name', 'Admin\OrderController@customer');
    Route::get('orders/customer/email', 'Admin\OrderController@customer');
    Route::get('orders/customer', 'Admin\OrderController@customerInfos')->name('admin.order.customer');
    Route::get('orders/product', 'Admin\OrderController@productsInfos')->name('admin.order.product');
    Route::get('orders/price/data', 'Admin\OrderController@priceData')->name('admin.order.price.data');

    Route::get('payments', 'Admin\PaymentController@index')->name('admin.payments');
    Route::get('payments/add', 'Admin\PaymentController@add')->name('admin.payments.get');
    Route::post('payments/add', 'Admin\PaymentController@store')->name('admin.payments.post');
    Route::post('payments/remain', 'Admin\PaymentController@remain')->name('admin.payments.remain.post');
    Route::post('payments/change/status', 'Admin\PaymentController@changePaymentStatus')->name('admin.payment.change.status');
    Route::post('payments/deactivate/{id}', 'Admin\PaymentController@deactivate')->name('admin.payment.deactivate');

    Route::get('customers', 'Admin\CustomerController@index')->name('admin.customers');
    Route::post('customers/change/status','Admin\CustomerController@changeCustomerStatus')->name('admin.customers.change.status');
    Route::get('customers/add', 'Admin\CustomerController@add')->name('admin.customers.get');
    Route::post('customers/add', 'Admin\CustomerController@store')->name('admin.customers.post');
    Route::get('customers/edit/{id}', 'Admin\CustomerController@edit')->name('admin.customers.edit.get');
    Route::post('customers/edit/{id}', 'Admin\CustomerController@update')->name('admin.customers.edit.post');

    Route::get('customers/messages', 'Admin\CustomerController@complains')->name('admin.message.get');
    Route::post('customers/messages/status/{id}', 'Admin\CustomerController@complainsStatus')->name('admin.message.status.get');

    Route::get('notifications', 'Admin\NotificationController@list')->name('admin.notification.list');
    Route::get('notifications/get', 'Admin\NotificationController@index')->name('admin.notification.get');
    Route::get('notifications/read/{id}', 'Admin\NotificationController@read')->name('admin.notification.read');

    Route::get('authors', 'Admin\AuthorController@list')->name('admin.authors');
    Route::get('authors/add', 'Admin\AuthorController@add')->name('admin.authors.add.get');
    Route::post('authors/add', 'Admin\AuthorController@store')->name('admin.authors.add.post');
    Route::get('authors/edit/{id}', 'Admin\AuthorController@edit')->name('admin.authors.edit.get');
    Route::post('authors/edit/{id}', 'Admin\AuthorController@update')->name('admin.authors.edit.post');
    Route::post('authors/change/status','Admin\AuthorController@changeAuthorStatus')->name('admin.author.change.status');

    Route::get('reports/sales-books', 'Admin\ReportController@salesReportByProduct')->name('admin.sales.report');
    Route::post('reports/sales-books', 'Admin\ReportController@salesReportByProduct')->name('admin.sales.report.post');

    Route::get('reports/sales-orders', 'Admin\ReportController@salesReportByOrder')->name('admin.sales.orders.report');
    Route::post('reports/sales-orders', 'Admin\ReportController@salesReportByOrder')->name('admin.sales.orders.report.post');

    Route::get('reports/sales-customers', 'Admin\ReportController@salesReportByCustomers')->name('admin.sales.customers.report');
    Route::post('reports/sales-customers', 'Admin\ReportController@salesReportByCustomers')->name('admin.sales.customers.report.post');

    Route::get('reports/sales-rep', 'Admin\ReportController@salesReportByRep')->name('admin.sales.rep.report');
    Route::post('reports/sales-rep', 'Admin\ReportController@salesReportByRep')->name('admin.sales.rep.report.post');

    Route::get('reports/sales-orders-status', 'Admin\ReportController@salesReportByOrderStatus')->name('admin.sales.orders.status.report');
    Route::post('reports/sales-orders-status', 'Admin\ReportController@salesReportByOrderStatus')->name('admin.sales.orders.status.report.post');

    Route::get('reports/available-books', 'Admin\ReportController@availableReportByBooks')->name('admin.available.books');
    Route::post('reports/available-books', 'Admin\ReportController@availableReportByBooks')->name('admin.available.books.post');
});
