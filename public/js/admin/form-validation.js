var FORM = function() {

    var handleUser = function() {
        jQuery.validator.addMethod("email", function(value, element) {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if(filter.test(value))
                {
                    return true;
                }
                return false;
         },'');
        $('.user-create-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    maxlength:255,
                    required: true,
                },
                first_name:{
                    maxlength:255,
                    required: true,
                },
                last_name:{
                    maxlength:255,
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    maxlength: 255,
                    minlength: 8,
                    required: true
                },
                user_type: {
                    required: true
                }
            },

            messages: {
                username: {
                    required: "The username field is required",
                    maxlength: "Maximum characters is 255"
                },
                first_name: {
                    required: "The first name field is required",
                    maxlength: "Maximum characters is 255"
                },
                last_name: {
                    required: "The last name field is required",
                    maxlength: "Maximum characters is 255"
                },
                email: {
                    required: "The email field is required",
                    email: "Email should be valid",
                },
                password: {
                    required: "The password field is required",
                    maxlength: "Maximum characters is 255",
                    minlength: 'Minimum characters is 8'
                },
                user_type: {
                    required: "The user type field is required"
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('.user-create-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.form-error'));
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.user-create-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.user-create-form').validate().form()) {
                    $('.user-create-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    };

    var handleUpdateUserInfo = function() {

        $('.update-profile-info').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                first_name:{
                    maxlength:255,
                    required: true,
                },
                last_name:{
                    maxlength:255,
                    required: true,
                },
            },

            messages: {

                first_name: {
                    required: "The first name field is required",
                    maxlength: "Maximum characters is 255"
                },
                last_name: {
                    required: "The last name field is required",
                    maxlength: "Maximum characters is 255"
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('.update-profile-info')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.form-error'));
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.update-profile-info input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.update-profile-info').validate().form()) {
                    $('.update-profile-info').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    };

    var handleChangePassword = function() {

        $('.change-password-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                current_password: {
                    maxlength: 255,
                    minlength: 6,
                    required: true
                },
                password: {
                    maxlength: 255,
                    minlength: 8,
                    required: true
                },
                password_confirmation: {
                    required: true,
                    equalTo: "#password"
                }
            },

            messages: {
                current_password: {
                    required: "The current password field is required",
                    maxlength: "Maximum characters is 255",
                    minlength: 'Minimum characters is 8'
                },
                password: {
                    required: "The password field is required",
                    maxlength: "Maximum characters is 255",
                    minlength: 'Minimum characters is 8'
                },
                password_confirmation: {
                    required: "The confirmation password is required",
                    equalTo: "The password doesn't match"
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('.change-password-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.form-error'));
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.change-password-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.change-password-form').validate().form()) {
                    $('.change-password-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    };


    return {
        //main function to initiate the module
        init: function() {

            handleUser();
            handleUpdateUserInfo();
            handleChangePassword();
        }
    };
}();

jQuery(document).ready(function() {
    FORM.init();
});
