
toastr.options = {
    progressBar: true,
    newestOnTop: false,
    closeButton: true,
    positionClass: "toast-bottom-right"
};
