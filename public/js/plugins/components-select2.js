var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "Select a Category";

        $("#category").select2({
            placeholder: placeholder,
            width: null
        });
        var placeholder = "Select a Sub category";
        $("#subcategory").select2({
            placeholder: placeholder,
            width: null
        });

        var placeholder2 = "Select a Products";
        $("#related_product").select2({
            placeholder: placeholder2,
            width: null
        });
        var placeholder2 = "Select an author";
        $("#product_author").select2({
            placeholder: placeholder2,
            width: null
        });

        var placeholder2 = "Select a customer by name";
        var origin   = window.location.origin;
        $("#customer_select_name").select2({
            placeholder: placeholder2,
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: origin + "/administrator/orders/customer/name",
                type: "GET",
                quietMillis: 50,
                data: function (term) {
                    return {
                        customer_name: term,
                        customer_email: null
                    };
                },
                processResults: function (data) {
                    obj = JSON.parse(data);
                    return {
                        results: $.map(obj, function (item) {
                            if(item){
                                return {
                                    text: item.id + " : " + item.name,
                                    id: item.id
                                }
                            }
                        })
                    };
                },
            }
        });
        var placeholder2 = "Select a customer by email";
        $("#customer_select_email").select2({
            placeholder: placeholder2,
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: origin + "/administrator/orders/customer/email",
                type: "GET",
                quietMillis: 50,
                data: function (term) {
                    return {
                        customer_name: null,
                        customer_email: term
                    };
                },
                processResults: function (data) {
                    obj = JSON.parse(data);
                    return {
                        results: $.map(obj, function (item) {
                            if(item){
                                return {
                                    text: item.id + " : " + item.email,
                                    id: item.id
                                }
                            }
                        })
                    };
                }
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {
        ComponentsSelect2.init();
    });
}
