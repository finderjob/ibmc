var customerFormValidation = function() {

    var customerProfile = function(){
        jQuery.validator.addMethod("telephone", function(value, element) {
            var mobile = $('.mobile_number').val();
            var phone = $('.phone_number').val();
            if(!phone && !mobile){
                return false
            }
            return true;
        },'');
        $('#customer_profile_form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                address_line_1: {
                    required: true
                },
                city: {
                    required: true
                },
                zip_code: {
                    required: true
                },
                phone_number: {
                    telephone: true
                },
                mobile_number: {
                    telephone: true
                }
            },

            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                address_line_1: {
                    required: "Address Line 1 is required"
                },
                city: {
                    required: "City is required"
                },
                zip_code: {
                    required: "Postcode / Zip is required"
                },
                phone_number: {
                    telephone: "Telephone or Mobile is required"
                },
                mobile_number: {
                    telephone: "Telephone or Mobile is required"
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('#customer_profile_form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.input_box').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.input_box').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });
        $('#customer_profile_form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#customer_profile_form').validate().form()) {
                    $('#customer_profile_form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    };
    var customerCheckoutForm = function () {
        jQuery.validator.addMethod("billing_telephone", function(value, element) {
            var mobile = $('.billing_mobile_number').val();
            var phone = $('.billing_phone_number').val();
            if(!phone && !mobile){
                return false
            }
            return true;
        },'');
        jQuery.validator.addMethod("shipping_telephone", function(value, element) {
            var mobile = $('.shipping_mobile_number').val();
            var phone = $('.shipping_phone_number').val();
            if(!phone && !mobile){
                return false
            }
            return true;
        },'');
        jQuery.validator.addMethod("shipping_details", function(value, element) {
            var checked = $('#ship_to_different_address');
            if(checked. prop("checked") == true){
                if(!value){
                    return false;
                }
            }
            return true;
        },'');
        $('#checkout-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                billing_first_name: {
                    required: true
                },
                billing_last_name: {
                    required: true
                },
                billing_address_line_1: {
                    required: true
                },
                billing_city: {
                    required: true
                },
                billing_zip_code: {
                    required: true
                },
                billing_phone_number: {
                    billing_telephone: true
                },
                billing_mobile_number: {
                    billing_telephone: true
                },
                shipping_first_name: {
                    shipping_details: true
                },
                shipping_last_name: {
                    shipping_details: true
                },
                shipping_address_line_1: {
                    shipping_details: true
                },
                shipping_city: {
                    shipping_details: true
                },
                shipping_zip_code: {
                    shipping_details: true
                },
                shipping_phone_number: {
                    shipping_telephone: true
                },
                shipping_mobile_number: {
                    shipping_telephone: true
                },
            },

            messages: {
                billing_first_name: {
                    required: "First name is required."
                },
                billing_last_name: {
                    required: "Last name is required."
                },
                billing_address_line_1: {
                    required: "Address Line 1 is required"
                },
                billing_city: {
                    required: "City is required"
                },
                billing_zip_code: {
                    required: "Postcode / Zip is required"
                },
                billing_phone_number: {
                    billing_telephone: "Telephone or Mobile is required"
                },
                billing_mobile_number: {
                    billing_telephone: "Telephone or Mobile is required"
                },
                shipping_first_name: {
                    shipping_details: "First name is required."
                },
                shipping_last_name: {
                    shipping_details: "Last name is required."
                },
                shipping_address_line_1: {
                    shipping_details: "Address Line 1 is required"
                },
                shipping_city: {
                    shipping_details: "City is required"
                },
                shipping_zip_code: {
                    shipping_details: "Postcode / Zip is required"
                },
                shipping_phone_number: {
                    shipping_telephone: "Telephone or Mobile is required"
                },
                shipping_mobile_number: {
                    shipping_telephone: "Telephone or Mobile is required"
                },
            }
            ,

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('#checkout-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.input_box').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.input_box').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });
        $('#checkout-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#checkout-form').validate().form()) {
                    $('#checkout-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    };
    return {
        //main function to initiate the module
        init: function() {
            customerCheckoutForm();
            customerProfile();
        }

    };

}();
jQuery(document).ready(function() {
    customerFormValidation.init();
});
