var ComponentsEditors = function () {

    var handleProductDescriptionSummernote = function () {
        $('#product_description').summernote({height: 300});
    };
    var handleProductOverviewSummernote = function () {
        $('#product_overview').summernote({height: 300});
    };
    var handleBlogContactSummernote = function () {
        $('#blog_content').summernote({height: 300});
    };
    var handleAboutUsSummernote = function () {
        $('#about_us_page_content').summernote({height: 200});
    };
    var handleOurMissionSummernote = function () {
        $('#our_mission_content').summernote({height: 200});
    };
    var handleOurVisionSummernote = function () {
        $('#our_vision_content').summernote({height: 200});
    };
    var handleOfficeInfoSummernote = function () {
        $('#office_info_content').summernote({height: 200});
    };
    var handleGetInTouchSummernote = function () {
        $('#get_in_touch_content').summernote({height: 200});
    };
    var handleAuthorSummernote = function () {
        $('#description').summernote({height: 200});
    };
    return {
        //main function to initiate the module
        init: function () {
            handleProductDescriptionSummernote();
            handleProductOverviewSummernote();
            handleOfficeInfoSummernote();
            handleBlogContactSummernote();
            handleAboutUsSummernote();
            handleOurMissionSummernote();
            handleOurVisionSummernote();
            handleGetInTouchSummernote();
            handleAuthorSummernote();
        }
    };

}();

jQuery(document).ready(function() {    
   ComponentsEditors.init();
});
