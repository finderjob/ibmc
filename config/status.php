<?php
/**
 * Created by PhpStorm.
 * User: Danuja Fernando
 * Date: 5/24/2019
 * Time: 9:04 PM
 */
return [
    'default' => [
        'active' => [
            'id' => 1,
            'name' => 'Activate',
            'class' => 'label-primary',
            'sort_by' => 1
        ],
        'deactivate' => [
            'id' => 2,
            'name' => 'Deactivate',
            'class' => 'label-danger',
            'sort_by' => 2
        ]
    ],


    'orders' => [
        'pending' => [
            'id' => 1,
            'name' => 'Pending',
            'class' => 'label-primary',
            'payment' => false,
            'close' => false,
            'return_qty' => false,
            'sort_by' => 1
        ],
        'ready_to_delivery' => [
            'id' => 2,
            'name'  => 'Ready to delivery',
            'class' =>  'label-warning',
            'payment' => false,
            'close' => false,
            'return_qty' => false,
            'sort_by' => 2
        ],
        'delivered' => [
            'id' => 3,
            'name' => 'Delivered',
            'class' => 'label-success',
            'payment' => false,
            'close' => false,
            'return_qty' => false,
            'sort_by' => 3
        ],
        'completed' => [
            'id' => 4,
            'name' => 'Completed',
            'class' => 'label-green',
            'payment' => true,
            'close' => true,
            'return_qty' => false,
            'sort_by' => 5
        ],
        'canceled'  => [
            'id'    => 5,
            'name'  => 'Canceled',
            'class' => 'label-danger',
            'payment' => false,
            'close' => true,
            'return_qty' => true,
            'sort_by' => 6
        ],
        'refunded'  => [
            'id'    => 6,
            'name'  => 'Refunded',
            'class' => 'label-orange',
            'payment' => true,
            'close' => true,
            'return_qty' => true,
            'sort_by' => 7
        ],
        'credited'  => [
            'id'    => 7,
            'name'  => 'Credited',
            'class' => 'label-purple',
            'payment' => false,
            'close' => false,
            'return_qty' => false,
            'sort_by' => 4
        ]
    ],

    'order_status_cannot_backward' => [ 4, 5, 6, 7],

    'payment_status' => [
        'accepted' => [
            'id' => 1,
            'name' => "Accept",
            'past_name' => "Accepted",
            'class' => 'label-green',
            'sort_by' => 1
        ],
        'decline' => [
            'id'    => 2,
            'name'  => 'Decline',
            'past_name' => 'Declined',
            'class' => 'label-danger',
            'sort_by'   => 2
        ],
        'refunded'  => [
            'id'    => 3,
            'name'  => 'Refunded',
            'class' => 'label-purple',
            'sort_by'   => 3
        ]
    ],

    'payment_types' => [
        'cod' => [
            'id'    => 1,
            'name'  => "Cash on delivery",
            'class' => 'label-orange',
            'sort_by'   => 3
        ]
    ],

    'order_payment_status'  => [
        'paid' => [
            'id' => 1,
            'name' => 'Paid',
            'class' => 'label-green',
            'sort_by' => '1'
        ],
        'unpaid' => [
            'id' => 0,
            'name' => 'Unpaid',
            'class' => 'label-warning',
            'sort_by' => '2'
        ],
        'refunded' => [
            'id' => 2,
            'name' => 'Refunded',
            'class' => 'label-purple',
            'sort_by' => '3'
        ],
    ],
    // default 14 days
    'order_completed_time_period' => 14,

    // default qty 10
    'less_product_quantity_count' => 10,

    'background_image' => [
        'pages' => [
            'about_us' => [
                'value' => 'about_us',
                'name' => 'About us'
            ],
            'contact_us' => [
                'value' => 'contact_us',
                'name' => 'Contact us'
            ],
            'shop' => [
                'value' => 'shop',
                'name' => 'Shop View'
            ],
            'product' => [
                'value' => 'product',
                'name' => 'Product View'
            ],
            'order' => [
                'value' => 'order',
                'name' => 'Order'
            ],
            'profile' => [
                'value' => 'profile',
                'name' => 'Profile'
            ],
            'auth'  => [
                'value' => 'auth',
                'name' => 'Login/Register'
            ],
            'wishlist'  => [
                'value' => 'wishlist',
                'name' => 'Wish List'
            ],
            'cart'  => [
                'value' => 'cart',
                'name' => 'Cart'
            ],
            'checkout'  => [
                'value' => 'checkout',
                'name' => 'Checkout'
            ],
            'blog' => [
                'value' => 'blog',
                'name' => 'Blog'
            ],
            'blog_details' => [
                'value' => 'blog_details',
                'name' => 'Blog Details'
            ],
            'authors' => [
                'value' => 'authors',
                'name' => 'Authors'
            ],
            'subscribe' => [
                'value' => 'subscribe',
                'name' => 'Subscribe'
            ],
            'sponsor' => [
                'value' => 'sponsor',
                'name' => 'Sponsor'
            ]
        ]
    ],
    'social_media' => [
        'facebook' => [
            'id' => 1,
            'value' => 'facebook',
            'name' => 'Facebook',
            'icon' => 'fa fa-facebook',
        ],
        'twitter' => [
            'id' => 2,
            'value' => 'twitter',
            'name' => 'Twitter',
            'icon' => 'fa fa-twitter',
        ],
        'instagram' => [
            'id' => 3,
            'value' => 'instagram',
            'name' => 'Instagram',
            'icon' => 'fa fa-instagram',
        ],
        'linkedin' => [
            'id' => 1,
            'value' => 'linkedin',
            'name' => 'Linkedin',
            'icon' => 'fa fa-linkedin',
        ],
        'tumblr' => [
            'id' => 1,
            'value' => 'tumblr',
            'name' => 'Tumblr',
            'icon' => 'fa fa-tumblr',
        ],
        'pinterest' => [
            'id' => 1,
            'value' => 'pinterest',
            'name' => 'Pinterest',
            'icon' => 'fa fa-pinterest',
        ]
    ],
    'order_mail_message' => 'Order message.',
    'currency_format' => 'LKR',
    'refund_percentage' => '20'
];
